package com.vertexinc.oseries;

import org.w3c.dom.Node;

interface NodeVisitor<T> {
  T visitNode(Node n);
}