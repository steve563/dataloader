package com.vertexinc.oseries;

/**
 * The information contained in a FieldSchema element in a ETL *.sch file
 */
class FieldSchema {
  final String name;
  final FieldSchemaType type;

  public FieldSchema(String name, FieldSchemaType type) {
    this.name = name;
    this.type = type;
  }
  
  @Override
  public String toString() {
    return String.format("%s %s", name, type.name());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((this.type == null) ? 0 : this.type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FieldSchema other = (FieldSchema) obj;
    if (this.name == null) {
      if (other.name != null)
        return false;
    }
    else if (!this.name.equals(other.name))
      return false;
    if (this.type != other.type)
      return false;
    return true;
  }


}