package com.vertexinc.oseries;

public class Constants {
  
  public static final String RUN_DDL_LOCATION = "RUN_DDL_LOCATION";
  public static final String TAXGIS_UPDATE_DATA = "taxgis_update_data.zip";
  
  public static final String DAT_FILE_DELIMITER = "~";
  public static final String SQL_DELIMITER = ";";
  
  
}
