package com.vertexinc.oseries;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnProvider {

  public static Connection getConnection(String url, String user, String pw) throws Exception {
    Connection conn = DriverManager.getConnection(url, user, pw);
    return conn;
  }

  /*
  public static void shouldBeAbleToCreateDatabase(String url, String user, String pw, Consumer<Statement> c) throws Exception {
    try (Connection conn = getConnection(url, user, pw)) {
      try (Statement stmt = conn.createStatement()) {
        c.accept(stmt);
      }
    }
  }
  */


}
