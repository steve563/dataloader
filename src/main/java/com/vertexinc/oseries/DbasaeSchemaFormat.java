package com.vertexinc.oseries;

public class DbasaeSchemaFormat {
  
  private final String tableName;

  public DbasaeSchemaFormat(String tableName) {
    super();
    this.tableName = tableName;
  }
  
  public String getTableName() {
    return this.tableName;
  }
  
}
