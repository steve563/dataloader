package com.vertexinc.oseries;

/*
 * A simple data structure to move around *.dat row data
 */
public class DataBucket {
  String stringVal;
  Long longVal;
  Double doubleVal;
  
  @Override
  public String toString() {
    return String.format("%s %d %f", stringVal, longVal, doubleVal);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
      + ((this.doubleVal == null) ? 0 : this.doubleVal.hashCode());
    result = prime * result
      + ((this.longVal == null) ? 0 : this.longVal.hashCode());
    result = prime * result
      + ((this.stringVal == null) ? 0 : this.stringVal.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DataBucket other = (DataBucket) obj;
    if (this.doubleVal == null) {
      if (other.doubleVal != null)
        return false;
    }
    else if (!this.doubleVal.equals(other.doubleVal))
      return false;
    if (this.longVal == null) {
      if (other.longVal != null)
        return false;
    }
    else if (!this.longVal.equals(other.longVal))
      return false;
    if (this.stringVal == null) {
      if (other.stringVal != null)
        return false;
    }
    else if (!this.stringVal.equals(other.stringVal))
      return false;
    return true;
  }
  
  
}
