package com.vertexinc.oseries;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.List;

public class DataSetExtractor implements NodeVisitor<DataSet> {
  
  private final DomScanner<DataSet> domScanner;

  /** 
   * Reads a manifest dom and extracts the dataset information
   */
  public DataSetExtractor(DomScanner<DataSet> domScanner) {
    this.domScanner = domScanner;
  }
  
  public List<DataSet> getDataSets() {
    return domScanner.getFields("DataSet", this);  }

  @Override
  public DataSet visitNode(Node node) {
    String name = null;
    NamedNodeMap attrs = node.getAttributes();
    for (int i = 0; i < attrs.getLength(); i++) {
      Node attr = attrs.item(i);
      switch (attr.getNodeName()) {
      case "name":
        name = attr.getNodeValue();
        break;
      case "rows":
        // I don't care
        break;
      case "deferCommit":
        // I don't care
        break;
      case "outsideTransaction":
        // I don't care
        break;
      default: throw new IllegalArgumentException("Something strange happened. attr.getNodeName():" + attr.getNodeName() );
      }
    }
    return new DataSet(name);
  }

}