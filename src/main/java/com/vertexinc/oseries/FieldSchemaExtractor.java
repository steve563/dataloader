package com.vertexinc.oseries;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.List;


/**
 * Generates a list of SchemaField objects based on the provided org.w3c.dom.Document
 *
 */
public class FieldSchemaExtractor implements NodeVisitor<FieldSchema>{

	private final DomScanner<FieldSchema> domScanner;

	public FieldSchemaExtractor(DomScanner<FieldSchema> domScanner) {
		this.domScanner = domScanner;
	}
	
  public FieldSchema[] getFields() {
    List<FieldSchema> list = domScanner.getFields("FieldSchema", this);
    return list.toArray(new FieldSchema[0]);
  }
	
  @Override
  public FieldSchema visitNode(Node node) {
		String name = null;
		FieldSchemaType type = null;
		NamedNodeMap attrs = node.getAttributes();
		for (int i = 0; i < attrs.getLength(); i++) {
			Node attr = attrs.item(i);
			switch (attr.getNodeName()) {
			case "name":
				name = attr.getNodeValue();
				break;
      case "type":
        type = FieldSchemaType.parseFieldSchemaType(attr.getNodeValue());
        break;
			case "nullable":
				// Don't care
				break;
			case "primaryKey":
				// Don't care
				break;
			case "size":
				// Don't care
				break;
      case "offset":
        // Don't care
        break;
      case "transient":
        // Don't care
        break;
      case "telecomunitconversion":
        // Don't care
        break;
      case "customTask":
        // Don't care
        break;
      case "insertOnly":
        // Don't care
        break;
      case "defaultvalue":
      case "defaultValue":
        // Don't care
        break;
      default: throw new IllegalArgumentException("Something strange happened. attr.getNodeName():" + attr.getNodeName() );
			}
		}
		return new FieldSchema(name, type);
	}

}
