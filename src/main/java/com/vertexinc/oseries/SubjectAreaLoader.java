package com.vertexinc.oseries;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SubjectAreaLoader {
  
  private final String subjectArea;
  private final ZipFileReader zipFileReader;
  private final BindTargetSource bindTargetSource;

  public SubjectAreaLoader(String subjectArea, ZipFileReader zipFileReader, BindTargetSource bindTargetSource) {
    this.subjectArea = subjectArea;
    this.zipFileReader = zipFileReader;
    this.bindTargetSource = bindTargetSource;
  }

  public void loadSubjectArea() throws Exception {
    String pathString = String.format("/%s/%s.man", subjectArea, subjectArea);
    String xml = zipFileReader.readAllLinesIntoAString(pathString);
    Document dom = buildDocumentFromString(xml);
    DataSetExtractor iut = new DataSetExtractor(new DomScanner<DataSet>(dom));
    List<DataSet> dataSets = iut.getDataSets();
    for (DataSet dataSet : dataSets) {
      loadOneDataSet(dataSet);
    }
  }

  private void loadOneDataSet(DataSet dataSet) throws Exception {
    System.out.printf("Loading dataSet: %s\n", dataSet.getName());
    String schemaPathString = String.format("/%s/%s.sch", subjectArea, dataSet.getName());
    String xml = zipFileReader.readAllLinesIntoAString(schemaPathString);
    Document dom = buildDocumentFromString(xml);
    
    // TODO - This is kind of ugly. The table name is case sensitive and needs to be gotten from the DbaseSchemaFormat element of
    // the *.sch file
    DbaseSchemaFormatExtractor tableExtractor = new DbaseSchemaFormatExtractor(new DomScanner<DbasaeSchemaFormat>(dom));
    DbasaeSchemaFormat[] f = tableExtractor.getFields();
    String tableName = f[0].getTableName();    
    
    FieldSchemaExtractor iut = new FieldSchemaExtractor(new DomScanner<FieldSchema>(dom));
    FieldSchema[] fields = iut.getFields();
    String datFilePathString = String.format("/%s/%s.dat", subjectArea, dataSet.getName());
    try (Stream<String> stream = zipFileReader.getStream(datFilePathString);
         BindTarget bindTarget = bindTargetSource.getBindTarget(tableName, getFieldNames(fields))) {
      DatFileLoader loader = new DatFileLoader(stream, fields, bindTarget);
      loader.loadDatFile();
    }
  }

  private String[] getFieldNames(FieldSchema[] fields) {
    String[] names = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      names[i] = fields[i].name;
    }
    return names;
  }

  
  // TODO - Where does this functionality belong?
  public static Document buildDocumentFromString(String xml)
    throws ParserConfigurationException, SAXException, IOException {
    try (InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))) {
      DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      Document document = docBuilder.parse(stream);
      return document;
    }
  }

}
