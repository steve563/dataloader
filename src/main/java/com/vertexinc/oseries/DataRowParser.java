package com.vertexinc.oseries;

public class DataRowParser {
  
  private final FieldSchema[] fields;

  public DataRowParser(FieldSchema[] fields) {
    this.fields = fields;
  }

  public void fillDatabucketsWithRowData(String line, DataBucket[] databuckets) {
    String[] parts = line.split(Constants.DAT_FILE_DELIMITER, -1);
    for (int i = 0; i < parts.length; i++) {
      
      // Don't make caller have to fill the array on the very first call
      if (databuckets[i]==null) {
        databuckets[i] = new DataBucket();
      }
      
      // Empty the 3 databucket values
      databuckets[i].stringVal = null;
      databuckets[i].longVal = null;
      databuckets[i].doubleVal = null;
      fillSingleBucket(fields[i], parts[i], databuckets[i]);
    }
  }

  private void fillSingleBucket(FieldSchema fieldSchema, String value, DataBucket dataBucket) {
    switch (fieldSchema.type) {
    case STRING:
      fillWithString(value, dataBucket);
      break;
    case LONG:
      fillWithLong(value, dataBucket);
      break;
    case DOUBLE:
      fillWithDouble(value, dataBucket);
      break;
    default: throw new IllegalArgumentException("Something strange happened");
    }
  }

  private void fillWithString(String value, DataBucket dataBucket) {
    String v = null;
    if (value != null && !value.isEmpty()) {
      v = value;
    }
    dataBucket.stringVal = v;
  }

  private void fillWithLong(String value, DataBucket dataBucket) {
    Long v = null;
    if (value != null && !value.isEmpty()) {
      v = Long.parseLong(value);
    }
    dataBucket.longVal = v;
  }

  private void fillWithDouble(String value, DataBucket dataBucket) {
    Double v = null;
    if (value != null && !value.isEmpty()) {
      v = Double.parseDouble(value);
    }
    dataBucket.doubleVal = v;
  }

}
