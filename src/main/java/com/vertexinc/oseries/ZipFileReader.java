package com.vertexinc.oseries;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class ZipFileReader {
  
  private final Path zipFile;

  public ZipFileReader(Path zipFile) {
    this.zipFile = zipFile;
  }

  public String readAllLinesIntoAString(String pathString) throws IOException {
    Path path = findFile(pathString);
    StringBuilder contentBuilder = new StringBuilder();
  
    try (Stream<String> stream = Files.lines( path, StandardCharsets.UTF_8)) {
      stream.forEach(ThrowingConsumer.unchecked(s -> contentBuilder.append(s).append(System.lineSeparator())));
    }
  
    return contentBuilder.toString();
  }

  // This will be used to read *.dat files. Right now
  // I am using this by then asking the stream for an iterator 
  public Stream<String> getStream(String pathString) throws IOException {
    Path path = findFile(pathString);
    return Files.lines( path, StandardCharsets.UTF_8);
  }

  private Path findFile(String pathString) throws IOException {
    FileSystem fileSystem = FileSystems.newFileSystem(zipFile, null);
    return fileSystem.getPath(pathString);
  }
  
}
