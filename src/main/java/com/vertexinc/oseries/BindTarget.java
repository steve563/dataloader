package com.vertexinc.oseries;

import java.sql.SQLException;

interface BindTarget extends AutoCloseable {
  
  void bind(FieldSchemaType type, String stringValue, Long longValue, Double doubleValue) throws SQLException;

  void execute() throws SQLException;
  
}