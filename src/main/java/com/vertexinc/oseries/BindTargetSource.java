package com.vertexinc.oseries;

import java.sql.SQLException;

interface BindTargetSource {

  BindTarget getBindTarget(String tableName, String[] fieldNames) throws SQLException;

}
