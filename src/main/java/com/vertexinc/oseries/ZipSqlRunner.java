package com.vertexinc.oseries;

import java.sql.Connection;
import java.sql.Statement;

/*
Needs a zip file and a file within that zip file, which is a SQL file. It
parses the sql file into individual statements and runs each of them as 
an execute update.
 */
public class ZipSqlRunner {
  
  private final String url;
  private final String user;
  private final String pw;
  private final ZipSqlParser parser;
  
  public ZipSqlRunner(ZipSqlParser parser, String url, String user, String pw) {
    this.parser = parser;
    this.url = url;
    this.user = user;
    this.pw = pw;
  }

  public void readAndRunSql(String zipFilePath, String filePath) throws Exception {
    String[] statements = parser.readFile(zipFilePath, filePath);
    try (Connection conn = ConnProvider.getConnection(url, user, pw)) {
      try (Statement stmt = conn.createStatement()) {
        for (String string : statements) {
          stmt.executeUpdate(string);
        }
      }
    }
  }

}
