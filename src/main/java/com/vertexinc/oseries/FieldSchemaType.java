package com.vertexinc.oseries;

public enum FieldSchemaType {
  
  STRING, LONG, DOUBLE;

  public static FieldSchemaType parseFieldSchemaType(String string) {
    switch (string) {
    case "STRING":
      return STRING;
    case "LONG":
      return LONG;
    case "DOUBLE":
      return DOUBLE;
    default: throw new IllegalArgumentException("Something strange happened");
    }
  }

}
