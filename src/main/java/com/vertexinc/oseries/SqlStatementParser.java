package com.vertexinc.oseries;

import java.util.ArrayList;
import java.util.List;

/**
 * This class merely parses a string and splits the it up into SQL statements by splitting on the semi-colon
 */
public class SqlStatementParser {

  public String[] parse(String sql) {
    String[] parts = sql.split(Constants.SQL_DELIMITER, -1);
    List<String> list = new ArrayList<>();
    for (int i = 0; i < parts.length; i++) {
      String s = parts[i];
      String trimmed = s.trim();
      if (!trimmed.isEmpty()) {
        list.add(trimmed);
      }
    }
    return list.toArray(new String[0]);
  }
  
  

}
