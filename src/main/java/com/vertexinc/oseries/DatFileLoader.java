package com.vertexinc.oseries;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.stream.Stream;


// TODO - There are two tables in gis that need rows to be dropped based on taxtype
public class DatFileLoader {
  
  private final Stream<String> stream;
  private final FieldSchema[] fields;
  private final BindTarget bindTarget;

  /**
   * This object did not create the stream so therefore will not be responsible for closing it.
   */
  public DatFileLoader(Stream<String> stream, FieldSchema[] fields, BindTarget bindTarget) {
    this.stream = stream;
    this.fields = fields;
    this.bindTarget = bindTarget;
  }

  public void loadDatFile() throws SQLException {
    DataBucket[] dataBucket = new DataBucket[fields.length];
    DataRowParser dataRowParser = new DataRowParser(fields);
    
    Iterator<String> iter = stream.iterator();
    while (iter.hasNext()) {
      String row = iter.next();
      //System.out.println("Row...");
      if (row != null && !row.isEmpty()) {
        dataRowParser.fillDatabucketsWithRowData(row, dataBucket);
        // Here's where we might just drop this row if it's one of the one's that needs to be skipped
        for (int i = 0; i < dataBucket.length; i++) {
          if (fields[i] == null) {
            System.out.println("Oh shit");
          }
          // System.out.printf("databucket length: %d, i: %d\n", dataBucket.length, i);
          FieldSchemaType t = fields[i].type;
          DataBucket db = dataBucket[i];
          // Discovered an instance where the *.dat file doesn't even have the same columns as
          // the *.sch file says it should. 
          String sv = null;
          Long lv = 20191114L; // FIXME - This is such a hack
          Double dv = null;
          if (dataBucket[i] != null){
            sv = db.stringVal;
            lv = db.longVal;
            dv = db.doubleVal;
          }
          bindTarget.bind(t, sv, lv, dv);
        }
        bindTarget.execute();
      }
    }
  }

}
