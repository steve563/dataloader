package com.vertexinc.oseries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementBindTarget implements BindTarget {
  
  private final PreparedStatement ps;
  private final Connection conn;
  private int index = 1;  // This increments for each field bound and then goes back to one after each execute;
  private int currentBatch = 0;
  private final int BATCH_SIZE = 10000;
  private final int REPORT_SIZE = BATCH_SIZE/1000; 

  public PreparedStatementBindTarget(Connection conn, PreparedStatement ps) throws SQLException {
    this.ps = ps;
    this.conn = conn;
    this.conn.setAutoCommit(false);
  }

  @Override
  public void close() throws Exception {
    System.out.println("Closing...");
    if (currentBatch > 0) {
      this.closeBatch();
    }
    ps.close();
    conn.commit();
    conn.close();
  }

  @Override
  public void bind(FieldSchemaType type, String stringValue, Long longValue, Double doubleValue) throws SQLException {
    switch(type) {
    case STRING:
      bindString(stringValue);
      break;
    case LONG:
      bindLong(longValue);
      break;
    case DOUBLE:
      bindDouble(doubleValue);
      break;
    default: throw new IllegalArgumentException("Something strange happened");
    }
  }

  private void bindString(String stringValue) throws SQLException {
    if (stringValue == null) {
      ps.setNull(index++, java.sql.Types.VARCHAR);
    }
    else {
      ps.setString(index++, stringValue);
    }
  }

  private void bindLong(Long longValue) throws SQLException {
    if (longValue == null) {
      ps.setNull(index++, java.sql.Types.NUMERIC);
    }
    else {
      ps.setLong(index++, longValue);
    }
  }

  private void bindDouble(Double doubleValue) throws SQLException {
    if (doubleValue == null) {
      ps.setNull(index++, java.sql.Types.FLOAT);
    }
    else {
      ps.setDouble(index++, doubleValue);
    }
  }

  @Override
  public void execute() throws SQLException {
    ps.addBatch();
    this.currentBatch++;
    if (this.currentBatch >= this.REPORT_SIZE) {
      System.out.print(".");
    }
    if (this.currentBatch >= this.BATCH_SIZE) {
      this.closeBatch();
    }
    index = 1;
  }
  
  private void closeBatch() throws SQLException {
    System.out.printf("Finishing a batch of size %d\n", this.currentBatch);
    ps.executeBatch();
    this.currentBatch = 0;
  }

}
