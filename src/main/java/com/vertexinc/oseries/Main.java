package com.vertexinc.oseries;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
  
  // Everything is hardcoded. Very bad. 
  public static void main(String[] args) throws Exception {
    ZipSqlParser parser = new ZipSqlParser();
    String url = "jdbc:jtds:sqlserver://localhost:1433/mvn_90_scratch_tps;prepareSQL=3";
    String user = "sa";
    String pw = "password";
    String run_ddl_target_dir = "c:/dev/oseries/9-0/oseries/oseries-run-ddl/target";
    ZipSqlRunner runner = new ZipSqlRunner(parser, url, user, pw);

    String tpsDropZip = run_ddl_target_dir + "/tps_drop.zip";
    runner.readAndRunSql(tpsDropZip, "/sql/microsoft sql server/drop.sql"); // drop tps
    
    String tpsCreateZip = run_ddl_target_dir + "/tps_create.zip";
    runner.readAndRunSql(tpsCreateZip, "/sql/microsoft sql server/create.sql"); // create tps
    //runner.readAndRunSql(tpsCreateZip, "/sql/microsoft sql server/createindices.sql"); // create tps indices
    
    PreparedStatementGenerator psg = new PreparedStatementGenerator();
    PreparedStatementBindTargetSource bindTargetSource = new PreparedStatementBindTargetSource(psg, url, user, pw);
    
    Path zipFile = Paths.get(run_ddl_target_dir + "/tps_update_data.zip");
    ZipFileReader zipFileReader = new ZipFileReader(zipFile);
    
    SubjectAreaLoader loader = new SubjectAreaLoader("tps", zipFileReader, bindTargetSource);
    loader.loadSubjectArea();
    
    System.out.println("A Boss");
  }

}
