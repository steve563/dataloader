package com.vertexinc.oseries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementBindTargetSource implements BindTargetSource {
  
  private PreparedStatementGenerator psg;
  private String url;
  private String user;
  private String pw;
  

  public PreparedStatementBindTargetSource(PreparedStatementGenerator psg,
    String url, String user, String pw) {
    super();
    this.psg = psg;
    this.url = url;
    this.user = user;
    this.pw = pw;
  }

  @Override
  public BindTarget getBindTarget(String tableName, String[] fieldNames) throws SQLException {
    String psString = psg.generate(tableName, fieldNames);
    Connection conn = DriverManager.getConnection(url, user, pw);
    PreparedStatement ps = conn.prepareStatement(psString);
    return new PreparedStatementBindTarget(conn, ps);
  }

}
