package com.vertexinc.oseries;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

class DomScanner<T> {
  
  private final Document document;

  public DomScanner(Document document) {
    this.document = document;
  }
  
  public List<T> getFields(String fieldName, NodeVisitor<T> visitor) {
    List<T> fields = new ArrayList<T>();
    Node n = document.getDocumentElement();
    processNode(n, fields, fieldName, visitor);
    return fields;
  }

  public void processNode(Node n, List<T> fields, String fieldName, NodeVisitor<T> visitor) {
    if (n.getNodeName().equals(fieldName)) {
      foundOne(n, fields, visitor);
    }
    NodeList nodeList = n.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node currentNode = nodeList.item(i);
      if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
        // calls this method for all the children which is Element
        processNode(currentNode, fields, fieldName, visitor);
      }
    }
  }

  private void foundOne(Node node, List<T> fields, NodeVisitor<T> visitor) {
    T newItem = visitor.visitNode(node);
    fields.add(newItem);
  }

}