package com.vertexinc.oseries;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ZipSqlParser {
  
  public String[] readFile(String zipFilePath, String filePath) throws IOException {
    Path zipFile = Paths.get(zipFilePath);
    ZipFileReader zipFileReader = new ZipFileReader(zipFile);
    String sql = zipFileReader.readAllLinesIntoAString(filePath);
    SqlStatementParser parser = new SqlStatementParser();
    return parser.parse(sql);
  }

}
