package com.vertexinc.oseries;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.List;

public class DbaseSchemaFormatExtractor implements NodeVisitor<DbasaeSchemaFormat>{

  private final DomScanner<DbasaeSchemaFormat> domScanner;

  public DbaseSchemaFormatExtractor(DomScanner<DbasaeSchemaFormat> domScanner) {
    this.domScanner = domScanner;
  }
  
  public DbasaeSchemaFormat[] getFields() {
    List<DbasaeSchemaFormat> list = domScanner.getFields("DbaseSchemaFormat", this);
    return list.toArray(new DbasaeSchemaFormat[0]);
  }
  
  @Override
  public DbasaeSchemaFormat visitNode(Node node) {
    String name = null;
    NamedNodeMap attrs = node.getAttributes();
    for (int i = 0; i < attrs.getLength(); i++) {
      Node attr = attrs.item(i);
      switch (attr.getNodeName()) {
      case "table":
        name = attr.getNodeValue();
        break;
      case "logicalName":
        // Don't care
        break;
      default: throw new IllegalArgumentException("Something strange happened. attr.getNodeName():" + attr.getNodeName() );
      }
    }
    return new DbasaeSchemaFormat(name);
  }


}
