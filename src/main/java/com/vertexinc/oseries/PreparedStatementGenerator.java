package com.vertexinc.oseries;

public class PreparedStatementGenerator {

  public String generate(String tableName, String[] fieldName) {
    StringBuffer sb = new StringBuffer();
    sb.append(String.format("insert into %s (", tableName));
    for (int i = 0; i < fieldName.length; i++) {
      sb.append(fieldName[i]);
      if (i < fieldName.length - 1) {
        sb.append(", ");
      }
    }
    sb.append(") values (");
    for (int i = 0; i < fieldName.length; i++) {
      sb.append("?");
      if (i < fieldName.length - 1) {
        sb.append(",");
      }
    }
    sb.append(")");
    return sb.toString();
  }

}
