<?xml version="1.0" encoding="UTF-8"?>
<SubjectArea xmlns="etl.vertexinc.com"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="etl.vertexinc.com vertex_etl.xsd"
             name="tos">

  <Description>Manifest to run drop DLL for TPS database</Description>

  <DataRelease fullReleaseNumber="2"
               interimReleaseNumber="0"
               name="TPS"
               type="FULL" >

    <Description>Drop DDL update for TPS database</Description>

  </DataRelease>

  <SchemaRelease id="209000001" name="TPS Schema" validate="false" >

    <Description>Schema validation for TPS database</Description>
    <SupportedSchemaRelease id="209000001" />

  </SchemaRelease>

</SubjectArea>
