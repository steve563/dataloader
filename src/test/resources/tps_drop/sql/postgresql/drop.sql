drop table if exists VATGroupMembers cascade;

drop table if exists VATGroupDetail cascade;

drop table if exists VATGroup cascade;

drop table if exists VATRegimeType cascade;

drop table if exists TaxAssistPhaseType cascade;

drop table if exists TpsDataReleaseEvent cascade;

drop table if exists TransactionEventType cascade;

drop table if exists FeatureResourceCategory cascade;

drop table if exists FeatureResourceImpositionType cascade;

drop table if exists FeatureResource cascade;

drop table if exists TransactionStatusType cascade;

drop table if exists DataUpdateImpactTaxArea cascade;

drop table if exists DataUpdateImpactTaxRule cascade;

drop table if exists SecurityQuestions cascade;

drop table if exists CertWizardUser cascade;

drop table if exists authorities cascade;

drop table if exists users cascade;

drop table if exists CertWizardLocationUser cascade;

drop table if exists VATRegistrationIdFormat cascade;

drop table if exists RoleParty cascade;

drop table if exists JurGeographicRegType cascade;

drop table if exists GeographicRegionType cascade;

drop table if exists Letter cascade;

drop table if exists FlexFieldDefVtxPrdTyp cascade;

drop table if exists LetterBatchJurisdiction cascade;

drop table if exists LetterBatchParty cascade;

drop table if exists LetterBatch cascade;

drop table if exists LetterTemplateText cascade;

drop table if exists LetterTemplate cascade;

drop table if exists LetterDeliveryMethod cascade;

drop table if exists LetterTemplateType cascade;

drop table if exists TaxRegImpJurisdiction cascade;

drop table if exists TaxRegistrationImposition cascade;

drop table if exists TaxRegistrationJurisdiction cascade;

drop table if exists TaxRegistration cascade;

drop table if exists TaxAuthorityJurisdiction cascade;

drop table if exists TaxAuthority cascade;

drop table if exists CertificateNumFormatValidation cascade;

drop table if exists CertificateValidationRule cascade;

drop table if exists CertificateNumberFormat cascade;

drop table if exists CertificateNumReqType cascade;

drop table if exists ExpirationRuleType cascade;

drop table if exists CertificateFormField cascade;

drop table if exists CertificateTransType cascade;

drop table if exists CertTxbltyDriver cascade;

drop table if exists CertificateImage cascade;

drop table if exists CertificateNote cascade;

drop table if exists CertImpositionJurisdiction cascade;

drop table if exists CertificateImposition cascade;

drop table if exists CertificateJurisdiction cascade;

drop table if exists CertificateCoverage cascade;

drop table if exists CertificateDetail cascade;

drop table if exists Certificate cascade;

drop table if exists FormFieldAttribute cascade;

drop table if exists FormFieldTypeValue cascade;

drop table if exists FormFieldDef cascade;

drop table if exists FormJurisdiction cascade;

drop table if exists FormDetail cascade;

drop table if exists Form cascade;

drop table if exists CertExemptionTypeJurImp cascade;

drop table if exists CertificateExemptionType cascade;

drop table if exists CertReasonTypeJurisdiction cascade;

drop table if exists CertificateReasonType cascade;

drop table if exists InputOutputType cascade;

drop table if exists DMStatusType cascade;

drop table if exists DMActivityType cascade;

drop table if exists SimplificationType cascade;

drop table if exists JurHierarchy cascade;

drop table if exists TaxAreaJurNames cascade;

drop table if exists TpsJurisdiction cascade;

drop table if exists TaxAssistAllocationColumnValue cascade;

drop table if exists TaxAssistAllocationColumn cascade;

drop table if exists TaxAssistAllocationTable cascade;

drop table if exists NumericType cascade;

drop table if exists TaxAssistLookupRcd cascade;

drop table if exists TaxAssistLookup cascade;

drop table if exists TaxAssistConclude cascade;

drop table if exists TaxAssistCondition cascade;

drop table if exists TaxAssistRule cascade;

drop table if exists TaxRecoverablePct cascade;

drop table if exists AllowCurrRndRule cascade;

drop table if exists CurrencyRndRule cascade;

drop table if exists SitusTrtmntPrspctv cascade;

drop table if exists SitusTrtmntRulNote cascade;

drop table if exists SitusTreatmentRule cascade;

drop table if exists TransTypePrspctv cascade;

drop table if exists StsTrtmntVtxPrdTyp cascade;

drop table if exists SitusTreatment cascade;

drop table if exists DMFilterNum cascade;

drop table if exists TaxRuleCondJur cascade;

drop table if exists SitusCondTxbltyCat cascade;

drop table if exists TaxRuleDescription cascade;

drop table if exists DMActivityLogFile cascade;

drop table if exists DMActivityLogStrng cascade;

drop table if exists TaxRuleTransType cascade;

drop table if exists FalseSitusCond cascade;

drop table if exists LineItemTaxDtlType cascade;

drop table if exists TaxabilityMapping cascade;

drop table if exists TaxBasisConclusion cascade;

drop table if exists ConditionalTaxExpr cascade;

drop table if exists ComputationFactor cascade;

drop table if exists TaxFactor cascade;

drop table if exists TaxFactorType cascade;

drop table if exists ComputationType cascade;

drop table if exists MaxTaxRuleAdditionalCond cascade;

drop table if exists TaxRuleTaxImposition cascade;

drop table if exists TaxRuleTaxType cascade;

drop table if exists TaxRuleQualCond cascade;

drop table if exists ExprConditionType cascade;

drop table if exists TaxRuleNote cascade;

drop table if exists TaxRule cascade;

drop table if exists Bracket cascade;

drop table if exists Tier cascade;

drop table if exists FilingOverride cascade;

drop table if exists TaxImpQualCond cascade;

drop table if exists TaxImpsnDetail cascade;

drop table if exists TaxImposition cascade;

drop table if exists ImpositionType cascade;

drop table if exists TaxStructure cascade;

drop table if exists DiscountType cascade;

drop table if exists DMFilterInd cascade;

drop table if exists InvoiceTextDetail cascade;

drop table if exists InvoiceText cascade;

drop table if exists InvoiceTextType cascade;

drop table if exists TxbltyDriverCatMap cascade;

drop table if exists TxbltyCatMap cascade;

drop table if exists TelecomUnitConversionLineType cascade;

drop table if exists TxbltyDvrVtxPrdTyp cascade;

drop table if exists TxbltyDriverNote cascade;

drop table if exists TxbltyDriverDetail cascade;

drop table if exists TaxabilityDriver cascade;

drop table if exists PartyContact cascade;

drop table if exists PartyNote cascade;

drop table if exists BusinessLocation cascade;

drop table if exists PartyVtxProdType cascade;

drop table if exists PartyRoleTaxResult cascade;

drop table if exists PartyDetail cascade;

drop table if exists Party cascade;

drop table if exists SitusConcNode cascade;

drop table if exists TrueSitusCond cascade;

drop table if exists SitusConditionNode cascade;

drop table if exists ApportionmentFriendlyStates cascade;

drop table if exists TpsPatchDataEvent cascade;

drop table if exists TpsSchVersion cascade;

drop table if exists DMActivityLogInd cascade;

drop table if exists DMActivityLogDate cascade;

drop table if exists DMActivityLogNum cascade;

drop table if exists DMActivityLog cascade;

drop table if exists TaxJurDetail cascade;

drop table if exists FlexFieldDefDetail cascade;

drop table if exists FlexFieldDef cascade;

drop table if exists TxbltyCatDetail cascade;

drop table if exists TaxabilityCategory cascade;

drop table if exists DiscountCategory cascade;

drop table if exists CertClassType cascade;

drop table if exists DMFilterStrng cascade;

drop table if exists ValidationType cascade;

drop table if exists TaxRuleTaxImpositionType cascade;

drop table if exists AccumulationType cascade;

drop table if exists AccumulationPeriodType cascade;

drop table if exists AccumulationByType cascade;

drop table if exists WithholdingType cascade;

drop table if exists TaxRuleType cascade;

drop table if exists TransOrigType cascade;

drop table if exists DeductionType cascade;

drop table if exists TaxStructureType cascade;

drop table if exists TelecomUnitConversion cascade;

drop table if exists PartyRoleType cascade;

drop table if exists VertexProductType cascade;

drop table if exists TransSubType cascade;

drop table if exists RoundingRule cascade;

drop table if exists TaxScope cascade;

drop table if exists SitusConclusion cascade;

drop table if exists FilingCategoryOvrd cascade;

drop table if exists FilingCategory cascade;

drop table if exists TaxType cascade;

drop table if exists SitusCondShippingTerms cascade;

drop table if exists SitusCondJur cascade;

drop table if exists ReasonCategoryJur cascade;

drop table if exists ReasonCategory cascade;

drop table if exists SitusConcType cascade;

drop table if exists BracketTaxCalcType cascade;

drop table if exists SitusCondition cascade;

drop table if exists MovementMethodType cascade;

drop table if exists CreationSource cascade;

drop table if exists CustomsStatusType cascade;

drop table if exists OutputNotice cascade;

drop table if exists DMFilterDate cascade;

drop table if exists BasisType cascade;

drop table if exists ShippingTerms cascade;

drop table if exists LocationRoleType cascade;

drop table if exists PartyType cascade;

drop table if exists RecoverableResultType cascade;

drop table if exists TaxResultType cascade;

drop table if exists JurTypeSetMember cascade;

drop table if exists JurTypeSet cascade;

drop table if exists InputParameterType cascade;

drop table if exists DataType cascade;

drop table if exists TransactionType cascade;

drop table if exists BusinessTransType cascade;

drop table if exists DMFilter cascade;

drop table if exists RateClassification cascade;

drop table if exists AssistedState cascade;

drop table if exists TitleTransferType cascade;

drop table if exists ChainTransType cascade;

drop table if exists SitusConditionType cascade;

drop table if exists ApportionmentType cascade;

drop table if exists TaxRegistrationType cascade;

drop table if exists FormFieldType cascade;

drop table if exists ContactRoleType cascade;

drop table if exists CertificateApprovalStatus cascade;

drop table if exists CertificateStatus cascade;

drop table if exists CoverageActionType cascade;

