drop table VATGroupMembers CASCADE CONSTRAINTS;

drop table VATGroupDetail CASCADE CONSTRAINTS;

drop table VATGroup CASCADE CONSTRAINTS;

drop table VATRegimeType CASCADE CONSTRAINTS;

drop table TaxAssistPhaseType CASCADE CONSTRAINTS;

drop table TpsDataReleaseEvent CASCADE CONSTRAINTS;

drop table TransactionEventType CASCADE CONSTRAINTS;

drop table FeatureResourceCategory CASCADE CONSTRAINTS;

drop table FeatureResourceImpositionType CASCADE CONSTRAINTS;

drop table FeatureResource CASCADE CONSTRAINTS;

drop table TransactionStatusType CASCADE CONSTRAINTS;

drop table DataUpdateImpactTaxArea CASCADE CONSTRAINTS;

drop table DataUpdateImpactTaxRule CASCADE CONSTRAINTS;

drop table SecurityQuestions CASCADE CONSTRAINTS;

drop table CertWizardUser CASCADE CONSTRAINTS;

drop table authorities CASCADE CONSTRAINTS;

drop table users CASCADE CONSTRAINTS;

drop table CertWizardLocationUser CASCADE CONSTRAINTS;

drop table VATRegistrationIdFormat CASCADE CONSTRAINTS;

drop table RoleParty CASCADE CONSTRAINTS;

drop table JurGeographicRegType CASCADE CONSTRAINTS;

drop table GeographicRegionType CASCADE CONSTRAINTS;

drop table Letter CASCADE CONSTRAINTS;

drop table FlexFieldDefVtxPrdTyp CASCADE CONSTRAINTS;

drop table LetterBatchJurisdiction CASCADE CONSTRAINTS;

drop table LetterBatchParty CASCADE CONSTRAINTS;

drop table LetterBatch CASCADE CONSTRAINTS;

drop table LetterTemplateText CASCADE CONSTRAINTS;

drop table LetterTemplate CASCADE CONSTRAINTS;

drop table LetterDeliveryMethod CASCADE CONSTRAINTS;

drop table LetterTemplateType CASCADE CONSTRAINTS;

drop table TaxRegImpJurisdiction CASCADE CONSTRAINTS;

drop table TaxRegistrationImposition CASCADE CONSTRAINTS;

drop table TaxRegistrationJurisdiction CASCADE CONSTRAINTS;

drop table TaxRegistration CASCADE CONSTRAINTS;

drop table TaxAuthorityJurisdiction CASCADE CONSTRAINTS;

drop table TaxAuthority CASCADE CONSTRAINTS;

drop table CertificateNumFormatValidation CASCADE CONSTRAINTS;

drop table CertificateValidationRule CASCADE CONSTRAINTS;

drop table CertificateNumberFormat CASCADE CONSTRAINTS;

drop table CertificateNumReqType CASCADE CONSTRAINTS;

drop table ExpirationRuleType CASCADE CONSTRAINTS;

drop table CertificateFormField CASCADE CONSTRAINTS;

drop table CertificateTransType CASCADE CONSTRAINTS;

drop table CertTxbltyDriver CASCADE CONSTRAINTS;

drop table CertificateImage CASCADE CONSTRAINTS;

drop table CertificateNote CASCADE CONSTRAINTS;

drop table CertImpositionJurisdiction CASCADE CONSTRAINTS;

drop table CertificateImposition CASCADE CONSTRAINTS;

drop table CertificateJurisdiction CASCADE CONSTRAINTS;

drop table CertificateCoverage CASCADE CONSTRAINTS;

drop table CertificateDetail CASCADE CONSTRAINTS;

drop table Certificate CASCADE CONSTRAINTS;

drop table FormFieldAttribute CASCADE CONSTRAINTS;

drop table FormFieldTypeValue CASCADE CONSTRAINTS;

drop table FormFieldDef CASCADE CONSTRAINTS;

drop table FormJurisdiction CASCADE CONSTRAINTS;

drop table FormDetail CASCADE CONSTRAINTS;

drop table Form CASCADE CONSTRAINTS;

drop table CertExemptionTypeJurImp CASCADE CONSTRAINTS;

drop table CertificateExemptionType CASCADE CONSTRAINTS;

drop table CertReasonTypeJurisdiction CASCADE CONSTRAINTS;

drop table CertificateReasonType CASCADE CONSTRAINTS;

drop table InputOutputType CASCADE CONSTRAINTS;

drop table DMStatusType CASCADE CONSTRAINTS;

drop table DMActivityType CASCADE CONSTRAINTS;

drop table SimplificationType CASCADE CONSTRAINTS;

drop table JurHierarchy CASCADE CONSTRAINTS;

drop table TaxAreaJurNames CASCADE CONSTRAINTS;

drop table TpsJurisdiction CASCADE CONSTRAINTS;

drop table TaxAssistAllocationColumnValue CASCADE CONSTRAINTS;

drop table TaxAssistAllocationColumn CASCADE CONSTRAINTS;

drop table TaxAssistAllocationTable CASCADE CONSTRAINTS;

drop table NumericType CASCADE CONSTRAINTS;

drop table TaxAssistLookupRcd CASCADE CONSTRAINTS;

drop table TaxAssistLookup CASCADE CONSTRAINTS;

drop table TaxAssistConclude CASCADE CONSTRAINTS;

drop table TaxAssistCondition CASCADE CONSTRAINTS;

drop table TaxAssistRule CASCADE CONSTRAINTS;

drop table TaxRecoverablePct CASCADE CONSTRAINTS;

drop table AllowCurrRndRule CASCADE CONSTRAINTS;

drop table CurrencyRndRule CASCADE CONSTRAINTS;

drop table SitusTrtmntPrspctv CASCADE CONSTRAINTS;

drop table SitusTrtmntRulNote CASCADE CONSTRAINTS;

drop table SitusTreatmentRule CASCADE CONSTRAINTS;

drop table TransTypePrspctv CASCADE CONSTRAINTS;

drop table StsTrtmntVtxPrdTyp CASCADE CONSTRAINTS;

drop table SitusTreatment CASCADE CONSTRAINTS;

drop table DMFilterNum CASCADE CONSTRAINTS;

drop table TaxRuleCondJur CASCADE CONSTRAINTS;

drop table SitusCondTxbltyCat CASCADE CONSTRAINTS;

drop table TaxRuleDescription CASCADE CONSTRAINTS;

drop table DMActivityLogFile CASCADE CONSTRAINTS;

drop table DMActivityLogStrng CASCADE CONSTRAINTS;

drop table TaxRuleTransType CASCADE CONSTRAINTS;

drop table FalseSitusCond CASCADE CONSTRAINTS;

drop table LineItemTaxDtlType CASCADE CONSTRAINTS;

drop table TaxabilityMapping CASCADE CONSTRAINTS;

drop table TaxBasisConclusion CASCADE CONSTRAINTS;

drop table ConditionalTaxExpr CASCADE CONSTRAINTS;

drop table ComputationFactor CASCADE CONSTRAINTS;

drop table TaxFactor CASCADE CONSTRAINTS;

drop table TaxFactorType CASCADE CONSTRAINTS;

drop table ComputationType CASCADE CONSTRAINTS;

drop table MaxTaxRuleAdditionalCond CASCADE CONSTRAINTS;

drop table TaxRuleTaxImposition CASCADE CONSTRAINTS;

drop table TaxRuleTaxType CASCADE CONSTRAINTS;

drop table TaxRuleQualCond CASCADE CONSTRAINTS;

drop table ExprConditionType CASCADE CONSTRAINTS;

drop table TaxRuleNote CASCADE CONSTRAINTS;

drop table TaxRule CASCADE CONSTRAINTS;

drop table Bracket CASCADE CONSTRAINTS;

drop table Tier CASCADE CONSTRAINTS;

drop table FilingOverride CASCADE CONSTRAINTS;

drop table TaxImpQualCond CASCADE CONSTRAINTS;

drop table TaxImpsnDetail CASCADE CONSTRAINTS;

drop table TaxImposition CASCADE CONSTRAINTS;

drop table ImpositionType CASCADE CONSTRAINTS;

drop table TaxStructure CASCADE CONSTRAINTS;

drop table DiscountType CASCADE CONSTRAINTS;

drop table DMFilterInd CASCADE CONSTRAINTS;

drop table InvoiceTextDetail CASCADE CONSTRAINTS;

drop table InvoiceText CASCADE CONSTRAINTS;

drop table InvoiceTextType CASCADE CONSTRAINTS;

drop table TxbltyDriverCatMap CASCADE CONSTRAINTS;

drop table TxbltyCatMap CASCADE CONSTRAINTS;

drop table TelecomUnitConversionLineType CASCADE CONSTRAINTS;

drop table TxbltyDvrVtxPrdTyp CASCADE CONSTRAINTS;

drop table TxbltyDriverNote CASCADE CONSTRAINTS;

drop table TxbltyDriverDetail CASCADE CONSTRAINTS;

drop table TaxabilityDriver CASCADE CONSTRAINTS;

drop table PartyContact CASCADE CONSTRAINTS;

drop table PartyNote CASCADE CONSTRAINTS;

drop table BusinessLocation CASCADE CONSTRAINTS;

drop table PartyVtxProdType CASCADE CONSTRAINTS;

drop table PartyRoleTaxResult CASCADE CONSTRAINTS;

drop table PartyDetail CASCADE CONSTRAINTS;

drop table Party CASCADE CONSTRAINTS;

drop table SitusConcNode CASCADE CONSTRAINTS;

drop table TrueSitusCond CASCADE CONSTRAINTS;

drop table SitusConditionNode CASCADE CONSTRAINTS;

drop table ApportionmentFriendlyStates CASCADE CONSTRAINTS;

drop table TpsPatchDataEvent CASCADE CONSTRAINTS;

drop table TpsSchVersion CASCADE CONSTRAINTS;

drop table DMActivityLogInd CASCADE CONSTRAINTS;

drop table DMActivityLogDate CASCADE CONSTRAINTS;

drop table DMActivityLogNum CASCADE CONSTRAINTS;

drop table DMActivityLog CASCADE CONSTRAINTS;

drop table TaxJurDetail CASCADE CONSTRAINTS;

drop table FlexFieldDefDetail CASCADE CONSTRAINTS;

drop table FlexFieldDef CASCADE CONSTRAINTS;

drop table TxbltyCatDetail CASCADE CONSTRAINTS;

drop table TaxabilityCategory CASCADE CONSTRAINTS;

drop table DiscountCategory CASCADE CONSTRAINTS;

drop table CertClassType CASCADE CONSTRAINTS;

drop table DMFilterStrng CASCADE CONSTRAINTS;

drop table ValidationType CASCADE CONSTRAINTS;

drop table TaxRuleTaxImpositionType CASCADE CONSTRAINTS;

drop table AccumulationType CASCADE CONSTRAINTS;

drop table AccumulationPeriodType CASCADE CONSTRAINTS;

drop table AccumulationByType CASCADE CONSTRAINTS;

drop table WithholdingType CASCADE CONSTRAINTS;

drop table TaxRuleType CASCADE CONSTRAINTS;

drop table TransOrigType CASCADE CONSTRAINTS;

drop table DeductionType CASCADE CONSTRAINTS;

drop table TaxStructureType CASCADE CONSTRAINTS;

drop table TelecomUnitConversion CASCADE CONSTRAINTS;

drop table PartyRoleType CASCADE CONSTRAINTS;

drop table VertexProductType CASCADE CONSTRAINTS;

drop table TransSubType CASCADE CONSTRAINTS;

drop table RoundingRule CASCADE CONSTRAINTS;

drop table TaxScope CASCADE CONSTRAINTS;

drop table SitusConclusion CASCADE CONSTRAINTS;

drop table FilingCategoryOvrd CASCADE CONSTRAINTS;

drop table FilingCategory CASCADE CONSTRAINTS;

drop table TaxType CASCADE CONSTRAINTS;

drop table SitusCondShippingTerms CASCADE CONSTRAINTS;

drop table SitusCondJur CASCADE CONSTRAINTS;

drop table ReasonCategoryJur CASCADE CONSTRAINTS;

drop table ReasonCategory CASCADE CONSTRAINTS;

drop table SitusConcType CASCADE CONSTRAINTS;

drop table BracketTaxCalcType CASCADE CONSTRAINTS;

drop table SitusCondition CASCADE CONSTRAINTS;

drop table MovementMethodType CASCADE CONSTRAINTS;

drop table CreationSource CASCADE CONSTRAINTS;

drop table CustomsStatusType CASCADE CONSTRAINTS;

drop table OutputNotice CASCADE CONSTRAINTS;

drop table DMFilterDate CASCADE CONSTRAINTS;

drop table BasisType CASCADE CONSTRAINTS;

drop table ShippingTerms CASCADE CONSTRAINTS;

drop table LocationRoleType CASCADE CONSTRAINTS;

drop table PartyType CASCADE CONSTRAINTS;

drop table RecoverableResultType CASCADE CONSTRAINTS;

drop table TaxResultType CASCADE CONSTRAINTS;

drop table JurTypeSetMember CASCADE CONSTRAINTS;

drop table JurTypeSet CASCADE CONSTRAINTS;

drop table InputParameterType CASCADE CONSTRAINTS;

drop table DataType CASCADE CONSTRAINTS;

drop table TransactionType CASCADE CONSTRAINTS;

drop table BusinessTransType CASCADE CONSTRAINTS;

drop table DMFilter CASCADE CONSTRAINTS;

drop table RateClassification CASCADE CONSTRAINTS;

drop table AssistedState CASCADE CONSTRAINTS;

drop table TitleTransferType CASCADE CONSTRAINTS;

drop table ChainTransType CASCADE CONSTRAINTS;

drop table SitusConditionType CASCADE CONSTRAINTS;

drop table ApportionmentType CASCADE CONSTRAINTS;

drop table TaxRegistrationType CASCADE CONSTRAINTS;

drop table FormFieldType CASCADE CONSTRAINTS;

drop table ContactRoleType CASCADE CONSTRAINTS;

drop table CertificateApprovalStatus CASCADE CONSTRAINTS;

drop table CertificateStatus CASCADE CONSTRAINTS;

drop table CoverageActionType CASCADE CONSTRAINTS;

