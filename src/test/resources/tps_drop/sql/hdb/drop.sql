drop table VATGroupMembers;

drop table VATGroupDetail;

drop table VATGroup;

drop table VATRegimeType;

drop table TaxAssistPhaseType;

drop table TpsDataReleaseEvent;

drop table TransactionEventType;

drop table FeatureResourceCategory;

drop table FeatureResourceImpositionType;

drop table FeatureResource;

drop table TransactionStatusType;

drop table DataUpdateImpactTaxArea;

drop table DataUpdateImpactTaxRule;

drop table SecurityQuestions;

drop table CertWizardUser;

drop table authorities;

drop table users;

drop table CertWizardLocationUser;

drop table VATRegistrationIdFormat;

drop table RoleParty;

drop table JurGeographicRegType;

drop table GeographicRegionType;

drop table Letter;

drop table FlexFieldDefVtxPrdTyp;

drop table LetterBatchJurisdiction;

drop table LetterBatchParty;

drop table LetterBatch;

drop table LetterTemplateText;

drop table LetterTemplate;

drop table LetterDeliveryMethod;

drop table LetterTemplateType;

drop table TaxRegImpJurisdiction;

drop table TaxRegistrationImposition;

drop table TaxRegistrationJurisdiction;

drop table TaxRegistration;

drop table TaxAuthorityJurisdiction;

drop table TaxAuthority;

drop table CertificateNumFormatValidation;

drop table CertificateValidationRule;

drop table CertificateNumberFormat;

drop table CertificateNumReqType;

drop table ExpirationRuleType;

drop table CertificateFormField;

drop table CertificateTransType;

drop table CertTxbltyDriver;

drop table CertificateImage;

drop table CertificateNote;

drop table CertImpositionJurisdiction;

drop table CertificateImposition;

drop table CertificateJurisdiction;

drop table CertificateCoverage;

drop table CertificateDetail;

drop table Certificate;

drop table FormFieldAttribute;

drop table FormFieldTypeValue;

drop table FormFieldDef;

drop table FormJurisdiction;

drop table FormDetail;

drop table Form;

drop table CertExemptionTypeJurImp;

drop table CertificateExemptionType;

drop table CertReasonTypeJurisdiction;

drop table CertificateReasonType;

drop table InputOutputType;

drop table DMStatusType;

drop table DMActivityType;

drop table SimplificationType;

drop table JurHierarchy;

drop table TaxAreaJurNames;

drop table TpsJurisdiction;

drop table TaxAssistAllocationColumnValue;

drop table TaxAssistAllocationColumn;

drop table TaxAssistAllocationTable;

drop table NumericType;

drop table TaxAssistLookupRcd;

drop table TaxAssistLookup;

drop table TaxAssistConclude;

drop table TaxAssistCondition;

drop table TaxAssistRule;

drop table TaxRecoverablePct;

drop table AllowCurrRndRule;

drop table CurrencyRndRule;

drop table SitusTrtmntPrspctv;

drop table SitusTrtmntRulNote;

drop table SitusTreatmentRule;

drop table TransTypePrspctv;

drop table StsTrtmntVtxPrdTyp;

drop table SitusTreatment;

drop table DMFilterNum;

drop table TaxRuleCondJur;

drop table SitusCondTxbltyCat;

drop table TaxRuleDescription;

drop table DMActivityLogFile;

drop table DMActivityLogStrng;

drop table TaxRuleTransType;

drop table FalseSitusCond;

drop table LineItemTaxDtlType;

drop table TaxabilityMapping;

drop table TaxBasisConclusion;

drop table ConditionalTaxExpr;

drop table ComputationFactor;

drop table TaxFactor;

drop table TaxFactorType;

drop table ComputationType;

drop table MaxTaxRuleAdditionalCond;

drop table TaxRuleTaxImposition;

drop table TaxRuleTaxType;

drop table TaxRuleQualCond;

drop table ExprConditionType;

drop table TaxRuleNote;

drop table TaxRule;

drop table Bracket;

drop table Tier;

drop table FilingOverride;

drop table TaxImpQualCond;

drop table TaxImpsnDetail;

drop table TaxImposition;

drop table ImpositionType;

drop table TaxStructure;

drop table DiscountType;

drop table DMFilterInd;

drop table InvoiceTextDetail;

drop table InvoiceText;

drop table InvoiceTextType;

drop table TxbltyDriverCatMap;

drop table TxbltyCatMap;

drop table TelecomUnitConversionLineType;

drop table TxbltyDvrVtxPrdTyp;

drop table TxbltyDriverNote;

drop table TxbltyDriverDetail;

drop table TaxabilityDriver;

drop table PartyContact;

drop table PartyNote;

drop table BusinessLocation;

drop table PartyVtxProdType;

drop table PartyRoleTaxResult;

drop table PartyDetail;

drop table Party;

drop table SitusConcNode;

drop table TrueSitusCond;

drop table SitusConditionNode;

drop table ApportionmentFriendlyStates;

drop table TpsPatchDataEvent;

drop table TpsSchVersion;

drop table DMActivityLogInd;

drop table DMActivityLogDate;

drop table DMActivityLogNum;

drop table DMActivityLog;

drop table TaxJurDetail;

drop table FlexFieldDefDetail;

drop table FlexFieldDef;

drop table TxbltyCatDetail;

drop table TaxabilityCategory;

drop table DiscountCategory;

drop table CertClassType;

drop table DMFilterStrng;

drop table ValidationType;

drop table TaxRuleTaxImpositionType;

drop table AccumulationType;

drop table AccumulationPeriodType;

drop table AccumulationByType;

drop table WithholdingType;

drop table TaxRuleType;

drop table TransOrigType;

drop table DeductionType;

drop table TaxStructureType;

drop table TelecomUnitConversion;

drop table PartyRoleType;

drop table VertexProductType;

drop table TransSubType;

drop table RoundingRule;

drop table TaxScope;

drop table SitusConclusion;

drop table FilingCategoryOvrd;

drop table FilingCategory;

drop table TaxType;

drop table SitusCondShippingTerms;

drop table SitusCondJur;

drop table ReasonCategoryJur;

drop table ReasonCategory;

drop table SitusConcType;

drop table BracketTaxCalcType;

drop table SitusCondition;

drop table MovementMethodType;

drop table CreationSource;

drop table CustomsStatusType;

drop table OutputNotice;

drop table DMFilterDate;

drop table BasisType;

drop table ShippingTerms;

drop table LocationRoleType;

drop table PartyType;

drop table RecoverableResultType;

drop table TaxResultType;

drop table JurTypeSetMember;

drop table JurTypeSet;

drop table InputParameterType;

drop table DataType;

drop table TransactionType;

drop table BusinessTransType;

drop table DMFilter;

drop table RateClassification;

drop table AssistedState;

drop table TitleTransferType;

drop table ChainTransType;

drop table SitusConditionType;

drop table ApportionmentType;

drop table TaxRegistrationType;

drop table FormFieldType;

drop table ContactRoleType;

drop table CertificateApprovalStatus;

drop table CertificateStatus;

drop table CoverageActionType;

