<?xml version="1.0" encoding="UTF-8"?>
<DataReleaseRoot xmlns="etl.vertexinc.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="-1" date="20060206" >
  <PreProcessing>
    <SqlProcess logicalName="TPS_DB" file="create.sql" ignoreErrors="false"/>
    <SqlProcess logicalName="TPS_DB" file="createindices.sql" ignoreErrors="false"/>
    <SqlProcess logicalName="TPS_DB" file="SchemaVersion.sql" ignoreErrors="false"/>
  </PreProcessing>
</DataReleaseRoot>
