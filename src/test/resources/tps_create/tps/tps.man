<?xml version="1.0" encoding="UTF-8"?>
<SubjectArea xmlns="etl.vertexinc.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="tps" >

  <DataRelease fullReleaseNumber="2"
               interimReleaseNumber="0"
               name="TPS"
               type="FULL" >

    <Description>DDL update for TPS database</Description>

  </DataRelease>


  <SchemaRelease id="${tps.schema.version.id}" date="20040527" validate="true" ></SchemaRelease>

</SubjectArea>
