create table CoverageActionType
(
    coverageActionTypeId bigint NOT NULL ,
    coverageActionTypeName varchar(60) NOT NULL 
);

ALTER TABLE CoverageActionType
    ADD CONSTRAINT XPKJICvrgActTyp
PRIMARY KEY (coverageActionTypeId);


create table CertificateStatus
(
    certificateStatusId bigint NOT NULL ,
    certificateStatusName varchar(60) NOT NULL 
);

ALTER TABLE CertificateStatus
    ADD CONSTRAINT XPKJICertStat
PRIMARY KEY (certificateStatusId);


create table CertificateApprovalStatus
(
    approvalStatusId bigint NOT NULL ,
    certApprovalStatusName varchar(60) NOT NULL 
);

ALTER TABLE CertificateApprovalStatus
    ADD CONSTRAINT XPKCertApprStatus
PRIMARY KEY (approvalStatusId);


create table ContactRoleType
(
    contactRoleTypeId bigint NOT NULL ,
    contactRoleTypeName varchar(60) NOT NULL 
);

ALTER TABLE ContactRoleType
    ADD CONSTRAINT XPKContactRoleType
PRIMARY KEY (contactRoleTypeId);


create table FormFieldType
(
    formFieldTypeId bigint NOT NULL ,
    name varchar(60) NULL 
);

ALTER TABLE FormFieldType
    ADD CONSTRAINT XPKFormField
PRIMARY KEY (formFieldTypeId);


create table TaxRegistrationType
(
    taxRegistrationTypeId bigint NOT NULL ,
    taxRegistrationTypeName varchar(60) NOT NULL 
);

ALTER TABLE TaxRegistrationType
    ADD CONSTRAINT XPKTaxRegType
PRIMARY KEY (taxRegistrationTypeId);


create table ApportionmentType
(
    apportionTypeId bigint NOT NULL ,
    apportionTypeName varchar(30) NOT NULL 
);

ALTER TABLE ApportionmentType
    ADD CONSTRAINT XPKApportionType
PRIMARY KEY (apportionTypeId);


create table SitusConditionType
(
    situsCondTypeId bigint NOT NULL ,
    situsCondTypeName varchar(30) NULL 
);

ALTER TABLE SitusConditionType
    ADD CONSTRAINT XPKSitusCondType
PRIMARY KEY (situsCondTypeId);


create table ChainTransType
(
    chainTransId bigint NOT NULL ,
    chainTransName varchar(60) NULL 
);

ALTER TABLE ChainTransType
    ADD CONSTRAINT XPKChainTransType
PRIMARY KEY (chainTransId);


create table TitleTransferType
(
    titleTransferId bigint NOT NULL ,
    titleTransferName varchar(60) NULL 
);

ALTER TABLE TitleTransferType
    ADD CONSTRAINT XPKTitleTransType
PRIMARY KEY (titleTransferId);


create table AssistedState
(
    assistedStateId bigint NOT NULL ,
    assistedStateName varchar(60) NULL 
);

ALTER TABLE AssistedState
    ADD CONSTRAINT XPKAssistedState
PRIMARY KEY (assistedStateId);


create table RateClassification
(
    rateClassId bigint NOT NULL ,
    rateClassName varchar(60) NULL 
);

ALTER TABLE RateClassification
    ADD CONSTRAINT XPKRateClass
PRIMARY KEY (rateClassId);


create table DMFilter
(
    filterId bigint NOT NULL ,
    filterName varchar(60) NOT NULL ,
    sourceId bigint NULL ,
    activityTypeId bigint NOT NULL ,
    filterDescription varchar(255) NULL ,
    followupInd numeric(1) NULL 
);

ALTER TABLE DMFilter
    ADD CONSTRAINT XPKDMFilter
PRIMARY KEY (filterId);


create table BusinessTransType
(
    busTransTypeId bigint NOT NULL ,
    busTransTypeName varchar(60) NOT NULL 
);

ALTER TABLE BusinessTransType
    ADD CONSTRAINT XPKBusTransType
PRIMARY KEY (busTransTypeId);


create table TransactionType
(
    transactionTypeId bigint NOT NULL ,
    transactionTypName varchar(60) NOT NULL 
);

ALTER TABLE TransactionType
    ADD CONSTRAINT XPKTransactionType
PRIMARY KEY (transactionTypeId);


create table DataType
(
    dataTypeId bigint NOT NULL ,
    dataTypeName varchar(60) NOT NULL 
);

ALTER TABLE DataType
    ADD CONSTRAINT XPKDataType
PRIMARY KEY (dataTypeId);


create table InputParameterType
(
    inputParamTypeId bigint NOT NULL ,
    inputParamTypeName varchar(60) NULL ,
    lookupStrategyId bigint NULL ,
    commodityCodeInd numeric(1) NULL ,
    commodityCodeLength bigint NULL ,
    isTelecommLineType numeric(1) NULL 
);

ALTER TABLE InputParameterType
    ADD CONSTRAINT XPKInputParamType
PRIMARY KEY (inputParamTypeId);


create table JurTypeSet
(
    jurTypeSetId bigint NOT NULL ,
    jurTypeSetName varchar(60) NULL 
);

ALTER TABLE JurTypeSet
    ADD CONSTRAINT XPKJurTypeSet
PRIMARY KEY (jurTypeSetId);


create table JurTypeSetMember
(
    jurTypeSetId bigint NOT NULL ,
    jurisdictionTypeId bigint NOT NULL 
);

ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT XPKJurTypeSetMem
PRIMARY KEY (jurTypeSetId, jurisdictionTypeId);


ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT f1JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

create table TaxResultType
(
    taxResultTypeId bigint NOT NULL ,
    taxResultTypeName varchar(60) NOT NULL 
);

ALTER TABLE TaxResultType
    ADD CONSTRAINT XPKTaxResultType
PRIMARY KEY (taxResultTypeId);


create table RecoverableResultType
(
    recoverableResultTypeId bigint NOT NULL ,
    recoverableResultTypeName varchar(60) NOT NULL 
);

ALTER TABLE RecoverableResultType
    ADD CONSTRAINT XPKRcvResultType
PRIMARY KEY (recoverableResultTypeId);


create table PartyType
(
    partyTypeId bigint NOT NULL ,
    partyTypeName varchar(20) NOT NULL 
);

ALTER TABLE PartyType
    ADD CONSTRAINT XPKPartyType
PRIMARY KEY (partyTypeId);


create table LocationRoleType
(
    locationRoleTypeId bigint NOT NULL ,
    locationRoleTypNam varchar(30) NOT NULL 
);

ALTER TABLE LocationRoleType
    ADD CONSTRAINT XPKLocationRoleTyp
PRIMARY KEY (locationRoleTypeId);


create table ShippingTerms
(
    shippingTermsId bigint NOT NULL ,
    shippingTermsName varchar(60) NOT NULL 
);

ALTER TABLE ShippingTerms
    ADD CONSTRAINT XPKShippingTerms
PRIMARY KEY (shippingTermsId);


create table BasisType
(
    basisTypeId bigint NOT NULL ,
    basisTypeName varchar(60) NULL 
);

ALTER TABLE BasisType
    ADD CONSTRAINT XPKBasisType
PRIMARY KEY (basisTypeId);


create table DMFilterDate
(
    dateTypeId bigint NOT NULL ,
    criteriaDate numeric(8) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    filterId bigint NOT NULL 
);

ALTER TABLE DMFilterDate
    ADD CONSTRAINT XPKDMFilterDate
PRIMARY KEY (filterId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMFilterDate
    ADD CONSTRAINT f1DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table OutputNotice
(
    outputNoticeId bigint NOT NULL ,
    outputNoticeName varchar(30) NOT NULL 
);

ALTER TABLE OutputNotice
    ADD CONSTRAINT XPKOutputNotice
PRIMARY KEY (outputNoticeId);


create table CustomsStatusType
(
    customsStatusId bigint NOT NULL ,
    customsStatusName varchar(60) NOT NULL 
);

ALTER TABLE CustomsStatusType
    ADD CONSTRAINT XPKCustomsStatType
PRIMARY KEY (customsStatusId);


create table CreationSource
(
    creationSourceId bigint NOT NULL ,
    name varchar(60) NOT NULL 
);

ALTER TABLE CreationSource
    ADD CONSTRAINT XPKCreationSource
PRIMARY KEY (creationSourceId);


create table MovementMethodType
(
    movementMethodId bigint NOT NULL ,
    movementMethodName varchar(60) NOT NULL 
);

ALTER TABLE MovementMethodType
    ADD CONSTRAINT XPKMvmntMethodType
PRIMARY KEY (movementMethodId);


create table SitusCondition
(
    situsConditionId bigint NOT NULL ,
    locRoleTypeId bigint NULL ,
    locRoleType2Id bigint NULL ,
    situsCondTypeId bigint NULL ,
    transactionTypeId bigint NULL ,
    transPrspctvTypId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    jurisdictionTypeId bigint NULL ,
    endDate numeric(8) NOT NULL ,
    jurTypeSetId bigint NULL ,
    partyRoleTypeId bigint NULL ,
    currencyUnitId bigint NULL ,
    locRoleTypeId1Name varchar(60) NULL ,
    locRoleTypeId2Name varchar(60) NULL ,
    stsSubRtnNodeId bigint NULL ,
    boolFactName varchar(60) NULL ,
    boolFactValue numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    customsStatusId bigint NULL ,
    movementMethodId bigint NULL ,
    titleTransferId bigint NULL 
);

ALTER TABLE SitusCondition
    ADD CONSTRAINT XPKSitusCondition
PRIMARY KEY (situsConditionId);


ALTER TABLE SitusCondition
    ADD CONSTRAINT f2LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1SCdTyp FOREIGN KEY (situsCondTypeId)
    REFERENCES SitusConditionType (situsCondTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1CstStT FOREIGN KEY (customsStatusId)
    REFERENCES CustomsStatusType (customsStatusId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1MvMthT FOREIGN KEY (movementMethodId)
    REFERENCES MovementMethodType (movementMethodId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1TtTfrT FOREIGN KEY (titleTransferId)
    REFERENCES TitleTransferType (titleTransferId)
;

create table BracketTaxCalcType
(
    brcktTaxCalcTypeId bigint NOT NULL ,
    brcktTaxCalcTypNam varchar(60) NULL 
);

ALTER TABLE BracketTaxCalcType
    ADD CONSTRAINT XPKBrcktTaxCalcTyp
PRIMARY KEY (brcktTaxCalcTypeId);


create table SitusConcType
(
    situsConcTypeId bigint NOT NULL ,
    situsConcTypeName varchar(60) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusConcType
    ADD CONSTRAINT XPKSitusConcType
PRIMARY KEY (situsConcTypeId);


create table ReasonCategory
(
    reasonCategoryId bigint NOT NULL ,
    reasonCategoryName varchar(60) NULL ,
    isUserDefined numeric(1) NULL 
);

ALTER TABLE ReasonCategory
    ADD CONSTRAINT XPKReasonCategory
PRIMARY KEY (reasonCategoryId);


create table ReasonCategoryJur
(
    reasonCategoryId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NULL 
);

ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT XPKReasonCatJur
PRIMARY KEY (reasonCategoryId, jurisdictionId, effDate);


ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT f7RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

create table SitusCondJur
(
    situsConditionId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL 
);

ALTER TABLE SitusCondJur
    ADD CONSTRAINT XPKSitusCondJur
PRIMARY KEY (situsConditionId, jurisdictionId);


ALTER TABLE SitusCondJur
    ADD CONSTRAINT f2StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table SitusCondShippingTerms
(
    situsConditionId bigint NOT NULL ,
    shippingTermsId bigint NOT NULL 
);

ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT XPKSitusCondShipTm
PRIMARY KEY (situsConditionId, shippingTermsId);


ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT f4StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TaxType
(
    taxTypeId bigint NOT NULL ,
    taxTypeName varchar(60) NOT NULL 
);

ALTER TABLE TaxType
    ADD CONSTRAINT XPKTaxType
PRIMARY KEY (taxTypeId);


create table FilingCategory
(
    filingCategoryId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    filingCategoryCode bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCategoryName varchar(60) NOT NULL ,
    primaryCategoryInd numeric(1) NOT NULL 
);

ALTER TABLE FilingCategory
    ADD CONSTRAINT XPKFilingCat
PRIMARY KEY (filingCategoryId);


create table FilingCategoryOvrd
(
    filingCatOvrdId bigint NOT NULL ,
    filingCategoryId bigint NOT NULL ,
    ovrdFilingCatId bigint NOT NULL ,
    taxTypeId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT XPKFCOvd
PRIMARY KEY (filingCatOvrdId);


ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f2FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f3FilCat FOREIGN KEY (ovrdFilingCatId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f5TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table SitusConclusion
(
    situsConclusionId bigint NOT NULL ,
    situsConcTypeId bigint NULL ,
    taxTypeId bigint NULL ,
    jurisdictionTypeId bigint NULL ,
    locRoleTypeId bigint NULL ,
    locRoleType2Id bigint NULL ,
    multiSitusRecTypId bigint NULL ,
    jurTypeSetId bigint NULL ,
    taxType2Id bigint NULL ,
    locRoleTypeId1Name varchar(60) NULL ,
    locRoleTypeId2Name varchar(60) NULL ,
    taxTypeIdName varchar(60) NULL ,
    boolFactName varchar(60) NULL ,
    boolFactValue numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    outputNoticeId bigint NULL ,
    impsnTypeId bigint NULL ,
    impsnTypeSourceId bigint NULL ,
    txbltyCatId bigint NULL 
);

ALTER TABLE SitusConclusion
    ADD CONSTRAINT XPKSitusConc
PRIMARY KEY (situsConclusionId);


ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1OutNot FOREIGN KEY (outputNoticeId)
    REFERENCES OutputNotice (outputNoticeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1SCcTyp FOREIGN KEY (situsConcTypeId)
    REFERENCES SitusConcType (situsConcTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3TaxTyp FOREIGN KEY (taxType2Id)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxScope
(
    taxScopeId bigint NOT NULL ,
    taxScopeName varchar(60) NOT NULL 
);

ALTER TABLE TaxScope
    ADD CONSTRAINT XPKTaxScope
PRIMARY KEY (taxScopeId);


create table RoundingRule
(
    roundingRuleId bigint NOT NULL ,
    initialPrecision bigint NULL ,
    taxScopeId bigint NOT NULL ,
    finalPrecision bigint NOT NULL ,
    threshold bigint NOT NULL ,
    decimalPosition bigint NOT NULL ,
    roundingTypeId bigint NULL 
);

ALTER TABLE RoundingRule
    ADD CONSTRAINT XPKRoundingRule
PRIMARY KEY (roundingRuleId);


ALTER TABLE RoundingRule
    ADD CONSTRAINT f2TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

create table TransSubType
(
    transSubTypeId bigint NOT NULL ,
    transSubTypeName varchar(60) NULL 
);

ALTER TABLE TransSubType
    ADD CONSTRAINT XPKTransSubType
PRIMARY KEY (transSubTypeId);


create table VertexProductType
(
    vertexProductId bigint NOT NULL ,
    vertexProductName varchar(60) NULL 
);

ALTER TABLE VertexProductType
    ADD CONSTRAINT XPKVertexProdType
PRIMARY KEY (vertexProductId);


create table PartyRoleType
(
    partyRoleTypeId bigint NOT NULL ,
    partyRoleTypeName varchar(60) NOT NULL 
);

ALTER TABLE PartyRoleType
    ADD CONSTRAINT XPKPartyRoleType
PRIMARY KEY (partyRoleTypeId);


create table TelecomUnitConversion
(
    telecomUnitConversionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    name varchar(60) NOT NULL ,
    sourceUnitOfMeasureISOCode varchar(20) NOT NULL ,
    targetUnitOfMeasureISOCode varchar(20) NOT NULL ,
    firstConvertCount bigint NOT NULL ,
    additionalConvertCount bigint NULL ,
    isDefault bigint NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversion
    ADD CONSTRAINT XPKTelComUntConvsn
PRIMARY KEY (telecomUnitConversionId, sourceId);


create table TaxStructureType
(
    taxStructureTypeId bigint NOT NULL ,
    taxStrucTypeName varchar(60) NOT NULL 
);

ALTER TABLE TaxStructureType
    ADD CONSTRAINT XPKTaxStrucType
PRIMARY KEY (taxStructureTypeId);


create table DeductionType
(
    deductionTypeId bigint NOT NULL ,
    deductionTypeName varchar(60) NULL 
);

ALTER TABLE DeductionType
    ADD CONSTRAINT XPKDeductionType
PRIMARY KEY (deductionTypeId);


create table TransOrigType
(
    transOrigTypeId bigint NOT NULL ,
    transOrigTypeName varchar(60) NULL 
);

ALTER TABLE TransOrigType
    ADD CONSTRAINT XPKTransOrigType
PRIMARY KEY (transOrigTypeId);


create table TaxRuleType
(
    taxRuleTypeId bigint NOT NULL ,
    taxRuleTypeName varchar(60) NULL 
);

ALTER TABLE TaxRuleType
    ADD CONSTRAINT XPKTaxRuleType
PRIMARY KEY (taxRuleTypeId);


create table WithholdingType
(
    withholdingTypeId bigint NOT NULL ,
    withholdingTypeName varchar(60) NULL 
);

ALTER TABLE WithholdingType
    ADD CONSTRAINT XPKWithholdingType
PRIMARY KEY (withholdingTypeId);


create table AccumulationByType
(
    id bigint NOT NULL ,
    name varchar(60) NULL 
);

ALTER TABLE AccumulationByType
    ADD CONSTRAINT XPKAccByType
PRIMARY KEY (id);


create table AccumulationPeriodType
(
    id bigint NOT NULL ,
    name varchar(60) NULL 
);

ALTER TABLE AccumulationPeriodType
    ADD CONSTRAINT XPKAccPeriodType
PRIMARY KEY (id);


create table AccumulationType
(
    id bigint NOT NULL ,
    name varchar(60) NULL 
);

ALTER TABLE AccumulationType
    ADD CONSTRAINT XPKAccType
PRIMARY KEY (id);


create table TaxRuleTaxImpositionType
(
    taxRuleTaxImpositionTypeId bigint NOT NULL ,
    taxRuleTaxImpositionTypeName varchar(60) NULL 
);

ALTER TABLE TaxRuleTaxImpositionType
    ADD CONSTRAINT XPKTxRuleImpsnType
PRIMARY KEY (taxRuleTaxImpositionTypeId);


create table ValidationType
(
    validationTypeId bigint NOT NULL ,
    validationTypeName varchar(60) NULL 
);

ALTER TABLE ValidationType
    ADD CONSTRAINT XPKValidationType
PRIMARY KEY (validationTypeId);


create table DMFilterStrng
(
    stringTypeId bigint NOT NULL ,
    criteriaString varchar(255) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    filterId bigint NOT NULL 
);

ALTER TABLE DMFilterStrng
    ADD CONSTRAINT XPKDMFilterStrng
PRIMARY KEY (filterId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMFilterStrng
    ADD CONSTRAINT f3DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table CertClassType
(
    certClassTypeId bigint NOT NULL ,
    certClassTypeName varchar(60) NULL 
);

ALTER TABLE CertClassType
    ADD CONSTRAINT XPKCertClassType
PRIMARY KEY (certClassTypeId);


create table DiscountCategory
(
    discountCatId bigint NOT NULL ,
    discountCatName varchar(60) NOT NULL ,
    discountCatDesc varchar(100) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE DiscountCategory
    ADD CONSTRAINT XPKDiscountCat
PRIMARY KEY (discountCatId);


create table TaxabilityCategory
(
    txbltyCatId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL 
);

ALTER TABLE TaxabilityCategory
    ADD CONSTRAINT XPKTaxabilityCat
PRIMARY KEY (txbltyCatId, txbltyCatSrcId);


create table TxbltyCatDetail
(
    txbltyCatDtlId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatCode varchar(60) NOT NULL ,
    txbltyCatName varchar(60) NOT NULL ,
    txbltyCatDesc varchar(1000) NULL ,
    txbltyCatDefaultId bigint NOT NULL ,
    prntTxbltyCatId bigint NULL ,
    prntTxbltyCatSrcId bigint NULL ,
    dataTypeId bigint NULL ,
    deletedInd numeric(1) NOT NULL ,
    allowRelatedInd numeric(1) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT XPKTxbltyCatDetail
PRIMARY KEY (txbltyCatDtlId, txbltyCatSrcId);


ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f2TaxCat FOREIGN KEY (prntTxbltyCatId, prntTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

create table FlexFieldDef
(
    flexFieldDefId bigint NOT NULL ,
    flexFieldDefSrcId bigint NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDef
    ADD CONSTRAINT XPKFlexibleField
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId);


create table FlexFieldDefDetail
(
    flexFieldDefDtlId bigint NOT NULL ,
    flexFieldDefSrcId bigint NOT NULL ,
    flexFieldDefId bigint NOT NULL ,
    dataTypeId bigint NOT NULL ,
    calcOutputInd numeric(1) DEFAULT 0 NOT NULL ,
    flexFieldDesc varchar(1000) NULL ,
    flexFieldDefRefNum bigint NOT NULL ,
    flexFieldDefSeqNum bigint NOT NULL ,
    shortName varchar(10) NOT NULL ,
    longName varchar(60) NULL ,
    txbltyCatId bigint NULL ,
    txbltyCatSrcId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT XPKFlexFieldDetail
PRIMARY KEY (flexFieldDefDtlId, flexFieldDefSrcId);


ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f2DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f5TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f3FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

create table TaxJurDetail
(
    taxTypeId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    sourceId bigint NOT NULL ,
    regGroupAllowedInd numeric(1) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    registrationReqInd numeric(1) NOT NULL ,
    reqLocsForRprtgInd numeric(1) NOT NULL ,
    reqLocsForSitusInd numeric(1) NOT NULL 
);

ALTER TABLE TaxJurDetail
    ADD CONSTRAINT XPKTaxJurDetail
PRIMARY KEY (jurisdictionId, taxTypeId, effDate, sourceId);


ALTER TABLE TaxJurDetail
    ADD CONSTRAINT f16TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table DMActivityLog
(
    activityLogId bigint NOT NULL ,
    filterName varchar(60) NULL ,
    activityTypeId bigint NOT NULL ,
    startTime timestamp NOT NULL ,
    endTime timestamp NULL ,
    lastPingTime timestamp NOT NULL ,
    nextPingTime timestamp NOT NULL ,
    userName varchar(60) NOT NULL ,
    userId bigint NOT NULL ,
    sourceName varchar(60) NULL ,
    sourceId bigint NULL ,
    activityStatusId bigint NOT NULL ,
    filterDescription varchar(255) NULL ,
    activityLogMessage varchar(1000) NULL ,
    hostName varchar(128) NOT NULL ,
    outputFileName varchar(255) NULL ,
    masterAdminInd numeric(1) NULL ,
    sysAdminInd numeric(1) NULL ,
    followupInd numeric(1) NULL 
);

ALTER TABLE DMActivityLog
    ADD CONSTRAINT XPKDMActivityLog
PRIMARY KEY (activityLogId);


create table DMActivityLogNum
(
    numberTypeId bigint NOT NULL ,
    criteriaNum bigint NULL ,
    criteriaOrderNum bigint NOT NULL ,
    activityLogId bigint NOT NULL 
);

ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT XPKDMActLogNum
PRIMARY KEY (activityLogId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT f2DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogDate
(
    dateTypeId bigint NOT NULL ,
    criteriaDate numeric(8) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    activityLogId bigint NOT NULL 
);

ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT XPKDMActLogDate
PRIMARY KEY (activityLogId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT f1DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogInd
(
    indicatorTypeId bigint NOT NULL ,
    criteriaIndicator numeric(1) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    activityLogId bigint NOT NULL 
);

ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT XPKDMActLogInd
PRIMARY KEY (activityLogId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT f4DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table TpsSchVersion
(
    schemaVersionId bigint NOT NULL ,
    subjectAreaId bigint NULL ,
    schemaVersionCode varchar(30) NOT NULL ,
    syncVersionId varchar(64) NULL ,
    schemaSubVersionId bigint NULL 
);

ALTER TABLE TpsSchVersion
    ADD CONSTRAINT XPKTpsSchemaVer
PRIMARY KEY (schemaVersionId);


create table TpsPatchDataEvent
(
    dataEventId bigint NOT NULL ,
    patchId bigint NOT NULL ,
    patchInterimId bigint NOT NULL ,
    eventDate numeric(8) NOT NULL 
);

ALTER TABLE TpsPatchDataEvent
    ADD CONSTRAINT XPKTPSPchDataEvent
PRIMARY KEY (dataEventId);


create table ApportionmentFriendlyStates
(
    friendlyStateId bigint NOT NULL ,
    jurId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE ApportionmentFriendlyStates
    ADD CONSTRAINT XPKApptionFriendly
PRIMARY KEY (friendlyStateId);


create table SitusConditionNode
(
    situsCondNodeId bigint NOT NULL ,
    situsConditionId bigint NOT NULL ,
    rootNodeInd numeric(1) NOT NULL 
);

ALTER TABLE SitusConditionNode
    ADD CONSTRAINT XPKSitusCondNode
PRIMARY KEY (situsCondNodeId);


ALTER TABLE SitusConditionNode
    ADD CONSTRAINT f1StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TrueSitusCond
(
    prntTruSitusCondId bigint NOT NULL ,
    chldTruSitusCondId bigint NOT NULL 
);

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT XPKTrueSitusCond
PRIMARY KEY (prntTruSitusCondId, chldTruSitusCondId);


ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f1StCdNd FOREIGN KEY (prntTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f2StCdNd FOREIGN KEY (chldTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table SitusConcNode
(
    situsConcNodeId bigint NOT NULL ,
    situsConclusionId bigint NOT NULL ,
    truForSitusCondId bigint NULL ,
    flsForSitusCondId bigint NULL 
);

ALTER TABLE SitusConcNode
    ADD CONSTRAINT XPKSitusConcNode
PRIMARY KEY (situsConcNodeId);


ALTER TABLE SitusConcNode
    ADD CONSTRAINT f1StsCnc FOREIGN KEY (situsConclusionId)
    REFERENCES SitusConclusion (situsConclusionId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f5StCdNd FOREIGN KEY (truForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f6StCdNd FOREIGN KEY (flsForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table Party
(
    partyId bigint NOT NULL ,
    partySourceId bigint NOT NULL ,
    partyCreationDate numeric(8) NOT NULL 
);

ALTER TABLE Party
    ADD CONSTRAINT XPKParty
PRIMARY KEY (partyId, partySourceId);


create table PartyDetail
(
    partyDtlId bigint NOT NULL ,
    partySourceId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    creationSourceId bigint DEFAULT 1 NOT NULL ,
    parentPartyId bigint NULL ,
    partyTypeId bigint NOT NULL ,
    partyName varchar(60) NOT NULL ,
    userPartyIdCode varchar(40) NOT NULL ,
    partyRelInd numeric(1) NOT NULL ,
    taxpayerType varchar(60) NULL ,
    filingEntityInd numeric(1) NOT NULL ,
    prntInheritenceInd numeric(1) NOT NULL ,
    ersInd numeric(1) NOT NULL ,
    taxThresholdAmt numeric(18,3) NULL ,
    taxThresholdPct numeric(10,6) NULL ,
    taxOvrThresholdAmt numeric(18,3) NULL ,
    taxOvrThresholdPct numeric(10,6) NULL ,
    partyClassInd numeric(1) NOT NULL ,
    shippingTermsId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    discountCatId bigint NULL ,
    customField1Value varchar(60) NULL ,
    customField2Value varchar(60) NULL ,
    customField3Value varchar(60) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    parentCustomerId bigint NULL 
);

ALTER TABLE PartyDetail
    ADD CONSTRAINT XPKPartyDtl
PRIMARY KEY (partyDtlId, partySourceId);


ALTER TABLE PartyDetail
    ADD CONSTRAINT f1ShpTrm FOREIGN KEY (shippingTermsId)
    REFERENCES ShippingTerms (shippingTermsId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1PtyTyp FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f2Party FOREIGN KEY (parentPartyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f33Party FOREIGN KEY (parentCustomerId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f5DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

create table PartyRoleTaxResult
(
    partyId bigint NOT NULL ,
    partySourceId bigint NOT NULL ,
    partyRoleTypeId bigint NOT NULL ,
    taxResultTypeId bigint NOT NULL ,
    reasonCategoryId bigint NULL 
);

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT XPKPrtyRoleTaxRslt
PRIMARY KEY (partyId, partyRoleTypeId, partySourceId);


ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f5RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f1PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table PartyVtxProdType
(
    partyId bigint NOT NULL ,
    partySourceId bigint NOT NULL ,
    vertexProductId bigint NOT NULL 
);

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT XPKPrtyVtxProdTyp
PRIMARY KEY (partyId, partySourceId, vertexProductId);


ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f1VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f8Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table BusinessLocation
(
    partyId bigint NOT NULL ,
    businessLocationId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxAreaId bigint NULL ,
    userLocationCode varchar(20) NULL ,
    registrationCode varchar(20) NULL ,
    streetInfoDesc varchar(100) NULL ,
    streetInfo2Desc varchar(100) NULL ,
    cityName varchar(60) NULL ,
    subDivisionName varchar(60) NULL ,
    mainDivisionName varchar(60) NULL ,
    postalCode varchar(20) NULL ,
    countryName varchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    busLocationName varchar(60) NULL ,
    partyRoleTypeId bigint NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE BusinessLocation
    ADD CONSTRAINT XPKBusinessLoc
PRIMARY KEY (businessLocationId, partyId, sourceId);


ALTER TABLE BusinessLocation
    ADD CONSTRAINT f4Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE BusinessLocation
    ADD CONSTRAINT f3PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table PartyNote
(
    partySourceId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    partyNoteText varchar(1000) NULL 
);

ALTER TABLE PartyNote
    ADD CONSTRAINT XPKPartyNote
PRIMARY KEY (partyId, partySourceId);


ALTER TABLE PartyNote
    ADD CONSTRAINT f14Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table PartyContact
(
    partyContactId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    contactRoleTypeId bigint NOT NULL ,
    contactFirstName varchar(60) NULL ,
    contactLastName varchar(60) NULL ,
    streetInfoDesc varchar(100) NULL ,
    streetInfo2Desc varchar(100) NULL ,
    cityName varchar(60) NULL ,
    mainDivisionName varchar(60) NULL ,
    postalCode varchar(20) NULL ,
    countryName varchar(60) NULL ,
    phoneNumber varchar(20) NULL ,
    phoneExtension varchar(20) NULL ,
    faxNumber varchar(20) NULL ,
    emailAddress varchar(100) NULL ,
    departmentIdCode varchar(20) NULL ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE PartyContact
    ADD CONSTRAINT XPKPartyContact
PRIMARY KEY (partyContactId, sourceId);


ALTER TABLE PartyContact
    ADD CONSTRAINT f6Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyContact
    ADD CONSTRAINT f1CntcRT FOREIGN KEY (contactRoleTypeId)
    REFERENCES ContactRoleType (contactRoleTypeId)
;

create table TaxabilityDriver
(
    txbltyDvrId bigint NOT NULL ,
    txbltyDvrSrcId bigint NOT NULL 
);

ALTER TABLE TaxabilityDriver
    ADD CONSTRAINT XPKTaxabilityDvr
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


create table TxbltyDriverDetail
(
    txbltyDvrDtlId bigint NOT NULL ,
    txbltyDvrSrcId bigint NOT NULL ,
    txbltyDvrId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    inputParamTypeId bigint NOT NULL ,
    txbltyDvrCode varchar(40) NOT NULL ,
    txbltyDvrName varchar(60) NOT NULL ,
    reasonCategoryId bigint NULL ,
    exemptInd numeric(1) NOT NULL ,
    taxpayerPartyId bigint NULL ,
    taxpayerSrcId bigint NULL ,
    discountCatId bigint NULL ,
    flexFieldDefId bigint NULL ,
    flexFieldDefSrcId bigint NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT XPKTxbltyDvrDetail
PRIMARY KEY (txbltyDvrDtlId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f1TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f8RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2InPTyp FOREIGN KEY (inputParamTypeId)
    REFERENCES InputParameterType (inputParamTypeId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f3DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

create table TxbltyDriverNote
(
    txbltyDvrId bigint NOT NULL ,
    txbltyDvrSrcId bigint NOT NULL ,
    txbltyDvrNoteText varchar(1000) NULL 
);

ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT XPKTxbltyDvrNote
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT f9TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table TxbltyDvrVtxPrdTyp
(
    vertexProductId bigint NOT NULL ,
    txbltyDvrId bigint NOT NULL ,
    txbltyDvrSrcId bigint NOT NULL 
);

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT XPKTxbltyDvrVtxPrd
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId, vertexProductId);


ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f2TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f8VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TelecomUnitConversionLineType
(
    telecomUnitConvnLineTypeId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    lineTypeId bigint NOT NULL ,
    lineTypeSourceId bigint NOT NULL ,
    telecomUnitConversionId bigint NOT NULL ,
    telecomUnitConversionSourceId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT XPKTCUntCvnLnType
PRIMARY KEY (telecomUnitConvnLineTypeId, sourceId);


ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f1conv FOREIGN KEY (telecomUnitConversionId, sourceId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f2LineType FOREIGN KEY (lineTypeId, lineTypeSourceId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table TxbltyCatMap
(
    txbltyCatMapId bigint NOT NULL ,
    txbltyCatMapSrcId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL ,
    taxpayerPartyId bigint NULL ,
    otherPartyId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatMapNote varchar(1000) NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT XPKTxbltyCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId);


ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f7TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f24Party FOREIGN KEY (taxpayerPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f25Party FOREIGN KEY (otherPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

create table TxbltyDriverCatMap
(
    txbltyCatMapId bigint NOT NULL ,
    txbltyCatMapSrcId bigint NOT NULL ,
    txbltyDvrId bigint NOT NULL 
);

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT XPKTxbltyDvrCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId);


ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f5TaxDvr FOREIGN KEY (txbltyDvrId, txbltyCatMapSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f1TxCMap FOREIGN KEY (txbltyCatMapId, txbltyCatMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

create table InvoiceTextType
(
    invoiceTextTypeId bigint NOT NULL ,
    invoiceTextTypeSrcId bigint NOT NULL ,
    invoiceTextTypeName varchar(60) NOT NULL 
);

ALTER TABLE InvoiceTextType
    ADD CONSTRAINT XPKInvoiceTextType
PRIMARY KEY (invoiceTextTypeId, invoiceTextTypeSrcId);


create table InvoiceText
(
    invoiceTextId bigint NOT NULL ,
    invoiceTextSrcId bigint NOT NULL 
);

ALTER TABLE InvoiceText
    ADD CONSTRAINT XPKInvText
PRIMARY KEY (invoiceTextId, invoiceTextSrcId);


create table InvoiceTextDetail
(
    invoiceTextDtlId bigint NOT NULL ,
    invoiceTextSrcId bigint NOT NULL ,
    invoiceTextId bigint NOT NULL ,
    invoiceTextTypeId bigint NOT NULL ,
    invoiceTextTypeSrcId bigint NOT NULL ,
    invoiceTextCode varchar(60) NOT NULL ,
    invoiceText varchar(200) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT XPKInvTextDetail
PRIMARY KEY (invoiceTextDtlId, invoiceTextSrcId);


ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f1InvText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f2InvTextTyp FOREIGN KEY (invoiceTextTypeId, invoiceTextTypeSrcId)
    REFERENCES InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId)
;

create table DMFilterInd
(
    indicatorTypeId bigint NOT NULL ,
    criteriaIndicator numeric(1) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    filterId bigint NOT NULL 
);

ALTER TABLE DMFilterInd
    ADD CONSTRAINT XPKDMFilterInd
PRIMARY KEY (filterId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMFilterInd
    ADD CONSTRAINT f4DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table DiscountType
(
    sourceId bigint NOT NULL ,
    discountTypeId bigint NOT NULL ,
    taxpayerPartyId bigint NOT NULL ,
    discountCatId bigint NOT NULL ,
    discountCode varchar(20) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE DiscountType
    ADD CONSTRAINT XPKDiscountType
PRIMARY KEY (discountTypeId, sourceId);


ALTER TABLE DiscountType
    ADD CONSTRAINT f1DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE DiscountType
    ADD CONSTRAINT f2DscCat FOREIGN KEY (taxpayerPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table TaxStructure
(
    taxStructureSrcId bigint NOT NULL ,
    taxStructureId bigint NOT NULL ,
    taxStructureTypeId bigint NOT NULL ,
    brcktMaximumBasis numeric(18,3) NULL ,
    reductAmtDedTypeId bigint NULL ,
    reductReasonCategoryId bigint NULL ,
    allAtTopTierTypInd numeric(1) NOT NULL ,
    singleRate numeric(12,8) NULL ,
    taxPerUnitAmount numeric(18,6) NULL ,
    unitOfMeasureQty numeric(18,6) NOT NULL ,
    childTaxStrucSrcId bigint NULL ,
    unitBasedInd numeric(1) NOT NULL ,
    basisReductFactor numeric(12,8) NULL ,
    brcktTaxCalcType bigint NULL ,
    unitOfMeasISOCode varchar(3) NULL ,
    deletedInd numeric(1) NOT NULL ,
    childTaxStrucId bigint NULL ,
    usesStdRateInd numeric(1) NOT NULL ,
    flatTaxAmt numeric(18,3) NULL ,
    telecomUnitConversionId bigint NULL ,
    telecomUnitConversionSrcId bigint NULL 
);

ALTER TABLE TaxStructure
    ADD CONSTRAINT XPKTaxStructure
PRIMARY KEY (taxStructureId, taxStructureSrcId);


ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TxSTyp FOREIGN KEY (taxStructureTypeId)
    REFERENCES TaxStructureType (taxStructureTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1BTCTyp FOREIGN KEY (brcktTaxCalcType)
    REFERENCES BracketTaxCalcType (brcktTaxCalcTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1DedTyp FOREIGN KEY (reductAmtDedTypeId)
    REFERENCES DeductionType (deductionTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TelCon FOREIGN KEY (telecomUnitConversionId, telecomUnitConversionSrcId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

create table ImpositionType
(
    impsnTypeId bigint NOT NULL ,
    impsnTypeSrcId bigint NOT NULL ,
    impsnTypeName varchar(60) NOT NULL ,
    creditInd bigint NULL ,
    notInTotalInd bigint NULL ,
    withholdingTypeId bigint NULL 
);

ALTER TABLE ImpositionType
    ADD CONSTRAINT XPKImpositionType
PRIMARY KEY (impsnTypeId, impsnTypeSrcId);


ALTER TABLE ImpositionType
    ADD CONSTRAINT f1WitTyp FOREIGN KEY (withholdingTypeId)
    REFERENCES WithholdingType (withholdingTypeId)
;

create table TaxImposition
(
    jurisdictionId bigint NOT NULL ,
    taxImpsnId bigint NOT NULL ,
    taxImpsnSrcId bigint NOT NULL 
);

ALTER TABLE TaxImposition
    ADD CONSTRAINT XPKTaxImp
PRIMARY KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId);


create table TaxImpsnDetail
(
    taxImpsnDtlId bigint NOT NULL ,
    taxImpsnSrcId bigint NOT NULL ,
    taxImpsnId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    impsnTypeId bigint NOT NULL ,
    impsnTypeSrcId bigint NOT NULL ,
    taxImpsnName varchar(60) NOT NULL ,
    taxImpsnAbrv varchar(15) NULL ,
    taxImpsnDesc varchar(100) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    taxTypeId bigint NULL ,
    taxResponsibilityRoleTypeId bigint NULL ,
    conditionInd numeric(1) NULL ,
    primaryImpositionInd numeric(1) NULL 
);

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT XPKTaxImpsnDetail
PRIMARY KEY (taxImpsnDtlId, taxImpsnSrcId);


ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1ImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f6TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f18TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table TaxImpQualCond
(
    taxImpQualCondId bigint NOT NULL ,
    taxImpsnSrcId bigint NOT NULL ,
    taxImpsnDtlId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL 
);

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT XPKTaxImpQualCond
PRIMARY KEY (taxImpQualCondId, taxImpsnSrcId);


ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f9TaxImp FOREIGN KEY (taxImpsnDtlId, taxImpsnSrcId)
    REFERENCES TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId)
;

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f12TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

create table FilingOverride
(
    filingOverrideId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    jurTypeSetId bigint NOT NULL ,
    filingCategoryId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FilingOverride
    ADD CONSTRAINT XPKFilingOverride
PRIMARY KEY (filingOverrideId);


ALTER TABLE FilingOverride
    ADD CONSTRAINT f5FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingOverride
    ADD CONSTRAINT f5JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

create table Tier
(
    minBasisAmount numeric(18,3) NULL ,
    tierNum bigint NOT NULL ,
    taxResultTypeId bigint NULL ,
    maxBasisAmount numeric(18,3) NULL ,
    minQuantity numeric(18,6) NULL ,
    maxQuantity numeric(18,6) NULL ,
    unitOfMeasISOCode varchar(3) NULL ,
    unitOfMeasureQty numeric(18,6) NULL ,
    taxPerUnitAmount numeric(18,6) NULL ,
    tierTaxRate numeric(12,8) NULL ,
    taxStructureSrcId bigint NOT NULL ,
    taxStructureId bigint NOT NULL ,
    reasonCategoryId bigint NULL ,
    usesStdRateInd numeric(1) NOT NULL ,
    filingCategoryId bigint NULL ,
    rateClassId bigint NULL ,
    taxStructureTypeId bigint NULL ,
    equivalentQuantity numeric(18,6) NULL ,
    additionalQuantity numeric(18,6) NULL ,
    equivalentAdditionalQuantity numeric(18,6) NULL 
);

ALTER TABLE Tier
    ADD CONSTRAINT XPKTier
PRIMARY KEY (tierNum, taxStructureSrcId, taxStructureId);


ALTER TABLE Tier
    ADD CONSTRAINT f2TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f5TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f4RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f6FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f3RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

create table Bracket
(
    taxStructureId bigint NOT NULL ,
    taxStructureSrcId bigint NOT NULL ,
    minBasisAmount numeric(18,3) NULL ,
    bracketNum bigint NOT NULL ,
    maxBasisAmount numeric(18,3) NULL ,
    bracketTaxAmount numeric(18,3) NULL 
);

ALTER TABLE Bracket
    ADD CONSTRAINT XPKBracket
PRIMARY KEY (taxStructureId, taxStructureSrcId, bracketNum);


ALTER TABLE Bracket
    ADD CONSTRAINT f4TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

create table TaxRule
(
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    partyRoleTypeId bigint NULL ,
    partyId bigint NULL ,
    partySourceId bigint NULL ,
    taxpayerRoleTypeId bigint NULL ,
    taxpayerPartyId bigint NULL ,
    taxpayerPartySrcId bigint NULL ,
    qualDetailDesc varchar(100) NULL ,
    uniqueToLevelInd numeric(1) NOT NULL ,
    conditionSeqNum bigint NULL ,
    taxRuleTypeId bigint NULL ,
    taxResultTypeId bigint NULL ,
    recoverableResultTypeId bigint NULL ,
    defrdJurTypeId bigint NULL ,
    defersToStdRuleInd numeric(1) NOT NULL ,
    taxStructureSrcId bigint NULL ,
    taxStructureId bigint NULL ,
    automaticRuleInd numeric(1) NOT NULL ,
    standardRuleInd numeric(1) NOT NULL ,
    reasonCategoryId bigint NULL ,
    filingCategoryId bigint NULL ,
    appToSingleImpInd numeric(1) NULL ,
    appToSingleJurInd numeric(1) NULL ,
    discountTypeId bigint NULL ,
    discountTypeSrcId bigint NULL ,
    maxTaxRuleType bigint NULL ,
    taxScopeId bigint NULL ,
    maxTaxAmount numeric(18,3) NULL ,
    jurisdictionId bigint NOT NULL ,
    taxImpsnId bigint NOT NULL ,
    taxImpsnSrcId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    discountCatId bigint NULL ,
    deletedInd numeric(1) NOT NULL ,
    apportionTypeId bigint NULL ,
    appnTxbltyCatId bigint NULL ,
    appnTxbltyCatSrcId bigint NULL ,
    appnFlexFieldDefId bigint NULL ,
    appnFlexFieldDefSrcId bigint NULL ,
    rateClassId bigint NULL ,
    locationRoleTypeId bigint NULL ,
    applyFilingCatInd numeric(1) DEFAULT 0 NULL ,
    recoverablePct numeric(10,6) NULL ,
    taxResponsibilityRoleTypeId bigint NULL ,
    currencyUnitId bigint NULL ,
    salestaxHolidayInd bigint NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    includesAllTaxCats numeric(1) NULL ,
    invoiceTextId bigint NULL ,
    invoiceTextSrcId bigint NULL ,
    computationTypeId bigint NULL ,
    defrdImpsnTypeId bigint NULL ,
    defrdImpsnTypeSrcId bigint NULL ,
    notAllowZeroRateInd numeric(1) NULL ,
    accumulationAsJurisdictionId bigint NULL ,
    accumulationAsTaxImpsnId bigint NULL ,
    accumulationAsTaxImpsnSrcId bigint NULL ,
    accumulationTypeId bigint NULL ,
    periodTypeId bigint NULL ,
    startMonth bigint NULL ,
    maxLines bigint NULL ,
    unitOfMeasISOCode varchar(3) NULL ,
    telecomUnitConversionId bigint NULL ,
    telecomUnitConversionSrcId bigint NULL ,
    lineTypeCatId bigint NULL ,
    lineTypeCatSourceId bigint NULL 
);

ALTER TABLE TaxRule
    ADD CONSTRAINT XPKTaxRule
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TRlTyp FOREIGN KEY (taxRuleTypeId)
    REFERENCES TaxRuleType (taxRuleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f3RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1DscTyp FOREIGN KEY (discountTypeId, discountTypeSrcId)
    REFERENCES DiscountType (discountTypeId, sourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f6TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f4DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f28Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f30Party FOREIGN KEY (taxpayerPartyId, taxpayerPartySrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1ApprtTyp FOREIGN KEY (apportionTypeId)
    REFERENCES ApportionmentType (apportionTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f7LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1InvoiceText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2ImpTyp FOREIGN KEY (defrdImpsnTypeId, defrdImpsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RcvRTyp FOREIGN KEY (recoverableResultTypeId)
    REFERENCES RecoverableResultType (recoverableResultTypeId)
;

create table TaxRuleNote
(
    taxRuleNoteId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    taxRuleNoteSrcId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleNoteText varchar(1000) NULL 
);

ALTER TABLE TaxRuleNote
    ADD CONSTRAINT XPKTaxRuleNote
PRIMARY KEY (taxRuleNoteId, taxRuleNoteSrcId);


ALTER TABLE TaxRuleNote
    ADD CONSTRAINT f4TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table ExprConditionType
(
    exprCondTypeId bigint NOT NULL ,
    exprCondTypeName varchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ExprConditionType
    ADD CONSTRAINT XPKExprCondType
PRIMARY KEY (exprCondTypeId);


create table TaxRuleQualCond
(
    taxRuleQualCondId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    txbltyDvrId bigint NULL ,
    txbltyDvrSrcId bigint NULL ,
    txbltyCatId bigint NULL ,
    txbltyCatSrcId bigint NULL ,
    flexFieldDefId bigint NULL ,
    flexFieldDefSrcId bigint NULL ,
    minDate numeric(8) NULL ,
    maxDate numeric(8) NULL ,
    compareValue numeric(18,3) NULL ,
    exprCondTypeId bigint NULL 
);

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT XPKTaxRuleQualCond
PRIMARY KEY (taxRuleQualCondId, taxRuleSourceId);


ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f8TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f4TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f7TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f1FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f2ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

create table TaxRuleTaxType
(
    taxRuleTaxTypeId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxTypeId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT XPKTaxRuleTaxType
PRIMARY KEY (taxRuleTaxTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f14TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f9TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table TaxRuleTaxImposition
(
    taxRuleTaxImpositionId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleTaxImpositionTypeId bigint NOT NULL ,
    jurisdictionId bigint NULL ,
    taxImpsnId bigint NULL ,
    taxImpsnSrcId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    txbltyCatId bigint NULL ,
    txbltyCatSrcId bigint NULL ,
    overTxbltyCatId bigint NULL ,
    overTxbltyCatSrcId bigint NULL ,
    underTxbltyCatId bigint NULL ,
    underTxbltyCatSrcId bigint NULL ,
    invoiceThresholdAmt numeric(18,5) NULL ,
    thresholdCurrencyUnitId bigint NULL ,
    includeTaxableAmountInd numeric(1) NULL ,
    includeTaxAmountInd numeric(1) NULL ,
    rate numeric(12,8) NULL ,
    locationRoleTypeId bigint NULL ,
    impositionTypeId bigint NULL ,
    impositionTypeSrcId bigint NULL ,
    jurTypeId bigint NULL ,
    taxTypeId bigint NULL ,
    isLeftInd numeric(1) NULL ,
    groupId bigint NULL 
);

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT XPKTaxRuleTaxImpsn
PRIMARY KEY (taxRuleTaxImpositionId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f7TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f9TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxCat FOREIGN KEY (overTxbltyCatId, overTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f11TaxCat FOREIGN KEY (underTxbltyCatId, underTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f12LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f13TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table MaxTaxRuleAdditionalCond
(
    maxTaxRuleCondId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL 
);

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT XPKMaxTaxRuleCond
PRIMARY KEY (maxTaxRuleCondId);


ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

create table ComputationType
(
    computationTypeId bigint NOT NULL ,
    compTypeName varchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ComputationType
    ADD CONSTRAINT XPKCompType
PRIMARY KEY (computationTypeId);


create table TaxFactorType
(
    taxFactorTypeId bigint NOT NULL ,
    taxFactorTypeName varchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE TaxFactorType
    ADD CONSTRAINT XPKTaxFactorType
PRIMARY KEY (taxFactorTypeId);


create table TaxFactor
(
    taxFactorId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxFactorTypeId bigint NOT NULL ,
    constantValue numeric(18,6) NULL ,
    basisTypeId bigint NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    txbltyCatId bigint NULL ,
    txbltyCatSrcId bigint NULL ,
    flexFieldDefId bigint NULL ,
    flexFieldDefSrcId bigint NULL ,
    impositionTypeId bigint NULL ,
    impositionTypeSrcId bigint NULL ,
    locationRoleTypeId bigint NULL ,
    jurTypeId bigint NULL ,
    taxTypeId bigint NULL 
);

ALTER TABLE TaxFactor
    ADD CONSTRAINT XPKTaxFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE TaxFactor
    ADD CONSTRAINT f8TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f4FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f1TxFcTp FOREIGN KEY (taxFactorTypeId)
    REFERENCES TaxFactorType (taxFactorTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f2BssTyp FOREIGN KEY (basisTypeId)
    REFERENCES BasisType (basisTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f17LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f19TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table ComputationFactor
(
    taxFactorId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    computationTypeId bigint NOT NULL ,
    leftTaxFactorId bigint NOT NULL ,
    rightTaxFactorId bigint NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ComputationFactor
    ADD CONSTRAINT XPKCompFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE ComputationFactor
    ADD CONSTRAINT f3TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f4TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f5TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f1CmpTyp FOREIGN KEY (computationTypeId)
    REFERENCES ComputationType (computationTypeId)
;

create table ConditionalTaxExpr
(
    condTaxExprId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    exprCondTypeId bigint NOT NULL ,
    leftTaxFactorId bigint NOT NULL ,
    rightTaxFactorId bigint NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT XPKCondTaxExpr
PRIMARY KEY (condTaxExprId, sourceId);


ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f2TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f6TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table TaxBasisConclusion
(
    taxBasisConcId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxFactorId bigint NOT NULL ,
    taxTypeId bigint NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT XPKTaxBasisConc
PRIMARY KEY (taxBasisConcId, sourceId);


ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f7TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxabilityMapping
(
    taxabilityMapId bigint NOT NULL ,
    taxabilityMapSrcId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    txbltyCatMapId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT XPKTaxabilityMap
PRIMARY KEY (taxabilityMapId, taxabilityMapSrcId);


ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f3TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f2TxCMap FOREIGN KEY (txbltyCatMapId, taxabilityMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

create table LineItemTaxDtlType
(
    lineItemTxDtlTypId bigint NOT NULL ,
    lineItemTxDtlTypNm varchar(60) NULL 
);

ALTER TABLE LineItemTaxDtlType
    ADD CONSTRAINT XPKLinItmTaxDtlTyp
PRIMARY KEY (lineItemTxDtlTypId);


create table FalseSitusCond
(
    prntFlsSitusCondId bigint NOT NULL ,
    chldFlsSitusCondId bigint NOT NULL 
);

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT XPKFalseSitusCond
PRIMARY KEY (prntFlsSitusCondId, chldFlsSitusCondId);


ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f4StCdNd FOREIGN KEY (chldFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f3StCdNd FOREIGN KEY (prntFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table TaxRuleTransType
(
    taxRuleTransTypeId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    transactionTypeId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT XPKTaxRuleTranTyp
PRIMARY KEY (taxRuleTransTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table DMActivityLogStrng
(
    stringTypeId bigint NOT NULL ,
    criteriaString varchar(255) NULL ,
    criteriaOrderNum bigint NOT NULL ,
    activityLogId bigint NOT NULL 
);

ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT XPKDMActLogStrng
PRIMARY KEY (activityLogId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT f3DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogFile
(
    activityLogId bigint NOT NULL ,
    fileId bigint NOT NULL ,
    fileName varchar(255) NOT NULL ,
    statusNum bigint NOT NULL 
);

ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT XPKDMActLogFile
PRIMARY KEY (activityLogId, fileId);


ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT f5DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table TaxRuleDescription
(
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    taxRuleDescText varchar(1000) NULL 
);

ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT XPKTaxRuleDesc
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT f5TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table SitusCondTxbltyCat
(
    txbltyCatSrcId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    situsConditionId bigint NOT NULL 
);

ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT XPKSitusCondTaxCat
PRIMARY KEY (situsConditionId, txbltyCatSrcId, txbltyCatId);


ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT f3StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TaxRuleCondJur
(
    taxRuleCondJurId bigint NOT NULL ,
    taxRuleSourceId bigint NOT NULL ,
    taxRuleId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT XPKTaxRuleCondJur
PRIMARY KEY (taxRuleCondJurId, taxRuleSourceId);


ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT f10TxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table DMFilterNum
(
    numberTypeId bigint NOT NULL ,
    criteriaNum bigint NULL ,
    criteriaOrderNum bigint NOT NULL ,
    filterId bigint NOT NULL 
);

ALTER TABLE DMFilterNum
    ADD CONSTRAINT XPKDMFilterNum
PRIMARY KEY (filterId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMFilterNum
    ADD CONSTRAINT f2DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table SitusTreatment
(
    situsTreatmentId bigint NOT NULL ,
    situsCondNodeId bigint NULL ,
    situsTreatmentName varchar(60) NOT NULL ,
    situsTreatmentDesc varchar(1000) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTreatment
    ADD CONSTRAINT XPKSitusTreatment
PRIMARY KEY (situsTreatmentId);


ALTER TABLE SitusTreatment
    ADD CONSTRAINT f7StCdNd FOREIGN KEY (situsCondNodeId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table StsTrtmntVtxPrdTyp
(
    situsTreatmentId bigint NOT NULL ,
    vertexProductId bigint NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT XPKStsTmtVtxPrdTyp
PRIMARY KEY (situsTreatmentId, vertexProductId);


ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f1Trmt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f2VxtPrd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TransTypePrspctv
(
    transTypePrspctvId bigint NOT NULL ,
    transactionTypeId bigint NULL ,
    partyRoleTypeId bigint NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    name varchar(60) NOT NULL 
);

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT XPKTransTypPrspctv
PRIMARY KEY (transTypePrspctvId);


ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f5PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f4TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

create table SitusTreatmentRule
(
    situsTrtmntRuleId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    situsTreatmentId bigint NOT NULL ,
    txbltyCatSrcId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL ,
    jurisdiction1Id bigint NULL ,
    jurisdiction2Id bigint NULL ,
    locationRoleTyp1Id bigint NULL ,
    locationRoleTyp2Id bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT XPKSitusTrtmntRule
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f6LRlTyp FOREIGN KEY (locationRoleTyp2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f5LRlTyp FOREIGN KEY (locationRoleTyp1Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f1SitTrt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

create table SitusTrtmntRulNote
(
    situsTrtmntRuleId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    noteText varchar(1000) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT XPKSitusTrtmntRlNt
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT f1SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

create table SitusTrtmntPrspctv
(
    situsTrtmntRuleId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    transTypePrspctvId bigint NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT XPKSitusTrtmntPrsp
PRIMARY KEY (situsTrtmntRuleId, sourceId, transTypePrspctvId);


ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f2SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f1TrTypP FOREIGN KEY (transTypePrspctvId)
    REFERENCES TransTypePrspctv (transTypePrspctvId)
;

create table CurrencyRndRule
(
    currencyRndRuleId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    currencyUnitId bigint NOT NULL ,
    roundingRuleId bigint NOT NULL ,
    taxTypeId bigint NULL ,
    jurisdictionId bigint NULL ,
    partyId bigint NULL ,
    configurableInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT XPKCurrRndRule
PRIMARY KEY (currencyRndRuleId, sourceId);


ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f22Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f1RndRul FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

create table AllowCurrRndRule
(
    allowCurrRndRuleId bigint NOT NULL ,
    currencyUnitId bigint NOT NULL ,
    roundingRuleId bigint NOT NULL ,
    taxTypeId bigint NULL ,
    jurisdictionId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT XPKAllowCurRndRule
PRIMARY KEY (allowCurrRndRuleId);


ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f2RndRule FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f10TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxRecoverablePct
(
    taxRecovPctId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    partySourceId bigint NOT NULL ,
    costCenter varchar(40) NULL ,
    recovPct numeric(10,6) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    accrualReliefInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT XPKTaxRecovPct
PRIMARY KEY (taxRecovPctId);


ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT f27Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table TaxAssistRule
(
    sourceId bigint NOT NULL ,
    ruleId bigint NOT NULL ,
    ruleCode varchar(60) NOT NULL ,
    ruleDesc varchar(1000) NULL ,
    vertexProductId bigint NOT NULL ,
    precedence bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    phaseId bigint DEFAULT 0 NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistRule
    ADD CONSTRAINT XPKTaxAssistRule
PRIMARY KEY (ruleId, sourceId);


ALTER TABLE TaxAssistRule
    ADD CONSTRAINT f2VtxProduct FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistCondition
(
    sourceId bigint NOT NULL ,
    ruleId bigint NOT NULL ,
    conditionId bigint NOT NULL ,
    conditionText varchar(2000) NULL ,
    conditionText2 varchar(2000) NULL 
);

ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT XPKTxAstCondition
PRIMARY KEY (ruleId, sourceId, conditionId);


ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT f1TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

create table TaxAssistConclude
(
    sourceId bigint NOT NULL ,
    ruleId bigint NOT NULL ,
    conclusionId bigint NOT NULL ,
    conclusionText varchar(2000) NULL ,
    conclusionText2 varchar(2000) NULL 
);

ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT XPKTxAstConclude
PRIMARY KEY (ruleId, sourceId, conclusionId);


ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT f2TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

create table TaxAssistLookup
(
    sourceId bigint NOT NULL ,
    tableId bigint NOT NULL ,
    tableName varchar(60) NOT NULL ,
    dataType bigint NOT NULL ,
    description varchar(1000) NULL ,
    vertexProductId bigint NOT NULL ,
    resultName varchar(60) NOT NULL ,
    param1Name varchar(60) NOT NULL ,
    param2Name varchar(60) NULL ,
    param3Name varchar(60) NULL ,
    param4Name varchar(60) NULL ,
    param5Name varchar(60) NULL ,
    param6Name varchar(60) NULL ,
    param7Name varchar(60) NULL ,
    param8Name varchar(60) NULL ,
    param9Name varchar(60) NULL ,
    param10Name varchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT XPKTxAstLookup
PRIMARY KEY (tableId, sourceId);


ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT f1TxAstLookup FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistLookupRcd
(
    sourceId bigint NOT NULL ,
    recordId bigint NOT NULL ,
    tableId bigint NOT NULL ,
    result varchar(200) NULL ,
    param1 varchar(200) NULL ,
    param2 varchar(200) NULL ,
    param3 varchar(200) NULL ,
    param4 varchar(200) NULL ,
    param5 varchar(200) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT XPKTxAstLookupRcd
PRIMARY KEY (recordId, sourceId);


ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT f1TxAsLk FOREIGN KEY (tableId, sourceId)
    REFERENCES TaxAssistLookup (tableId, sourceId)
;

create table NumericType
(
    numericTypeId bigint NOT NULL ,
    numericTypeName varchar(60) NOT NULL 
);

ALTER TABLE NumericType
    ADD CONSTRAINT XPKNumericType
PRIMARY KEY (numericTypeId);


create table TaxAssistAllocationTable
(
    sourceId bigint NOT NULL ,
    allocationTableId bigint NOT NULL ,
    vertexProductId bigint NOT NULL ,
    allocationTableName varchar(60) NOT NULL ,
    allocationTableDesc varchar(1000) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT XPKTxAstAlocTbl
PRIMARY KEY (allocationTableId, sourceId);


ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT f1TxAstAlloc FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistAllocationColumn
(
    sourceId bigint NOT NULL ,
    allocationTableId bigint NOT NULL ,
    columnId bigint NOT NULL ,
    columnName varchar(60) NOT NULL ,
    dataTypeId bigint NOT NULL ,
    columnSeqNum bigint NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT XPKTxAstAllocCol
PRIMARY KEY (allocationTableId, sourceId, columnId);


ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsAC FOREIGN KEY (allocationTableId, sourceId)
    REFERENCES TaxAssistAllocationTable (allocationTableId, sourceId)
;

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsACDT FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

create table TaxAssistAllocationColumnValue
(
    sourceId bigint NOT NULL ,
    allocationTableId bigint NOT NULL ,
    columnId bigint NOT NULL ,
    recordId bigint NOT NULL ,
    recordCode varchar(20) NOT NULL ,
    numericTypeId bigint NULL ,
    columnValue varchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT XPKTxAstAllocCoVal
PRIMARY KEY (allocationTableId, sourceId, columnId, recordId, recordCode);


ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACV FOREIGN KEY (allocationTableId, sourceId, columnId)
    REFERENCES TaxAssistAllocationColumn (allocationTableId, sourceId, columnId)
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACVNT FOREIGN KEY (numericTypeId)
    REFERENCES NumericType (numericTypeId)
;

create table TpsJurisdiction
(
    jurId bigint NOT NULL ,
    jurVersionId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    jurLayerId bigint NOT NULL ,
    jurTypeId bigint NOT NULL ,
    jurTypeName varchar(60) NOT NULL ,
    name varchar(60) NOT NULL ,
    standardName varchar(60) NULL ,
    description varchar(80) NULL ,
    updateId bigint NOT NULL 
);

ALTER TABLE TpsJurisdiction
    ADD CONSTRAINT XPKTpsJurisdiction
PRIMARY KEY (jurId, jurVersionId);


create table TaxAreaJurNames
(
    taxAreaId bigint NOT NULL ,
    countryName varchar(60) NOT NULL ,
    countryISOCode2 varchar(2) NULL ,
    countryISOCode3 varchar(3) NOT NULL ,
    mainDivJurTypeName varchar(60) NULL ,
    mainDivName varchar(60) NULL ,
    subDivJurTypeName varchar(60) NULL ,
    subDivName varchar(60) NULL ,
    cityJurTypeName varchar(60) NULL ,
    cityName varchar(60) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxAreaJurNames
    ADD CONSTRAINT XPKTaxAreaJurNames
PRIMARY KEY (taxAreaId);


create table JurHierarchy
(
    depthFromParent bigint NOT NULL ,
    topmostInd bigint NOT NULL ,
    chldJurisdictionId bigint NOT NULL ,
    prntJurisdictionId bigint NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE JurHierarchy
    ADD CONSTRAINT XPKJurHierarchy
PRIMARY KEY (prntJurisdictionId, chldJurisdictionId);


create table SimplificationType
(
    simpTypeId bigint NOT NULL ,
    simpTypeName varchar(60) NOT NULL 
);

ALTER TABLE SimplificationType
    ADD CONSTRAINT XPKSimpType
PRIMARY KEY (simpTypeId);


create table DMActivityType
(
    activityTypeId bigint NOT NULL ,
    activityTypeName varchar(60) NOT NULL 
);

ALTER TABLE DMActivityType
    ADD CONSTRAINT XPKDMActivityType
PRIMARY KEY (activityTypeId);


create table DMStatusType
(
    statusTypeId bigint NOT NULL ,
    statusTypeName varchar(60) NOT NULL 
);

ALTER TABLE DMStatusType
    ADD CONSTRAINT XPKDMStatusType
PRIMARY KEY (statusTypeId);


create table InputOutputType
(
    inputOutputTypeId bigint NOT NULL ,
    inputOutputTypeName varchar(60) NOT NULL 
);

ALTER TABLE InputOutputType
    ADD CONSTRAINT XPKInpOutType
PRIMARY KEY (inputOutputTypeId);


create table CertificateReasonType
(
    certificateReasonTypeId bigint NOT NULL ,
    certificateReasonTypeName varchar(60) NOT NULL 
);

ALTER TABLE CertificateReasonType
    ADD CONSTRAINT XPKCertRsnType
PRIMARY KEY (certificateReasonTypeId);


create table CertReasonTypeJurisdiction
(
    certReasonTypeJurId bigint NOT NULL ,
    certificateReasonTypeId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    reasonCategoryId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT XPKCertRsnTypeJur
PRIMARY KEY (certReasonTypeJurId);


ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

create table CertificateExemptionType
(
    certificateExemptionTypeId bigint NOT NULL ,
    certificateExemptionTypeName varchar(60) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateExemptionType
    ADD CONSTRAINT XPKCertExemType
PRIMARY KEY (certificateExemptionTypeId);


create table CertExemptionTypeJurImp
(
    certExemptionTypeJurImpId bigint NOT NULL ,
    certificateExemptionTypeId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    impsnTypeId bigint NOT NULL ,
    impsnTypeSrcId bigint NOT NULL ,
    allStatesInd numeric(1) NULL ,
    allCitiesInd numeric(1) NULL ,
    allCountiesInd numeric(1) NULL ,
    allOthersInd numeric(1) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT XPKCertExmTypeJur
PRIMARY KEY (certExemptionTypeJurImpId);


ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1ImpCrtExmTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1CrtExmTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

create table Form
(
    formId bigint NOT NULL ,
    sourceId bigint NOT NULL 
);

ALTER TABLE Form
    ADD CONSTRAINT XPKForm
PRIMARY KEY (formId, sourceId);


create table FormDetail
(
    formDetailId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    formId bigint NOT NULL ,
    formIdCode varchar(30) NOT NULL ,
    formName varchar(60) NULL ,
    formDesc varchar(255) NULL ,
    formImageFileName varchar(255) NULL ,
    replacedByFormId bigint NULL ,
    replacementDate numeric(8) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE FormDetail
    ADD CONSTRAINT XPKFormDtl
PRIMARY KEY (formDetailId, sourceId);


ALTER TABLE FormDetail
    ADD CONSTRAINT f1FormDtlForm FOREIGN KEY (replacedByFormId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE FormDetail
    ADD CONSTRAINT f2FormDtlForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormJurisdiction
(
    formJurisdictionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    formId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormJurisdiction
    ADD CONSTRAINT XPKFormJur
PRIMARY KEY (formJurisdictionId, sourceId);


ALTER TABLE FormJurisdiction
    ADD CONSTRAINT f1FormJurForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormFieldDef
(
    fieldDefId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    name varchar(60) NOT NULL ,
    formId bigint NOT NULL ,
    jurisdictionId bigint NULL ,
    formFieldTypeId bigint NOT NULL ,
    requiredInd bigint DEFAULT 0 NULL ,
    hiddenInd bigint DEFAULT 0 NULL ,
    attributeId bigint NULL ,
    fieldDefPageNum bigint NOT NULL ,
    groupName varchar(60) NULL ,
    parentFieldDefId bigint NULL ,
    parentFieldDefSelectInd bigint DEFAULT 1 NULL ,
    parentValue varchar(20) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormFieldDef
    ADD CONSTRAINT XPKFormFieldDef
PRIMARY KEY (fieldDefId, sourceId);


ALTER TABLE FormFieldDef
    ADD CONSTRAINT f1FormFieldDef FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormFieldTypeValue
(
    fieldDefId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    validValue varchar(20) NULL ,
    trueValue varchar(20) NULL ,
    falseValue varchar(20) NULL ,
    isDefault bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormFieldTypeValue
    ADD CONSTRAINT XPKFormFldTypeVle
PRIMARY KEY (fieldDefId, sourceId);


create table FormFieldAttribute
(
    attributeId bigint NOT NULL ,
    name varchar(60) NOT NULL 
);

ALTER TABLE FormFieldAttribute
    ADD CONSTRAINT XPKFormFieldAttr
PRIMARY KEY (attributeId);


create table Certificate
(
    certificateId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateCreationDate numeric(8) NOT NULL 
);

ALTER TABLE Certificate
    ADD CONSTRAINT XPKCertificate
PRIMARY KEY (certificateId, sourceId);


create table CertificateDetail
(
    certificateDetailId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    creationSourceId bigint DEFAULT 1 NOT NULL ,
    certificateId bigint NOT NULL ,
    certificateHolderPartyId bigint NOT NULL ,
    certClassTypeId bigint NOT NULL ,
    formId bigint NULL ,
    formSourceId bigint NULL ,
    taxResultTypeId bigint NULL ,
    certificateStatusId bigint NOT NULL ,
    partyContactId bigint NULL ,
    txbltyCatId bigint NULL ,
    txbltyCatSrcId bigint NULL ,
    certCopyRcvdInd numeric(1) NULL ,
    industryTypeName varchar(60) NULL ,
    hardCopyLocationName varchar(255) NULL ,
    certBlanketInd numeric(1) NULL ,
    singleUseInd numeric(1) NULL ,
    invoiceNumber varchar(50) NULL ,
    replacedByCertificateId bigint NULL ,
    vertexProductId bigint NOT NULL ,
    completedInd numeric(1) DEFAULT 1 NOT NULL ,
    approvalStatusId bigint DEFAULT 3 NOT NULL ,
    uuid varchar(36) NULL ,
    ecwCertificateId bigint NULL ,
    ecwSyncId bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE CertificateDetail
    ADD CONSTRAINT XPKCertDtl
PRIMARY KEY (certificateDetailId, sourceId);


ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1Certificate FOREIGN KEY (replacedByCertificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtParty FOREIGN KEY (certificateHolderPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtClsTyp FOREIGN KEY (certClassTypeId)
    REFERENCES CertClassType (certClassTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtTxRsltTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtPtyCntct FOREIGN KEY (partyContactId, sourceId)
    REFERENCES PartyContact (partyContactId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f5VtxProd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtForm FOREIGN KEY (formId, formSourceId)
    REFERENCES Form (formId, sourceId)
;

create table CertificateCoverage
(
    certificateCoverageId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    certificateIdCode varchar(30) NULL ,
    validationTypeId bigint NULL ,
    validationDate numeric(8) NULL ,
    certificateReasonTypeId bigint NULL ,
    certificateExemptionTypeId bigint NULL ,
    certIssueDate numeric(8) NULL ,
    certExpDate numeric(8) NULL ,
    certReviewDate numeric(8) NULL ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1 NULL ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT XPKCertCvrg
PRIMARY KEY (certificateCoverageId, sourceId);


ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

create table CertificateJurisdiction
(
    certificateJurisdictionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateCoverageId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    certificateExemptionTypeId bigint NULL ,
    coverageActionTypeId bigint NOT NULL 
);

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT XPKCertJur
PRIMARY KEY (certificateJurisdictionId, sourceId);


ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f2CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table CertificateImposition
(
    certificateImpositionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateCoverageId bigint NOT NULL ,
    jurTypeSetId bigint NOT NULL ,
    impsnTypeId bigint NOT NULL ,
    impsnTypeSrcId bigint NOT NULL ,
    certificateExemptionTypeId bigint NULL ,
    impActiveInd numeric(1) NOT NULL 
);

ALTER TABLE CertificateImposition
    ADD CONSTRAINT XPKCertImp
PRIMARY KEY (certificateImpositionId, sourceId);


ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f3CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

create table CertImpositionJurisdiction
(
    certImpJurisdictionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateImpositionId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    coverageActionTypeId bigint NOT NULL 
);

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT XPKCertImpJur
PRIMARY KEY (certImpJurisdictionId, sourceId);


ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurImp FOREIGN KEY (certificateImpositionId, sourceId)
    REFERENCES CertificateImposition (certificateImpositionId, sourceId)
;

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table CertificateNote
(
    certNoteText varchar(1000) NULL ,
    certificateId bigint NOT NULL ,
    sourceId bigint NOT NULL 
);

ALTER TABLE CertificateNote
    ADD CONSTRAINT XPKCertNote
PRIMARY KEY (certificateId, sourceId);


ALTER TABLE CertificateNote
    ADD CONSTRAINT f2Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

create table CertificateImage
(
    certificateImageId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    creationSourceId bigint DEFAULT 1 NOT NULL ,
    certificateId bigint NOT NULL ,
    imageLocationName varchar(255) NOT NULL 
);

ALTER TABLE CertificateImage
    ADD CONSTRAINT XPKCertImage
PRIMARY KEY (certificateImageId, sourceId);


ALTER TABLE CertificateImage
    ADD CONSTRAINT f5Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

create table CertTxbltyDriver
(
    certTxbltyDriverId bigint NOT NULL ,
    certificateId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    txbltyDvrId bigint NOT NULL ,
    txbltyDvrSrcId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT XPKCertTxbltyDvr
PRIMARY KEY (certTxbltyDriverId);


ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table CertificateTransType
(
    certTransTypeId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateId bigint NOT NULL ,
    transactionTypeId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateTransType
    ADD CONSTRAINT XPKCertTransType
PRIMARY KEY (certTransTypeId, sourceId);


ALTER TABLE CertificateTransType
    ADD CONSTRAINT f1CertTransTypCrt FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateTransType
    ADD CONSTRAINT f2CertTransTypTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

create table CertificateFormField
(
    certificateFormFieldId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateId bigint NOT NULL ,
    formId bigint NOT NULL ,
    formSourceId bigint NOT NULL ,
    fieldDefId bigint NOT NULL ,
    value varchar(500) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateFormField
    ADD CONSTRAINT XPKCertFormField
PRIMARY KEY (certificateFormFieldId, sourceId);


create table ExpirationRuleType
(
    expirationRuleTypeId bigint NOT NULL ,
    expirationRuleTypeName varchar(60) NOT NULL 
);

ALTER TABLE ExpirationRuleType
    ADD CONSTRAINT XPKExpRuleType
PRIMARY KEY (expirationRuleTypeId);


create table CertificateNumReqType
(
    certificateNumReqTypeId bigint NOT NULL ,
    certificateNumReqTypeName varchar(60) NOT NULL 
);

ALTER TABLE CertificateNumReqType
    ADD CONSTRAINT XPKCertNumReqTyp
PRIMARY KEY (certificateNumReqTypeId);


create table CertificateNumberFormat
(
    certificateNumberFormatId bigint NOT NULL ,
    certificateNumberMask varchar(30) NOT NULL ,
    certificateNumberExample varchar(30) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateNumberFormat
    ADD CONSTRAINT XPKCertNumFmt
PRIMARY KEY (certificateNumberFormatId);


create table CertificateValidationRule
(
    certificateValidationRuleId bigint NOT NULL ,
    certificateNumValidationDesc varchar(100) NULL ,
    jurisdictionId bigint NOT NULL ,
    certificateReasonTypeId bigint NOT NULL ,
    certificateNumReqTypeId bigint NOT NULL ,
    expirationRuleTypeId bigint NOT NULL ,
    numberOfYears bigint NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT XPKCertValRule
PRIMARY KEY (certificateValidationRuleId);


ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleExpTyp FOREIGN KEY (expirationRuleTypeId)
    REFERENCES ExpirationRuleType (expirationRuleTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleReqTyp FOREIGN KEY (certificateNumReqTypeId)
    REFERENCES CertificateNumReqType (certificateNumReqTypeId)
;

create table CertificateNumFormatValidation
(
    certNumFormatValidationId bigint NOT NULL ,
    certificateValidationRuleId bigint NOT NULL ,
    certificateNumberFormatId bigint NOT NULL 
);

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT XPKCertNumFmtVal
PRIMARY KEY (certNumFormatValidationId);


ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValRule FOREIGN KEY (certificateValidationRuleId)
    REFERENCES CertificateValidationRule (certificateValidationRuleId)
;

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValNFmt FOREIGN KEY (certificateNumberFormatId)
    REFERENCES CertificateNumberFormat (certificateNumberFormatId)
;

create table TaxAuthority
(
    taxAuthorityId bigint NOT NULL ,
    taxAuthorityName varchar(60) NOT NULL ,
    phoneNumber varchar(20) NULL ,
    webSiteAddress varchar(100) NULL 
);

ALTER TABLE TaxAuthority
    ADD CONSTRAINT XPKTaxAuth
PRIMARY KEY (taxAuthorityId);


create table TaxAuthorityJurisdiction
(
    taxAuthorityJurisdictionId bigint NOT NULL ,
    taxAuthorityId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL 
);

ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT XPKTaxAuthJur
PRIMARY KEY (taxAuthorityJurisdictionId);


ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT f1TaxAuthJur FOREIGN KEY (taxAuthorityId)
    REFERENCES TaxAuthority (taxAuthorityId)
;

create table TaxRegistration
(
    taxRegistrationId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    taxRegistrationIdCode varchar(40) NULL ,
    taxRegistrationTypeId bigint NULL ,
    validationTypeId bigint NULL ,
    validationDate numeric(8) NULL ,
    formatValidationTypeId bigint NULL ,
    formatValidationDate numeric(8) NULL ,
    physicalPresInd numeric(1) NOT NULL ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1 NULL ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCurrencyUnitId bigint NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxRegistration
    ADD CONSTRAINT XPKTaxReg
PRIMARY KEY (taxRegistrationId, sourceId);


ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgTxpyr FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

create table TaxRegistrationJurisdiction
(
    taxRegJurisdictionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxRegistrationId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    coverageActionTypeId bigint NOT NULL 
);

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT XPKTxRgJur
PRIMARY KEY (taxRegJurisdictionId, sourceId);


ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table TaxRegistrationImposition
(
    taxRegImpositionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxRegistrationId bigint NOT NULL ,
    taxRegistrationIdCode varchar(40) NULL ,
    taxRegistrationTypeId bigint NULL ,
    jurTypeSetId bigint NOT NULL ,
    impsnTypeId bigint NOT NULL ,
    impsnTypeSrcId bigint NOT NULL ,
    impActiveInd numeric(1) NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL ,
    formatValidationTypeId bigint NULL ,
    formatValidationDate numeric(8) NULL 
);

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT XPKTxRgImp
PRIMARY KEY (taxRegImpositionId, sourceId);


ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

create table TaxRegImpJurisdiction
(
    taxRegImpJurisdictionId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    taxRegImpositionId bigint NOT NULL ,
    taxRegistrationIdCode varchar(40) NULL ,
    taxRegistrationTypeId bigint NULL ,
    jurisdictionId bigint NOT NULL ,
    coverageActionTypeId bigint NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT XPKTxRgImpJur
PRIMARY KEY (taxRegImpJurisdictionId, sourceId);


ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurImp FOREIGN KEY (taxRegImpositionId, sourceId)
    REFERENCES TaxRegistrationImposition (taxRegImpositionId, sourceId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

create table LetterTemplateType
(
    letterTemplateTypeId bigint NOT NULL ,
    letterTemplateTypeName varchar(60) NOT NULL 
);

ALTER TABLE LetterTemplateType
    ADD CONSTRAINT XPKLtrTempType
PRIMARY KEY (letterTemplateTypeId);


create table LetterDeliveryMethod
(
    letterDeliveryMethodId bigint NOT NULL ,
    letterDeliveryMethodName varchar(60) NOT NULL 
);

ALTER TABLE LetterDeliveryMethod
    ADD CONSTRAINT XPKLtrDlvryMthd
PRIMARY KEY (letterDeliveryMethodId);


create table LetterTemplate
(
    letterTemplateId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    letterTemplateTypeId bigint NOT NULL ,
    letterTemplateName varchar(60) NOT NULL ,
    letterTemplateDesc varchar(100) NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE LetterTemplate
    ADD CONSTRAINT XPKLtrTemp
PRIMARY KEY (letterTemplateId, sourceId);


ALTER TABLE LetterTemplate
    ADD CONSTRAINT f1LtrTmpType FOREIGN KEY (letterTemplateTypeId)
    REFERENCES LetterTemplateType (letterTemplateTypeId)
;

create table LetterTemplateText
(
    letterTemplateId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    templateTextSeqNum bigint NOT NULL ,
    templateText varchar(2000) NULL 
);

ALTER TABLE LetterTemplateText
    ADD CONSTRAINT XPKLtrTempText
PRIMARY KEY (letterTemplateId, sourceId, templateTextSeqNum);


ALTER TABLE LetterTemplateText
    ADD CONSTRAINT f1LtrTmp FOREIGN KEY (letterTemplateId, sourceId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

create table LetterBatch
(
    letterBatchId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    letterTemplateId bigint NOT NULL ,
    letterTemplateSrcId bigint NOT NULL ,
    certificateStatusId bigint NULL ,
    daysToExpirationNum bigint NULL ,
    daysSinceExpirationNum bigint NULL ,
    expirationRangeStartDate numeric(8) NULL ,
    expirationRangeEndDate numeric(8) NULL ,
    noCertificateInd numeric(1) NULL ,
    incompleteInd numeric(1) NULL ,
    rejectedInd numeric(1) NULL ,
    formReplacedInd numeric(1) NULL ,
    emailSubject varchar(100) NULL ,
    letterBatchDesc varchar(100) NULL ,
    letterBatchDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE LetterBatch
    ADD CONSTRAINT XPKLtrBatch
PRIMARY KEY (letterBatchId, sourceId);


ALTER TABLE LetterBatch
    ADD CONSTRAINT f1LtrBtchCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE LetterBatch
    ADD CONSTRAINT f2LtrBtchTmp FOREIGN KEY (letterTemplateId, letterTemplateSrcId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

create table LetterBatchParty
(
    letterBatchId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    partyId bigint NOT NULL ,
    partyTypeId bigint NOT NULL 
);

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT XPKLtrBatchPty
PRIMARY KEY (letterBatchId, sourceId, partyId);


ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f1LtrBtchPty FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f2LtrBtchPty FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f3LtrBtchPty FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

create table LetterBatchJurisdiction
(
    letterBatchId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL 
);

ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT XPKLtrBatchJur
PRIMARY KEY (letterBatchId, sourceId, jurisdictionId);


ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT f1LtrBtchJur FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

create table FlexFieldDefVtxPrdTyp
(
    flexFieldDefId bigint NOT NULL ,
    flexFieldDefSrcId bigint NOT NULL ,
    vertexProductId bigint NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT XPKFFDefVtxPrdTyp
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId, vertexProductId);


ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f1FFDrfVtxPrdTyp FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f2FFDrfVtxPrdTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table Letter
(
    letterId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    certificateId bigint NULL ,
    overridePartyId bigint NULL ,
    letterBatchId bigint NOT NULL ,
    letterDeliveryMethodId bigint NOT NULL ,
    formId bigint NULL ,
    formSrcId bigint NULL ,
    letterSentDate numeric(8) NOT NULL 
);

ALTER TABLE Letter
    ADD CONSTRAINT XPKLetter
PRIMARY KEY (letterId, sourceId);


ALTER TABLE Letter
    ADD CONSTRAINT f1LtrDlvryMethod FOREIGN KEY (letterDeliveryMethodId)
    REFERENCES LetterDeliveryMethod (letterDeliveryMethodId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f2LtrLtrBatch FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f3LtrCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f4LtrForm FOREIGN KEY (formId, formSrcId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f5LtrParty FOREIGN KEY (overridePartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table GeographicRegionType
(
    geoRegionTypeId bigint NOT NULL ,
    geoRegionLicenseCode varchar(30) NOT NULL ,
    geoRegionDisplayName varchar(30) NOT NULL 
);

ALTER TABLE GeographicRegionType
    ADD CONSTRAINT XPKGeoRegionCode
PRIMARY KEY (geoRegionTypeId);


create table JurGeographicRegType
(
    jurGeoRegTypeId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    geoRegionTypeId bigint NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT XPKJurGeoRegType
PRIMARY KEY (jurGeoRegTypeId);


ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT f1GeoRegionType FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

create table RoleParty
(
    roleId bigint NOT NULL ,
    sourceId bigint NOT NULL ,
    partyId bigint NOT NULL 
);

ALTER TABLE RoleParty
    ADD CONSTRAINT XPKRoleParty
PRIMARY KEY (roleId, sourceId, partyId);


create table VATRegistrationIdFormat
(
    vatRegistrationIdFormatId bigint NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    vatRegistrationIdMask varchar(40) NOT NULL ,
    vatRegistrationIdExample varchar(40) NULL ,
    vatRegistrationIdDescription varchar(255) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE VATRegistrationIdFormat
    ADD CONSTRAINT XPKVATRegIdFmt
PRIMARY KEY (vatRegistrationIdFormatId);


create table CertWizardLocationUser
(
    certWizardUserId bigint NOT NULL ,
    name varchar(50) NOT NULL ,
    userId bigint NOT NULL ,
    sourceId bigint NULL ,
    partyId bigint NULL ,
    displayName varchar(100) NULL 
);

ALTER TABLE CertWizardLocationUser
    ADD CONSTRAINT CertWizardLocationUser_PK
PRIMARY KEY (certWizardUserId);


create table users
(
    username varchar(64) NOT NULL ,
    password varchar(255) NOT NULL ,
    enabled numeric(1) NULL ,
    sourceId bigint NULL 
);

ALTER TABLE users
    ADD CONSTRAINT users_PK
PRIMARY KEY (username);


create table authorities
(
    username varchar(64) NOT NULL ,
    authority varchar(50) NOT NULL ,
    sourceId bigint NULL 
);

ALTER TABLE authorities
    ADD CONSTRAINT f1Auth FOREIGN KEY (username)
    REFERENCES users (username)
;

create table CertWizardUser
(
    loginId bigint NOT NULL ,
    certWizardUserId bigint NOT NULL ,
    userName varchar(64) NULL ,
    companyEmail varchar(100) NOT NULL ,
    personalEmail varchar(100) NULL ,
    oseriesCustomerCode varchar(80) NULL ,
    firstName varchar(60) NOT NULL ,
    lastName varchar(60) NOT NULL ,
    countryJurId bigint NOT NULL ,
    mainDivisionJurId bigint NOT NULL ,
    cityName varchar(60) NULL ,
    street1 varchar(100) NULL ,
    street2 varchar(100) NULL ,
    postalCode varchar(20) NULL ,
    phoneNumber varchar(20) NULL ,
    securityQuestion1Id bigint NOT NULL ,
    securityQuestion2Id bigint NOT NULL ,
    securityQuestion3Id bigint NOT NULL ,
    securityAnswer1 varchar(100) NOT NULL ,
    securityAnswer2 varchar(100) NOT NULL ,
    securityAnswer3 varchar(100) NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    newCustomerInd bigint NULL 
);

ALTER TABLE CertWizardUser
    ADD CONSTRAINT CertWizardUser_PK
PRIMARY KEY (loginId);


ALTER TABLE CertWizardUser
    ADD CONSTRAINT f2Users FOREIGN KEY (userName)
    REFERENCES users (username)
;

create table SecurityQuestions
(
    securityQuestionId bigint NOT NULL ,
    name varchar(255) NOT NULL ,
    question varchar(255) NOT NULL 
);

ALTER TABLE SecurityQuestions
    ADD CONSTRAINT SecurityQuestions_PK
PRIMARY KEY (securityQuestionId);


create table DataUpdateImpactTaxRule
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxRuleId bigint NOT NULL ,
    geoRegionTypeId bigint NOT NULL ,
    countryName varchar(60) NOT NULL ,
    mainDivisionName varchar(60) NULL ,
    jurName varchar(60) NOT NULL ,
    jurTypeName varchar(60) NOT NULL ,
    impositionName varchar(60) NOT NULL ,
    categoryName varchar(60) NOT NULL ,
    taxRuleType varchar(60) NOT NULL ,
    rate varchar(60) NULL ,
    salesTaxHolidayInd numeric(1) DEFAULT 0 NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType varchar(60) NOT NULL ,
    details varchar(1000) NULL ,
    newCategoryInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT XPKDataImpTaxRule
PRIMARY KEY (dataUpdateNumber, taxRuleId, geoRegionTypeId);


ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT f1GeoRegionType1 FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

create table DataUpdateImpactTaxArea
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxAreaId bigint NOT NULL ,
    countryName varchar(60) NOT NULL ,
    mainDivisionName varchar(60) NULL ,
    subDivisionName varchar(60) NULL ,
    cityName varchar(60) NULL ,
    districtName1 varchar(60) NULL ,
    districtName2 varchar(60) NULL ,
    districtName3 varchar(60) NULL ,
    districtName4 varchar(60) NULL ,
    districtName5 varchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType varchar(60) NOT NULL ,
    affectedTaxAreaId bigint NOT NULL ,
    affectedCountryName varchar(60) NULL ,
    affectedMainDivisionName varchar(60) NULL ,
    affectedSubDivisionName varchar(60) NULL ,
    affectedCityName varchar(60) NULL ,
    affectedDistrictName1 varchar(60) NULL ,
    affectedDistrictName2 varchar(60) NULL ,
    affectedDistrictName3 varchar(60) NULL ,
    affectedDistrictName4 varchar(60) NULL ,
    affectedDistrictName5 varchar(60) NULL 
);

ALTER TABLE DataUpdateImpactTaxArea
    ADD CONSTRAINT XPKDataImpTaxArea
PRIMARY KEY (dataUpdateNumber, taxAreaId, affectedTaxAreaId);


create table TransactionStatusType
(
    transStatusTypeId bigint NOT NULL ,
    transStatusTypeName varchar(60) NOT NULL 
);

ALTER TABLE TransactionStatusType
    ADD CONSTRAINT XPKTransStatusType
PRIMARY KEY (transStatusTypeId);


create table FeatureResource
(
    featureResourceId bigint NOT NULL ,
    featureResourceName varchar(60) NOT NULL 
);

ALTER TABLE FeatureResource
    ADD CONSTRAINT XPKFeatureResource
PRIMARY KEY (featureResourceId);


create table FeatureResourceImpositionType
(
    featureResourceId bigint NOT NULL ,
    impsnTypeId bigint NOT NULL 
);

ALTER TABLE FeatureResourceImpositionType
    ADD CONSTRAINT XPKFeatResImpType
PRIMARY KEY (featureResourceId, impsnTypeId);


create table FeatureResourceCategory
(
    featureResourceId bigint NOT NULL ,
    txbltyCatId bigint NOT NULL 
);

ALTER TABLE FeatureResourceCategory
    ADD CONSTRAINT XPKFeatResCat
PRIMARY KEY (featureResourceId, txbltyCatId);


create table TransactionEventType
(
    transEventTypeId bigint NOT NULL ,
    transEventTypeName varchar(60) NOT NULL 
);

ALTER TABLE TransactionEventType
    ADD CONSTRAINT XPKTransEventType
PRIMARY KEY (transEventTypeId);


create table TpsDataReleaseEvent
(
    fullRlsId bigint NULL ,
    interimRlsId bigint NULL ,
    rlsTypeId bigint NULL ,
    appliedDate numeric(8) NOT NULL ,
    rlsName varchar(255) NULL 
);

create table TaxAssistPhaseType
(
    phaseId bigint NOT NULL ,
    phaseName varchar(60) NOT NULL 
);

ALTER TABLE TaxAssistPhaseType
    ADD CONSTRAINT XPKTaxAstPhaseType
PRIMARY KEY (phaseId);


create table VATRegimeType
(
    vatRegimeTypeId bigint NOT NULL ,
    vatRegimeTypeName varchar(60) NOT NULL 
);

ALTER TABLE VATRegimeType
    ADD CONSTRAINT XPKVATRegimeType
PRIMARY KEY (vatRegimeTypeId);


create table VATGroup
(
    vatGroupId bigint NOT NULL ,
    vatGroupSourceId bigint NOT NULL ,
    vatGrpCreationDate numeric(8) NOT NULL 
);

ALTER TABLE VATGroup
    ADD CONSTRAINT XPKVATGroup
PRIMARY KEY (vatGroupId, vatGroupSourceId);


create table VATGroupDetail
(
    vatGroupDtlId bigint NOT NULL ,
    vatGroupSourceId bigint NOT NULL ,
    vatGroupId bigint NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    repMemTxprId bigint NOT NULL ,
    vatGroupIdentifier varchar(15) NOT NULL ,
    vatGroupName varchar(30) NOT NULL ,
    jurisdictionId bigint NOT NULL ,
    vatRegimeTypeId bigint NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT XPKVATGroupDtl
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId);


ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VatGroup FOREIGN KEY (vatGroupId, vatGroupSourceId)
    REFERENCES VATGroup (vatGroupId, vatGroupSourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f31Party FOREIGN KEY (repMemTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VATRegimeType FOREIGN KEY (vatRegimeTypeId)
    REFERENCES VATRegimeType (vatRegimeTypeId)
;

create table VATGroupMembers
(
    vatGroupDtlId bigint NOT NULL ,
    vatGroupSourceId bigint NOT NULL ,
    memTxprId bigint NOT NULL 
);

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT XPKVATGroupMembers
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId, memTxprId);


ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f1VATGroupDtl FOREIGN KEY (vatGroupDtlId, vatGroupSourceId)
    REFERENCES VATGroupDetail (vatGroupDtlId, vatGroupSourceId)
;

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

