create table CoverageActionType
(
    coverageActionTypeId numeric(18) NOT NULL ,
    coverageActionTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKJICvrgActTyp
 ON CoverageActionType (coverageActionTypeId);

ALTER TABLE CoverageActionType
    ADD CONSTRAINT XPKJICvrgActTyp
PRIMARY KEY (coverageActionTypeId);


create table CertificateStatus
(
    certificateStatusId numeric(18) NOT NULL ,
    certificateStatusName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKJICertStat
 ON CertificateStatus (certificateStatusId);

ALTER TABLE CertificateStatus
    ADD CONSTRAINT XPKJICertStat
PRIMARY KEY (certificateStatusId);


create table CertificateApprovalStatus
(
    approvalStatusId numeric(18) NOT NULL ,
    certApprovalStatusName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertApprStatus
 ON CertificateApprovalStatus (approvalStatusId);

ALTER TABLE CertificateApprovalStatus
    ADD CONSTRAINT XPKCertApprStatus
PRIMARY KEY (approvalStatusId);


create table ContactRoleType
(
    contactRoleTypeId numeric(18) NOT NULL ,
    contactRoleTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKContactRoleType
 ON ContactRoleType (contactRoleTypeId);

ALTER TABLE ContactRoleType
    ADD CONSTRAINT XPKContactRoleType
PRIMARY KEY (contactRoleTypeId);


create table FormFieldType
(
    formFieldTypeId numeric(18) NOT NULL ,
    name varchar(120)  
);

CREATE UNIQUE INDEX XPKFormField
 ON FormFieldType (formFieldTypeId);

ALTER TABLE FormFieldType
    ADD CONSTRAINT XPKFormField
PRIMARY KEY (formFieldTypeId);


create table TaxRegistrationType
(
    taxRegistrationTypeId numeric(18) NOT NULL ,
    taxRegistrationTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxRegType
 ON TaxRegistrationType (taxRegistrationTypeId);

ALTER TABLE TaxRegistrationType
    ADD CONSTRAINT XPKTaxRegType
PRIMARY KEY (taxRegistrationTypeId);


create table ApportionmentType
(
    apportionTypeId numeric(18) NOT NULL ,
    apportionTypeName varchar(60) NOT NULL 
);

CREATE UNIQUE INDEX XPKApportionType
 ON ApportionmentType (apportionTypeId);

ALTER TABLE ApportionmentType
    ADD CONSTRAINT XPKApportionType
PRIMARY KEY (apportionTypeId);


create table SitusConditionType
(
    situsCondTypeId numeric(18) NOT NULL ,
    situsCondTypeName varchar(60)  
);

CREATE UNIQUE INDEX XPKSitusCondType
 ON SitusConditionType (situsCondTypeId);

ALTER TABLE SitusConditionType
    ADD CONSTRAINT XPKSitusCondType
PRIMARY KEY (situsCondTypeId);


create table ChainTransType
(
    chainTransId numeric(18) NOT NULL ,
    chainTransName varchar(120)  
);

CREATE UNIQUE INDEX XPKChainTransType
 ON ChainTransType (chainTransId);

ALTER TABLE ChainTransType
    ADD CONSTRAINT XPKChainTransType
PRIMARY KEY (chainTransId);


create table TitleTransferType
(
    titleTransferId numeric(18) NOT NULL ,
    titleTransferName varchar(120)  
);

CREATE UNIQUE INDEX XPKTitleTransType
 ON TitleTransferType (titleTransferId);

ALTER TABLE TitleTransferType
    ADD CONSTRAINT XPKTitleTransType
PRIMARY KEY (titleTransferId);


create table AssistedState
(
    assistedStateId numeric(18) NOT NULL ,
    assistedStateName varchar(120)  
);

CREATE UNIQUE INDEX XPKAssistedState
 ON AssistedState (assistedStateId);

ALTER TABLE AssistedState
    ADD CONSTRAINT XPKAssistedState
PRIMARY KEY (assistedStateId);


create table RateClassification
(
    rateClassId numeric(18) NOT NULL ,
    rateClassName varchar(120)  
);

CREATE UNIQUE INDEX XPKRateClass
 ON RateClassification (rateClassId);

ALTER TABLE RateClassification
    ADD CONSTRAINT XPKRateClass
PRIMARY KEY (rateClassId);


create table DMFilter
(
    filterId numeric(18) NOT NULL ,
    filterName varchar(120) NOT NULL ,
    sourceId numeric(18)  ,
    activityTypeId numeric(18) NOT NULL ,
    filterDescription varchar(510)  ,
    followupInd numeric(1)  
);

CREATE UNIQUE INDEX XPKDMFilter
 ON DMFilter (filterId);

ALTER TABLE DMFilter
    ADD CONSTRAINT XPKDMFilter
PRIMARY KEY (filterId);


create table BusinessTransType
(
    busTransTypeId numeric(18) NOT NULL ,
    busTransTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKBusTransType
 ON BusinessTransType (busTransTypeId);

ALTER TABLE BusinessTransType
    ADD CONSTRAINT XPKBusTransType
PRIMARY KEY (busTransTypeId);


create table TransactionType
(
    transactionTypeId numeric(18) NOT NULL ,
    transactionTypName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTransactionType
 ON TransactionType (transactionTypeId);

ALTER TABLE TransactionType
    ADD CONSTRAINT XPKTransactionType
PRIMARY KEY (transactionTypeId);


create table DataType
(
    dataTypeId numeric(18) NOT NULL ,
    dataTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKDataType
 ON DataType (dataTypeId);

ALTER TABLE DataType
    ADD CONSTRAINT XPKDataType
PRIMARY KEY (dataTypeId);


create table InputParameterType
(
    inputParamTypeId numeric(18) NOT NULL ,
    inputParamTypeName varchar(120)  ,
    lookupStrategyId numeric(18)  ,
    commodityCodeInd numeric(1)  ,
    commodityCodeLength numeric(18)  ,
    isTelecommLineType numeric(1)  
);

CREATE UNIQUE INDEX XPKInputParamType
 ON InputParameterType (inputParamTypeId);

ALTER TABLE InputParameterType
    ADD CONSTRAINT XPKInputParamType
PRIMARY KEY (inputParamTypeId);


create table JurTypeSet
(
    jurTypeSetId numeric(18) NOT NULL ,
    jurTypeSetName varchar(120)  
);

CREATE UNIQUE INDEX XPKJurTypeSet
 ON JurTypeSet (jurTypeSetId);

ALTER TABLE JurTypeSet
    ADD CONSTRAINT XPKJurTypeSet
PRIMARY KEY (jurTypeSetId);


create table JurTypeSetMember
(
    jurTypeSetId numeric(18) NOT NULL ,
    jurisdictionTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKJurTypeSetMem
 ON JurTypeSetMember (jurTypeSetId, jurisdictionTypeId);

ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT XPKJurTypeSetMem
PRIMARY KEY (jurTypeSetId, jurisdictionTypeId);


create table TaxResultType
(
    taxResultTypeId numeric(18) NOT NULL ,
    taxResultTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxResultType
 ON TaxResultType (taxResultTypeId);

ALTER TABLE TaxResultType
    ADD CONSTRAINT XPKTaxResultType
PRIMARY KEY (taxResultTypeId);


create table RecoverableResultType
(
    recoverableResultTypeId numeric(18) NOT NULL ,
    recoverableResultTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKRcvResultType
 ON RecoverableResultType (recoverableResultTypeId);

ALTER TABLE RecoverableResultType
    ADD CONSTRAINT XPKRcvResultType
PRIMARY KEY (recoverableResultTypeId);


create table PartyType
(
    partyTypeId numeric(18) NOT NULL ,
    partyTypeName varchar(40) NOT NULL 
);

CREATE UNIQUE INDEX XPKPartyType
 ON PartyType (partyTypeId);

ALTER TABLE PartyType
    ADD CONSTRAINT XPKPartyType
PRIMARY KEY (partyTypeId);


create table LocationRoleType
(
    locationRoleTypeId numeric(18) NOT NULL ,
    locationRoleTypNam varchar(60) NOT NULL 
);

CREATE UNIQUE INDEX XPKLocationRoleTyp
 ON LocationRoleType (locationRoleTypeId);

ALTER TABLE LocationRoleType
    ADD CONSTRAINT XPKLocationRoleTyp
PRIMARY KEY (locationRoleTypeId);


create table ShippingTerms
(
    shippingTermsId numeric(18) NOT NULL ,
    shippingTermsName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKShippingTerms
 ON ShippingTerms (shippingTermsId);

ALTER TABLE ShippingTerms
    ADD CONSTRAINT XPKShippingTerms
PRIMARY KEY (shippingTermsId);


create table BasisType
(
    basisTypeId numeric(18) NOT NULL ,
    basisTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKBasisType
 ON BasisType (basisTypeId);

ALTER TABLE BasisType
    ADD CONSTRAINT XPKBasisType
PRIMARY KEY (basisTypeId);


create table DMFilterDate
(
    dateTypeId numeric(18) NOT NULL ,
    criteriaDate numeric(8)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMFilterDate
 ON DMFilterDate (filterId, dateTypeId, criteriaOrderNum);

ALTER TABLE DMFilterDate
    ADD CONSTRAINT XPKDMFilterDate
PRIMARY KEY (filterId, dateTypeId, criteriaOrderNum);


create table OutputNotice
(
    outputNoticeId numeric(18) NOT NULL ,
    outputNoticeName varchar(60) NOT NULL 
);

CREATE UNIQUE INDEX XPKOutputNotice
 ON OutputNotice (outputNoticeId);

ALTER TABLE OutputNotice
    ADD CONSTRAINT XPKOutputNotice
PRIMARY KEY (outputNoticeId);


create table CustomsStatusType
(
    customsStatusId numeric(18) NOT NULL ,
    customsStatusName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKCustomsStatType
 ON CustomsStatusType (customsStatusId);

ALTER TABLE CustomsStatusType
    ADD CONSTRAINT XPKCustomsStatType
PRIMARY KEY (customsStatusId);


create table CreationSource
(
    creationSourceId numeric(18) NOT NULL ,
    name varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKCreationSource
 ON CreationSource (creationSourceId);

ALTER TABLE CreationSource
    ADD CONSTRAINT XPKCreationSource
PRIMARY KEY (creationSourceId);


create table MovementMethodType
(
    movementMethodId numeric(18) NOT NULL ,
    movementMethodName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKMvmntMethodType
 ON MovementMethodType (movementMethodId);

ALTER TABLE MovementMethodType
    ADD CONSTRAINT XPKMvmntMethodType
PRIMARY KEY (movementMethodId);


create table SitusCondition
(
    situsConditionId numeric(18) NOT NULL ,
    locRoleTypeId numeric(18)  ,
    locRoleType2Id numeric(18)  ,
    situsCondTypeId numeric(18)  ,
    transactionTypeId numeric(18)  ,
    transPrspctvTypId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    jurisdictionTypeId numeric(18)  ,
    endDate numeric(8) NOT NULL ,
    jurTypeSetId numeric(18)  ,
    partyRoleTypeId numeric(18)  ,
    currencyUnitId numeric(18)  ,
    locRoleTypeId1Name varchar(120)  ,
    locRoleTypeId2Name varchar(120)  ,
    stsSubRtnNodeId numeric(18)  ,
    boolFactName varchar(120)  ,
    boolFactValue numeric(1)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  ,
    customsStatusId numeric(18)  ,
    movementMethodId numeric(18)  ,
    titleTransferId numeric(18)  
);

CREATE UNIQUE INDEX XPKSitusCondition
 ON SitusCondition (situsConditionId);

ALTER TABLE SitusCondition
    ADD CONSTRAINT XPKSitusCondition
PRIMARY KEY (situsConditionId);


create table BracketTaxCalcType
(
    brcktTaxCalcTypeId numeric(18) NOT NULL ,
    brcktTaxCalcTypNam varchar(120)  
);

CREATE UNIQUE INDEX XPKBrcktTaxCalcTyp
 ON BracketTaxCalcType (brcktTaxCalcTypeId);

ALTER TABLE BracketTaxCalcType
    ADD CONSTRAINT XPKBrcktTaxCalcTyp
PRIMARY KEY (brcktTaxCalcTypeId);


create table SitusConcType
(
    situsConcTypeId numeric(18) NOT NULL ,
    situsConcTypeName varchar(120)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKSitusConcType
 ON SitusConcType (situsConcTypeId);

ALTER TABLE SitusConcType
    ADD CONSTRAINT XPKSitusConcType
PRIMARY KEY (situsConcTypeId);


create table ReasonCategory
(
    reasonCategoryId numeric(18) NOT NULL ,
    reasonCategoryName varchar(120)  ,
    isUserDefined numeric(1)  
);

CREATE UNIQUE INDEX XPKReasonCategory
 ON ReasonCategory (reasonCategoryId);

ALTER TABLE ReasonCategory
    ADD CONSTRAINT XPKReasonCategory
PRIMARY KEY (reasonCategoryId);


create table ReasonCategoryJur
(
    reasonCategoryId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8)  
);

CREATE UNIQUE INDEX XPKReasonCatJur
 ON ReasonCategoryJur (reasonCategoryId, jurisdictionId, effDate);

ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT XPKReasonCatJur
PRIMARY KEY (reasonCategoryId, jurisdictionId, effDate);


create table SitusCondJur
(
    situsConditionId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKSitusCondJur
 ON SitusCondJur (situsConditionId, jurisdictionId);

ALTER TABLE SitusCondJur
    ADD CONSTRAINT XPKSitusCondJur
PRIMARY KEY (situsConditionId, jurisdictionId);


create table SitusCondShippingTerms
(
    situsConditionId numeric(18) NOT NULL ,
    shippingTermsId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKSitusCondShipTm
 ON SitusCondShippingTerms (situsConditionId, shippingTermsId);

ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT XPKSitusCondShipTm
PRIMARY KEY (situsConditionId, shippingTermsId);


create table TaxType
(
    taxTypeId numeric(18) NOT NULL ,
    taxTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxType
 ON TaxType (taxTypeId);

ALTER TABLE TaxType
    ADD CONSTRAINT XPKTaxType
PRIMARY KEY (taxTypeId);


create table FilingCategory
(
    filingCategoryId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    filingCategoryCode numeric(5) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCategoryName varchar(120) NOT NULL ,
    primaryCategoryInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKFilingCat
 ON FilingCategory (filingCategoryId);

ALTER TABLE FilingCategory
    ADD CONSTRAINT XPKFilingCat
PRIMARY KEY (filingCategoryId);


create table FilingCategoryOvrd
(
    filingCatOvrdId numeric(18) NOT NULL ,
    filingCategoryId numeric(18) NOT NULL ,
    ovrdFilingCatId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKFCOvd
 ON FilingCategoryOvrd (filingCatOvrdId);

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT XPKFCOvd
PRIMARY KEY (filingCatOvrdId);


create table SitusConclusion
(
    situsConclusionId numeric(18) NOT NULL ,
    situsConcTypeId numeric(18)  ,
    taxTypeId numeric(18)  ,
    jurisdictionTypeId numeric(18)  ,
    locRoleTypeId numeric(18)  ,
    locRoleType2Id numeric(18)  ,
    multiSitusRecTypId numeric(18)  ,
    jurTypeSetId numeric(18)  ,
    taxType2Id numeric(18)  ,
    locRoleTypeId1Name varchar(120)  ,
    locRoleTypeId2Name varchar(120)  ,
    taxTypeIdName varchar(120)  ,
    boolFactName varchar(120)  ,
    boolFactValue numeric(1)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  ,
    outputNoticeId numeric(18)  ,
    impsnTypeId numeric(18)  ,
    impsnTypeSourceId numeric(18)  ,
    txbltyCatId numeric(18)  
);

CREATE UNIQUE INDEX XPKSitusConc
 ON SitusConclusion (situsConclusionId);

ALTER TABLE SitusConclusion
    ADD CONSTRAINT XPKSitusConc
PRIMARY KEY (situsConclusionId);


create table TaxScope
(
    taxScopeId numeric(18) NOT NULL ,
    taxScopeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxScope
 ON TaxScope (taxScopeId);

ALTER TABLE TaxScope
    ADD CONSTRAINT XPKTaxScope
PRIMARY KEY (taxScopeId);


create table RoundingRule
(
    roundingRuleId numeric(18) NOT NULL ,
    initialPrecision numeric(15)  ,
    taxScopeId numeric(18) NOT NULL ,
    finalPrecision numeric(15) NOT NULL ,
    threshold numeric(15) NOT NULL ,
    decimalPosition numeric(15) NOT NULL ,
    roundingTypeId numeric(18)  
);

CREATE UNIQUE INDEX XPKRoundingRule
 ON RoundingRule (roundingRuleId);

ALTER TABLE RoundingRule
    ADD CONSTRAINT XPKRoundingRule
PRIMARY KEY (roundingRuleId);


create table TransSubType
(
    transSubTypeId numeric(18) NOT NULL ,
    transSubTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKTransSubType
 ON TransSubType (transSubTypeId);

ALTER TABLE TransSubType
    ADD CONSTRAINT XPKTransSubType
PRIMARY KEY (transSubTypeId);


create table VertexProductType
(
    vertexProductId numeric(18) NOT NULL ,
    vertexProductName varchar(120)  
);

CREATE UNIQUE INDEX XPKVertexProdType
 ON VertexProductType (vertexProductId);

ALTER TABLE VertexProductType
    ADD CONSTRAINT XPKVertexProdType
PRIMARY KEY (vertexProductId);


create table PartyRoleType
(
    partyRoleTypeId numeric(18) NOT NULL ,
    partyRoleTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKPartyRoleType
 ON PartyRoleType (partyRoleTypeId);

ALTER TABLE PartyRoleType
    ADD CONSTRAINT XPKPartyRoleType
PRIMARY KEY (partyRoleTypeId);


create table TelecomUnitConversion
(
    telecomUnitConversionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    name varchar(120) NOT NULL ,
    sourceUnitOfMeasureISOCode varchar(40) NOT NULL ,
    targetUnitOfMeasureISOCode varchar(40) NOT NULL ,
    firstConvertCount numeric(8) NOT NULL ,
    additionalConvertCount numeric(8)  ,
    isDefault numeric(1)  ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTelComUntConvsn
 ON TelecomUnitConversion (telecomUnitConversionId, sourceId);

ALTER TABLE TelecomUnitConversion
    ADD CONSTRAINT XPKTelComUntConvsn
PRIMARY KEY (telecomUnitConversionId, sourceId);


create table TaxStructureType
(
    taxStructureTypeId numeric(18) NOT NULL ,
    taxStrucTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxStrucType
 ON TaxStructureType (taxStructureTypeId);

ALTER TABLE TaxStructureType
    ADD CONSTRAINT XPKTaxStrucType
PRIMARY KEY (taxStructureTypeId);


create table DeductionType
(
    deductionTypeId numeric(18) NOT NULL ,
    deductionTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKDeductionType
 ON DeductionType (deductionTypeId);

ALTER TABLE DeductionType
    ADD CONSTRAINT XPKDeductionType
PRIMARY KEY (deductionTypeId);


create table TransOrigType
(
    transOrigTypeId numeric(18) NOT NULL ,
    transOrigTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKTransOrigType
 ON TransOrigType (transOrigTypeId);

ALTER TABLE TransOrigType
    ADD CONSTRAINT XPKTransOrigType
PRIMARY KEY (transOrigTypeId);


create table TaxRuleType
(
    taxRuleTypeId numeric(18) NOT NULL ,
    taxRuleTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKTaxRuleType
 ON TaxRuleType (taxRuleTypeId);

ALTER TABLE TaxRuleType
    ADD CONSTRAINT XPKTaxRuleType
PRIMARY KEY (taxRuleTypeId);


create table WithholdingType
(
    withholdingTypeId numeric(18) NOT NULL ,
    withholdingTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKWithholdingType
 ON WithholdingType (withholdingTypeId);

ALTER TABLE WithholdingType
    ADD CONSTRAINT XPKWithholdingType
PRIMARY KEY (withholdingTypeId);


create table AccumulationByType
(
    id numeric(18) NOT NULL ,
    name varchar(120)  
);

CREATE UNIQUE INDEX XPKAccByType
 ON AccumulationByType (id);

ALTER TABLE AccumulationByType
    ADD CONSTRAINT XPKAccByType
PRIMARY KEY (id);


create table AccumulationPeriodType
(
    id numeric(18) NOT NULL ,
    name varchar(120)  
);

CREATE UNIQUE INDEX XPKAccPeriodType
 ON AccumulationPeriodType (id);

ALTER TABLE AccumulationPeriodType
    ADD CONSTRAINT XPKAccPeriodType
PRIMARY KEY (id);


create table AccumulationType
(
    id numeric(18) NOT NULL ,
    name varchar(120)  
);

CREATE UNIQUE INDEX XPKAccType
 ON AccumulationType (id);

ALTER TABLE AccumulationType
    ADD CONSTRAINT XPKAccType
PRIMARY KEY (id);


create table TaxRuleTaxImpositionType
(
    taxRuleTaxImpositionTypeId numeric(18) NOT NULL ,
    taxRuleTaxImpositionTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKTxRuleImpsnType
 ON TaxRuleTaxImpositionType (taxRuleTaxImpositionTypeId);

ALTER TABLE TaxRuleTaxImpositionType
    ADD CONSTRAINT XPKTxRuleImpsnType
PRIMARY KEY (taxRuleTaxImpositionTypeId);


create table ValidationType
(
    validationTypeId numeric(18) NOT NULL ,
    validationTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKValidationType
 ON ValidationType (validationTypeId);

ALTER TABLE ValidationType
    ADD CONSTRAINT XPKValidationType
PRIMARY KEY (validationTypeId);


create table DMFilterStrng
(
    stringTypeId numeric(18) NOT NULL ,
    criteriaString varchar(510)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMFilterStrng
 ON DMFilterStrng (filterId, stringTypeId, criteriaOrderNum);

ALTER TABLE DMFilterStrng
    ADD CONSTRAINT XPKDMFilterStrng
PRIMARY KEY (filterId, stringTypeId, criteriaOrderNum);


create table CertClassType
(
    certClassTypeId numeric(18) NOT NULL ,
    certClassTypeName varchar(120)  
);

CREATE UNIQUE INDEX XPKCertClassType
 ON CertClassType (certClassTypeId);

ALTER TABLE CertClassType
    ADD CONSTRAINT XPKCertClassType
PRIMARY KEY (certClassTypeId);


create table DiscountCategory
(
    discountCatId numeric(18) NOT NULL ,
    discountCatName varchar(120) NOT NULL ,
    discountCatDesc varchar(200) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKDiscountCat
 ON DiscountCategory (discountCatId);

ALTER TABLE DiscountCategory
    ADD CONSTRAINT XPKDiscountCat
PRIMARY KEY (discountCatId);


create table TaxabilityCategory
(
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxabilityCat
 ON TaxabilityCategory (txbltyCatId, txbltyCatSrcId);

ALTER TABLE TaxabilityCategory
    ADD CONSTRAINT XPKTaxabilityCat
PRIMARY KEY (txbltyCatId, txbltyCatSrcId);


create table TxbltyCatDetail
(
    txbltyCatDtlId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatCode varchar(120) NOT NULL ,
    txbltyCatName varchar(120) NOT NULL ,
    txbltyCatDesc varchar(2000)  ,
    txbltyCatDefaultId numeric(18) NOT NULL ,
    prntTxbltyCatId numeric(18)  ,
    prntTxbltyCatSrcId numeric(18)  ,
    dataTypeId numeric(18)  ,
    deletedInd numeric(1) NOT NULL ,
    allowRelatedInd numeric(1)  ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxbltyCatDetail
 ON TxbltyCatDetail (txbltyCatDtlId, txbltyCatSrcId);

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT XPKTxbltyCatDetail
PRIMARY KEY (txbltyCatDtlId, txbltyCatSrcId);


create table FlexFieldDef
(
    flexFieldDefId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKFlexibleField
 ON FlexFieldDef (flexFieldDefId, flexFieldDefSrcId);

ALTER TABLE FlexFieldDef
    ADD CONSTRAINT XPKFlexibleField
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId);


create table FlexFieldDefDetail
(
    flexFieldDefDtlId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    flexFieldDefId numeric(18) NOT NULL ,
    dataTypeId numeric(18) NOT NULL ,
    calcOutputInd numeric(1) DEFAULT 0 NOT NULL ,
    flexFieldDesc varchar(2000)  ,
    flexFieldDefRefNum numeric(2) NOT NULL ,
    flexFieldDefSeqNum numeric(8) NOT NULL ,
    shortName varchar(20) NOT NULL ,
    longName varchar(120)  ,
    txbltyCatId numeric(18)  ,
    txbltyCatSrcId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKFlexFieldDetail
 ON FlexFieldDefDetail (flexFieldDefDtlId, flexFieldDefSrcId);

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT XPKFlexFieldDetail
PRIMARY KEY (flexFieldDefDtlId, flexFieldDefSrcId);


create table TaxJurDetail
(
    taxTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    regGroupAllowedInd numeric(1) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    registrationReqInd numeric(1) NOT NULL ,
    reqLocsForRprtgInd numeric(1) NOT NULL ,
    reqLocsForSitusInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxJurDetail
 ON TaxJurDetail (jurisdictionId, taxTypeId, effDate, sourceId);

ALTER TABLE TaxJurDetail
    ADD CONSTRAINT XPKTaxJurDetail
PRIMARY KEY (jurisdictionId, taxTypeId, effDate, sourceId);


create table DMActivityLog
(
    activityLogId numeric(18) NOT NULL ,
    filterName varchar(120)  ,
    activityTypeId numeric(18) NOT NULL ,
    startTime timestamp NOT NULL ,
    endTime timestamp  ,
    lastPingTime timestamp NOT NULL ,
    nextPingTime timestamp NOT NULL ,
    userName varchar(120) NOT NULL ,
    userId numeric(18) NOT NULL ,
    sourceName varchar(120)  ,
    sourceId numeric(18)  ,
    activityStatusId numeric(18) NOT NULL ,
    filterDescription varchar(510)  ,
    activityLogMessage varchar(2000)  ,
    hostName varchar(256) NOT NULL ,
    outputFileName varchar(510)  ,
    masterAdminInd numeric(1)  ,
    sysAdminInd numeric(1)  ,
    followupInd numeric(1)  
);

CREATE UNIQUE INDEX XPKDMActivityLog
 ON DMActivityLog (activityLogId);

ALTER TABLE DMActivityLog
    ADD CONSTRAINT XPKDMActivityLog
PRIMARY KEY (activityLogId);


create table DMActivityLogNum
(
    numberTypeId numeric(18) NOT NULL ,
    criteriaNum numeric(18)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActLogNum
 ON DMActivityLogNum (activityLogId, numberTypeId, criteriaOrderNum);

ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT XPKDMActLogNum
PRIMARY KEY (activityLogId, numberTypeId, criteriaOrderNum);


create table DMActivityLogDate
(
    dateTypeId numeric(18) NOT NULL ,
    criteriaDate numeric(8)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActLogDate
 ON DMActivityLogDate (activityLogId, dateTypeId, criteriaOrderNum);

ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT XPKDMActLogDate
PRIMARY KEY (activityLogId, dateTypeId, criteriaOrderNum);


create table DMActivityLogInd
(
    indicatorTypeId numeric(18) NOT NULL ,
    criteriaIndicator numeric(1)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActLogInd
 ON DMActivityLogInd (activityLogId, indicatorTypeId, criteriaOrderNum);

ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT XPKDMActLogInd
PRIMARY KEY (activityLogId, indicatorTypeId, criteriaOrderNum);


create table TpsSchVersion
(
    schemaVersionId numeric(18) NOT NULL ,
    subjectAreaId numeric(18)  ,
    schemaVersionCode varchar(60) NOT NULL ,
    syncVersionId varchar(128)  ,
    schemaSubVersionId numeric(18)  
);

CREATE UNIQUE INDEX XPKTpsSchemaVer
 ON TpsSchVersion (schemaVersionId);

ALTER TABLE TpsSchVersion
    ADD CONSTRAINT XPKTpsSchemaVer
PRIMARY KEY (schemaVersionId);


create table TpsPatchDataEvent
(
    dataEventId numeric(18) NOT NULL ,
    patchId numeric(18) NOT NULL ,
    patchInterimId numeric(18) NOT NULL ,
    eventDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKTPSPchDataEvent
 ON TpsPatchDataEvent (dataEventId);

ALTER TABLE TpsPatchDataEvent
    ADD CONSTRAINT XPKTPSPchDataEvent
PRIMARY KEY (dataEventId);


create table ApportionmentFriendlyStates
(
    friendlyStateId numeric(18) NOT NULL ,
    jurId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

CREATE UNIQUE INDEX XPKApptionFriendly
 ON ApportionmentFriendlyStates (friendlyStateId);

ALTER TABLE ApportionmentFriendlyStates
    ADD CONSTRAINT XPKApptionFriendly
PRIMARY KEY (friendlyStateId);


create table SitusConditionNode
(
    situsCondNodeId numeric(18) NOT NULL ,
    situsConditionId numeric(18) NOT NULL ,
    rootNodeInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKSitusCondNode
 ON SitusConditionNode (situsCondNodeId);

ALTER TABLE SitusConditionNode
    ADD CONSTRAINT XPKSitusCondNode
PRIMARY KEY (situsCondNodeId);


create table TrueSitusCond
(
    prntTruSitusCondId numeric(18) NOT NULL ,
    chldTruSitusCondId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTrueSitusCond
 ON TrueSitusCond (prntTruSitusCondId, chldTruSitusCondId);

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT XPKTrueSitusCond
PRIMARY KEY (prntTruSitusCondId, chldTruSitusCondId);


create table SitusConcNode
(
    situsConcNodeId numeric(18) NOT NULL ,
    situsConclusionId numeric(18) NOT NULL ,
    truForSitusCondId numeric(18)  ,
    flsForSitusCondId numeric(18)  
);

CREATE UNIQUE INDEX XPKSitusConcNode
 ON SitusConcNode (situsConcNodeId);

ALTER TABLE SitusConcNode
    ADD CONSTRAINT XPKSitusConcNode
PRIMARY KEY (situsConcNodeId);


create table Party
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyCreationDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKParty
 ON Party (partyId, partySourceId);

ALTER TABLE Party
    ADD CONSTRAINT XPKParty
PRIMARY KEY (partyId, partySourceId);


create table PartyDetail
(
    partyDtlId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    parentPartyId numeric(18)  ,
    partyTypeId numeric(18) NOT NULL ,
    partyName varchar(120) NOT NULL ,
    userPartyIdCode varchar(80) NOT NULL ,
    partyRelInd numeric(1) NOT NULL ,
    taxpayerType varchar(120)  ,
    filingEntityInd numeric(1) NOT NULL ,
    prntInheritenceInd numeric(1) NOT NULL ,
    ersInd numeric(1) NOT NULL ,
    taxThresholdAmt numeric(18,3)  ,
    taxThresholdPct numeric(10,6)  ,
    taxOvrThresholdAmt numeric(18,3)  ,
    taxOvrThresholdPct numeric(10,6)  ,
    partyClassInd numeric(1) NOT NULL ,
    shippingTermsId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    discountCatId numeric(18)  ,
    customField1Value varchar(120)  ,
    customField2Value varchar(120)  ,
    customField3Value varchar(120)  ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    parentCustomerId numeric(18)  
);

CREATE UNIQUE INDEX XPKPartyDtl
 ON PartyDetail (partyDtlId, partySourceId);

ALTER TABLE PartyDetail
    ADD CONSTRAINT XPKPartyDtl
PRIMARY KEY (partyDtlId, partySourceId);


create table PartyRoleTaxResult
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyRoleTypeId numeric(18) NOT NULL ,
    taxResultTypeId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18)  
);

CREATE UNIQUE INDEX XPKPrtyRoleTaxRslt
 ON PartyRoleTaxResult (partyId, partyRoleTypeId, partySourceId);

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT XPKPrtyRoleTaxRslt
PRIMARY KEY (partyId, partyRoleTypeId, partySourceId);


create table PartyVtxProdType
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKPrtyVtxProdTyp
 ON PartyVtxProdType (partyId, partySourceId, vertexProductId);

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT XPKPrtyVtxProdTyp
PRIMARY KEY (partyId, partySourceId, vertexProductId);


create table BusinessLocation
(
    partyId numeric(18) NOT NULL ,
    businessLocationId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxAreaId numeric(18)  ,
    userLocationCode varchar(40)  ,
    registrationCode varchar(40)  ,
    streetInfoDesc varchar(200)  ,
    streetInfo2Desc varchar(200)  ,
    cityName varchar(120)  ,
    subDivisionName varchar(120)  ,
    mainDivisionName varchar(120)  ,
    postalCode varchar(40)  ,
    countryName varchar(120)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    busLocationName varchar(120)  ,
    partyRoleTypeId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKBusinessLoc
 ON BusinessLocation (businessLocationId, partyId, sourceId);

ALTER TABLE BusinessLocation
    ADD CONSTRAINT XPKBusinessLoc
PRIMARY KEY (businessLocationId, partyId, sourceId);


create table PartyNote
(
    partySourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partyNoteText varchar(2000)  
);

CREATE UNIQUE INDEX XPKPartyNote
 ON PartyNote (partyId, partySourceId);

ALTER TABLE PartyNote
    ADD CONSTRAINT XPKPartyNote
PRIMARY KEY (partyId, partySourceId);


create table PartyContact
(
    partyContactId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    contactRoleTypeId numeric(18) NOT NULL ,
    contactFirstName varchar(120)  ,
    contactLastName varchar(120)  ,
    streetInfoDesc varchar(200)  ,
    streetInfo2Desc varchar(200)  ,
    cityName varchar(120)  ,
    mainDivisionName varchar(120)  ,
    postalCode varchar(40)  ,
    countryName varchar(120)  ,
    phoneNumber varchar(40)  ,
    phoneExtension varchar(40)  ,
    faxNumber varchar(40)  ,
    emailAddress varchar(200)  ,
    departmentIdCode varchar(40)  ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

CREATE UNIQUE INDEX XPKPartyContact
 ON PartyContact (partyContactId, sourceId);

ALTER TABLE PartyContact
    ADD CONSTRAINT XPKPartyContact
PRIMARY KEY (partyContactId, sourceId);


create table TaxabilityDriver
(
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxabilityDvr
 ON TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId);

ALTER TABLE TaxabilityDriver
    ADD CONSTRAINT XPKTaxabilityDvr
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


create table TxbltyDriverDetail
(
    txbltyDvrDtlId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    inputParamTypeId numeric(18) NOT NULL ,
    txbltyDvrCode varchar(80) NOT NULL ,
    txbltyDvrName varchar(120) NOT NULL ,
    reasonCategoryId numeric(18)  ,
    exemptInd numeric(1) NOT NULL ,
    taxpayerPartyId numeric(18)  ,
    taxpayerSrcId numeric(18)  ,
    discountCatId numeric(18)  ,
    flexFieldDefId numeric(18)  ,
    flexFieldDefSrcId numeric(18)  ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxbltyDvrDetail
 ON TxbltyDriverDetail (txbltyDvrDtlId, txbltyDvrSrcId);

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT XPKTxbltyDvrDetail
PRIMARY KEY (txbltyDvrDtlId, txbltyDvrSrcId);


create table TxbltyDriverNote
(
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    txbltyDvrNoteText varchar(2000)  
);

CREATE UNIQUE INDEX XPKTxbltyDvrNote
 ON TxbltyDriverNote (txbltyDvrId, txbltyDvrSrcId);

ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT XPKTxbltyDvrNote
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


create table TxbltyDvrVtxPrdTyp
(
    vertexProductId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxbltyDvrVtxPrd
 ON TxbltyDvrVtxPrdTyp (txbltyDvrId, txbltyDvrSrcId, vertexProductId);

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT XPKTxbltyDvrVtxPrd
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId, vertexProductId);


create table TelecomUnitConversionLineType
(
    telecomUnitConvnLineTypeId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    lineTypeId numeric(18) NOT NULL ,
    lineTypeSourceId numeric(18) NOT NULL ,
    telecomUnitConversionId numeric(18) NOT NULL ,
    telecomUnitConversionSourceId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTCUntCvnLnType
 ON TelecomUnitConversionLineType (telecomUnitConvnLineTypeId, sourceId);

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT XPKTCUntCvnLnType
PRIMARY KEY (telecomUnitConvnLineTypeId, sourceId);


create table TxbltyCatMap
(
    txbltyCatMapId numeric(18) NOT NULL ,
    txbltyCatMapSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    taxpayerPartyId numeric(18)  ,
    otherPartyId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatMapNote varchar(2000)  ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxbltyCatMap
 ON TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId);

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT XPKTxbltyCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId);


create table TxbltyDriverCatMap
(
    txbltyCatMapId numeric(18) NOT NULL ,
    txbltyCatMapSrcId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxbltyDvrCatMap
 ON TxbltyDriverCatMap (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId);

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT XPKTxbltyDvrCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId);


create table InvoiceTextType
(
    invoiceTextTypeId numeric(18) NOT NULL ,
    invoiceTextTypeSrcId numeric(18) NOT NULL ,
    invoiceTextTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKInvoiceTextType
 ON InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId);

ALTER TABLE InvoiceTextType
    ADD CONSTRAINT XPKInvoiceTextType
PRIMARY KEY (invoiceTextTypeId, invoiceTextTypeSrcId);


create table InvoiceText
(
    invoiceTextId numeric(18) NOT NULL ,
    invoiceTextSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKInvText
 ON InvoiceText (invoiceTextId, invoiceTextSrcId);

ALTER TABLE InvoiceText
    ADD CONSTRAINT XPKInvText
PRIMARY KEY (invoiceTextId, invoiceTextSrcId);


create table InvoiceTextDetail
(
    invoiceTextDtlId numeric(18) NOT NULL ,
    invoiceTextSrcId numeric(18) NOT NULL ,
    invoiceTextId numeric(18) NOT NULL ,
    invoiceTextTypeId numeric(18) NOT NULL ,
    invoiceTextTypeSrcId numeric(18) NOT NULL ,
    invoiceTextCode varchar(120) NOT NULL ,
    invoiceText varchar(400) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKInvTextDetail
 ON InvoiceTextDetail (invoiceTextDtlId, invoiceTextSrcId);

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT XPKInvTextDetail
PRIMARY KEY (invoiceTextDtlId, invoiceTextSrcId);


create table DMFilterInd
(
    indicatorTypeId numeric(18) NOT NULL ,
    criteriaIndicator numeric(1)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMFilterInd
 ON DMFilterInd (filterId, indicatorTypeId, criteriaOrderNum);

ALTER TABLE DMFilterInd
    ADD CONSTRAINT XPKDMFilterInd
PRIMARY KEY (filterId, indicatorTypeId, criteriaOrderNum);


create table DiscountType
(
    sourceId numeric(18) NOT NULL ,
    discountTypeId numeric(18) NOT NULL ,
    taxpayerPartyId numeric(18) NOT NULL ,
    discountCatId numeric(18) NOT NULL ,
    discountCode varchar(40) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKDiscountType
 ON DiscountType (discountTypeId, sourceId);

ALTER TABLE DiscountType
    ADD CONSTRAINT XPKDiscountType
PRIMARY KEY (discountTypeId, sourceId);


create table TaxStructure
(
    taxStructureSrcId numeric(18) NOT NULL ,
    taxStructureId numeric(18) NOT NULL ,
    taxStructureTypeId numeric(18) NOT NULL ,
    brcktMaximumBasis numeric(18,3)  ,
    reductAmtDedTypeId numeric(18)  ,
    reductReasonCategoryId numeric(18)  ,
    allAtTopTierTypInd numeric(1) NOT NULL ,
    singleRate numeric(12,8)  ,
    taxPerUnitAmount numeric(18,6)  ,
    unitOfMeasureQty numeric(18,6) NOT NULL ,
    childTaxStrucSrcId numeric(18)  ,
    unitBasedInd numeric(1) NOT NULL ,
    basisReductFactor numeric(12,8)  ,
    brcktTaxCalcType numeric(18)  ,
    unitOfMeasISOCode varchar(6)  ,
    deletedInd numeric(1) NOT NULL ,
    childTaxStrucId numeric(18)  ,
    usesStdRateInd numeric(1) NOT NULL ,
    flatTaxAmt numeric(18,3)  ,
    telecomUnitConversionId numeric(18)  ,
    telecomUnitConversionSrcId numeric(18)  
);

CREATE UNIQUE INDEX XPKTaxStructure
 ON TaxStructure (taxStructureId, taxStructureSrcId);

ALTER TABLE TaxStructure
    ADD CONSTRAINT XPKTaxStructure
PRIMARY KEY (taxStructureId, taxStructureSrcId);


create table ImpositionType
(
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    impsnTypeName varchar(120) NOT NULL ,
    creditInd numeric(1)  ,
    notInTotalInd numeric(1)  ,
    withholdingTypeId numeric(18)  
);

CREATE UNIQUE INDEX XPKImpositionType
 ON ImpositionType (impsnTypeId, impsnTypeSrcId);

ALTER TABLE ImpositionType
    ADD CONSTRAINT XPKImpositionType
PRIMARY KEY (impsnTypeId, impsnTypeSrcId);


create table TaxImposition
(
    jurisdictionId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxImp
 ON TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId);

ALTER TABLE TaxImposition
    ADD CONSTRAINT XPKTaxImp
PRIMARY KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId);


create table TaxImpsnDetail
(
    taxImpsnDtlId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    taxImpsnName varchar(120) NOT NULL ,
    taxImpsnAbrv varchar(30)  ,
    taxImpsnDesc varchar(200)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    taxTypeId numeric(18)  ,
    taxResponsibilityRoleTypeId numeric(18)  ,
    conditionInd numeric(1)  ,
    primaryImpositionInd numeric(1)  
);

CREATE UNIQUE INDEX XPKTaxImpsnDetail
 ON TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId);

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT XPKTaxImpsnDetail
PRIMARY KEY (taxImpsnDtlId, taxImpsnSrcId);


create table TaxImpQualCond
(
    taxImpQualCondId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    taxImpsnDtlId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxImpQualCond
 ON TaxImpQualCond (taxImpQualCondId, taxImpsnSrcId);

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT XPKTaxImpQualCond
PRIMARY KEY (taxImpQualCondId, taxImpsnSrcId);


create table FilingOverride
(
    filingOverrideId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    jurTypeSetId numeric(18) NOT NULL ,
    filingCategoryId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKFilingOverride
 ON FilingOverride (filingOverrideId);

ALTER TABLE FilingOverride
    ADD CONSTRAINT XPKFilingOverride
PRIMARY KEY (filingOverrideId);


create table Tier
(
    minBasisAmount numeric(18,3)  ,
    tierNum numeric(3) NOT NULL ,
    taxResultTypeId numeric(18)  ,
    maxBasisAmount numeric(18,3)  ,
    minQuantity numeric(18,6)  ,
    maxQuantity numeric(18,6)  ,
    unitOfMeasISOCode varchar(6)  ,
    unitOfMeasureQty numeric(18,6)  ,
    taxPerUnitAmount numeric(18,6)  ,
    tierTaxRate numeric(12,8)  ,
    taxStructureSrcId numeric(18) NOT NULL ,
    taxStructureId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18)  ,
    usesStdRateInd numeric(1) NOT NULL ,
    filingCategoryId numeric(18)  ,
    rateClassId numeric(18)  ,
    taxStructureTypeId numeric(18)  ,
    equivalentQuantity numeric(18,6)  ,
    additionalQuantity numeric(18,6)  ,
    equivalentAdditionalQuantity numeric(18,6)  
);

CREATE UNIQUE INDEX XPKTier
 ON Tier (tierNum, taxStructureSrcId, taxStructureId);

ALTER TABLE Tier
    ADD CONSTRAINT XPKTier
PRIMARY KEY (tierNum, taxStructureSrcId, taxStructureId);


create table Bracket
(
    taxStructureId numeric(18) NOT NULL ,
    taxStructureSrcId numeric(18) NOT NULL ,
    minBasisAmount numeric(18,3)  ,
    bracketNum numeric(3) NOT NULL ,
    maxBasisAmount numeric(18,3)  ,
    bracketTaxAmount numeric(18,3)  
);

CREATE UNIQUE INDEX XPKBracket
 ON Bracket (taxStructureId, taxStructureSrcId, bracketNum);

ALTER TABLE Bracket
    ADD CONSTRAINT XPKBracket
PRIMARY KEY (taxStructureId, taxStructureSrcId, bracketNum);


create table TaxRule
(
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    partyRoleTypeId numeric(18)  ,
    partyId numeric(18)  ,
    partySourceId numeric(18)  ,
    taxpayerRoleTypeId numeric(18)  ,
    taxpayerPartyId numeric(18)  ,
    taxpayerPartySrcId numeric(18)  ,
    qualDetailDesc varchar(200)  ,
    uniqueToLevelInd numeric(1) NOT NULL ,
    conditionSeqNum numeric(18)  ,
    taxRuleTypeId numeric(18)  ,
    taxResultTypeId numeric(18)  ,
    recoverableResultTypeId numeric(18)  ,
    defrdJurTypeId numeric(18)  ,
    defersToStdRuleInd numeric(1) NOT NULL ,
    taxStructureSrcId numeric(18)  ,
    taxStructureId numeric(18)  ,
    automaticRuleInd numeric(1) NOT NULL ,
    standardRuleInd numeric(1) NOT NULL ,
    reasonCategoryId numeric(18)  ,
    filingCategoryId numeric(18)  ,
    appToSingleImpInd numeric(1)  ,
    appToSingleJurInd numeric(1)  ,
    discountTypeId numeric(18)  ,
    discountTypeSrcId numeric(18)  ,
    maxTaxRuleType numeric(18)  ,
    taxScopeId numeric(18)  ,
    maxTaxAmount numeric(18,3)  ,
    jurisdictionId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    discountCatId numeric(18)  ,
    deletedInd numeric(1) NOT NULL ,
    apportionTypeId numeric(18)  ,
    appnTxbltyCatId numeric(18)  ,
    appnTxbltyCatSrcId numeric(18)  ,
    appnFlexFieldDefId numeric(18)  ,
    appnFlexFieldDefSrcId numeric(18)  ,
    rateClassId numeric(18)  ,
    locationRoleTypeId numeric(18)  ,
    applyFilingCatInd numeric(1) DEFAULT 0  ,
    recoverablePct numeric(10,6)  ,
    taxResponsibilityRoleTypeId numeric(18)  ,
    currencyUnitId numeric(18)  ,
    salestaxHolidayInd numeric(1)  ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    includesAllTaxCats numeric(1)  ,
    invoiceTextId numeric(18)  ,
    invoiceTextSrcId numeric(18)  ,
    computationTypeId numeric(18)  ,
    defrdImpsnTypeId numeric(18)  ,
    defrdImpsnTypeSrcId numeric(18)  ,
    notAllowZeroRateInd numeric(1)  ,
    accumulationAsJurisdictionId numeric(18)  ,
    accumulationAsTaxImpsnId numeric(18)  ,
    accumulationAsTaxImpsnSrcId numeric(18)  ,
    accumulationTypeId numeric(18)  ,
    periodTypeId numeric(18)  ,
    startMonth numeric(2)  ,
    maxLines numeric(18)  ,
    unitOfMeasISOCode varchar(6)  ,
    telecomUnitConversionId numeric(18)  ,
    telecomUnitConversionSrcId numeric(18)  ,
    lineTypeCatId numeric(18)  ,
    lineTypeCatSourceId numeric(18)  
);

CREATE UNIQUE INDEX XPKTaxRule
 ON TaxRule (taxRuleId, taxRuleSourceId);

ALTER TABLE TaxRule
    ADD CONSTRAINT XPKTaxRule
PRIMARY KEY (taxRuleId, taxRuleSourceId);


create table TaxRuleNote
(
    taxRuleNoteId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleNoteSrcId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleNoteText varchar(2000)  
);

CREATE UNIQUE INDEX XPKTaxRuleNote
 ON TaxRuleNote (taxRuleNoteId, taxRuleNoteSrcId);

ALTER TABLE TaxRuleNote
    ADD CONSTRAINT XPKTaxRuleNote
PRIMARY KEY (taxRuleNoteId, taxRuleNoteSrcId);


create table ExprConditionType
(
    exprCondTypeId numeric(18) NOT NULL ,
    exprCondTypeName varchar(120) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKExprCondType
 ON ExprConditionType (exprCondTypeId);

ALTER TABLE ExprConditionType
    ADD CONSTRAINT XPKExprCondType
PRIMARY KEY (exprCondTypeId);


create table TaxRuleQualCond
(
    taxRuleQualCondId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18)  ,
    txbltyDvrSrcId numeric(18)  ,
    txbltyCatId numeric(18)  ,
    txbltyCatSrcId numeric(18)  ,
    flexFieldDefId numeric(18)  ,
    flexFieldDefSrcId numeric(18)  ,
    minDate numeric(8)  ,
    maxDate numeric(8)  ,
    compareValue numeric(18,3)  ,
    exprCondTypeId numeric(18)  
);

CREATE UNIQUE INDEX XPKTaxRuleQualCond
 ON TaxRuleQualCond (taxRuleQualCondId, taxRuleSourceId);

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT XPKTaxRuleQualCond
PRIMARY KEY (taxRuleQualCondId, taxRuleSourceId);


create table TaxRuleTaxType
(
    taxRuleTaxTypeId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxRuleTaxType
 ON TaxRuleTaxType (taxRuleTaxTypeId, taxRuleSourceId);

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT XPKTaxRuleTaxType
PRIMARY KEY (taxRuleTaxTypeId, taxRuleSourceId);


create table TaxRuleTaxImposition
(
    taxRuleTaxImpositionId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleTaxImpositionTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18)  ,
    taxImpsnId numeric(18)  ,
    taxImpsnSrcId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    txbltyCatId numeric(18)  ,
    txbltyCatSrcId numeric(18)  ,
    overTxbltyCatId numeric(18)  ,
    overTxbltyCatSrcId numeric(18)  ,
    underTxbltyCatId numeric(18)  ,
    underTxbltyCatSrcId numeric(18)  ,
    invoiceThresholdAmt numeric(18,5)  ,
    thresholdCurrencyUnitId numeric(18)  ,
    includeTaxableAmountInd numeric(1)  ,
    includeTaxAmountInd numeric(1)  ,
    rate numeric(12,8)  ,
    locationRoleTypeId numeric(18)  ,
    impositionTypeId numeric(18)  ,
    impositionTypeSrcId numeric(18)  ,
    jurTypeId numeric(18)  ,
    taxTypeId numeric(18)  ,
    isLeftInd numeric(1)  ,
    groupId numeric(3)  
);

CREATE UNIQUE INDEX XPKTaxRuleTaxImpsn
 ON TaxRuleTaxImposition (taxRuleTaxImpositionId, taxRuleSourceId);

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT XPKTaxRuleTaxImpsn
PRIMARY KEY (taxRuleTaxImpositionId, taxRuleSourceId);


create table MaxTaxRuleAdditionalCond
(
    maxTaxRuleCondId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKMaxTaxRuleCond
 ON MaxTaxRuleAdditionalCond (maxTaxRuleCondId);

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT XPKMaxTaxRuleCond
PRIMARY KEY (maxTaxRuleCondId);


create table ComputationType
(
    computationTypeId numeric(18) NOT NULL ,
    compTypeName varchar(120) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKCompType
 ON ComputationType (computationTypeId);

ALTER TABLE ComputationType
    ADD CONSTRAINT XPKCompType
PRIMARY KEY (computationTypeId);


create table TaxFactorType
(
    taxFactorTypeId numeric(18) NOT NULL ,
    taxFactorTypeName varchar(120) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKTaxFactorType
 ON TaxFactorType (taxFactorTypeId);

ALTER TABLE TaxFactorType
    ADD CONSTRAINT XPKTaxFactorType
PRIMARY KEY (taxFactorTypeId);


create table TaxFactor
(
    taxFactorId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxFactorTypeId numeric(18) NOT NULL ,
    constantValue numeric(18,6)  ,
    basisTypeId numeric(18)  ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  ,
    txbltyCatId numeric(18)  ,
    txbltyCatSrcId numeric(18)  ,
    flexFieldDefId numeric(18)  ,
    flexFieldDefSrcId numeric(18)  ,
    impositionTypeId numeric(18)  ,
    impositionTypeSrcId numeric(18)  ,
    locationRoleTypeId numeric(18)  ,
    jurTypeId numeric(18)  ,
    taxTypeId numeric(18)  
);

CREATE UNIQUE INDEX XPKTaxFactor
 ON TaxFactor (taxFactorId, sourceId);

ALTER TABLE TaxFactor
    ADD CONSTRAINT XPKTaxFactor
PRIMARY KEY (taxFactorId, sourceId);


create table ComputationFactor
(
    taxFactorId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    computationTypeId numeric(18) NOT NULL ,
    leftTaxFactorId numeric(18) NOT NULL ,
    rightTaxFactorId numeric(18) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKCompFactor
 ON ComputationFactor (taxFactorId, sourceId);

ALTER TABLE ComputationFactor
    ADD CONSTRAINT XPKCompFactor
PRIMARY KEY (taxFactorId, sourceId);


create table ConditionalTaxExpr
(
    condTaxExprId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    exprCondTypeId numeric(18) NOT NULL ,
    leftTaxFactorId numeric(18) NOT NULL ,
    rightTaxFactorId numeric(18)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKCondTaxExpr
 ON ConditionalTaxExpr (condTaxExprId, sourceId);

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT XPKCondTaxExpr
PRIMARY KEY (condTaxExprId, sourceId);


create table TaxBasisConclusion
(
    taxBasisConcId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxFactorId numeric(18) NOT NULL ,
    taxTypeId numeric(18)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKTaxBasisConc
 ON TaxBasisConclusion (taxBasisConcId, sourceId);

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT XPKTaxBasisConc
PRIMARY KEY (taxBasisConcId, sourceId);


create table TaxabilityMapping
(
    taxabilityMapId numeric(18) NOT NULL ,
    taxabilityMapSrcId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyCatMapId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxabilityMap
 ON TaxabilityMapping (taxabilityMapId, taxabilityMapSrcId);

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT XPKTaxabilityMap
PRIMARY KEY (taxabilityMapId, taxabilityMapSrcId);


create table LineItemTaxDtlType
(
    lineItemTxDtlTypId numeric(18) NOT NULL ,
    lineItemTxDtlTypNm varchar(120)  
);

CREATE UNIQUE INDEX XPKLinItmTaxDtlTyp
 ON LineItemTaxDtlType (lineItemTxDtlTypId);

ALTER TABLE LineItemTaxDtlType
    ADD CONSTRAINT XPKLinItmTaxDtlTyp
PRIMARY KEY (lineItemTxDtlTypId);


create table FalseSitusCond
(
    prntFlsSitusCondId numeric(18) NOT NULL ,
    chldFlsSitusCondId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKFalseSitusCond
 ON FalseSitusCond (prntFlsSitusCondId, chldFlsSitusCondId);

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT XPKFalseSitusCond
PRIMARY KEY (prntFlsSitusCondId, chldFlsSitusCondId);


create table TaxRuleTransType
(
    taxRuleTransTypeId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    transactionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxRuleTranTyp
 ON TaxRuleTransType (taxRuleTransTypeId, taxRuleSourceId);

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT XPKTaxRuleTranTyp
PRIMARY KEY (taxRuleTransTypeId, taxRuleSourceId);


create table DMActivityLogStrng
(
    stringTypeId numeric(18) NOT NULL ,
    criteriaString varchar(510)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActLogStrng
 ON DMActivityLogStrng (activityLogId, stringTypeId, criteriaOrderNum);

ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT XPKDMActLogStrng
PRIMARY KEY (activityLogId, stringTypeId, criteriaOrderNum);


create table DMActivityLogFile
(
    activityLogId numeric(18) NOT NULL ,
    fileId numeric(18) NOT NULL ,
    fileName varchar(510) NOT NULL ,
    statusNum numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActLogFile
 ON DMActivityLogFile (activityLogId, fileId);

ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT XPKDMActLogFile
PRIMARY KEY (activityLogId, fileId);


create table TaxRuleDescription
(
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleDescText varchar(2000)  
);

CREATE UNIQUE INDEX XPKTaxRuleDesc
 ON TaxRuleDescription (taxRuleId, taxRuleSourceId);

ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT XPKTaxRuleDesc
PRIMARY KEY (taxRuleId, taxRuleSourceId);


create table SitusCondTxbltyCat
(
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    situsConditionId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKSitusCondTaxCat
 ON SitusCondTxbltyCat (situsConditionId, txbltyCatSrcId, txbltyCatId);

ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT XPKSitusCondTaxCat
PRIMARY KEY (situsConditionId, txbltyCatSrcId, txbltyCatId);


create table TaxRuleCondJur
(
    taxRuleCondJurId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxRuleCondJur
 ON TaxRuleCondJur (taxRuleCondJurId, taxRuleSourceId);

ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT XPKTaxRuleCondJur
PRIMARY KEY (taxRuleCondJurId, taxRuleSourceId);


create table DMFilterNum
(
    numberTypeId numeric(18) NOT NULL ,
    criteriaNum numeric(18)  ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMFilterNum
 ON DMFilterNum (filterId, numberTypeId, criteriaOrderNum);

ALTER TABLE DMFilterNum
    ADD CONSTRAINT XPKDMFilterNum
PRIMARY KEY (filterId, numberTypeId, criteriaOrderNum);


create table SitusTreatment
(
    situsTreatmentId numeric(18) NOT NULL ,
    situsCondNodeId numeric(18)  ,
    situsTreatmentName varchar(120) NOT NULL ,
    situsTreatmentDesc varchar(2000)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKSitusTreatment
 ON SitusTreatment (situsTreatmentId);

ALTER TABLE SitusTreatment
    ADD CONSTRAINT XPKSitusTreatment
PRIMARY KEY (situsTreatmentId);


create table StsTrtmntVtxPrdTyp
(
    situsTreatmentId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKStsTmtVtxPrdTyp
 ON StsTrtmntVtxPrdTyp (situsTreatmentId, vertexProductId);

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT XPKStsTmtVtxPrdTyp
PRIMARY KEY (situsTreatmentId, vertexProductId);


create table TransTypePrspctv
(
    transTypePrspctvId numeric(18) NOT NULL ,
    transactionTypeId numeric(18)  ,
    partyRoleTypeId numeric(18)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  ,
    name varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTransTypPrspctv
 ON TransTypePrspctv (transTypePrspctvId);

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT XPKTransTypPrspctv
PRIMARY KEY (transTypePrspctvId);


create table SitusTreatmentRule
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    situsTreatmentId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    jurisdiction1Id numeric(18)  ,
    jurisdiction2Id numeric(18)  ,
    locationRoleTyp1Id numeric(18)  ,
    locationRoleTyp2Id numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1)  ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKSitusTrtmntRule
 ON SitusTreatmentRule (situsTrtmntRuleId, sourceId);

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT XPKSitusTrtmntRule
PRIMARY KEY (situsTrtmntRuleId, sourceId);


create table SitusTrtmntRulNote
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    noteText varchar(2000) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKSitusTrtmntRlNt
 ON SitusTrtmntRulNote (situsTrtmntRuleId, sourceId);

ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT XPKSitusTrtmntRlNt
PRIMARY KEY (situsTrtmntRuleId, sourceId);


create table SitusTrtmntPrspctv
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    transTypePrspctvId numeric(18) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKSitusTrtmntPrsp
 ON SitusTrtmntPrspctv (situsTrtmntRuleId, sourceId, transTypePrspctvId);

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT XPKSitusTrtmntPrsp
PRIMARY KEY (situsTrtmntRuleId, sourceId, transTypePrspctvId);


create table CurrencyRndRule
(
    currencyRndRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    currencyUnitId numeric(18) NOT NULL ,
    roundingRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18)  ,
    jurisdictionId numeric(18)  ,
    partyId numeric(18)  ,
    configurableInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8)  ,
    lastUpdateDate numeric(8)  
);

CREATE UNIQUE INDEX XPKCurrRndRule
 ON CurrencyRndRule (currencyRndRuleId, sourceId);

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT XPKCurrRndRule
PRIMARY KEY (currencyRndRuleId, sourceId);


create table AllowCurrRndRule
(
    allowCurrRndRuleId numeric(18) NOT NULL ,
    currencyUnitId numeric(18) NOT NULL ,
    roundingRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18)  ,
    jurisdictionId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKAllowCurRndRule
 ON AllowCurrRndRule (allowCurrRndRuleId);

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT XPKAllowCurRndRule
PRIMARY KEY (allowCurrRndRuleId);


create table TaxRecoverablePct
(
    taxRecovPctId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    costCenter varchar(80)  ,
    recovPct numeric(10,6) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    accrualReliefInd numeric(1) DEFAULT 0 NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxRecovPct
 ON TaxRecoverablePct (taxRecovPctId);

ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT XPKTaxRecovPct
PRIMARY KEY (taxRecovPctId);


create table TaxAssistRule
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    ruleCode varchar(120) NOT NULL ,
    ruleDesc varchar(2000)  ,
    vertexProductId numeric(18) NOT NULL ,
    precedence numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    phaseId numeric(1) DEFAULT 0 NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxAssistRule
 ON TaxAssistRule (ruleId, sourceId);

ALTER TABLE TaxAssistRule
    ADD CONSTRAINT XPKTaxAssistRule
PRIMARY KEY (ruleId, sourceId);


create table TaxAssistCondition
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    conditionId numeric(18) NOT NULL ,
    conditionText varchar(4000)  ,
    conditionText2 varchar(4000)  
);

CREATE UNIQUE INDEX XPKTxAstCondition
 ON TaxAssistCondition (ruleId, sourceId, conditionId);

ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT XPKTxAstCondition
PRIMARY KEY (ruleId, sourceId, conditionId);


create table TaxAssistConclude
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    conclusionId numeric(18) NOT NULL ,
    conclusionText varchar(4000)  ,
    conclusionText2 varchar(4000)  
);

CREATE UNIQUE INDEX XPKTxAstConclude
 ON TaxAssistConclude (ruleId, sourceId, conclusionId);

ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT XPKTxAstConclude
PRIMARY KEY (ruleId, sourceId, conclusionId);


create table TaxAssistLookup
(
    sourceId numeric(18) NOT NULL ,
    tableId numeric(18) NOT NULL ,
    tableName varchar(120) NOT NULL ,
    dataType numeric(18) NOT NULL ,
    description varchar(2000)  ,
    vertexProductId numeric(18) NOT NULL ,
    resultName varchar(120) NOT NULL ,
    param1Name varchar(120) NOT NULL ,
    param2Name varchar(120)  ,
    param3Name varchar(120)  ,
    param4Name varchar(120)  ,
    param5Name varchar(120)  ,
    param6Name varchar(120)  ,
    param7Name varchar(120)  ,
    param8Name varchar(120)  ,
    param9Name varchar(120)  ,
    param10Name varchar(120)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxAstLookup
 ON TaxAssistLookup (tableId, sourceId);

ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT XPKTxAstLookup
PRIMARY KEY (tableId, sourceId);


create table TaxAssistLookupRcd
(
    sourceId numeric(18) NOT NULL ,
    recordId numeric(18) NOT NULL ,
    tableId numeric(18) NOT NULL ,
    result varchar(400)  ,
    param1 varchar(400)  ,
    param2 varchar(400)  ,
    param3 varchar(400)  ,
    param4 varchar(400)  ,
    param5 varchar(400)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxAstLookupRcd
 ON TaxAssistLookupRcd (recordId, sourceId);

ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT XPKTxAstLookupRcd
PRIMARY KEY (recordId, sourceId);


create table NumericType
(
    numericTypeId numeric(18) NOT NULL ,
    numericTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKNumericType
 ON NumericType (numericTypeId);

ALTER TABLE NumericType
    ADD CONSTRAINT XPKNumericType
PRIMARY KEY (numericTypeId);


create table TaxAssistAllocationTable
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    allocationTableName varchar(120) NOT NULL ,
    allocationTableDesc varchar(2000)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxAstAlocTbl
 ON TaxAssistAllocationTable (allocationTableId, sourceId);

ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT XPKTxAstAlocTbl
PRIMARY KEY (allocationTableId, sourceId);


create table TaxAssistAllocationColumn
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    columnId numeric(18) NOT NULL ,
    columnName varchar(120) NOT NULL ,
    dataTypeId numeric(18) NOT NULL ,
    columnSeqNum numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxAstAllocCol
 ON TaxAssistAllocationColumn (allocationTableId, sourceId, columnId);

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT XPKTxAstAllocCol
PRIMARY KEY (allocationTableId, sourceId, columnId);


create table TaxAssistAllocationColumnValue
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    columnId numeric(18) NOT NULL ,
    recordId numeric(18) NOT NULL ,
    recordCode varchar(40) NOT NULL ,
    numericTypeId numeric(18)  ,
    columnValue varchar(120)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxAstAllocCoVal
 ON TaxAssistAllocationColumnValue (allocationTableId, sourceId, columnId, recordId, recordCode);

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT XPKTxAstAllocCoVal
PRIMARY KEY (allocationTableId, sourceId, columnId, recordId, recordCode);


create table TpsJurisdiction
(
    jurId numeric(18) NOT NULL ,
    jurVersionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    jurLayerId numeric(18) NOT NULL ,
    jurTypeId numeric(18) NOT NULL ,
    jurTypeName varchar(120) NOT NULL ,
    name varchar(120) NOT NULL ,
    standardName varchar(120)  ,
    description varchar(160)  ,
    updateId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTpsJurisdiction
 ON TpsJurisdiction (jurId, jurVersionId);

ALTER TABLE TpsJurisdiction
    ADD CONSTRAINT XPKTpsJurisdiction
PRIMARY KEY (jurId, jurVersionId);


create table TaxAreaJurNames
(
    taxAreaId numeric(18) NOT NULL ,
    countryName varchar(120) NOT NULL ,
    countryISOCode2 varchar(4)  ,
    countryISOCode3 varchar(6) NOT NULL ,
    mainDivJurTypeName varchar(120)  ,
    mainDivName varchar(120)  ,
    subDivJurTypeName varchar(120)  ,
    subDivName varchar(120)  ,
    cityJurTypeName varchar(120)  ,
    cityName varchar(120)  ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxAreaJurNames
 ON TaxAreaJurNames (taxAreaId);

ALTER TABLE TaxAreaJurNames
    ADD CONSTRAINT XPKTaxAreaJurNames
PRIMARY KEY (taxAreaId);


create table JurHierarchy
(
    depthFromParent numeric(18) NOT NULL ,
    topmostInd numeric(1) NOT NULL ,
    chldJurisdictionId numeric(18) NOT NULL ,
    prntJurisdictionId numeric(18) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKJurHierarchy
 ON JurHierarchy (prntJurisdictionId, chldJurisdictionId);

ALTER TABLE JurHierarchy
    ADD CONSTRAINT XPKJurHierarchy
PRIMARY KEY (prntJurisdictionId, chldJurisdictionId);


create table SimplificationType
(
    simpTypeId numeric(18) NOT NULL ,
    simpTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKSimpType
 ON SimplificationType (simpTypeId);

ALTER TABLE SimplificationType
    ADD CONSTRAINT XPKSimpType
PRIMARY KEY (simpTypeId);


create table DMActivityType
(
    activityTypeId numeric(18) NOT NULL ,
    activityTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMActivityType
 ON DMActivityType (activityTypeId);

ALTER TABLE DMActivityType
    ADD CONSTRAINT XPKDMActivityType
PRIMARY KEY (activityTypeId);


create table DMStatusType
(
    statusTypeId numeric(18) NOT NULL ,
    statusTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKDMStatusType
 ON DMStatusType (statusTypeId);

ALTER TABLE DMStatusType
    ADD CONSTRAINT XPKDMStatusType
PRIMARY KEY (statusTypeId);


create table InputOutputType
(
    inputOutputTypeId numeric(18) NOT NULL ,
    inputOutputTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKInpOutType
 ON InputOutputType (inputOutputTypeId);

ALTER TABLE InputOutputType
    ADD CONSTRAINT XPKInpOutType
PRIMARY KEY (inputOutputTypeId);


create table CertificateReasonType
(
    certificateReasonTypeId numeric(18) NOT NULL ,
    certificateReasonTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertRsnType
 ON CertificateReasonType (certificateReasonTypeId);

ALTER TABLE CertificateReasonType
    ADD CONSTRAINT XPKCertRsnType
PRIMARY KEY (certificateReasonTypeId);


create table CertReasonTypeJurisdiction
(
    certReasonTypeJurId numeric(18) NOT NULL ,
    certificateReasonTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertRsnTypeJur
 ON CertReasonTypeJurisdiction (certReasonTypeJurId);

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT XPKCertRsnTypeJur
PRIMARY KEY (certReasonTypeJurId);


create table CertificateExemptionType
(
    certificateExemptionTypeId numeric(18) NOT NULL ,
    certificateExemptionTypeName varchar(120) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertExemType
 ON CertificateExemptionType (certificateExemptionTypeId);

ALTER TABLE CertificateExemptionType
    ADD CONSTRAINT XPKCertExemType
PRIMARY KEY (certificateExemptionTypeId);


create table CertExemptionTypeJurImp
(
    certExemptionTypeJurImpId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    allStatesInd numeric(1)  ,
    allCitiesInd numeric(1)  ,
    allCountiesInd numeric(1)  ,
    allOthersInd numeric(1)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertExmTypeJur
 ON CertExemptionTypeJurImp (certExemptionTypeJurImpId);

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT XPKCertExmTypeJur
PRIMARY KEY (certExemptionTypeJurImpId);


create table Form
(
    formId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKForm
 ON Form (formId, sourceId);

ALTER TABLE Form
    ADD CONSTRAINT XPKForm
PRIMARY KEY (formId, sourceId);


create table FormDetail
(
    formDetailId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    formIdCode varchar(60) NOT NULL ,
    formName varchar(120)  ,
    formDesc varchar(510)  ,
    formImageFileName varchar(510)  ,
    replacedByFormId numeric(18)  ,
    replacementDate numeric(8)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKFormDtl
 ON FormDetail (formDetailId, sourceId);

ALTER TABLE FormDetail
    ADD CONSTRAINT XPKFormDtl
PRIMARY KEY (formDetailId, sourceId);


create table FormJurisdiction
(
    formJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKFormJur
 ON FormJurisdiction (formJurisdictionId, sourceId);

ALTER TABLE FormJurisdiction
    ADD CONSTRAINT XPKFormJur
PRIMARY KEY (formJurisdictionId, sourceId);


create table FormFieldDef
(
    fieldDefId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    name varchar(120) NOT NULL ,
    formId numeric(18) NOT NULL ,
    jurisdictionId numeric(18)  ,
    formFieldTypeId numeric(18) NOT NULL ,
    requiredInd numeric(1) DEFAULT 0  ,
    hiddenInd numeric(1) DEFAULT 0  ,
    attributeId numeric(18)  ,
    fieldDefPageNum numeric(18) NOT NULL ,
    groupName varchar(120)  ,
    parentFieldDefId numeric(18)  ,
    parentFieldDefSelectInd numeric(1) DEFAULT 1  ,
    parentValue varchar(40)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKFormFieldDef
 ON FormFieldDef (fieldDefId, sourceId);

ALTER TABLE FormFieldDef
    ADD CONSTRAINT XPKFormFieldDef
PRIMARY KEY (fieldDefId, sourceId);


create table FormFieldTypeValue
(
    fieldDefId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    validValue varchar(40)  ,
    trueValue varchar(40)  ,
    falseValue varchar(40)  ,
    isDefault numeric(1)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKFormFldTypeVle
 ON FormFieldTypeValue (fieldDefId, sourceId);

ALTER TABLE FormFieldTypeValue
    ADD CONSTRAINT XPKFormFldTypeVle
PRIMARY KEY (fieldDefId, sourceId);


create table FormFieldAttribute
(
    attributeId numeric(18) NOT NULL ,
    name varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKFormFieldAttr
 ON FormFieldAttribute (attributeId);

ALTER TABLE FormFieldAttribute
    ADD CONSTRAINT XPKFormFieldAttr
PRIMARY KEY (attributeId);


create table Certificate
(
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCreationDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertificate
 ON Certificate (certificateId, sourceId);

ALTER TABLE Certificate
    ADD CONSTRAINT XPKCertificate
PRIMARY KEY (certificateId, sourceId);


create table CertificateDetail
(
    certificateDetailId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    certificateHolderPartyId numeric(18) NOT NULL ,
    certClassTypeId numeric(18) NOT NULL ,
    formId numeric(18)  ,
    formSourceId numeric(18)  ,
    taxResultTypeId numeric(18)  ,
    certificateStatusId numeric(18) NOT NULL ,
    partyContactId numeric(18)  ,
    txbltyCatId numeric(18)  ,
    txbltyCatSrcId numeric(18)  ,
    certCopyRcvdInd numeric(1)  ,
    industryTypeName varchar(120)  ,
    hardCopyLocationName varchar(510)  ,
    certBlanketInd numeric(1)  ,
    singleUseInd numeric(1)  ,
    invoiceNumber varchar(100)  ,
    replacedByCertificateId numeric(18)  ,
    vertexProductId numeric(18) NOT NULL ,
    completedInd numeric(1) DEFAULT 1 NOT NULL ,
    approvalStatusId numeric(18) DEFAULT 3 NOT NULL ,
    uuid varchar(72)  ,
    ecwCertificateId numeric(18)  ,
    ecwSyncId numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertDtl
 ON CertificateDetail (certificateDetailId, sourceId);

ALTER TABLE CertificateDetail
    ADD CONSTRAINT XPKCertDtl
PRIMARY KEY (certificateDetailId, sourceId);


create table CertificateCoverage
(
    certificateCoverageId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateIdCode varchar(60)  ,
    validationTypeId numeric(18)  ,
    validationDate numeric(8)  ,
    certificateReasonTypeId numeric(18)  ,
    certificateExemptionTypeId numeric(18)  ,
    certIssueDate numeric(8)  ,
    certExpDate numeric(8)  ,
    certReviewDate numeric(8)  ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1  ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertCvrg
 ON CertificateCoverage (certificateCoverageId, sourceId);

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT XPKCertCvrg
PRIMARY KEY (certificateCoverageId, sourceId);


create table CertificateJurisdiction
(
    certificateJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCoverageId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18)  ,
    coverageActionTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertJur
 ON CertificateJurisdiction (certificateJurisdictionId, sourceId);

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT XPKCertJur
PRIMARY KEY (certificateJurisdictionId, sourceId);


create table CertificateImposition
(
    certificateImpositionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCoverageId numeric(18) NOT NULL ,
    jurTypeSetId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18)  ,
    impActiveInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertImp
 ON CertificateImposition (certificateImpositionId, sourceId);

ALTER TABLE CertificateImposition
    ADD CONSTRAINT XPKCertImp
PRIMARY KEY (certificateImpositionId, sourceId);


create table CertImpositionJurisdiction
(
    certImpJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateImpositionId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertImpJur
 ON CertImpositionJurisdiction (certImpJurisdictionId, sourceId);

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT XPKCertImpJur
PRIMARY KEY (certImpJurisdictionId, sourceId);


create table CertificateNote
(
    certNoteText varchar(2000)  ,
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertNote
 ON CertificateNote (certificateId, sourceId);

ALTER TABLE CertificateNote
    ADD CONSTRAINT XPKCertNote
PRIMARY KEY (certificateId, sourceId);


create table CertificateImage
(
    certificateImageId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    imageLocationName varchar(510) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertImage
 ON CertificateImage (certificateImageId, sourceId);

ALTER TABLE CertificateImage
    ADD CONSTRAINT XPKCertImage
PRIMARY KEY (certificateImageId, sourceId);


create table CertTxbltyDriver
(
    certTxbltyDriverId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertTxbltyDvr
 ON CertTxbltyDriver (certTxbltyDriverId);

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT XPKCertTxbltyDvr
PRIMARY KEY (certTxbltyDriverId);


create table CertificateTransType
(
    certTransTypeId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    transactionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertTransType
 ON CertificateTransType (certTransTypeId, sourceId);

ALTER TABLE CertificateTransType
    ADD CONSTRAINT XPKCertTransType
PRIMARY KEY (certTransTypeId, sourceId);


create table CertificateFormField
(
    certificateFormFieldId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    formSourceId numeric(18) NOT NULL ,
    fieldDefId numeric(18) NOT NULL ,
    value varchar(1000)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertFormField
 ON CertificateFormField (certificateFormFieldId, sourceId);

ALTER TABLE CertificateFormField
    ADD CONSTRAINT XPKCertFormField
PRIMARY KEY (certificateFormFieldId, sourceId);


create table ExpirationRuleType
(
    expirationRuleTypeId numeric(18) NOT NULL ,
    expirationRuleTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKExpRuleType
 ON ExpirationRuleType (expirationRuleTypeId);

ALTER TABLE ExpirationRuleType
    ADD CONSTRAINT XPKExpRuleType
PRIMARY KEY (expirationRuleTypeId);


create table CertificateNumReqType
(
    certificateNumReqTypeId numeric(18) NOT NULL ,
    certificateNumReqTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertNumReqTyp
 ON CertificateNumReqType (certificateNumReqTypeId);

ALTER TABLE CertificateNumReqType
    ADD CONSTRAINT XPKCertNumReqTyp
PRIMARY KEY (certificateNumReqTypeId);


create table CertificateNumberFormat
(
    certificateNumberFormatId numeric(18) NOT NULL ,
    certificateNumberMask varchar(60) NOT NULL ,
    certificateNumberExample varchar(60)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertNumFmt
 ON CertificateNumberFormat (certificateNumberFormatId);

ALTER TABLE CertificateNumberFormat
    ADD CONSTRAINT XPKCertNumFmt
PRIMARY KEY (certificateNumberFormatId);


create table CertificateValidationRule
(
    certificateValidationRuleId numeric(18) NOT NULL ,
    certificateNumValidationDesc varchar(200)  ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateReasonTypeId numeric(18) NOT NULL ,
    certificateNumReqTypeId numeric(18) NOT NULL ,
    expirationRuleTypeId numeric(18) NOT NULL ,
    numberOfYears numeric(18)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertValRule
 ON CertificateValidationRule (certificateValidationRuleId);

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT XPKCertValRule
PRIMARY KEY (certificateValidationRuleId);


create table CertificateNumFormatValidation
(
    certNumFormatValidationId numeric(18) NOT NULL ,
    certificateValidationRuleId numeric(18) NOT NULL ,
    certificateNumberFormatId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKCertNumFmtVal
 ON CertificateNumFormatValidation (certNumFormatValidationId);

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT XPKCertNumFmtVal
PRIMARY KEY (certNumFormatValidationId);


create table TaxAuthority
(
    taxAuthorityId numeric(18) NOT NULL ,
    taxAuthorityName varchar(120) NOT NULL ,
    phoneNumber varchar(40)  ,
    webSiteAddress varchar(200)  
);

CREATE UNIQUE INDEX XPKTaxAuth
 ON TaxAuthority (taxAuthorityId);

ALTER TABLE TaxAuthority
    ADD CONSTRAINT XPKTaxAuth
PRIMARY KEY (taxAuthorityId);


create table TaxAuthorityJurisdiction
(
    taxAuthorityJurisdictionId numeric(18) NOT NULL ,
    taxAuthorityId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxAuthJur
 ON TaxAuthorityJurisdiction (taxAuthorityJurisdictionId);

ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT XPKTaxAuthJur
PRIMARY KEY (taxAuthorityJurisdictionId);


create table TaxRegistration
(
    taxRegistrationId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    taxRegistrationIdCode varchar(80)  ,
    taxRegistrationTypeId numeric(18)  ,
    validationTypeId numeric(18)  ,
    validationDate numeric(8)  ,
    formatValidationTypeId numeric(18)  ,
    formatValidationDate numeric(8)  ,
    physicalPresInd numeric(1) NOT NULL ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1  ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCurrencyUnitId numeric(18)  ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxReg
 ON TaxRegistration (taxRegistrationId, sourceId);

ALTER TABLE TaxRegistration
    ADD CONSTRAINT XPKTaxReg
PRIMARY KEY (taxRegistrationId, sourceId);


create table TaxRegistrationJurisdiction
(
    taxRegJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegistrationId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKTxRgJur
 ON TaxRegistrationJurisdiction (taxRegJurisdictionId, sourceId);

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT XPKTxRgJur
PRIMARY KEY (taxRegJurisdictionId, sourceId);


create table TaxRegistrationImposition
(
    taxRegImpositionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegistrationId numeric(18) NOT NULL ,
    taxRegistrationIdCode varchar(80)  ,
    taxRegistrationTypeId numeric(18)  ,
    jurTypeSetId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    impActiveInd numeric(1) NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL ,
    formatValidationTypeId numeric(18)  ,
    formatValidationDate numeric(8)  
);

CREATE UNIQUE INDEX XPKTxRgImp
 ON TaxRegistrationImposition (taxRegImpositionId, sourceId);

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT XPKTxRgImp
PRIMARY KEY (taxRegImpositionId, sourceId);


create table TaxRegImpJurisdiction
(
    taxRegImpJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegImpositionId numeric(18) NOT NULL ,
    taxRegistrationIdCode varchar(80)  ,
    taxRegistrationTypeId numeric(18)  ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL 
);

CREATE UNIQUE INDEX XPKTxRgImpJur
 ON TaxRegImpJurisdiction (taxRegImpJurisdictionId, sourceId);

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT XPKTxRgImpJur
PRIMARY KEY (taxRegImpJurisdictionId, sourceId);


create table LetterTemplateType
(
    letterTemplateTypeId numeric(18) NOT NULL ,
    letterTemplateTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrTempType
 ON LetterTemplateType (letterTemplateTypeId);

ALTER TABLE LetterTemplateType
    ADD CONSTRAINT XPKLtrTempType
PRIMARY KEY (letterTemplateTypeId);


create table LetterDeliveryMethod
(
    letterDeliveryMethodId numeric(18) NOT NULL ,
    letterDeliveryMethodName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrDlvryMthd
 ON LetterDeliveryMethod (letterDeliveryMethodId);

ALTER TABLE LetterDeliveryMethod
    ADD CONSTRAINT XPKLtrDlvryMthd
PRIMARY KEY (letterDeliveryMethodId);


create table LetterTemplate
(
    letterTemplateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    letterTemplateTypeId numeric(18) NOT NULL ,
    letterTemplateName varchar(120) NOT NULL ,
    letterTemplateDesc varchar(200)  ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrTemp
 ON LetterTemplate (letterTemplateId, sourceId);

ALTER TABLE LetterTemplate
    ADD CONSTRAINT XPKLtrTemp
PRIMARY KEY (letterTemplateId, sourceId);


create table LetterTemplateText
(
    letterTemplateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    templateTextSeqNum numeric(18) NOT NULL ,
    templateText varchar(4000)  
);

CREATE UNIQUE INDEX XPKLtrTempText
 ON LetterTemplateText (letterTemplateId, sourceId, templateTextSeqNum);

ALTER TABLE LetterTemplateText
    ADD CONSTRAINT XPKLtrTempText
PRIMARY KEY (letterTemplateId, sourceId, templateTextSeqNum);


create table LetterBatch
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    letterTemplateId numeric(18) NOT NULL ,
    letterTemplateSrcId numeric(18) NOT NULL ,
    certificateStatusId numeric(18)  ,
    daysToExpirationNum numeric(18)  ,
    daysSinceExpirationNum numeric(18)  ,
    expirationRangeStartDate numeric(8)  ,
    expirationRangeEndDate numeric(8)  ,
    noCertificateInd numeric(1)  ,
    incompleteInd numeric(1)  ,
    rejectedInd numeric(1)  ,
    formReplacedInd numeric(1)  ,
    emailSubject varchar(200)  ,
    letterBatchDesc varchar(200)  ,
    letterBatchDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrBatch
 ON LetterBatch (letterBatchId, sourceId);

ALTER TABLE LetterBatch
    ADD CONSTRAINT XPKLtrBatch
PRIMARY KEY (letterBatchId, sourceId);


create table LetterBatchParty
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partyTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrBatchPty
 ON LetterBatchParty (letterBatchId, sourceId, partyId);

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT XPKLtrBatchPty
PRIMARY KEY (letterBatchId, sourceId, partyId);


create table LetterBatchJurisdiction
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKLtrBatchJur
 ON LetterBatchJurisdiction (letterBatchId, sourceId, jurisdictionId);

ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT XPKLtrBatchJur
PRIMARY KEY (letterBatchId, sourceId, jurisdictionId);


create table FlexFieldDefVtxPrdTyp
(
    flexFieldDefId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKFFDefVtxPrdTyp
 ON FlexFieldDefVtxPrdTyp (flexFieldDefId, flexFieldDefSrcId, vertexProductId);

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT XPKFFDefVtxPrdTyp
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId, vertexProductId);


create table Letter
(
    letterId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18)  ,
    overridePartyId numeric(18)  ,
    letterBatchId numeric(18) NOT NULL ,
    letterDeliveryMethodId numeric(18) NOT NULL ,
    formId numeric(18)  ,
    formSrcId numeric(18)  ,
    letterSentDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKLetter
 ON Letter (letterId, sourceId);

ALTER TABLE Letter
    ADD CONSTRAINT XPKLetter
PRIMARY KEY (letterId, sourceId);


create table GeographicRegionType
(
    geoRegionTypeId numeric(18) NOT NULL ,
    geoRegionLicenseCode varchar(60) NOT NULL ,
    geoRegionDisplayName varchar(60) NOT NULL 
);

CREATE UNIQUE INDEX XPKGeoRegionCode
 ON GeographicRegionType (geoRegionTypeId);

ALTER TABLE GeographicRegionType
    ADD CONSTRAINT XPKGeoRegionCode
PRIMARY KEY (geoRegionTypeId);


create table JurGeographicRegType
(
    jurGeoRegTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    geoRegionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKJurGeoRegType
 ON JurGeographicRegType (jurGeoRegTypeId);

ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT XPKJurGeoRegType
PRIMARY KEY (jurGeoRegTypeId);


create table RoleParty
(
    roleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKRoleParty
 ON RoleParty (roleId, sourceId, partyId);

ALTER TABLE RoleParty
    ADD CONSTRAINT XPKRoleParty
PRIMARY KEY (roleId, sourceId, partyId);


create table VATRegistrationIdFormat
(
    vatRegistrationIdFormatId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    vatRegistrationIdMask varchar(80) NOT NULL ,
    vatRegistrationIdExample varchar(80)  ,
    vatRegistrationIdDescription varchar(510)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKVATRegIdFmt
 ON VATRegistrationIdFormat (vatRegistrationIdFormatId);

ALTER TABLE VATRegistrationIdFormat
    ADD CONSTRAINT XPKVATRegIdFmt
PRIMARY KEY (vatRegistrationIdFormatId);


create table CertWizardLocationUser
(
    certWizardUserId numeric(18) NOT NULL ,
    name varchar(100) NOT NULL ,
    userId numeric(18) NOT NULL ,
    sourceId numeric(18)  ,
    partyId numeric(18)  ,
    displayName varchar(200)  
);

CREATE UNIQUE INDEX CertWizardLocationUser_PK
 ON CertWizardLocationUser (certWizardUserId);

ALTER TABLE CertWizardLocationUser
    ADD CONSTRAINT CertWizardLocationUser_PK
PRIMARY KEY (certWizardUserId);


create table users
(
    username varchar(128) NOT NULL ,
    password varchar(510) NOT NULL ,
    enabled numeric(1)  ,
    sourceId numeric(18)  
);

CREATE UNIQUE INDEX users_PK
 ON users (username);

ALTER TABLE users
    ADD CONSTRAINT users_PK
PRIMARY KEY (username);


create table authorities
(
    username varchar(128) NOT NULL ,
    authority varchar(100) NOT NULL ,
    sourceId numeric(18)  
);

create table CertWizardUser
(
    loginId numeric(18) NOT NULL ,
    certWizardUserId numeric(18) NOT NULL ,
    userName varchar(128)  ,
    companyEmail varchar(200) NOT NULL ,
    personalEmail varchar(200)  ,
    oseriesCustomerCode varchar(160)  ,
    firstName varchar(120) NOT NULL ,
    lastName varchar(120) NOT NULL ,
    countryJurId numeric(18) NOT NULL ,
    mainDivisionJurId numeric(18) NOT NULL ,
    cityName varchar(120)  ,
    street1 varchar(200)  ,
    street2 varchar(200)  ,
    postalCode varchar(40)  ,
    phoneNumber varchar(40)  ,
    securityQuestion1Id numeric(18) NOT NULL ,
    securityQuestion2Id numeric(18) NOT NULL ,
    securityQuestion3Id numeric(18) NOT NULL ,
    securityAnswer1 varchar(200) NOT NULL ,
    securityAnswer2 varchar(200) NOT NULL ,
    securityAnswer3 varchar(200) NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    newCustomerInd numeric(1)  
);

CREATE UNIQUE INDEX CertWizardUser_PK
 ON CertWizardUser (loginId);

ALTER TABLE CertWizardUser
    ADD CONSTRAINT CertWizardUser_PK
PRIMARY KEY (loginId);


create table SecurityQuestions
(
    securityQuestionId numeric(18) NOT NULL ,
    name varchar(510) NOT NULL ,
    question varchar(510) NOT NULL 
);

CREATE UNIQUE INDEX SecurityQuestions_PK
 ON SecurityQuestions (securityQuestionId);

ALTER TABLE SecurityQuestions
    ADD CONSTRAINT SecurityQuestions_PK
PRIMARY KEY (securityQuestionId);


create table DataUpdateImpactTaxRule
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    geoRegionTypeId numeric(18) NOT NULL ,
    countryName varchar(120) NOT NULL ,
    mainDivisionName varchar(120)  ,
    jurName varchar(120) NOT NULL ,
    jurTypeName varchar(120) NOT NULL ,
    impositionName varchar(120) NOT NULL ,
    categoryName varchar(120) NOT NULL ,
    taxRuleType varchar(120) NOT NULL ,
    rate varchar(120)  ,
    salesTaxHolidayInd numeric(1) DEFAULT 0 NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType varchar(120) NOT NULL ,
    details varchar(2000)  ,
    newCategoryInd numeric(1) DEFAULT 0 NOT NULL 
);

CREATE UNIQUE INDEX XPKDataImpTaxRule
 ON DataUpdateImpactTaxRule (dataUpdateNumber, taxRuleId, geoRegionTypeId);

ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT XPKDataImpTaxRule
PRIMARY KEY (dataUpdateNumber, taxRuleId, geoRegionTypeId);


create table DataUpdateImpactTaxArea
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxAreaId numeric(18) NOT NULL ,
    countryName varchar(120) NOT NULL ,
    mainDivisionName varchar(120)  ,
    subDivisionName varchar(120)  ,
    cityName varchar(120)  ,
    districtName1 varchar(120)  ,
    districtName2 varchar(120)  ,
    districtName3 varchar(120)  ,
    districtName4 varchar(120)  ,
    districtName5 varchar(120)  ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType varchar(120) NOT NULL ,
    affectedTaxAreaId numeric(18) NOT NULL ,
    affectedCountryName varchar(120)  ,
    affectedMainDivisionName varchar(120)  ,
    affectedSubDivisionName varchar(120)  ,
    affectedCityName varchar(120)  ,
    affectedDistrictName1 varchar(120)  ,
    affectedDistrictName2 varchar(120)  ,
    affectedDistrictName3 varchar(120)  ,
    affectedDistrictName4 varchar(120)  ,
    affectedDistrictName5 varchar(120)  
);

CREATE UNIQUE INDEX XPKDataImpTaxArea
 ON DataUpdateImpactTaxArea (dataUpdateNumber, taxAreaId, affectedTaxAreaId);

ALTER TABLE DataUpdateImpactTaxArea
    ADD CONSTRAINT XPKDataImpTaxArea
PRIMARY KEY (dataUpdateNumber, taxAreaId, affectedTaxAreaId);


create table TransactionStatusType
(
    transStatusTypeId numeric(18) NOT NULL ,
    transStatusTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTransStatusType
 ON TransactionStatusType (transStatusTypeId);

ALTER TABLE TransactionStatusType
    ADD CONSTRAINT XPKTransStatusType
PRIMARY KEY (transStatusTypeId);


create table FeatureResource
(
    featureResourceId numeric(18) NOT NULL ,
    featureResourceName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKFeatureResource
 ON FeatureResource (featureResourceId);

ALTER TABLE FeatureResource
    ADD CONSTRAINT XPKFeatureResource
PRIMARY KEY (featureResourceId);


create table FeatureResourceImpositionType
(
    featureResourceId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKFeatResImpType
 ON FeatureResourceImpositionType (featureResourceId, impsnTypeId);

ALTER TABLE FeatureResourceImpositionType
    ADD CONSTRAINT XPKFeatResImpType
PRIMARY KEY (featureResourceId, impsnTypeId);


create table FeatureResourceCategory
(
    featureResourceId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKFeatResCat
 ON FeatureResourceCategory (featureResourceId, txbltyCatId);

ALTER TABLE FeatureResourceCategory
    ADD CONSTRAINT XPKFeatResCat
PRIMARY KEY (featureResourceId, txbltyCatId);


create table TransactionEventType
(
    transEventTypeId numeric(18) NOT NULL ,
    transEventTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTransEventType
 ON TransactionEventType (transEventTypeId);

ALTER TABLE TransactionEventType
    ADD CONSTRAINT XPKTransEventType
PRIMARY KEY (transEventTypeId);


create table TpsDataReleaseEvent
(
    fullRlsId numeric(18)  ,
    interimRlsId numeric(18)  ,
    rlsTypeId numeric(1)  ,
    appliedDate numeric(8) NOT NULL ,
    rlsName varchar(510)  
);

create table TaxAssistPhaseType
(
    phaseId numeric(18) NOT NULL ,
    phaseName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKTaxAstPhaseType
 ON TaxAssistPhaseType (phaseId);

ALTER TABLE TaxAssistPhaseType
    ADD CONSTRAINT XPKTaxAstPhaseType
PRIMARY KEY (phaseId);


create table VATRegimeType
(
    vatRegimeTypeId numeric(18) NOT NULL ,
    vatRegimeTypeName varchar(120) NOT NULL 
);

CREATE UNIQUE INDEX XPKVATRegimeType
 ON VATRegimeType (vatRegimeTypeId);

ALTER TABLE VATRegimeType
    ADD CONSTRAINT XPKVATRegimeType
PRIMARY KEY (vatRegimeTypeId);


create table VATGroup
(
    vatGroupId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    vatGrpCreationDate numeric(8) NOT NULL 
);

CREATE UNIQUE INDEX XPKVATGroup
 ON VATGroup (vatGroupId, vatGroupSourceId);

ALTER TABLE VATGroup
    ADD CONSTRAINT XPKVATGroup
PRIMARY KEY (vatGroupId, vatGroupSourceId);


create table VATGroupDetail
(
    vatGroupDtlId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    vatGroupId numeric(18) NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    repMemTxprId numeric(18) NOT NULL ,
    vatGroupIdentifier varchar(30) NOT NULL ,
    vatGroupName varchar(60) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    vatRegimeTypeId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKVATGroupDtl
 ON VATGroupDetail (vatGroupDtlId, vatGroupSourceId);

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT XPKVATGroupDtl
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId);


create table VATGroupMembers
(
    vatGroupDtlId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    memTxprId numeric(18) NOT NULL 
);

CREATE UNIQUE INDEX XPKVATGroupMembers
 ON VATGroupMembers (vatGroupDtlId, vatGroupSourceId, memTxprId);

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT XPKVATGroupMembers
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId, memTxprId);


ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT f1JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE DMFilterDate
    ADD CONSTRAINT f1DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f2LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1SCdTyp FOREIGN KEY (situsCondTypeId)
    REFERENCES SitusConditionType (situsCondTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1CstStT FOREIGN KEY (customsStatusId)
    REFERENCES CustomsStatusType (customsStatusId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1MvMthT FOREIGN KEY (movementMethodId)
    REFERENCES MovementMethodType (movementMethodId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1TtTfrT FOREIGN KEY (titleTransferId)
    REFERENCES TitleTransferType (titleTransferId)
;

ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT f7RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE SitusCondJur
    ADD CONSTRAINT f2StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT f4StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f2FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f3FilCat FOREIGN KEY (ovrdFilingCatId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f5TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1OutNot FOREIGN KEY (outputNoticeId)
    REFERENCES OutputNotice (outputNoticeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1SCcTyp FOREIGN KEY (situsConcTypeId)
    REFERENCES SitusConcType (situsConcTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3TaxTyp FOREIGN KEY (taxType2Id)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE RoundingRule
    ADD CONSTRAINT f2TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

ALTER TABLE DMFilterStrng
    ADD CONSTRAINT f3DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f2TaxCat FOREIGN KEY (prntTxbltyCatId, prntTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f2DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f5TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f3FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxJurDetail
    ADD CONSTRAINT f16TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT f2DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT f1DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT f4DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

ALTER TABLE SitusConditionNode
    ADD CONSTRAINT f1StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f1StCdNd FOREIGN KEY (prntTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f2StCdNd FOREIGN KEY (chldTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f1StsCnc FOREIGN KEY (situsConclusionId)
    REFERENCES SitusConclusion (situsConclusionId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f5StCdNd FOREIGN KEY (truForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f6StCdNd FOREIGN KEY (flsForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1ShpTrm FOREIGN KEY (shippingTermsId)
    REFERENCES ShippingTerms (shippingTermsId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1PtyTyp FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f2Party FOREIGN KEY (parentPartyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f33Party FOREIGN KEY (parentCustomerId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f5DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f5RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f1PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f1VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f8Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE BusinessLocation
    ADD CONSTRAINT f4Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE BusinessLocation
    ADD CONSTRAINT f3PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE PartyNote
    ADD CONSTRAINT f14Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyContact
    ADD CONSTRAINT f6Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyContact
    ADD CONSTRAINT f1CntcRT FOREIGN KEY (contactRoleTypeId)
    REFERENCES ContactRoleType (contactRoleTypeId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f1TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f8RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2InPTyp FOREIGN KEY (inputParamTypeId)
    REFERENCES InputParameterType (inputParamTypeId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f3DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT f9TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f2TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f8VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f1conv FOREIGN KEY (telecomUnitConversionId, sourceId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f2LineType FOREIGN KEY (lineTypeId, lineTypeSourceId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f7TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f24Party FOREIGN KEY (taxpayerPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f25Party FOREIGN KEY (otherPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f5TaxDvr FOREIGN KEY (txbltyDvrId, txbltyCatMapSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f1TxCMap FOREIGN KEY (txbltyCatMapId, txbltyCatMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f1InvText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f2InvTextTyp FOREIGN KEY (invoiceTextTypeId, invoiceTextTypeSrcId)
    REFERENCES InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId)
;

ALTER TABLE DMFilterInd
    ADD CONSTRAINT f4DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

ALTER TABLE DiscountType
    ADD CONSTRAINT f1DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE DiscountType
    ADD CONSTRAINT f2DscCat FOREIGN KEY (taxpayerPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TxSTyp FOREIGN KEY (taxStructureTypeId)
    REFERENCES TaxStructureType (taxStructureTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1BTCTyp FOREIGN KEY (brcktTaxCalcType)
    REFERENCES BracketTaxCalcType (brcktTaxCalcTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1DedTyp FOREIGN KEY (reductAmtDedTypeId)
    REFERENCES DeductionType (deductionTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TelCon FOREIGN KEY (telecomUnitConversionId, telecomUnitConversionSrcId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

ALTER TABLE ImpositionType
    ADD CONSTRAINT f1WitTyp FOREIGN KEY (withholdingTypeId)
    REFERENCES WithholdingType (withholdingTypeId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1ImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f6TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f18TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f9TaxImp FOREIGN KEY (taxImpsnDtlId, taxImpsnSrcId)
    REFERENCES TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId)
;

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f12TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE FilingOverride
    ADD CONSTRAINT f5FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingOverride
    ADD CONSTRAINT f5JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f2TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f5TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f4RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f6FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f3RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

ALTER TABLE Bracket
    ADD CONSTRAINT f4TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TRlTyp FOREIGN KEY (taxRuleTypeId)
    REFERENCES TaxRuleType (taxRuleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f3RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1DscTyp FOREIGN KEY (discountTypeId, discountTypeSrcId)
    REFERENCES DiscountType (discountTypeId, sourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f6TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f4DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f28Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f30Party FOREIGN KEY (taxpayerPartyId, taxpayerPartySrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1ApprtTyp FOREIGN KEY (apportionTypeId)
    REFERENCES ApportionmentType (apportionTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f7LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1InvoiceText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2ImpTyp FOREIGN KEY (defrdImpsnTypeId, defrdImpsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RcvRTyp FOREIGN KEY (recoverableResultTypeId)
    REFERENCES RecoverableResultType (recoverableResultTypeId)
;

ALTER TABLE TaxRuleNote
    ADD CONSTRAINT f4TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f8TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f4TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f7TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f1FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f2ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f14TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f9TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f7TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f9TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxCat FOREIGN KEY (overTxbltyCatId, overTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f11TaxCat FOREIGN KEY (underTxbltyCatId, underTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f12LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f13TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f8TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f4FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f1TxFcTp FOREIGN KEY (taxFactorTypeId)
    REFERENCES TaxFactorType (taxFactorTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f2BssTyp FOREIGN KEY (basisTypeId)
    REFERENCES BasisType (basisTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f17LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f19TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f3TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f4TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f5TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f1CmpTyp FOREIGN KEY (computationTypeId)
    REFERENCES ComputationType (computationTypeId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f2TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f6TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f7TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f3TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f2TxCMap FOREIGN KEY (txbltyCatMapId, taxabilityMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f4StCdNd FOREIGN KEY (chldFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f3StCdNd FOREIGN KEY (prntFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT f3DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT f5DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT f5TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT f3StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT f10TxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE DMFilterNum
    ADD CONSTRAINT f2DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

ALTER TABLE SitusTreatment
    ADD CONSTRAINT f7StCdNd FOREIGN KEY (situsCondNodeId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f1Trmt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f2VxtPrd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f5PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f4TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f6LRlTyp FOREIGN KEY (locationRoleTyp2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f5LRlTyp FOREIGN KEY (locationRoleTyp1Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f1SitTrt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT f1SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f2SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f1TrTypP FOREIGN KEY (transTypePrspctvId)
    REFERENCES TransTypePrspctv (transTypePrspctvId)
;

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f22Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f1RndRul FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f2RndRule FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f10TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT f27Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxAssistRule
    ADD CONSTRAINT f2VtxProduct FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT f1TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT f2TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT f1TxAstLookup FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT f1TxAsLk FOREIGN KEY (tableId, sourceId)
    REFERENCES TaxAssistLookup (tableId, sourceId)
;

ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT f1TxAstAlloc FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsAC FOREIGN KEY (allocationTableId, sourceId)
    REFERENCES TaxAssistAllocationTable (allocationTableId, sourceId)
;

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsACDT FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACV FOREIGN KEY (allocationTableId, sourceId, columnId)
    REFERENCES TaxAssistAllocationColumn (allocationTableId, sourceId, columnId)
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACVNT FOREIGN KEY (numericTypeId)
    REFERENCES NumericType (numericTypeId)
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1ImpCrtExmTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1CrtExmTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE FormDetail
    ADD CONSTRAINT f1FormDtlForm FOREIGN KEY (replacedByFormId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE FormDetail
    ADD CONSTRAINT f2FormDtlForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE FormJurisdiction
    ADD CONSTRAINT f1FormJurForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE FormFieldDef
    ADD CONSTRAINT f1FormFieldDef FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1Certificate FOREIGN KEY (replacedByCertificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtParty FOREIGN KEY (certificateHolderPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtClsTyp FOREIGN KEY (certClassTypeId)
    REFERENCES CertClassType (certClassTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtTxRsltTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtPtyCntct FOREIGN KEY (partyContactId, sourceId)
    REFERENCES PartyContact (partyContactId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f5VtxProd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtForm FOREIGN KEY (formId, formSourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f2CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f3CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurImp FOREIGN KEY (certificateImpositionId, sourceId)
    REFERENCES CertificateImposition (certificateImpositionId, sourceId)
;

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE CertificateNote
    ADD CONSTRAINT f2Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateImage
    ADD CONSTRAINT f5Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE CertificateTransType
    ADD CONSTRAINT f1CertTransTypCrt FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateTransType
    ADD CONSTRAINT f2CertTransTypTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleExpTyp FOREIGN KEY (expirationRuleTypeId)
    REFERENCES ExpirationRuleType (expirationRuleTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleReqTyp FOREIGN KEY (certificateNumReqTypeId)
    REFERENCES CertificateNumReqType (certificateNumReqTypeId)
;

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValRule FOREIGN KEY (certificateValidationRuleId)
    REFERENCES CertificateValidationRule (certificateValidationRuleId)
;

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValNFmt FOREIGN KEY (certificateNumberFormatId)
    REFERENCES CertificateNumberFormat (certificateNumberFormatId)
;

ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT f1TaxAuthJur FOREIGN KEY (taxAuthorityId)
    REFERENCES TaxAuthority (taxAuthorityId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgTxpyr FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurImp FOREIGN KEY (taxRegImpositionId, sourceId)
    REFERENCES TaxRegistrationImposition (taxRegImpositionId, sourceId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

ALTER TABLE LetterTemplate
    ADD CONSTRAINT f1LtrTmpType FOREIGN KEY (letterTemplateTypeId)
    REFERENCES LetterTemplateType (letterTemplateTypeId)
;

ALTER TABLE LetterTemplateText
    ADD CONSTRAINT f1LtrTmp FOREIGN KEY (letterTemplateId, sourceId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

ALTER TABLE LetterBatch
    ADD CONSTRAINT f1LtrBtchCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE LetterBatch
    ADD CONSTRAINT f2LtrBtchTmp FOREIGN KEY (letterTemplateId, letterTemplateSrcId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f1LtrBtchPty FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f2LtrBtchPty FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f3LtrBtchPty FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT f1LtrBtchJur FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f1FFDrfVtxPrdTyp FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f2FFDrfVtxPrdTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f1LtrDlvryMethod FOREIGN KEY (letterDeliveryMethodId)
    REFERENCES LetterDeliveryMethod (letterDeliveryMethodId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f2LtrLtrBatch FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f3LtrCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f4LtrForm FOREIGN KEY (formId, formSrcId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f5LtrParty FOREIGN KEY (overridePartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT f1GeoRegionType FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

ALTER TABLE authorities
    ADD CONSTRAINT f1Auth FOREIGN KEY (username)
    REFERENCES users (username)
;

ALTER TABLE CertWizardUser
    ADD CONSTRAINT f2Users FOREIGN KEY (userName)
    REFERENCES users (username)
;

ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT f1GeoRegionType1 FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VatGroup FOREIGN KEY (vatGroupId, vatGroupSourceId)
    REFERENCES VATGroup (vatGroupId, vatGroupSourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f31Party FOREIGN KEY (repMemTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VATRegimeType FOREIGN KEY (vatRegimeTypeId)
    REFERENCES VATRegimeType (vatRegimeTypeId)
;

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f1VATGroupDtl FOREIGN KEY (vatGroupDtlId, vatGroupSourceId)
    REFERENCES VATGroupDetail (vatGroupDtlId, vatGroupSourceId)
;

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

