create table CoverageActionType
(
    coverageActionTypeId numeric(18) NOT NULL ,
    coverageActionTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE CoverageActionType
    ADD CONSTRAINT XPKJICvrgActTyp
PRIMARY KEY (coverageActionTypeId);


create table CertificateStatus
(
    certificateStatusId numeric(18) NOT NULL ,
    certificateStatusName nvarchar(60) NOT NULL 
);

ALTER TABLE CertificateStatus
    ADD CONSTRAINT XPKJICertStat
PRIMARY KEY (certificateStatusId);


create table CertificateApprovalStatus
(
    approvalStatusId numeric(18) NOT NULL ,
    certApprovalStatusName nvarchar(60) NOT NULL 
);

ALTER TABLE CertificateApprovalStatus
    ADD CONSTRAINT XPKCertApprStatus
PRIMARY KEY (approvalStatusId);


create table ContactRoleType
(
    contactRoleTypeId numeric(18) NOT NULL ,
    contactRoleTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE ContactRoleType
    ADD CONSTRAINT XPKContactRoleType
PRIMARY KEY (contactRoleTypeId);


create table FormFieldType
(
    formFieldTypeId numeric(18) NOT NULL ,
    name nvarchar(60) NULL 
);

ALTER TABLE FormFieldType
    ADD CONSTRAINT XPKFormField
PRIMARY KEY (formFieldTypeId);


create table TaxRegistrationType
(
    taxRegistrationTypeId numeric(18) NOT NULL ,
    taxRegistrationTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxRegistrationType
    ADD CONSTRAINT XPKTaxRegType
PRIMARY KEY (taxRegistrationTypeId);


create table ApportionmentType
(
    apportionTypeId numeric(18) NOT NULL ,
    apportionTypeName nvarchar(30) NOT NULL 
);

ALTER TABLE ApportionmentType
    ADD CONSTRAINT XPKApportionType
PRIMARY KEY (apportionTypeId);


create table SitusConditionType
(
    situsCondTypeId numeric(18) NOT NULL ,
    situsCondTypeName nvarchar(30) NULL 
);

ALTER TABLE SitusConditionType
    ADD CONSTRAINT XPKSitusCondType
PRIMARY KEY (situsCondTypeId);


create table ChainTransType
(
    chainTransId numeric(18) NOT NULL ,
    chainTransName nvarchar(60) NULL 
);

ALTER TABLE ChainTransType
    ADD CONSTRAINT XPKChainTransType
PRIMARY KEY (chainTransId);


create table TitleTransferType
(
    titleTransferId numeric(18) NOT NULL ,
    titleTransferName nvarchar(60) NULL 
);

ALTER TABLE TitleTransferType
    ADD CONSTRAINT XPKTitleTransType
PRIMARY KEY (titleTransferId);


create table AssistedState
(
    assistedStateId numeric(18) NOT NULL ,
    assistedStateName nvarchar(60) NULL 
);

ALTER TABLE AssistedState
    ADD CONSTRAINT XPKAssistedState
PRIMARY KEY (assistedStateId);


create table RateClassification
(
    rateClassId numeric(18) NOT NULL ,
    rateClassName nvarchar(60) NULL 
);

ALTER TABLE RateClassification
    ADD CONSTRAINT XPKRateClass
PRIMARY KEY (rateClassId);


create table DMFilter
(
    filterId numeric(18) NOT NULL ,
    filterName nvarchar(60) NOT NULL ,
    sourceId numeric(18) NULL ,
    activityTypeId numeric(18) NOT NULL ,
    filterDescription nvarchar(255) NULL ,
    followupInd numeric(1) NULL 
);

ALTER TABLE DMFilter
    ADD CONSTRAINT XPKDMFilter
PRIMARY KEY (filterId);


create table BusinessTransType
(
    busTransTypeId numeric(18) NOT NULL ,
    busTransTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE BusinessTransType
    ADD CONSTRAINT XPKBusTransType
PRIMARY KEY (busTransTypeId);


create table TransactionType
(
    transactionTypeId numeric(18) NOT NULL ,
    transactionTypName nvarchar(60) NOT NULL 
);

ALTER TABLE TransactionType
    ADD CONSTRAINT XPKTransactionType
PRIMARY KEY (transactionTypeId);


create table DataType
(
    dataTypeId numeric(18) NOT NULL ,
    dataTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE DataType
    ADD CONSTRAINT XPKDataType
PRIMARY KEY (dataTypeId);


create table InputParameterType
(
    inputParamTypeId numeric(18) NOT NULL ,
    inputParamTypeName nvarchar(60) NULL ,
    lookupStrategyId numeric(18) NULL ,
    commodityCodeInd numeric(1) NULL ,
    commodityCodeLength numeric(18) NULL ,
    isTelecommLineType numeric(1) NULL 
);

ALTER TABLE InputParameterType
    ADD CONSTRAINT XPKInputParamType
PRIMARY KEY (inputParamTypeId);


create table JurTypeSet
(
    jurTypeSetId numeric(18) NOT NULL ,
    jurTypeSetName nvarchar(60) NULL 
);

ALTER TABLE JurTypeSet
    ADD CONSTRAINT XPKJurTypeSet
PRIMARY KEY (jurTypeSetId);


create table JurTypeSetMember
(
    jurTypeSetId numeric(18) NOT NULL ,
    jurisdictionTypeId numeric(18) NOT NULL 
);

ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT XPKJurTypeSetMem
PRIMARY KEY (jurTypeSetId, jurisdictionTypeId);


ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT f1JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

create table TaxResultType
(
    taxResultTypeId numeric(18) NOT NULL ,
    taxResultTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxResultType
    ADD CONSTRAINT XPKTaxResultType
PRIMARY KEY (taxResultTypeId);


create table RecoverableResultType
(
    recoverableResultTypeId numeric(18) NOT NULL ,
    recoverableResultTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE RecoverableResultType
    ADD CONSTRAINT XPKRcvResultType
PRIMARY KEY (recoverableResultTypeId);


create table PartyType
(
    partyTypeId numeric(18) NOT NULL ,
    partyTypeName nvarchar(20) NOT NULL 
);

ALTER TABLE PartyType
    ADD CONSTRAINT XPKPartyType
PRIMARY KEY (partyTypeId);


create table LocationRoleType
(
    locationRoleTypeId numeric(18) NOT NULL ,
    locationRoleTypNam nvarchar(30) NOT NULL 
);

ALTER TABLE LocationRoleType
    ADD CONSTRAINT XPKLocationRoleTyp
PRIMARY KEY (locationRoleTypeId);


create table ShippingTerms
(
    shippingTermsId numeric(18) NOT NULL ,
    shippingTermsName nvarchar(60) NOT NULL 
);

ALTER TABLE ShippingTerms
    ADD CONSTRAINT XPKShippingTerms
PRIMARY KEY (shippingTermsId);


create table BasisType
(
    basisTypeId numeric(18) NOT NULL ,
    basisTypeName nvarchar(60) NULL 
);

ALTER TABLE BasisType
    ADD CONSTRAINT XPKBasisType
PRIMARY KEY (basisTypeId);


create table DMFilterDate
(
    dateTypeId numeric(18) NOT NULL ,
    criteriaDate numeric(8) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

ALTER TABLE DMFilterDate
    ADD CONSTRAINT XPKDMFilterDate
PRIMARY KEY (filterId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMFilterDate
    ADD CONSTRAINT f1DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table OutputNotice
(
    outputNoticeId numeric(18) NOT NULL ,
    outputNoticeName nvarchar(30) NOT NULL 
);

ALTER TABLE OutputNotice
    ADD CONSTRAINT XPKOutputNotice
PRIMARY KEY (outputNoticeId);


create table CustomsStatusType
(
    customsStatusId numeric(18) NOT NULL ,
    customsStatusName nvarchar(60) NOT NULL 
);

ALTER TABLE CustomsStatusType
    ADD CONSTRAINT XPKCustomsStatType
PRIMARY KEY (customsStatusId);


create table CreationSource
(
    creationSourceId numeric(18) NOT NULL ,
    name nvarchar(60) NOT NULL 
);

ALTER TABLE CreationSource
    ADD CONSTRAINT XPKCreationSource
PRIMARY KEY (creationSourceId);


create table MovementMethodType
(
    movementMethodId numeric(18) NOT NULL ,
    movementMethodName nvarchar(60) NOT NULL 
);

ALTER TABLE MovementMethodType
    ADD CONSTRAINT XPKMvmntMethodType
PRIMARY KEY (movementMethodId);


create table SitusCondition
(
    situsConditionId numeric(18) NOT NULL ,
    locRoleTypeId numeric(18) NULL ,
    locRoleType2Id numeric(18) NULL ,
    situsCondTypeId numeric(18) NULL ,
    transactionTypeId numeric(18) NULL ,
    transPrspctvTypId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    jurisdictionTypeId numeric(18) NULL ,
    endDate numeric(8) NOT NULL ,
    jurTypeSetId numeric(18) NULL ,
    partyRoleTypeId numeric(18) NULL ,
    currencyUnitId numeric(18) NULL ,
    locRoleTypeId1Name nvarchar(60) NULL ,
    locRoleTypeId2Name nvarchar(60) NULL ,
    stsSubRtnNodeId numeric(18) NULL ,
    boolFactName nvarchar(60) NULL ,
    boolFactValue numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    customsStatusId numeric(18) NULL ,
    movementMethodId numeric(18) NULL ,
    titleTransferId numeric(18) NULL 
);

ALTER TABLE SitusCondition
    ADD CONSTRAINT XPKSitusCondition
PRIMARY KEY (situsConditionId);


ALTER TABLE SitusCondition
    ADD CONSTRAINT f2LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1SCdTyp FOREIGN KEY (situsCondTypeId)
    REFERENCES SitusConditionType (situsCondTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1CstStT FOREIGN KEY (customsStatusId)
    REFERENCES CustomsStatusType (customsStatusId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1MvMthT FOREIGN KEY (movementMethodId)
    REFERENCES MovementMethodType (movementMethodId)
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1TtTfrT FOREIGN KEY (titleTransferId)
    REFERENCES TitleTransferType (titleTransferId)
;

create table BracketTaxCalcType
(
    brcktTaxCalcTypeId numeric(18) NOT NULL ,
    brcktTaxCalcTypNam nvarchar(60) NULL 
);

ALTER TABLE BracketTaxCalcType
    ADD CONSTRAINT XPKBrcktTaxCalcTyp
PRIMARY KEY (brcktTaxCalcTypeId);


create table SitusConcType
(
    situsConcTypeId numeric(18) NOT NULL ,
    situsConcTypeName nvarchar(60) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusConcType
    ADD CONSTRAINT XPKSitusConcType
PRIMARY KEY (situsConcTypeId);


create table ReasonCategory
(
    reasonCategoryId numeric(18) NOT NULL ,
    reasonCategoryName nvarchar(60) NULL ,
    isUserDefined numeric(1) NULL 
);

ALTER TABLE ReasonCategory
    ADD CONSTRAINT XPKReasonCategory
PRIMARY KEY (reasonCategoryId);


create table ReasonCategoryJur
(
    reasonCategoryId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NULL 
);

ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT XPKReasonCatJur
PRIMARY KEY (reasonCategoryId, jurisdictionId, effDate);


ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT f7RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

create table SitusCondJur
(
    situsConditionId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

ALTER TABLE SitusCondJur
    ADD CONSTRAINT XPKSitusCondJur
PRIMARY KEY (situsConditionId, jurisdictionId);


ALTER TABLE SitusCondJur
    ADD CONSTRAINT f2StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table SitusCondShippingTerms
(
    situsConditionId numeric(18) NOT NULL ,
    shippingTermsId numeric(18) NOT NULL 
);

ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT XPKSitusCondShipTm
PRIMARY KEY (situsConditionId, shippingTermsId);


ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT f4StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TaxType
(
    taxTypeId numeric(18) NOT NULL ,
    taxTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxType
    ADD CONSTRAINT XPKTaxType
PRIMARY KEY (taxTypeId);


create table FilingCategory
(
    filingCategoryId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    filingCategoryCode numeric(5) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCategoryName nvarchar(60) NOT NULL ,
    primaryCategoryInd numeric(1) NOT NULL 
);

ALTER TABLE FilingCategory
    ADD CONSTRAINT XPKFilingCat
PRIMARY KEY (filingCategoryId);


create table FilingCategoryOvrd
(
    filingCatOvrdId numeric(18) NOT NULL ,
    filingCategoryId numeric(18) NOT NULL ,
    ovrdFilingCatId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT XPKFCOvd
PRIMARY KEY (filingCatOvrdId);


ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f2FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f3FilCat FOREIGN KEY (ovrdFilingCatId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f5TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table SitusConclusion
(
    situsConclusionId numeric(18) NOT NULL ,
    situsConcTypeId numeric(18) NULL ,
    taxTypeId numeric(18) NULL ,
    jurisdictionTypeId numeric(18) NULL ,
    locRoleTypeId numeric(18) NULL ,
    locRoleType2Id numeric(18) NULL ,
    multiSitusRecTypId numeric(18) NULL ,
    jurTypeSetId numeric(18) NULL ,
    taxType2Id numeric(18) NULL ,
    locRoleTypeId1Name nvarchar(60) NULL ,
    locRoleTypeId2Name nvarchar(60) NULL ,
    taxTypeIdName nvarchar(60) NULL ,
    boolFactName nvarchar(60) NULL ,
    boolFactValue numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    outputNoticeId numeric(18) NULL ,
    impsnTypeId numeric(18) NULL ,
    impsnTypeSourceId numeric(18) NULL ,
    txbltyCatId numeric(18) NULL 
);

ALTER TABLE SitusConclusion
    ADD CONSTRAINT XPKSitusConc
PRIMARY KEY (situsConclusionId);


ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1OutNot FOREIGN KEY (outputNoticeId)
    REFERENCES OutputNotice (outputNoticeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1SCcTyp FOREIGN KEY (situsConcTypeId)
    REFERENCES SitusConcType (situsConcTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3TaxTyp FOREIGN KEY (taxType2Id)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxScope
(
    taxScopeId numeric(18) NOT NULL ,
    taxScopeName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxScope
    ADD CONSTRAINT XPKTaxScope
PRIMARY KEY (taxScopeId);


create table RoundingRule
(
    roundingRuleId numeric(18) NOT NULL ,
    initialPrecision numeric(15) NULL ,
    taxScopeId numeric(18) NOT NULL ,
    finalPrecision numeric(15) NOT NULL ,
    threshold numeric(15) NOT NULL ,
    decimalPosition numeric(15) NOT NULL ,
    roundingTypeId numeric(18) NULL 
);

ALTER TABLE RoundingRule
    ADD CONSTRAINT XPKRoundingRule
PRIMARY KEY (roundingRuleId);


ALTER TABLE RoundingRule
    ADD CONSTRAINT f2TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

create table TransSubType
(
    transSubTypeId numeric(18) NOT NULL ,
    transSubTypeName nvarchar(60) NULL 
);

ALTER TABLE TransSubType
    ADD CONSTRAINT XPKTransSubType
PRIMARY KEY (transSubTypeId);


create table VertexProductType
(
    vertexProductId numeric(18) NOT NULL ,
    vertexProductName nvarchar(60) NULL 
);

ALTER TABLE VertexProductType
    ADD CONSTRAINT XPKVertexProdType
PRIMARY KEY (vertexProductId);


create table PartyRoleType
(
    partyRoleTypeId numeric(18) NOT NULL ,
    partyRoleTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE PartyRoleType
    ADD CONSTRAINT XPKPartyRoleType
PRIMARY KEY (partyRoleTypeId);


create table TelecomUnitConversion
(
    telecomUnitConversionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    name nvarchar(60) NOT NULL ,
    sourceUnitOfMeasureISOCode nvarchar(20) NOT NULL ,
    targetUnitOfMeasureISOCode nvarchar(20) NOT NULL ,
    firstConvertCount numeric(8) NOT NULL ,
    additionalConvertCount numeric(8) NULL ,
    isDefault numeric(1) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversion
    ADD CONSTRAINT XPKTelComUntConvsn
PRIMARY KEY (telecomUnitConversionId, sourceId);


create table TaxStructureType
(
    taxStructureTypeId numeric(18) NOT NULL ,
    taxStrucTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxStructureType
    ADD CONSTRAINT XPKTaxStrucType
PRIMARY KEY (taxStructureTypeId);


create table DeductionType
(
    deductionTypeId numeric(18) NOT NULL ,
    deductionTypeName nvarchar(60) NULL 
);

ALTER TABLE DeductionType
    ADD CONSTRAINT XPKDeductionType
PRIMARY KEY (deductionTypeId);


create table TransOrigType
(
    transOrigTypeId numeric(18) NOT NULL ,
    transOrigTypeName nvarchar(60) NULL 
);

ALTER TABLE TransOrigType
    ADD CONSTRAINT XPKTransOrigType
PRIMARY KEY (transOrigTypeId);


create table TaxRuleType
(
    taxRuleTypeId numeric(18) NOT NULL ,
    taxRuleTypeName nvarchar(60) NULL 
);

ALTER TABLE TaxRuleType
    ADD CONSTRAINT XPKTaxRuleType
PRIMARY KEY (taxRuleTypeId);


create table WithholdingType
(
    withholdingTypeId numeric(18) NOT NULL ,
    withholdingTypeName nvarchar(60) NULL 
);

ALTER TABLE WithholdingType
    ADD CONSTRAINT XPKWithholdingType
PRIMARY KEY (withholdingTypeId);


create table AccumulationByType
(
    id numeric(18) NOT NULL ,
    name nvarchar(60) NULL 
);

ALTER TABLE AccumulationByType
    ADD CONSTRAINT XPKAccByType
PRIMARY KEY (id);


create table AccumulationPeriodType
(
    id numeric(18) NOT NULL ,
    name nvarchar(60) NULL 
);

ALTER TABLE AccumulationPeriodType
    ADD CONSTRAINT XPKAccPeriodType
PRIMARY KEY (id);


create table AccumulationType
(
    id numeric(18) NOT NULL ,
    name nvarchar(60) NULL 
);

ALTER TABLE AccumulationType
    ADD CONSTRAINT XPKAccType
PRIMARY KEY (id);


create table TaxRuleTaxImpositionType
(
    taxRuleTaxImpositionTypeId numeric(18) NOT NULL ,
    taxRuleTaxImpositionTypeName nvarchar(60) NULL 
);

ALTER TABLE TaxRuleTaxImpositionType
    ADD CONSTRAINT XPKTxRuleImpsnType
PRIMARY KEY (taxRuleTaxImpositionTypeId);


create table ValidationType
(
    validationTypeId numeric(18) NOT NULL ,
    validationTypeName nvarchar(60) NULL 
);

ALTER TABLE ValidationType
    ADD CONSTRAINT XPKValidationType
PRIMARY KEY (validationTypeId);


create table DMFilterStrng
(
    stringTypeId numeric(18) NOT NULL ,
    criteriaString nvarchar(255) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

ALTER TABLE DMFilterStrng
    ADD CONSTRAINT XPKDMFilterStrng
PRIMARY KEY (filterId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMFilterStrng
    ADD CONSTRAINT f3DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table CertClassType
(
    certClassTypeId numeric(18) NOT NULL ,
    certClassTypeName nvarchar(60) NULL 
);

ALTER TABLE CertClassType
    ADD CONSTRAINT XPKCertClassType
PRIMARY KEY (certClassTypeId);


create table DiscountCategory
(
    discountCatId numeric(18) NOT NULL ,
    discountCatName nvarchar(60) NOT NULL ,
    discountCatDesc nvarchar(100) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE DiscountCategory
    ADD CONSTRAINT XPKDiscountCat
PRIMARY KEY (discountCatId);


create table TaxabilityCategory
(
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

ALTER TABLE TaxabilityCategory
    ADD CONSTRAINT XPKTaxabilityCat
PRIMARY KEY (txbltyCatId, txbltyCatSrcId);


create table TxbltyCatDetail
(
    txbltyCatDtlId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatCode nvarchar(60) NOT NULL ,
    txbltyCatName nvarchar(60) NOT NULL ,
    txbltyCatDesc nvarchar(1000) NULL ,
    txbltyCatDefaultId numeric(18) NOT NULL ,
    prntTxbltyCatId numeric(18) NULL ,
    prntTxbltyCatSrcId numeric(18) NULL ,
    dataTypeId numeric(18) NULL ,
    deletedInd numeric(1) NOT NULL ,
    allowRelatedInd numeric(1) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT XPKTxbltyCatDetail
PRIMARY KEY (txbltyCatDtlId, txbltyCatSrcId);


ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f2TaxCat FOREIGN KEY (prntTxbltyCatId, prntTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

create table FlexFieldDef
(
    flexFieldDefId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDef
    ADD CONSTRAINT XPKFlexibleField
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId);


create table FlexFieldDefDetail
(
    flexFieldDefDtlId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    flexFieldDefId numeric(18) NOT NULL ,
    dataTypeId numeric(18) NOT NULL ,
    calcOutputInd numeric(1) DEFAULT 0 NOT NULL ,
    flexFieldDesc nvarchar(1000) NULL ,
    flexFieldDefRefNum numeric(2) NOT NULL ,
    flexFieldDefSeqNum numeric(8) NOT NULL ,
    shortName nvarchar(10) NOT NULL ,
    longName nvarchar(60) NULL ,
    txbltyCatId numeric(18) NULL ,
    txbltyCatSrcId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT XPKFlexFieldDetail
PRIMARY KEY (flexFieldDefDtlId, flexFieldDefSrcId);


ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f2DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f5TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f3FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

create table TaxJurDetail
(
    taxTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    regGroupAllowedInd numeric(1) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    registrationReqInd numeric(1) NOT NULL ,
    reqLocsForRprtgInd numeric(1) NOT NULL ,
    reqLocsForSitusInd numeric(1) NOT NULL 
);

ALTER TABLE TaxJurDetail
    ADD CONSTRAINT XPKTaxJurDetail
PRIMARY KEY (jurisdictionId, taxTypeId, effDate, sourceId);


ALTER TABLE TaxJurDetail
    ADD CONSTRAINT f16TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table DMActivityLog
(
    activityLogId numeric(18) NOT NULL ,
    filterName nvarchar(60) NULL ,
    activityTypeId numeric(18) NOT NULL ,
    startTime datetime NOT NULL ,
    endTime datetime NULL ,
    lastPingTime datetime NOT NULL ,
    nextPingTime datetime NOT NULL ,
    userName nvarchar(60) NOT NULL ,
    userId numeric(18) NOT NULL ,
    sourceName nvarchar(60) NULL ,
    sourceId numeric(18) NULL ,
    activityStatusId numeric(18) NOT NULL ,
    filterDescription nvarchar(255) NULL ,
    activityLogMessage nvarchar(1000) NULL ,
    hostName nvarchar(128) NOT NULL ,
    outputFileName nvarchar(255) NULL ,
    masterAdminInd numeric(1) NULL ,
    sysAdminInd numeric(1) NULL ,
    followupInd numeric(1) NULL 
);

ALTER TABLE DMActivityLog
    ADD CONSTRAINT XPKDMActivityLog
PRIMARY KEY (activityLogId);


create table DMActivityLogNum
(
    numberTypeId numeric(18) NOT NULL ,
    criteriaNum numeric(18) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT XPKDMActLogNum
PRIMARY KEY (activityLogId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT f2DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogDate
(
    dateTypeId numeric(18) NOT NULL ,
    criteriaDate numeric(8) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT XPKDMActLogDate
PRIMARY KEY (activityLogId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT f1DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogInd
(
    indicatorTypeId numeric(18) NOT NULL ,
    criteriaIndicator numeric(1) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT XPKDMActLogInd
PRIMARY KEY (activityLogId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT f4DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table TpsSchVersion
(
    schemaVersionId numeric(18) NOT NULL ,
    subjectAreaId numeric(18) NULL ,
    schemaVersionCode nvarchar(30) NOT NULL ,
    syncVersionId nvarchar(64) NULL ,
    schemaSubVersionId numeric(18) NULL 
);

ALTER TABLE TpsSchVersion
    ADD CONSTRAINT XPKTpsSchemaVer
PRIMARY KEY (schemaVersionId);


create table TpsPatchDataEvent
(
    dataEventId numeric(18) NOT NULL ,
    patchId numeric(18) NOT NULL ,
    patchInterimId numeric(18) NOT NULL ,
    eventDate numeric(8) NOT NULL 
);

ALTER TABLE TpsPatchDataEvent
    ADD CONSTRAINT XPKTPSPchDataEvent
PRIMARY KEY (dataEventId);


create table ApportionmentFriendlyStates
(
    friendlyStateId numeric(18) NOT NULL ,
    jurId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE ApportionmentFriendlyStates
    ADD CONSTRAINT XPKApptionFriendly
PRIMARY KEY (friendlyStateId);


create table SitusConditionNode
(
    situsCondNodeId numeric(18) NOT NULL ,
    situsConditionId numeric(18) NOT NULL ,
    rootNodeInd numeric(1) NOT NULL 
);

ALTER TABLE SitusConditionNode
    ADD CONSTRAINT XPKSitusCondNode
PRIMARY KEY (situsCondNodeId);


ALTER TABLE SitusConditionNode
    ADD CONSTRAINT f1StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TrueSitusCond
(
    prntTruSitusCondId numeric(18) NOT NULL ,
    chldTruSitusCondId numeric(18) NOT NULL 
);

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT XPKTrueSitusCond
PRIMARY KEY (prntTruSitusCondId, chldTruSitusCondId);


ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f1StCdNd FOREIGN KEY (prntTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f2StCdNd FOREIGN KEY (chldTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table SitusConcNode
(
    situsConcNodeId numeric(18) NOT NULL ,
    situsConclusionId numeric(18) NOT NULL ,
    truForSitusCondId numeric(18) NULL ,
    flsForSitusCondId numeric(18) NULL 
);

ALTER TABLE SitusConcNode
    ADD CONSTRAINT XPKSitusConcNode
PRIMARY KEY (situsConcNodeId);


ALTER TABLE SitusConcNode
    ADD CONSTRAINT f1StsCnc FOREIGN KEY (situsConclusionId)
    REFERENCES SitusConclusion (situsConclusionId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f5StCdNd FOREIGN KEY (truForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f6StCdNd FOREIGN KEY (flsForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table Party
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyCreationDate numeric(8) NOT NULL 
);

ALTER TABLE Party
    ADD CONSTRAINT XPKParty
PRIMARY KEY (partyId, partySourceId);


create table PartyDetail
(
    partyDtlId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    parentPartyId numeric(18) NULL ,
    partyTypeId numeric(18) NOT NULL ,
    partyName nvarchar(60) NOT NULL ,
    userPartyIdCode nvarchar(40) NOT NULL ,
    partyRelInd numeric(1) NOT NULL ,
    taxpayerType nvarchar(60) NULL ,
    filingEntityInd numeric(1) NOT NULL ,
    prntInheritenceInd numeric(1) NOT NULL ,
    ersInd numeric(1) NOT NULL ,
    taxThresholdAmt numeric(18,3) NULL ,
    taxThresholdPct numeric(10,6) NULL ,
    taxOvrThresholdAmt numeric(18,3) NULL ,
    taxOvrThresholdPct numeric(10,6) NULL ,
    partyClassInd numeric(1) NOT NULL ,
    shippingTermsId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    discountCatId numeric(18) NULL ,
    customField1Value nvarchar(60) NULL ,
    customField2Value nvarchar(60) NULL ,
    customField3Value nvarchar(60) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    parentCustomerId numeric(18) NULL 
);

ALTER TABLE PartyDetail
    ADD CONSTRAINT XPKPartyDtl
PRIMARY KEY (partyDtlId, partySourceId);


ALTER TABLE PartyDetail
    ADD CONSTRAINT f1ShpTrm FOREIGN KEY (shippingTermsId)
    REFERENCES ShippingTerms (shippingTermsId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1PtyTyp FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f2Party FOREIGN KEY (parentPartyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f33Party FOREIGN KEY (parentCustomerId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f5DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

create table PartyRoleTaxResult
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    partyRoleTypeId numeric(18) NOT NULL ,
    taxResultTypeId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18) NULL 
);

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT XPKPrtyRoleTaxRslt
PRIMARY KEY (partyId, partyRoleTypeId, partySourceId);


ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f5RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f1PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table PartyVtxProdType
(
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL 
);

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT XPKPrtyVtxProdTyp
PRIMARY KEY (partyId, partySourceId, vertexProductId);


ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f1VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f8Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table BusinessLocation
(
    partyId numeric(18) NOT NULL ,
    businessLocationId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxAreaId numeric(18) NULL ,
    userLocationCode nvarchar(20) NULL ,
    registrationCode nvarchar(20) NULL ,
    streetInfoDesc nvarchar(100) NULL ,
    streetInfo2Desc nvarchar(100) NULL ,
    cityName nvarchar(60) NULL ,
    subDivisionName nvarchar(60) NULL ,
    mainDivisionName nvarchar(60) NULL ,
    postalCode nvarchar(20) NULL ,
    countryName nvarchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    busLocationName nvarchar(60) NULL ,
    partyRoleTypeId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE BusinessLocation
    ADD CONSTRAINT XPKBusinessLoc
PRIMARY KEY (businessLocationId, partyId, sourceId);


ALTER TABLE BusinessLocation
    ADD CONSTRAINT f4Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE BusinessLocation
    ADD CONSTRAINT f3PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table PartyNote
(
    partySourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partyNoteText nvarchar(1000) NULL 
);

ALTER TABLE PartyNote
    ADD CONSTRAINT XPKPartyNote
PRIMARY KEY (partyId, partySourceId);


ALTER TABLE PartyNote
    ADD CONSTRAINT f14Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table PartyContact
(
    partyContactId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    contactRoleTypeId numeric(18) NOT NULL ,
    contactFirstName nvarchar(60) NULL ,
    contactLastName nvarchar(60) NULL ,
    streetInfoDesc nvarchar(100) NULL ,
    streetInfo2Desc nvarchar(100) NULL ,
    cityName nvarchar(60) NULL ,
    mainDivisionName nvarchar(60) NULL ,
    postalCode nvarchar(20) NULL ,
    countryName nvarchar(60) NULL ,
    phoneNumber nvarchar(20) NULL ,
    phoneExtension nvarchar(20) NULL ,
    faxNumber nvarchar(20) NULL ,
    emailAddress nvarchar(100) NULL ,
    departmentIdCode nvarchar(20) NULL ,
    deletedInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE PartyContact
    ADD CONSTRAINT XPKPartyContact
PRIMARY KEY (partyContactId, sourceId);


ALTER TABLE PartyContact
    ADD CONSTRAINT f6Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE PartyContact
    ADD CONSTRAINT f1CntcRT FOREIGN KEY (contactRoleTypeId)
    REFERENCES ContactRoleType (contactRoleTypeId)
;

create table TaxabilityDriver
(
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL 
);

ALTER TABLE TaxabilityDriver
    ADD CONSTRAINT XPKTaxabilityDvr
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


create table TxbltyDriverDetail
(
    txbltyDvrDtlId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    inputParamTypeId numeric(18) NOT NULL ,
    txbltyDvrCode nvarchar(40) NOT NULL ,
    txbltyDvrName nvarchar(60) NOT NULL ,
    reasonCategoryId numeric(18) NULL ,
    exemptInd numeric(1) NOT NULL ,
    taxpayerPartyId numeric(18) NULL ,
    taxpayerSrcId numeric(18) NULL ,
    discountCatId numeric(18) NULL ,
    flexFieldDefId numeric(18) NULL ,
    flexFieldDefSrcId numeric(18) NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT XPKTxbltyDvrDetail
PRIMARY KEY (txbltyDvrDtlId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f1TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f8RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2InPTyp FOREIGN KEY (inputParamTypeId)
    REFERENCES InputParameterType (inputParamTypeId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f3DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

create table TxbltyDriverNote
(
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    txbltyDvrNoteText nvarchar(1000) NULL 
);

ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT XPKTxbltyDvrNote
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT f9TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table TxbltyDvrVtxPrdTyp
(
    vertexProductId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL 
);

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT XPKTxbltyDvrVtxPrd
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId, vertexProductId);


ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f2TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f8VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TelecomUnitConversionLineType
(
    telecomUnitConvnLineTypeId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    lineTypeId numeric(18) NOT NULL ,
    lineTypeSourceId numeric(18) NOT NULL ,
    telecomUnitConversionId numeric(18) NOT NULL ,
    telecomUnitConversionSourceId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT XPKTCUntCvnLnType
PRIMARY KEY (telecomUnitConvnLineTypeId, sourceId);


ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f1conv FOREIGN KEY (telecomUnitConversionId, sourceId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f2LineType FOREIGN KEY (lineTypeId, lineTypeSourceId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table TxbltyCatMap
(
    txbltyCatMapId numeric(18) NOT NULL ,
    txbltyCatMapSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    taxpayerPartyId numeric(18) NULL ,
    otherPartyId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    txbltyCatMapNote nvarchar(1000) NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT XPKTxbltyCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId);


ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f7TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f24Party FOREIGN KEY (taxpayerPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f25Party FOREIGN KEY (otherPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId)
;

create table TxbltyDriverCatMap
(
    txbltyCatMapId numeric(18) NOT NULL ,
    txbltyCatMapSrcId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL 
);

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT XPKTxbltyDvrCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId);


ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f5TaxDvr FOREIGN KEY (txbltyDvrId, txbltyCatMapSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f1TxCMap FOREIGN KEY (txbltyCatMapId, txbltyCatMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

create table InvoiceTextType
(
    invoiceTextTypeId numeric(18) NOT NULL ,
    invoiceTextTypeSrcId numeric(18) NOT NULL ,
    invoiceTextTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE InvoiceTextType
    ADD CONSTRAINT XPKInvoiceTextType
PRIMARY KEY (invoiceTextTypeId, invoiceTextTypeSrcId);


create table InvoiceText
(
    invoiceTextId numeric(18) NOT NULL ,
    invoiceTextSrcId numeric(18) NOT NULL 
);

ALTER TABLE InvoiceText
    ADD CONSTRAINT XPKInvText
PRIMARY KEY (invoiceTextId, invoiceTextSrcId);


create table InvoiceTextDetail
(
    invoiceTextDtlId numeric(18) NOT NULL ,
    invoiceTextSrcId numeric(18) NOT NULL ,
    invoiceTextId numeric(18) NOT NULL ,
    invoiceTextTypeId numeric(18) NOT NULL ,
    invoiceTextTypeSrcId numeric(18) NOT NULL ,
    invoiceTextCode nvarchar(60) NOT NULL ,
    invoiceText nvarchar(200) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT XPKInvTextDetail
PRIMARY KEY (invoiceTextDtlId, invoiceTextSrcId);


ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f1InvText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f2InvTextTyp FOREIGN KEY (invoiceTextTypeId, invoiceTextTypeSrcId)
    REFERENCES InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId)
;

create table DMFilterInd
(
    indicatorTypeId numeric(18) NOT NULL ,
    criteriaIndicator numeric(1) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

ALTER TABLE DMFilterInd
    ADD CONSTRAINT XPKDMFilterInd
PRIMARY KEY (filterId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMFilterInd
    ADD CONSTRAINT f4DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table DiscountType
(
    sourceId numeric(18) NOT NULL ,
    discountTypeId numeric(18) NOT NULL ,
    taxpayerPartyId numeric(18) NOT NULL ,
    discountCatId numeric(18) NOT NULL ,
    discountCode nvarchar(20) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE DiscountType
    ADD CONSTRAINT XPKDiscountType
PRIMARY KEY (discountTypeId, sourceId);


ALTER TABLE DiscountType
    ADD CONSTRAINT f1DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE DiscountType
    ADD CONSTRAINT f2DscCat FOREIGN KEY (taxpayerPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table TaxStructure
(
    taxStructureSrcId numeric(18) NOT NULL ,
    taxStructureId numeric(18) NOT NULL ,
    taxStructureTypeId numeric(18) NOT NULL ,
    brcktMaximumBasis numeric(18,3) NULL ,
    reductAmtDedTypeId numeric(18) NULL ,
    reductReasonCategoryId numeric(18) NULL ,
    allAtTopTierTypInd numeric(1) NOT NULL ,
    singleRate numeric(12,8) NULL ,
    taxPerUnitAmount numeric(18,6) NULL ,
    unitOfMeasureQty numeric(18,6) NOT NULL ,
    childTaxStrucSrcId numeric(18) NULL ,
    unitBasedInd numeric(1) NOT NULL ,
    basisReductFactor numeric(12,8) NULL ,
    brcktTaxCalcType numeric(18) NULL ,
    unitOfMeasISOCode nvarchar(3) NULL ,
    deletedInd numeric(1) NOT NULL ,
    childTaxStrucId numeric(18) NULL ,
    usesStdRateInd numeric(1) NOT NULL ,
    flatTaxAmt numeric(18,3) NULL ,
    telecomUnitConversionId numeric(18) NULL ,
    telecomUnitConversionSrcId numeric(18) NULL 
);

ALTER TABLE TaxStructure
    ADD CONSTRAINT XPKTaxStructure
PRIMARY KEY (taxStructureId, taxStructureSrcId);


ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TxSTyp FOREIGN KEY (taxStructureTypeId)
    REFERENCES TaxStructureType (taxStructureTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1BTCTyp FOREIGN KEY (brcktTaxCalcType)
    REFERENCES BracketTaxCalcType (brcktTaxCalcTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1DedTyp FOREIGN KEY (reductAmtDedTypeId)
    REFERENCES DeductionType (deductionTypeId)
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TelCon FOREIGN KEY (telecomUnitConversionId, telecomUnitConversionSrcId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId)
;

create table ImpositionType
(
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    impsnTypeName nvarchar(60) NOT NULL ,
    creditInd numeric(1) NULL ,
    notInTotalInd numeric(1) NULL ,
    withholdingTypeId numeric(18) NULL 
);

ALTER TABLE ImpositionType
    ADD CONSTRAINT XPKImpositionType
PRIMARY KEY (impsnTypeId, impsnTypeSrcId);


ALTER TABLE ImpositionType
    ADD CONSTRAINT f1WitTyp FOREIGN KEY (withholdingTypeId)
    REFERENCES WithholdingType (withholdingTypeId)
;

create table TaxImposition
(
    jurisdictionId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL 
);

ALTER TABLE TaxImposition
    ADD CONSTRAINT XPKTaxImp
PRIMARY KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId);


create table TaxImpsnDetail
(
    taxImpsnDtlId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    taxImpsnName nvarchar(60) NOT NULL ,
    taxImpsnAbrv nvarchar(15) NULL ,
    taxImpsnDesc nvarchar(100) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    taxTypeId numeric(18) NULL ,
    taxResponsibilityRoleTypeId numeric(18) NULL ,
    conditionInd numeric(1) NULL ,
    primaryImpositionInd numeric(1) NULL 
);

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT XPKTaxImpsnDetail
PRIMARY KEY (taxImpsnDtlId, taxImpsnSrcId);


ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1ImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f6TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f18TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

create table TaxImpQualCond
(
    taxImpQualCondId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    taxImpsnDtlId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT XPKTaxImpQualCond
PRIMARY KEY (taxImpQualCondId, taxImpsnSrcId);


ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f9TaxImp FOREIGN KEY (taxImpsnDtlId, taxImpsnSrcId)
    REFERENCES TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId)
;

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f12TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

create table FilingOverride
(
    filingOverrideId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    jurTypeSetId numeric(18) NOT NULL ,
    filingCategoryId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FilingOverride
    ADD CONSTRAINT XPKFilingOverride
PRIMARY KEY (filingOverrideId);


ALTER TABLE FilingOverride
    ADD CONSTRAINT f5FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE FilingOverride
    ADD CONSTRAINT f5JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

create table Tier
(
    minBasisAmount numeric(18,3) NULL ,
    tierNum numeric(3) NOT NULL ,
    taxResultTypeId numeric(18) NULL ,
    maxBasisAmount numeric(18,3) NULL ,
    minQuantity numeric(18,6) NULL ,
    maxQuantity numeric(18,6) NULL ,
    unitOfMeasISOCode nvarchar(3) NULL ,
    unitOfMeasureQty numeric(18,6) NULL ,
    taxPerUnitAmount numeric(18,6) NULL ,
    tierTaxRate numeric(12,8) NULL ,
    taxStructureSrcId numeric(18) NOT NULL ,
    taxStructureId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18) NULL ,
    usesStdRateInd numeric(1) NOT NULL ,
    filingCategoryId numeric(18) NULL ,
    rateClassId numeric(18) NULL ,
    taxStructureTypeId numeric(18) NULL ,
    equivalentQuantity numeric(18,6) NULL ,
    additionalQuantity numeric(18,6) NULL ,
    equivalentAdditionalQuantity numeric(18,6) NULL 
);

ALTER TABLE Tier
    ADD CONSTRAINT XPKTier
PRIMARY KEY (tierNum, taxStructureSrcId, taxStructureId);


ALTER TABLE Tier
    ADD CONSTRAINT f2TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f5TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f4RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f6FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE Tier
    ADD CONSTRAINT f3RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

create table Bracket
(
    taxStructureId numeric(18) NOT NULL ,
    taxStructureSrcId numeric(18) NOT NULL ,
    minBasisAmount numeric(18,3) NULL ,
    bracketNum numeric(3) NOT NULL ,
    maxBasisAmount numeric(18,3) NULL ,
    bracketTaxAmount numeric(18,3) NULL 
);

ALTER TABLE Bracket
    ADD CONSTRAINT XPKBracket
PRIMARY KEY (taxStructureId, taxStructureSrcId, bracketNum);


ALTER TABLE Bracket
    ADD CONSTRAINT f4TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

create table TaxRule
(
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    partyRoleTypeId numeric(18) NULL ,
    partyId numeric(18) NULL ,
    partySourceId numeric(18) NULL ,
    taxpayerRoleTypeId numeric(18) NULL ,
    taxpayerPartyId numeric(18) NULL ,
    taxpayerPartySrcId numeric(18) NULL ,
    qualDetailDesc nvarchar(100) NULL ,
    uniqueToLevelInd numeric(1) NOT NULL ,
    conditionSeqNum numeric(18) NULL ,
    taxRuleTypeId numeric(18) NULL ,
    taxResultTypeId numeric(18) NULL ,
    recoverableResultTypeId numeric(18) NULL ,
    defrdJurTypeId numeric(18) NULL ,
    defersToStdRuleInd numeric(1) NOT NULL ,
    taxStructureSrcId numeric(18) NULL ,
    taxStructureId numeric(18) NULL ,
    automaticRuleInd numeric(1) NOT NULL ,
    standardRuleInd numeric(1) NOT NULL ,
    reasonCategoryId numeric(18) NULL ,
    filingCategoryId numeric(18) NULL ,
    appToSingleImpInd numeric(1) NULL ,
    appToSingleJurInd numeric(1) NULL ,
    discountTypeId numeric(18) NULL ,
    discountTypeSrcId numeric(18) NULL ,
    maxTaxRuleType numeric(18) NULL ,
    taxScopeId numeric(18) NULL ,
    maxTaxAmount numeric(18,3) NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    taxImpsnId numeric(18) NOT NULL ,
    taxImpsnSrcId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    discountCatId numeric(18) NULL ,
    deletedInd numeric(1) NOT NULL ,
    apportionTypeId numeric(18) NULL ,
    appnTxbltyCatId numeric(18) NULL ,
    appnTxbltyCatSrcId numeric(18) NULL ,
    appnFlexFieldDefId numeric(18) NULL ,
    appnFlexFieldDefSrcId numeric(18) NULL ,
    rateClassId numeric(18) NULL ,
    locationRoleTypeId numeric(18) NULL ,
    applyFilingCatInd numeric(1) DEFAULT 0 NULL ,
    recoverablePct numeric(10,6) NULL ,
    taxResponsibilityRoleTypeId numeric(18) NULL ,
    currencyUnitId numeric(18) NULL ,
    salestaxHolidayInd numeric(1) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    includesAllTaxCats numeric(1) NULL ,
    invoiceTextId numeric(18) NULL ,
    invoiceTextSrcId numeric(18) NULL ,
    computationTypeId numeric(18) NULL ,
    defrdImpsnTypeId numeric(18) NULL ,
    defrdImpsnTypeSrcId numeric(18) NULL ,
    notAllowZeroRateInd numeric(1) NULL ,
    accumulationAsJurisdictionId numeric(18) NULL ,
    accumulationAsTaxImpsnId numeric(18) NULL ,
    accumulationAsTaxImpsnSrcId numeric(18) NULL ,
    accumulationTypeId numeric(18) NULL ,
    periodTypeId numeric(18) NULL ,
    startMonth numeric(2) NULL ,
    maxLines numeric(18) NULL ,
    unitOfMeasISOCode nvarchar(3) NULL ,
    telecomUnitConversionId numeric(18) NULL ,
    telecomUnitConversionSrcId numeric(18) NULL ,
    lineTypeCatId numeric(18) NULL ,
    lineTypeCatSourceId numeric(18) NULL 
);

ALTER TABLE TaxRule
    ADD CONSTRAINT XPKTaxRule
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TRlTyp FOREIGN KEY (taxRuleTypeId)
    REFERENCES TaxRuleType (taxRuleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f3RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1DscTyp FOREIGN KEY (discountTypeId, discountTypeSrcId)
    REFERENCES DiscountType (discountTypeId, sourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f6TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f4DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f28Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f30Party FOREIGN KEY (taxpayerPartyId, taxpayerPartySrcId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1ApprtTyp FOREIGN KEY (apportionTypeId)
    REFERENCES ApportionmentType (apportionTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f7LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1InvoiceText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2ImpTyp FOREIGN KEY (defrdImpsnTypeId, defrdImpsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RcvRTyp FOREIGN KEY (recoverableResultTypeId)
    REFERENCES RecoverableResultType (recoverableResultTypeId)
;

create table TaxRuleNote
(
    taxRuleNoteId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleNoteSrcId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleNoteText nvarchar(1000) NULL 
);

ALTER TABLE TaxRuleNote
    ADD CONSTRAINT XPKTaxRuleNote
PRIMARY KEY (taxRuleNoteId, taxRuleNoteSrcId);


ALTER TABLE TaxRuleNote
    ADD CONSTRAINT f4TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table ExprConditionType
(
    exprCondTypeId numeric(18) NOT NULL ,
    exprCondTypeName nvarchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ExprConditionType
    ADD CONSTRAINT XPKExprCondType
PRIMARY KEY (exprCondTypeId);


create table TaxRuleQualCond
(
    taxRuleQualCondId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NULL ,
    txbltyDvrSrcId numeric(18) NULL ,
    txbltyCatId numeric(18) NULL ,
    txbltyCatSrcId numeric(18) NULL ,
    flexFieldDefId numeric(18) NULL ,
    flexFieldDefSrcId numeric(18) NULL ,
    minDate numeric(8) NULL ,
    maxDate numeric(8) NULL ,
    compareValue numeric(18,3) NULL ,
    exprCondTypeId numeric(18) NULL 
);

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT XPKTaxRuleQualCond
PRIMARY KEY (taxRuleQualCondId, taxRuleSourceId);


ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f8TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f4TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f7TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f1FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f2ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

create table TaxRuleTaxType
(
    taxRuleTaxTypeId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT XPKTaxRuleTaxType
PRIMARY KEY (taxRuleTaxTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f14TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f9TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table TaxRuleTaxImposition
(
    taxRuleTaxImpositionId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleTaxImpositionTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NULL ,
    taxImpsnId numeric(18) NULL ,
    taxImpsnSrcId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    txbltyCatId numeric(18) NULL ,
    txbltyCatSrcId numeric(18) NULL ,
    overTxbltyCatId numeric(18) NULL ,
    overTxbltyCatSrcId numeric(18) NULL ,
    underTxbltyCatId numeric(18) NULL ,
    underTxbltyCatSrcId numeric(18) NULL ,
    invoiceThresholdAmt numeric(18,5) NULL ,
    thresholdCurrencyUnitId numeric(18) NULL ,
    includeTaxableAmountInd numeric(1) NULL ,
    includeTaxAmountInd numeric(1) NULL ,
    rate numeric(12,8) NULL ,
    locationRoleTypeId numeric(18) NULL ,
    impositionTypeId numeric(18) NULL ,
    impositionTypeSrcId numeric(18) NULL ,
    jurTypeId numeric(18) NULL ,
    taxTypeId numeric(18) NULL ,
    isLeftInd numeric(1) NULL ,
    groupId numeric(3) NULL 
);

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT XPKTaxRuleTaxImpsn
PRIMARY KEY (taxRuleTaxImpositionId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f7TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f9TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxCat FOREIGN KEY (overTxbltyCatId, overTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f11TaxCat FOREIGN KEY (underTxbltyCatId, underTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f12LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f13TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table MaxTaxRuleAdditionalCond
(
    maxTaxRuleCondId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL 
);

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT XPKMaxTaxRuleCond
PRIMARY KEY (maxTaxRuleCondId);


ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

create table ComputationType
(
    computationTypeId numeric(18) NOT NULL ,
    compTypeName nvarchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ComputationType
    ADD CONSTRAINT XPKCompType
PRIMARY KEY (computationTypeId);


create table TaxFactorType
(
    taxFactorTypeId numeric(18) NOT NULL ,
    taxFactorTypeName nvarchar(60) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE TaxFactorType
    ADD CONSTRAINT XPKTaxFactorType
PRIMARY KEY (taxFactorTypeId);


create table TaxFactor
(
    taxFactorId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxFactorTypeId numeric(18) NOT NULL ,
    constantValue numeric(18,6) NULL ,
    basisTypeId numeric(18) NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    txbltyCatId numeric(18) NULL ,
    txbltyCatSrcId numeric(18) NULL ,
    flexFieldDefId numeric(18) NULL ,
    flexFieldDefSrcId numeric(18) NULL ,
    impositionTypeId numeric(18) NULL ,
    impositionTypeSrcId numeric(18) NULL ,
    locationRoleTypeId numeric(18) NULL ,
    jurTypeId numeric(18) NULL ,
    taxTypeId numeric(18) NULL 
);

ALTER TABLE TaxFactor
    ADD CONSTRAINT XPKTaxFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE TaxFactor
    ADD CONSTRAINT f8TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f4FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f1TxFcTp FOREIGN KEY (taxFactorTypeId)
    REFERENCES TaxFactorType (taxFactorTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f2BssTyp FOREIGN KEY (basisTypeId)
    REFERENCES BasisType (basisTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f17LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f19TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table ComputationFactor
(
    taxFactorId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    computationTypeId numeric(18) NOT NULL ,
    leftTaxFactorId numeric(18) NOT NULL ,
    rightTaxFactorId numeric(18) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ComputationFactor
    ADD CONSTRAINT XPKCompFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE ComputationFactor
    ADD CONSTRAINT f3TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f4TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f5TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f1CmpTyp FOREIGN KEY (computationTypeId)
    REFERENCES ComputationType (computationTypeId)
;

create table ConditionalTaxExpr
(
    condTaxExprId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    exprCondTypeId numeric(18) NOT NULL ,
    leftTaxFactorId numeric(18) NOT NULL ,
    rightTaxFactorId numeric(18) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT XPKCondTaxExpr
PRIMARY KEY (condTaxExprId, sourceId);


ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f2TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId)
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f6TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table TaxBasisConclusion
(
    taxBasisConcId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxFactorId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT XPKTaxBasisConc
PRIMARY KEY (taxBasisConcId, sourceId);


ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f7TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId)
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxabilityMapping
(
    taxabilityMapId numeric(18) NOT NULL ,
    taxabilityMapSrcId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    txbltyCatMapId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT XPKTaxabilityMap
PRIMARY KEY (taxabilityMapId, taxabilityMapSrcId);


ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f3TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f2TxCMap FOREIGN KEY (txbltyCatMapId, taxabilityMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId)
;

create table LineItemTaxDtlType
(
    lineItemTxDtlTypId numeric(18) NOT NULL ,
    lineItemTxDtlTypNm nvarchar(60) NULL 
);

ALTER TABLE LineItemTaxDtlType
    ADD CONSTRAINT XPKLinItmTaxDtlTyp
PRIMARY KEY (lineItemTxDtlTypId);


create table FalseSitusCond
(
    prntFlsSitusCondId numeric(18) NOT NULL ,
    chldFlsSitusCondId numeric(18) NOT NULL 
);

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT XPKFalseSitusCond
PRIMARY KEY (prntFlsSitusCondId, chldFlsSitusCondId);


ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f4StCdNd FOREIGN KEY (chldFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f3StCdNd FOREIGN KEY (prntFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table TaxRuleTransType
(
    taxRuleTransTypeId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    transactionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT XPKTaxRuleTranTyp
PRIMARY KEY (taxRuleTransTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table DMActivityLogStrng
(
    stringTypeId numeric(18) NOT NULL ,
    criteriaString nvarchar(255) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    activityLogId numeric(18) NOT NULL 
);

ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT XPKDMActLogStrng
PRIMARY KEY (activityLogId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT f3DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table DMActivityLogFile
(
    activityLogId numeric(18) NOT NULL ,
    fileId numeric(18) NOT NULL ,
    fileName nvarchar(255) NOT NULL ,
    statusNum numeric(1) NOT NULL 
);

ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT XPKDMActLogFile
PRIMARY KEY (activityLogId, fileId);


ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT f5DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId)
;

create table TaxRuleDescription
(
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    taxRuleDescText nvarchar(1000) NULL 
);

ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT XPKTaxRuleDesc
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT f5TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table SitusCondTxbltyCat
(
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    situsConditionId numeric(18) NOT NULL 
);

ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT XPKSitusCondTaxCat
PRIMARY KEY (situsConditionId, txbltyCatSrcId, txbltyCatId);


ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT f3StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId)
;

create table TaxRuleCondJur
(
    taxRuleCondJurId numeric(18) NOT NULL ,
    taxRuleSourceId numeric(18) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT XPKTaxRuleCondJur
PRIMARY KEY (taxRuleCondJurId, taxRuleSourceId);


ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT f10TxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId)
;

create table DMFilterNum
(
    numberTypeId numeric(18) NOT NULL ,
    criteriaNum numeric(18) NULL ,
    criteriaOrderNum numeric(18) NOT NULL ,
    filterId numeric(18) NOT NULL 
);

ALTER TABLE DMFilterNum
    ADD CONSTRAINT XPKDMFilterNum
PRIMARY KEY (filterId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMFilterNum
    ADD CONSTRAINT f2DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId)
;

create table SitusTreatment
(
    situsTreatmentId numeric(18) NOT NULL ,
    situsCondNodeId numeric(18) NULL ,
    situsTreatmentName nvarchar(60) NOT NULL ,
    situsTreatmentDesc nvarchar(1000) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTreatment
    ADD CONSTRAINT XPKSitusTreatment
PRIMARY KEY (situsTreatmentId);


ALTER TABLE SitusTreatment
    ADD CONSTRAINT f7StCdNd FOREIGN KEY (situsCondNodeId)
    REFERENCES SitusConditionNode (situsCondNodeId)
;

create table StsTrtmntVtxPrdTyp
(
    situsTreatmentId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT XPKStsTmtVtxPrdTyp
PRIMARY KEY (situsTreatmentId, vertexProductId);


ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f1Trmt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f2VxtPrd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TransTypePrspctv
(
    transTypePrspctvId numeric(18) NOT NULL ,
    transactionTypeId numeric(18) NULL ,
    partyRoleTypeId numeric(18) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL ,
    name nvarchar(60) NOT NULL 
);

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT XPKTransTypPrspctv
PRIMARY KEY (transTypePrspctvId);


ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f5PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId)
;

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f4TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

create table SitusTreatmentRule
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    situsTreatmentId numeric(18) NOT NULL ,
    txbltyCatSrcId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL ,
    jurisdiction1Id numeric(18) NULL ,
    jurisdiction2Id numeric(18) NULL ,
    locationRoleTyp1Id numeric(18) NULL ,
    locationRoleTyp2Id numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT XPKSitusTrtmntRule
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f6LRlTyp FOREIGN KEY (locationRoleTyp2Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f5LRlTyp FOREIGN KEY (locationRoleTyp1Id)
    REFERENCES LocationRoleType (locationRoleTypeId)
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f1SitTrt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId)
;

create table SitusTrtmntRulNote
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    noteText nvarchar(1000) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT XPKSitusTrtmntRlNt
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT f1SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

create table SitusTrtmntPrspctv
(
    situsTrtmntRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    transTypePrspctvId numeric(18) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT XPKSitusTrtmntPrsp
PRIMARY KEY (situsTrtmntRuleId, sourceId, transTypePrspctvId);


ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f2SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId)
;

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f1TrTypP FOREIGN KEY (transTypePrspctvId)
    REFERENCES TransTypePrspctv (transTypePrspctvId)
;

create table CurrencyRndRule
(
    currencyRndRuleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    currencyUnitId numeric(18) NOT NULL ,
    roundingRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NULL ,
    jurisdictionId numeric(18) NULL ,
    partyId numeric(18) NULL ,
    configurableInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(8) NULL ,
    lastUpdateDate numeric(8) NULL 
);

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT XPKCurrRndRule
PRIMARY KEY (currencyRndRuleId, sourceId);


ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f22Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f1RndRul FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

create table AllowCurrRndRule
(
    allowCurrRndRuleId numeric(18) NOT NULL ,
    currencyUnitId numeric(18) NOT NULL ,
    roundingRuleId numeric(18) NOT NULL ,
    taxTypeId numeric(18) NULL ,
    jurisdictionId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT XPKAllowCurRndRule
PRIMARY KEY (allowCurrRndRuleId);


ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f2RndRule FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId)
;

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f10TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId)
;

create table TaxRecoverablePct
(
    taxRecovPctId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partySourceId numeric(18) NOT NULL ,
    costCenter nvarchar(40) NULL ,
    recovPct numeric(10,6) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    accrualReliefInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT XPKTaxRecovPct
PRIMARY KEY (taxRecovPctId);


ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT f27Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table TaxAssistRule
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    ruleCode nvarchar(60) NOT NULL ,
    ruleDesc nvarchar(1000) NULL ,
    vertexProductId numeric(18) NOT NULL ,
    precedence numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    phaseId numeric(1) DEFAULT 0 NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistRule
    ADD CONSTRAINT XPKTaxAssistRule
PRIMARY KEY (ruleId, sourceId);


ALTER TABLE TaxAssistRule
    ADD CONSTRAINT f2VtxProduct FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistCondition
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    conditionId numeric(18) NOT NULL ,
    conditionText nvarchar(2000) NULL ,
    conditionText2 nvarchar(2000) NULL 
);

ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT XPKTxAstCondition
PRIMARY KEY (ruleId, sourceId, conditionId);


ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT f1TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

create table TaxAssistConclude
(
    sourceId numeric(18) NOT NULL ,
    ruleId numeric(18) NOT NULL ,
    conclusionId numeric(18) NOT NULL ,
    conclusionText nvarchar(2000) NULL ,
    conclusionText2 nvarchar(2000) NULL 
);

ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT XPKTxAstConclude
PRIMARY KEY (ruleId, sourceId, conclusionId);


ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT f2TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId)
;

create table TaxAssistLookup
(
    sourceId numeric(18) NOT NULL ,
    tableId numeric(18) NOT NULL ,
    tableName nvarchar(60) NOT NULL ,
    dataType numeric(18) NOT NULL ,
    description nvarchar(1000) NULL ,
    vertexProductId numeric(18) NOT NULL ,
    resultName nvarchar(60) NOT NULL ,
    param1Name nvarchar(60) NOT NULL ,
    param2Name nvarchar(60) NULL ,
    param3Name nvarchar(60) NULL ,
    param4Name nvarchar(60) NULL ,
    param5Name nvarchar(60) NULL ,
    param6Name nvarchar(60) NULL ,
    param7Name nvarchar(60) NULL ,
    param8Name nvarchar(60) NULL ,
    param9Name nvarchar(60) NULL ,
    param10Name nvarchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT XPKTxAstLookup
PRIMARY KEY (tableId, sourceId);


ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT f1TxAstLookup FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistLookupRcd
(
    sourceId numeric(18) NOT NULL ,
    recordId numeric(18) NOT NULL ,
    tableId numeric(18) NOT NULL ,
    result nvarchar(200) NULL ,
    param1 nvarchar(200) NULL ,
    param2 nvarchar(200) NULL ,
    param3 nvarchar(200) NULL ,
    param4 nvarchar(200) NULL ,
    param5 nvarchar(200) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT XPKTxAstLookupRcd
PRIMARY KEY (recordId, sourceId);


ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT f1TxAsLk FOREIGN KEY (tableId, sourceId)
    REFERENCES TaxAssistLookup (tableId, sourceId)
;

create table NumericType
(
    numericTypeId numeric(18) NOT NULL ,
    numericTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE NumericType
    ADD CONSTRAINT XPKNumericType
PRIMARY KEY (numericTypeId);


create table TaxAssistAllocationTable
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    allocationTableName nvarchar(60) NOT NULL ,
    allocationTableDesc nvarchar(1000) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT XPKTxAstAlocTbl
PRIMARY KEY (allocationTableId, sourceId);


ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT f1TxAstAlloc FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table TaxAssistAllocationColumn
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    columnId numeric(18) NOT NULL ,
    columnName nvarchar(60) NOT NULL ,
    dataTypeId numeric(18) NOT NULL ,
    columnSeqNum numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT XPKTxAstAllocCol
PRIMARY KEY (allocationTableId, sourceId, columnId);


ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsAC FOREIGN KEY (allocationTableId, sourceId)
    REFERENCES TaxAssistAllocationTable (allocationTableId, sourceId)
;

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsACDT FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId)
;

create table TaxAssistAllocationColumnValue
(
    sourceId numeric(18) NOT NULL ,
    allocationTableId numeric(18) NOT NULL ,
    columnId numeric(18) NOT NULL ,
    recordId numeric(18) NOT NULL ,
    recordCode nvarchar(20) NOT NULL ,
    numericTypeId numeric(18) NULL ,
    columnValue nvarchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT XPKTxAstAllocCoVal
PRIMARY KEY (allocationTableId, sourceId, columnId, recordId, recordCode);


ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACV FOREIGN KEY (allocationTableId, sourceId, columnId)
    REFERENCES TaxAssistAllocationColumn (allocationTableId, sourceId, columnId)
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACVNT FOREIGN KEY (numericTypeId)
    REFERENCES NumericType (numericTypeId)
;

create table TpsJurisdiction
(
    jurId numeric(18) NOT NULL ,
    jurVersionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    jurLayerId numeric(18) NOT NULL ,
    jurTypeId numeric(18) NOT NULL ,
    jurTypeName nvarchar(60) NOT NULL ,
    name nvarchar(60) NOT NULL ,
    standardName nvarchar(60) NULL ,
    description nvarchar(80) NULL ,
    updateId numeric(18) NOT NULL 
);

ALTER TABLE TpsJurisdiction
    ADD CONSTRAINT XPKTpsJurisdiction
PRIMARY KEY (jurId, jurVersionId);


create table TaxAreaJurNames
(
    taxAreaId numeric(18) NOT NULL ,
    countryName nvarchar(60) NOT NULL ,
    countryISOCode2 nvarchar(2) NULL ,
    countryISOCode3 nvarchar(3) NOT NULL ,
    mainDivJurTypeName nvarchar(60) NULL ,
    mainDivName nvarchar(60) NULL ,
    subDivJurTypeName nvarchar(60) NULL ,
    subDivName nvarchar(60) NULL ,
    cityJurTypeName nvarchar(60) NULL ,
    cityName nvarchar(60) NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE TaxAreaJurNames
    ADD CONSTRAINT XPKTaxAreaJurNames
PRIMARY KEY (taxAreaId);


create table JurHierarchy
(
    depthFromParent numeric(18) NOT NULL ,
    topmostInd numeric(1) NOT NULL ,
    chldJurisdictionId numeric(18) NOT NULL ,
    prntJurisdictionId numeric(18) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE JurHierarchy
    ADD CONSTRAINT XPKJurHierarchy
PRIMARY KEY (prntJurisdictionId, chldJurisdictionId);


create table SimplificationType
(
    simpTypeId numeric(18) NOT NULL ,
    simpTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE SimplificationType
    ADD CONSTRAINT XPKSimpType
PRIMARY KEY (simpTypeId);


create table DMActivityType
(
    activityTypeId numeric(18) NOT NULL ,
    activityTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE DMActivityType
    ADD CONSTRAINT XPKDMActivityType
PRIMARY KEY (activityTypeId);


create table DMStatusType
(
    statusTypeId numeric(18) NOT NULL ,
    statusTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE DMStatusType
    ADD CONSTRAINT XPKDMStatusType
PRIMARY KEY (statusTypeId);


create table InputOutputType
(
    inputOutputTypeId numeric(18) NOT NULL ,
    inputOutputTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE InputOutputType
    ADD CONSTRAINT XPKInpOutType
PRIMARY KEY (inputOutputTypeId);


create table CertificateReasonType
(
    certificateReasonTypeId numeric(18) NOT NULL ,
    certificateReasonTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE CertificateReasonType
    ADD CONSTRAINT XPKCertRsnType
PRIMARY KEY (certificateReasonTypeId);


create table CertReasonTypeJurisdiction
(
    certReasonTypeJurId numeric(18) NOT NULL ,
    certificateReasonTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    reasonCategoryId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT XPKCertRsnTypeJur
PRIMARY KEY (certReasonTypeJurId);


ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId)
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

create table CertificateExemptionType
(
    certificateExemptionTypeId numeric(18) NOT NULL ,
    certificateExemptionTypeName nvarchar(60) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateExemptionType
    ADD CONSTRAINT XPKCertExemType
PRIMARY KEY (certificateExemptionTypeId);


create table CertExemptionTypeJurImp
(
    certExemptionTypeJurImpId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    allStatesInd numeric(1) NULL ,
    allCitiesInd numeric(1) NULL ,
    allCountiesInd numeric(1) NULL ,
    allOthersInd numeric(1) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT XPKCertExmTypeJur
PRIMARY KEY (certExemptionTypeJurImpId);


ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1ImpCrtExmTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1CrtExmTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

create table Form
(
    formId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL 
);

ALTER TABLE Form
    ADD CONSTRAINT XPKForm
PRIMARY KEY (formId, sourceId);


create table FormDetail
(
    formDetailId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    formIdCode nvarchar(30) NOT NULL ,
    formName nvarchar(60) NULL ,
    formDesc nvarchar(255) NULL ,
    formImageFileName nvarchar(255) NULL ,
    replacedByFormId numeric(18) NULL ,
    replacementDate numeric(8) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL 
);

ALTER TABLE FormDetail
    ADD CONSTRAINT XPKFormDtl
PRIMARY KEY (formDetailId, sourceId);


ALTER TABLE FormDetail
    ADD CONSTRAINT f1FormDtlForm FOREIGN KEY (replacedByFormId, sourceId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE FormDetail
    ADD CONSTRAINT f2FormDtlForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormJurisdiction
(
    formJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormJurisdiction
    ADD CONSTRAINT XPKFormJur
PRIMARY KEY (formJurisdictionId, sourceId);


ALTER TABLE FormJurisdiction
    ADD CONSTRAINT f1FormJurForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormFieldDef
(
    fieldDefId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    name nvarchar(60) NOT NULL ,
    formId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NULL ,
    formFieldTypeId numeric(18) NOT NULL ,
    requiredInd numeric(1) DEFAULT 0 NULL ,
    hiddenInd numeric(1) DEFAULT 0 NULL ,
    attributeId numeric(18) NULL ,
    fieldDefPageNum numeric(18) NOT NULL ,
    groupName nvarchar(60) NULL ,
    parentFieldDefId numeric(18) NULL ,
    parentFieldDefSelectInd numeric(1) DEFAULT 1 NULL ,
    parentValue nvarchar(20) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormFieldDef
    ADD CONSTRAINT XPKFormFieldDef
PRIMARY KEY (fieldDefId, sourceId);


ALTER TABLE FormFieldDef
    ADD CONSTRAINT f1FormFieldDef FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId)
;

create table FormFieldTypeValue
(
    fieldDefId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    validValue nvarchar(20) NULL ,
    trueValue nvarchar(20) NULL ,
    falseValue nvarchar(20) NULL ,
    isDefault numeric(1) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE FormFieldTypeValue
    ADD CONSTRAINT XPKFormFldTypeVle
PRIMARY KEY (fieldDefId, sourceId);


create table FormFieldAttribute
(
    attributeId numeric(18) NOT NULL ,
    name nvarchar(60) NOT NULL 
);

ALTER TABLE FormFieldAttribute
    ADD CONSTRAINT XPKFormFieldAttr
PRIMARY KEY (attributeId);


create table Certificate
(
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCreationDate numeric(8) NOT NULL 
);

ALTER TABLE Certificate
    ADD CONSTRAINT XPKCertificate
PRIMARY KEY (certificateId, sourceId);


create table CertificateDetail
(
    certificateDetailId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    certificateHolderPartyId numeric(18) NOT NULL ,
    certClassTypeId numeric(18) NOT NULL ,
    formId numeric(18) NULL ,
    formSourceId numeric(18) NULL ,
    taxResultTypeId numeric(18) NULL ,
    certificateStatusId numeric(18) NOT NULL ,
    partyContactId numeric(18) NULL ,
    txbltyCatId numeric(18) NULL ,
    txbltyCatSrcId numeric(18) NULL ,
    certCopyRcvdInd numeric(1) NULL ,
    industryTypeName nvarchar(60) NULL ,
    hardCopyLocationName nvarchar(255) NULL ,
    certBlanketInd numeric(1) NULL ,
    singleUseInd numeric(1) NULL ,
    invoiceNumber nvarchar(50) NULL ,
    replacedByCertificateId numeric(18) NULL ,
    vertexProductId numeric(18) NOT NULL ,
    completedInd numeric(1) DEFAULT 1 NOT NULL ,
    approvalStatusId numeric(18) DEFAULT 3 NOT NULL ,
    uuid nvarchar(36) NULL ,
    ecwCertificateId numeric(18) NULL ,
    ecwSyncId numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    createDate numeric(18) NOT NULL ,
    lastUpdateDate numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE CertificateDetail
    ADD CONSTRAINT XPKCertDtl
PRIMARY KEY (certificateDetailId, sourceId);


ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1Certificate FOREIGN KEY (replacedByCertificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtParty FOREIGN KEY (certificateHolderPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtClsTyp FOREIGN KEY (certClassTypeId)
    REFERENCES CertClassType (certClassTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtTxRsltTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtPtyCntct FOREIGN KEY (partyContactId, sourceId)
    REFERENCES PartyContact (partyContactId, sourceId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f5VtxProd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtForm FOREIGN KEY (formId, formSourceId)
    REFERENCES Form (formId, sourceId)
;

create table CertificateCoverage
(
    certificateCoverageId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateIdCode nvarchar(30) NULL ,
    validationTypeId numeric(18) NULL ,
    validationDate numeric(8) NULL ,
    certificateReasonTypeId numeric(18) NULL ,
    certificateExemptionTypeId numeric(18) NULL ,
    certIssueDate numeric(8) NULL ,
    certExpDate numeric(8) NULL ,
    certReviewDate numeric(8) NULL ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1 NULL ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT XPKCertCvrg
PRIMARY KEY (certificateCoverageId, sourceId);


ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

create table CertificateJurisdiction
(
    certificateJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCoverageId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18) NULL ,
    coverageActionTypeId numeric(18) NOT NULL 
);

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT XPKCertJur
PRIMARY KEY (certificateJurisdictionId, sourceId);


ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f2CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table CertificateImposition
(
    certificateImpositionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateCoverageId numeric(18) NOT NULL ,
    jurTypeSetId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    certificateExemptionTypeId numeric(18) NULL ,
    impActiveInd numeric(1) NOT NULL 
);

ALTER TABLE CertificateImposition
    ADD CONSTRAINT XPKCertImp
PRIMARY KEY (certificateImpositionId, sourceId);


ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f3CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId)
;

create table CertImpositionJurisdiction
(
    certImpJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateImpositionId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL 
);

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT XPKCertImpJur
PRIMARY KEY (certImpJurisdictionId, sourceId);


ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurImp FOREIGN KEY (certificateImpositionId, sourceId)
    REFERENCES CertificateImposition (certificateImpositionId, sourceId)
;

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table CertificateNote
(
    certNoteText nvarchar(1000) NULL ,
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL 
);

ALTER TABLE CertificateNote
    ADD CONSTRAINT XPKCertNote
PRIMARY KEY (certificateId, sourceId);


ALTER TABLE CertificateNote
    ADD CONSTRAINT f2Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

create table CertificateImage
(
    certificateImageId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    creationSourceId numeric(18) DEFAULT 1 NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    imageLocationName nvarchar(255) NOT NULL 
);

ALTER TABLE CertificateImage
    ADD CONSTRAINT XPKCertImage
PRIMARY KEY (certificateImageId, sourceId);


ALTER TABLE CertificateImage
    ADD CONSTRAINT f5Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

create table CertTxbltyDriver
(
    certTxbltyDriverId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    txbltyDvrId numeric(18) NOT NULL ,
    txbltyDvrSrcId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT XPKCertTxbltyDvr
PRIMARY KEY (certTxbltyDriverId);


ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId)
;

create table CertificateTransType
(
    certTransTypeId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    transactionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateTransType
    ADD CONSTRAINT XPKCertTransType
PRIMARY KEY (certTransTypeId, sourceId);


ALTER TABLE CertificateTransType
    ADD CONSTRAINT f1CertTransTypCrt FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE CertificateTransType
    ADD CONSTRAINT f2CertTransTypTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId)
;

create table CertificateFormField
(
    certificateFormFieldId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NOT NULL ,
    formId numeric(18) NOT NULL ,
    formSourceId numeric(18) NOT NULL ,
    fieldDefId numeric(18) NOT NULL ,
    value nvarchar(500) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateFormField
    ADD CONSTRAINT XPKCertFormField
PRIMARY KEY (certificateFormFieldId, sourceId);


create table ExpirationRuleType
(
    expirationRuleTypeId numeric(18) NOT NULL ,
    expirationRuleTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE ExpirationRuleType
    ADD CONSTRAINT XPKExpRuleType
PRIMARY KEY (expirationRuleTypeId);


create table CertificateNumReqType
(
    certificateNumReqTypeId numeric(18) NOT NULL ,
    certificateNumReqTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE CertificateNumReqType
    ADD CONSTRAINT XPKCertNumReqTyp
PRIMARY KEY (certificateNumReqTypeId);


create table CertificateNumberFormat
(
    certificateNumberFormatId numeric(18) NOT NULL ,
    certificateNumberMask nvarchar(30) NOT NULL ,
    certificateNumberExample nvarchar(30) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateNumberFormat
    ADD CONSTRAINT XPKCertNumFmt
PRIMARY KEY (certificateNumberFormatId);


create table CertificateValidationRule
(
    certificateValidationRuleId numeric(18) NOT NULL ,
    certificateNumValidationDesc nvarchar(100) NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    certificateReasonTypeId numeric(18) NOT NULL ,
    certificateNumReqTypeId numeric(18) NOT NULL ,
    expirationRuleTypeId numeric(18) NOT NULL ,
    numberOfYears numeric(18) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT XPKCertValRule
PRIMARY KEY (certificateValidationRuleId);


ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleExpTyp FOREIGN KEY (expirationRuleTypeId)
    REFERENCES ExpirationRuleType (expirationRuleTypeId)
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleReqTyp FOREIGN KEY (certificateNumReqTypeId)
    REFERENCES CertificateNumReqType (certificateNumReqTypeId)
;

create table CertificateNumFormatValidation
(
    certNumFormatValidationId numeric(18) NOT NULL ,
    certificateValidationRuleId numeric(18) NOT NULL ,
    certificateNumberFormatId numeric(18) NOT NULL 
);

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT XPKCertNumFmtVal
PRIMARY KEY (certNumFormatValidationId);


ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValRule FOREIGN KEY (certificateValidationRuleId)
    REFERENCES CertificateValidationRule (certificateValidationRuleId)
;

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValNFmt FOREIGN KEY (certificateNumberFormatId)
    REFERENCES CertificateNumberFormat (certificateNumberFormatId)
;

create table TaxAuthority
(
    taxAuthorityId numeric(18) NOT NULL ,
    taxAuthorityName nvarchar(60) NOT NULL ,
    phoneNumber nvarchar(20) NULL ,
    webSiteAddress nvarchar(100) NULL 
);

ALTER TABLE TaxAuthority
    ADD CONSTRAINT XPKTaxAuth
PRIMARY KEY (taxAuthorityId);


create table TaxAuthorityJurisdiction
(
    taxAuthorityJurisdictionId numeric(18) NOT NULL ,
    taxAuthorityId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT XPKTaxAuthJur
PRIMARY KEY (taxAuthorityJurisdictionId);


ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT f1TaxAuthJur FOREIGN KEY (taxAuthorityId)
    REFERENCES TaxAuthority (taxAuthorityId)
;

create table TaxRegistration
(
    taxRegistrationId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    taxRegistrationIdCode nvarchar(40) NULL ,
    taxRegistrationTypeId numeric(18) NULL ,
    validationTypeId numeric(18) NULL ,
    validationDate numeric(8) NULL ,
    formatValidationTypeId numeric(18) NULL ,
    formatValidationDate numeric(8) NULL ,
    physicalPresInd numeric(1) NOT NULL ,
    allStatesInd numeric(1) NOT NULL ,
    allCitiesInd numeric(1) NOT NULL ,
    allCountiesInd numeric(1) NOT NULL ,
    allOthersInd numeric(1) NOT NULL ,
    allImpositionsInd numeric(1) DEFAULT 1 NULL ,
    jurActiveInd numeric(1) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    filingCurrencyUnitId numeric(18) NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE TaxRegistration
    ADD CONSTRAINT XPKTaxReg
PRIMARY KEY (taxRegistrationId, sourceId);


ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgTxpyr FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId)
;

create table TaxRegistrationJurisdiction
(
    taxRegJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegistrationId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL 
);

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT XPKTxRgJur
PRIMARY KEY (taxRegJurisdictionId, sourceId);


ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

create table TaxRegistrationImposition
(
    taxRegImpositionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegistrationId numeric(18) NOT NULL ,
    taxRegistrationIdCode nvarchar(40) NULL ,
    taxRegistrationTypeId numeric(18) NULL ,
    jurTypeSetId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL ,
    impsnTypeSrcId numeric(18) NOT NULL ,
    impActiveInd numeric(1) NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL ,
    formatValidationTypeId numeric(18) NULL ,
    formatValidationDate numeric(8) NULL 
);

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT XPKTxRgImp
PRIMARY KEY (taxRegImpositionId, sourceId);


ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId)
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

create table TaxRegImpJurisdiction
(
    taxRegImpJurisdictionId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    taxRegImpositionId numeric(18) NOT NULL ,
    taxRegistrationIdCode nvarchar(40) NULL ,
    taxRegistrationTypeId numeric(18) NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    coverageActionTypeId numeric(18) NOT NULL ,
    reverseChargeInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT XPKTxRgImpJur
PRIMARY KEY (taxRegImpJurisdictionId, sourceId);


ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurImp FOREIGN KEY (taxRegImpositionId, sourceId)
    REFERENCES TaxRegistrationImposition (taxRegImpositionId, sourceId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId)
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId)
;

create table LetterTemplateType
(
    letterTemplateTypeId numeric(18) NOT NULL ,
    letterTemplateTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE LetterTemplateType
    ADD CONSTRAINT XPKLtrTempType
PRIMARY KEY (letterTemplateTypeId);


create table LetterDeliveryMethod
(
    letterDeliveryMethodId numeric(18) NOT NULL ,
    letterDeliveryMethodName nvarchar(60) NOT NULL 
);

ALTER TABLE LetterDeliveryMethod
    ADD CONSTRAINT XPKLtrDlvryMthd
PRIMARY KEY (letterDeliveryMethodId);


create table LetterTemplate
(
    letterTemplateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    letterTemplateTypeId numeric(18) NOT NULL ,
    letterTemplateName nvarchar(60) NOT NULL ,
    letterTemplateDesc nvarchar(100) NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE LetterTemplate
    ADD CONSTRAINT XPKLtrTemp
PRIMARY KEY (letterTemplateId, sourceId);


ALTER TABLE LetterTemplate
    ADD CONSTRAINT f1LtrTmpType FOREIGN KEY (letterTemplateTypeId)
    REFERENCES LetterTemplateType (letterTemplateTypeId)
;

create table LetterTemplateText
(
    letterTemplateId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    templateTextSeqNum numeric(18) NOT NULL ,
    templateText nvarchar(2000) NULL 
);

ALTER TABLE LetterTemplateText
    ADD CONSTRAINT XPKLtrTempText
PRIMARY KEY (letterTemplateId, sourceId, templateTextSeqNum);


ALTER TABLE LetterTemplateText
    ADD CONSTRAINT f1LtrTmp FOREIGN KEY (letterTemplateId, sourceId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

create table LetterBatch
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    letterTemplateId numeric(18) NOT NULL ,
    letterTemplateSrcId numeric(18) NOT NULL ,
    certificateStatusId numeric(18) NULL ,
    daysToExpirationNum numeric(18) NULL ,
    daysSinceExpirationNum numeric(18) NULL ,
    expirationRangeStartDate numeric(8) NULL ,
    expirationRangeEndDate numeric(8) NULL ,
    noCertificateInd numeric(1) NULL ,
    incompleteInd numeric(1) NULL ,
    rejectedInd numeric(1) NULL ,
    formReplacedInd numeric(1) NULL ,
    emailSubject nvarchar(100) NULL ,
    letterBatchDesc nvarchar(100) NULL ,
    letterBatchDate numeric(8) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE LetterBatch
    ADD CONSTRAINT XPKLtrBatch
PRIMARY KEY (letterBatchId, sourceId);


ALTER TABLE LetterBatch
    ADD CONSTRAINT f1LtrBtchCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId)
;

ALTER TABLE LetterBatch
    ADD CONSTRAINT f2LtrBtchTmp FOREIGN KEY (letterTemplateId, letterTemplateSrcId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId)
;

create table LetterBatchParty
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL ,
    partyTypeId numeric(18) NOT NULL 
);

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT XPKLtrBatchPty
PRIMARY KEY (letterBatchId, sourceId, partyId);


ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f1LtrBtchPty FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f2LtrBtchPty FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f3LtrBtchPty FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId)
;

create table LetterBatchJurisdiction
(
    letterBatchId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL 
);

ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT XPKLtrBatchJur
PRIMARY KEY (letterBatchId, sourceId, jurisdictionId);


ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT f1LtrBtchJur FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

create table FlexFieldDefVtxPrdTyp
(
    flexFieldDefId numeric(18) NOT NULL ,
    flexFieldDefSrcId numeric(18) NOT NULL ,
    vertexProductId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT XPKFFDefVtxPrdTyp
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId, vertexProductId);


ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f1FFDrfVtxPrdTyp FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId)
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f2FFDrfVtxPrdTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId)
;

create table Letter
(
    letterId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    certificateId numeric(18) NULL ,
    overridePartyId numeric(18) NULL ,
    letterBatchId numeric(18) NOT NULL ,
    letterDeliveryMethodId numeric(18) NOT NULL ,
    formId numeric(18) NULL ,
    formSrcId numeric(18) NULL ,
    letterSentDate numeric(8) NOT NULL 
);

ALTER TABLE Letter
    ADD CONSTRAINT XPKLetter
PRIMARY KEY (letterId, sourceId);


ALTER TABLE Letter
    ADD CONSTRAINT f1LtrDlvryMethod FOREIGN KEY (letterDeliveryMethodId)
    REFERENCES LetterDeliveryMethod (letterDeliveryMethodId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f2LtrLtrBatch FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f3LtrCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f4LtrForm FOREIGN KEY (formId, formSrcId)
    REFERENCES Form (formId, sourceId)
;

ALTER TABLE Letter
    ADD CONSTRAINT f5LtrParty FOREIGN KEY (overridePartyId, sourceId)
    REFERENCES Party (partyId, partySourceId)
;

create table GeographicRegionType
(
    geoRegionTypeId numeric(18) NOT NULL ,
    geoRegionLicenseCode nvarchar(30) NOT NULL ,
    geoRegionDisplayName nvarchar(30) NOT NULL 
);

ALTER TABLE GeographicRegionType
    ADD CONSTRAINT XPKGeoRegionCode
PRIMARY KEY (geoRegionTypeId);


create table JurGeographicRegType
(
    jurGeoRegTypeId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    geoRegionTypeId numeric(18) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT XPKJurGeoRegType
PRIMARY KEY (jurGeoRegTypeId);


ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT f1GeoRegionType FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

create table RoleParty
(
    roleId numeric(18) NOT NULL ,
    sourceId numeric(18) NOT NULL ,
    partyId numeric(18) NOT NULL 
);

ALTER TABLE RoleParty
    ADD CONSTRAINT XPKRoleParty
PRIMARY KEY (roleId, sourceId, partyId);


create table VATRegistrationIdFormat
(
    vatRegistrationIdFormatId numeric(18) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    vatRegistrationIdMask nvarchar(40) NOT NULL ,
    vatRegistrationIdExample nvarchar(40) NULL ,
    vatRegistrationIdDescription nvarchar(255) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL 
);

ALTER TABLE VATRegistrationIdFormat
    ADD CONSTRAINT XPKVATRegIdFmt
PRIMARY KEY (vatRegistrationIdFormatId);


create table CertWizardLocationUser
(
    certWizardUserId numeric(18) NOT NULL ,
    name nvarchar(50) NOT NULL ,
    userId numeric(18) NOT NULL ,
    sourceId numeric(18) NULL ,
    partyId numeric(18) NULL ,
    displayName nvarchar(100) NULL 
);

ALTER TABLE CertWizardLocationUser
    ADD CONSTRAINT CertWizardLocationUser_PK
PRIMARY KEY (certWizardUserId);


create table users
(
    username nvarchar(64) NOT NULL ,
    password nvarchar(255) NOT NULL ,
    enabled numeric(1) NULL ,
    sourceId numeric(18) NULL 
);

ALTER TABLE users
    ADD CONSTRAINT users_PK
PRIMARY KEY (username);


create table authorities
(
    username nvarchar(64) NOT NULL ,
    authority nvarchar(50) NOT NULL ,
    sourceId numeric(18) NULL 
);

ALTER TABLE authorities
    ADD CONSTRAINT f1Auth FOREIGN KEY (username)
    REFERENCES users (username)
;

create table CertWizardUser
(
    loginId numeric(18) NOT NULL ,
    certWizardUserId numeric(18) NOT NULL ,
    userName nvarchar(64) NULL ,
    companyEmail nvarchar(100) NOT NULL ,
    personalEmail nvarchar(100) NULL ,
    oseriesCustomerCode nvarchar(80) NULL ,
    firstName nvarchar(60) NOT NULL ,
    lastName nvarchar(60) NOT NULL ,
    countryJurId numeric(18) NOT NULL ,
    mainDivisionJurId numeric(18) NOT NULL ,
    cityName nvarchar(60) NULL ,
    street1 nvarchar(100) NULL ,
    street2 nvarchar(100) NULL ,
    postalCode nvarchar(20) NULL ,
    phoneNumber nvarchar(20) NULL ,
    securityQuestion1Id numeric(18) NOT NULL ,
    securityQuestion2Id numeric(18) NOT NULL ,
    securityQuestion3Id numeric(18) NOT NULL ,
    securityAnswer1 nvarchar(100) NOT NULL ,
    securityAnswer2 nvarchar(100) NOT NULL ,
    securityAnswer3 nvarchar(100) NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    newCustomerInd numeric(1) NULL 
);

ALTER TABLE CertWizardUser
    ADD CONSTRAINT CertWizardUser_PK
PRIMARY KEY (loginId);


ALTER TABLE CertWizardUser
    ADD CONSTRAINT f2Users FOREIGN KEY (userName)
    REFERENCES users (username)
;

create table SecurityQuestions
(
    securityQuestionId numeric(18) NOT NULL ,
    name nvarchar(255) NOT NULL ,
    question nvarchar(255) NOT NULL 
);

ALTER TABLE SecurityQuestions
    ADD CONSTRAINT SecurityQuestions_PK
PRIMARY KEY (securityQuestionId);


create table DataUpdateImpactTaxRule
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxRuleId numeric(18) NOT NULL ,
    geoRegionTypeId numeric(18) NOT NULL ,
    countryName nvarchar(60) NOT NULL ,
    mainDivisionName nvarchar(60) NULL ,
    jurName nvarchar(60) NOT NULL ,
    jurTypeName nvarchar(60) NOT NULL ,
    impositionName nvarchar(60) NOT NULL ,
    categoryName nvarchar(60) NOT NULL ,
    taxRuleType nvarchar(60) NOT NULL ,
    rate nvarchar(60) NULL ,
    salesTaxHolidayInd numeric(1) DEFAULT 0 NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType nvarchar(60) NOT NULL ,
    details nvarchar(1000) NULL ,
    newCategoryInd numeric(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT XPKDataImpTaxRule
PRIMARY KEY (dataUpdateNumber, taxRuleId, geoRegionTypeId);


ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT f1GeoRegionType1 FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId)
;

create table DataUpdateImpactTaxArea
(
    dataUpdateNumber numeric(8,1) NOT NULL ,
    taxAreaId numeric(18) NOT NULL ,
    countryName nvarchar(60) NOT NULL ,
    mainDivisionName nvarchar(60) NULL ,
    subDivisionName nvarchar(60) NULL ,
    cityName nvarchar(60) NULL ,
    districtName1 nvarchar(60) NULL ,
    districtName2 nvarchar(60) NULL ,
    districtName3 nvarchar(60) NULL ,
    districtName4 nvarchar(60) NULL ,
    districtName5 nvarchar(60) NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    changeType nvarchar(60) NOT NULL ,
    affectedTaxAreaId numeric(18) NOT NULL ,
    affectedCountryName nvarchar(60) NULL ,
    affectedMainDivisionName nvarchar(60) NULL ,
    affectedSubDivisionName nvarchar(60) NULL ,
    affectedCityName nvarchar(60) NULL ,
    affectedDistrictName1 nvarchar(60) NULL ,
    affectedDistrictName2 nvarchar(60) NULL ,
    affectedDistrictName3 nvarchar(60) NULL ,
    affectedDistrictName4 nvarchar(60) NULL ,
    affectedDistrictName5 nvarchar(60) NULL 
);

ALTER TABLE DataUpdateImpactTaxArea
    ADD CONSTRAINT XPKDataImpTaxArea
PRIMARY KEY (dataUpdateNumber, taxAreaId, affectedTaxAreaId);


create table TransactionStatusType
(
    transStatusTypeId numeric(18) NOT NULL ,
    transStatusTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TransactionStatusType
    ADD CONSTRAINT XPKTransStatusType
PRIMARY KEY (transStatusTypeId);


create table FeatureResource
(
    featureResourceId numeric(18) NOT NULL ,
    featureResourceName nvarchar(60) NOT NULL 
);

ALTER TABLE FeatureResource
    ADD CONSTRAINT XPKFeatureResource
PRIMARY KEY (featureResourceId);


create table FeatureResourceImpositionType
(
    featureResourceId numeric(18) NOT NULL ,
    impsnTypeId numeric(18) NOT NULL 
);

ALTER TABLE FeatureResourceImpositionType
    ADD CONSTRAINT XPKFeatResImpType
PRIMARY KEY (featureResourceId, impsnTypeId);


create table FeatureResourceCategory
(
    featureResourceId numeric(18) NOT NULL ,
    txbltyCatId numeric(18) NOT NULL 
);

ALTER TABLE FeatureResourceCategory
    ADD CONSTRAINT XPKFeatResCat
PRIMARY KEY (featureResourceId, txbltyCatId);


create table TransactionEventType
(
    transEventTypeId numeric(18) NOT NULL ,
    transEventTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE TransactionEventType
    ADD CONSTRAINT XPKTransEventType
PRIMARY KEY (transEventTypeId);


create table TpsDataReleaseEvent
(
    fullRlsId numeric(18) NULL ,
    interimRlsId numeric(18) NULL ,
    rlsTypeId numeric(1) NULL ,
    appliedDate numeric(8) NOT NULL ,
    rlsName nvarchar(255) NULL 
);

create table TaxAssistPhaseType
(
    phaseId numeric(18) NOT NULL ,
    phaseName nvarchar(60) NOT NULL 
);

ALTER TABLE TaxAssistPhaseType
    ADD CONSTRAINT XPKTaxAstPhaseType
PRIMARY KEY (phaseId);


create table VATRegimeType
(
    vatRegimeTypeId numeric(18) NOT NULL ,
    vatRegimeTypeName nvarchar(60) NOT NULL 
);

ALTER TABLE VATRegimeType
    ADD CONSTRAINT XPKVATRegimeType
PRIMARY KEY (vatRegimeTypeId);


create table VATGroup
(
    vatGroupId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    vatGrpCreationDate numeric(8) NOT NULL 
);

ALTER TABLE VATGroup
    ADD CONSTRAINT XPKVATGroup
PRIMARY KEY (vatGroupId, vatGroupSourceId);


create table VATGroupDetail
(
    vatGroupDtlId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    vatGroupId numeric(18) NOT NULL ,
    creationDate numeric(8) NOT NULL ,
    effDate numeric(8) NOT NULL ,
    endDate numeric(8) NOT NULL ,
    repMemTxprId numeric(18) NOT NULL ,
    vatGroupIdentifier nvarchar(15) NOT NULL ,
    vatGroupName nvarchar(30) NOT NULL ,
    jurisdictionId numeric(18) NOT NULL ,
    vatRegimeTypeId numeric(18) NOT NULL ,
    deletedInd numeric(1) NOT NULL 
);

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT XPKVATGroupDtl
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId);


ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VatGroup FOREIGN KEY (vatGroupId, vatGroupSourceId)
    REFERENCES VATGroup (vatGroupId, vatGroupSourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f31Party FOREIGN KEY (repMemTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VATRegimeType FOREIGN KEY (vatRegimeTypeId)
    REFERENCES VATRegimeType (vatRegimeTypeId)
;

create table VATGroupMembers
(
    vatGroupDtlId numeric(18) NOT NULL ,
    vatGroupSourceId numeric(18) NOT NULL ,
    memTxprId numeric(18) NOT NULL 
);

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT XPKVATGroupMembers
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId, memTxprId);


ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f1VATGroupDtl FOREIGN KEY (vatGroupDtlId, vatGroupSourceId)
    REFERENCES VATGroupDetail (vatGroupDtlId, vatGroupSourceId)
;

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId)
;

