CREATE INDEX XIF1JurTypSetMem ON JurTypeSetMember (jurTypeSetId ASC);

CREATE INDEX XIF1DMFilterDate ON DMFilterDate (filterId ASC);

CREATE INDEX XIF6SitusCondition ON SitusCondition (jurTypeSetId ASC);

CREATE INDEX XIF4SitusCondition ON SitusCondition (locRoleType2Id ASC);

CREATE INDEX XIF3SitusCondition ON SitusCondition (locRoleTypeId ASC);

CREATE INDEX XIF1SitusCondition ON SitusCondition (transactionTypeId ASC);

CREATE INDEX XIF2SitusCondition ON SitusCondition (situsCondTypeId ASC);

CREATE INDEX XIF1ReasonCatJur ON ReasonCategoryJur (reasonCategoryId ASC);

CREATE INDEX XIF2SitusCondJur ON SitusCondJur (situsConditionId ASC);

CREATE INDEX XIF2SitusCondShpTm ON SitusCondShippingTerms (situsConditionId ASC);

CREATE INDEX XIF1SitusConc ON SitusConclusion (locRoleTypeId ASC);

CREATE INDEX XIF7SitusConc ON SitusConclusion (situsConcTypeId ASC);

CREATE INDEX XIF5SitusConc ON SitusConclusion (locRoleType2Id ASC);

CREATE INDEX XIF3SitusConc ON SitusConclusion (multiSitusRecTypId ASC);

CREATE INDEX XIF4SitusConc ON SitusConclusion (jurTypeSetId ASC);

CREATE INDEX XIF6SitusConc ON SitusConclusion (taxType2Id ASC);

CREATE INDEX XIF2SitusConc ON SitusConclusion (taxTypeId ASC);

CREATE INDEX XIF1RoundingRule ON RoundingRule (taxScopeId ASC);

CREATE INDEX XIF1DMFilterStrng ON DMFilterStrng (filterId ASC);

CREATE INDEX XA1TaxCategory ON TaxabilityCategory (txbltyCatSrcId ASC);

CREATE INDEX XIF1TaxCatDetail ON TxbltyCatDetail (txbltyCatId ASC, txbltyCatSrcId ASC);

CREATE INDEX XIF2TaxCatDetail ON TxbltyCatDetail (prntTxbltyCatId ASC, prntTxbltyCatSrcId ASC);

CREATE INDEX XIF1TaxJurDetail ON TaxJurDetail (jurisdictionId ASC, taxTypeId ASC);

CREATE INDEX XIF1DMActLogNum ON DMActivityLogNum (activityLogId ASC);

CREATE INDEX XIF1DMActLogDate ON DMActivityLogDate (activityLogId ASC);

CREATE INDEX XIF1DMActLogInd ON DMActivityLogInd (activityLogId ASC);

CREATE INDEX XIF1SitusCondNode ON SitusConditionNode (situsConditionId ASC);

CREATE INDEX XIF1TrueSitusCond ON TrueSitusCond (chldTruSitusCondId ASC);

CREATE INDEX XIF2TrueSitusCond ON TrueSitusCond (prntTruSitusCondId ASC);

CREATE INDEX XIF1SitusConcNode ON SitusConcNode (flsForSitusCondId ASC);

CREATE INDEX XIF2SitusConcNode ON SitusConcNode (truForSitusCondId ASC);

CREATE INDEX XIF3SitusConcNode ON SitusConcNode (situsConclusionId ASC);

CREATE INDEX XIF1PartyDtl ON PartyDetail (partyTypeId ASC);

CREATE INDEX XIF3PartyDtl ON PartyDetail (shippingTermsId ASC);

CREATE INDEX XIF4PartyDtl ON PartyDetail (partyId ASC, partySourceId ASC, partyTypeId ASC, deletedInd ASC);

CREATE INDEX XIF5PartyDtl ON PartyDetail (userPartyIdCode ASC, partySourceId ASC, partyTypeId ASC, deletedInd ASC);

CREATE INDEX XIF2PrtyRolTaxRslt ON PartyRoleTaxResult (partyRoleTypeId ASC);

CREATE INDEX XIF4PrtyRolTaxRslt ON PartyRoleTaxResult (reasonCategoryId ASC);

CREATE INDEX XIF3PrtyRolTaxRslt ON PartyRoleTaxResult (taxResultTypeId ASC);

CREATE INDEX XA1PrtyRoleTaxRslt ON PartyRoleTaxResult (partyId ASC,partySourceId ASC,taxResultTypeId ASC);

CREATE INDEX XIF1PrtyRolTaxRslt ON PartyRoleTaxResult (partyId ASC,partySourceId ASC);

CREATE INDEX XIF2PrtyVtxProdTyp ON PartyVtxProdType (vertexProductId ASC);

CREATE INDEX XIF1PrtyVtxProdTyp ON PartyVtxProdType (partyId ASC,partySourceId ASC);

CREATE INDEX XIF2BusinessLoc ON BusinessLocation (partyRoleTypeId ASC);

CREATE INDEX XIF1BusinessLoc ON BusinessLocation (partyId ASC,sourceId ASC);

CREATE INDEX XIF1PartyContact ON PartyContact (partyId ASC,sourceId ASC);

CREATE INDEX XIF1TxDvrDtlNtKey ON TxbltyDriverDetail (taxpayerPartyId ASC, txbltyDvrSrcId ASC,inputParamTypeId ASC, txbltyDvrCode ASC);

CREATE INDEX XIF2TxDvrDtl ON TxbltyDriverDetail (txbltyDvrId ASC);

CREATE INDEX XIF1TaxDvrPrdTyp ON TxbltyDvrVtxPrdTyp (txbltyDvrId ASC,txbltyDvrSrcId ASC);

CREATE INDEX XIF1DMFilterInd ON DMFilterInd (filterId ASC);

CREATE INDEX XIF1DiscountType ON DiscountType (discountCatId ASC);

CREATE INDEX XIF2DiscountType ON DiscountType (taxpayerPartyId ASC,sourceId ASC);

CREATE INDEX XIF6TaxStructure ON TaxStructure (brcktTaxCalcType ASC);

CREATE INDEX XIF5TaxStructure ON TaxStructure (reductAmtDedTypeId ASC);

CREATE INDEX XIF1TaxStructure ON TaxStructure (taxStructureTypeId ASC);

CREATE INDEX XAK1TaxImpsnDtl ON TaxImpsnDetail (taxImpsnId ASC, jurisdictionId ASC, taxImpsnSrcId ASC);

CREATE INDEX XAK1TaxImpQualCond ON TaxImpQualCond (taxImpsnDtlId ASC, taxImpsnSrcId ASC);

CREATE INDEX XIF4Tier ON Tier (reasonCategoryId ASC);

CREATE INDEX XIF2Tier ON Tier (taxStructureId ASC,taxStructureSrcId ASC);

CREATE INDEX XIF3Tier ON Tier (taxResultTypeId ASC);

CREATE INDEX XIF3Bracket ON Bracket (taxStructureId ASC,taxStructureSrcId ASC);

CREATE INDEX XIF3TaxRule ON TaxRule (reasonCategoryId ASC);

CREATE INDEX XIF7TaxRule ON TaxRule (taxStructureId ASC, taxStructureSrcId ASC);

CREATE INDEX XIF5TaxRule ON TaxRule (taxScopeId ASC);

CREATE INDEX XIF6TaxRule ON TaxRule (taxResultTypeId ASC);

CREATE INDEX XIF4TaxRule ON TaxRule (taxRuleTypeId ASC);

CREATE INDEX XIF15TaxRule ON TaxRule (discountTypeId ASC,discountTypeSrcId ASC);

CREATE INDEX XXAK1TaxRule ON TaxRule (jurisdictionId ASC,taxImpsnId ASC, taxImpsnSrcId ASC);

CREATE UNIQUE INDEX XAK1TaxRuleNote ON TaxRuleNote (taxRuleId ASC);

CREATE INDEX XIF1TaxRuleNote ON TaxRuleNote (taxRuleId ASC, taxRuleSourceId ASC);

CREATE INDEX XAK1TaxRulQualCond ON TaxRuleQualCond (taxRuleId ASC, taxRuleSourceId ASC);

CREATE INDEX XAK1TaxRuleTaxType ON TaxRuleTaxType (taxRuleId ASC, taxRuleSourceId ASC,taxTypeId ASC);

CREATE INDEX XAK1TxRuleTaxImpsn ON TaxRuleTaxImposition (taxRuleId ASC, taxRuleSourceId ASC);

CREATE INDEX XIF1TaxFactor ON TaxFactor (taxFactorTypeId ASC);

CREATE INDEX XIF2TaxFactor ON TaxFactor (basisTypeId ASC);

CREATE INDEX XIF1CompFactor ON ComputationFactor (computationTypeId ASC);

CREATE INDEX XIF3CompFactor ON ComputationFactor (leftTaxFactorId ASC,sourceId ASC);

CREATE INDEX XIF2CompFactor ON ComputationFactor (rightTaxFactorId ASC,sourceId ASC);

CREATE INDEX XIF1CondTaxExpr ON ConditionalTaxExpr (taxRuleId ASC, sourceId ASC);

CREATE INDEX XIF2CondTaxExpr ON ConditionalTaxExpr (exprCondTypeId ASC);

CREATE INDEX XIF3CondTaxExpr ON ConditionalTaxExpr (leftTaxFactorId ASC, sourceId ASC);

CREATE INDEX XIF4CondTaxExpr ON ConditionalTaxExpr (rightTaxFactorId ASC, sourceId ASC);

CREATE INDEX XIF3TaxBasisConc ON TaxBasisConclusion (taxRuleId ASC, sourceId ASC);

CREATE INDEX XIF4TaxBasisConc ON TaxBasisConclusion (taxFactorId ASC, sourceId ASC);

CREATE INDEX XIF2TaxBasisConc ON TaxBasisConclusion (taxTypeId ASC);

CREATE INDEX XIF4TaxabilityMap ON TaxabilityMapping (taxabilityMapId ASC);

CREATE INDEX XIF3TaxabilityMap ON TaxabilityMapping (taxRuleId ASC, taxRuleSourceId ASC);

CREATE INDEX XIF2FalseSitusCond ON FalseSitusCond (prntFlsSitusCondId ASC);

CREATE INDEX XIF1FalseSitusCond ON FalseSitusCond (chldFlsSitusCondId ASC);

CREATE INDEX XIF3TaxRuleTranTyp ON TaxRuleTransType (taxRuleId ASC, taxRuleSourceId ASC);

CREATE INDEX XIF2TaxRuleTranTyp ON TaxRuleTransType (transactionTypeId ASC);

CREATE INDEX XIF1DMActLogStrng ON DMActivityLogStrng (activityLogId ASC);

CREATE INDEX XIF1DMActivityLogF ON DMActivityLogFile (statusNum ASC, activityLogId ASC);

CREATE INDEX XIF2DMActivityLogF ON DMActivityLogFile (fileName ASC, activityLogId ASC);

CREATE INDEX XIF1SitusCndTaxCat ON SitusCondTxbltyCat (situsConditionId ASC);

CREATE INDEX XIF1TxRlCondJur ON TaxRuleCondJur (taxRuleId ASC,taxRuleSourceId ASC);

CREATE INDEX XIF1DMFilterNum ON DMFilterNum (filterId ASC);

CREATE INDEX XIF1TxAstLookupRcd ON TaxAssistLookupRcd (tableId ASC, sourceId ASC);

CREATE INDEX XIF1TxAstAllocCol ON TaxAssistAllocationColumn (allocationTableId ASC, sourceId ASC);

CREATE INDEX XIF1TxAstAllocCV ON TaxAssistAllocationColumnValue (allocationTableId ASC,sourceId ASC,columnId ASC);

CREATE INDEX XIF1FormDtlIds ON FormDetail (formId ASC,sourceId ASC);

CREATE INDEX XIF1FormJuris ON FormJurisdiction (formId ASC,sourceId ASC);

CREATE INDEX XIF1FormFieldDef ON FormFieldDef (formId ASC,sourceId ASC);

CREATE INDEX XIF1CertDtlIds ON CertificateDetail (certificateId ASC,sourceId ASC);

CREATE INDEX XIF2CertDtlPrtySrc ON CertificateDetail (certificateHolderPartyId ASC, sourceId ASC);

CREATE INDEX XIF3EcwCertSrc ON CertificateDetail (ecwCertificateId ASC);

CREATE INDEX XIF4UUID ON CertificateDetail (uuid ASC);

CREATE INDEX XIF1CertCvrgIds ON CertificateCoverage (certificateId ASC,sourceId ASC);

CREATE INDEX XIF2CertCvrgJurId ON CertificateCoverage (jurisdictionId ASC);

CREATE INDEX XIF1CertJurCovId ON CertificateJurisdiction (certificateCoverageId ASC);

CREATE INDEX XIF2CertJurJurId ON CertificateJurisdiction (jurisdictionId ASC);

CREATE INDEX XIF1CertImpCovId ON CertificateImposition (certificateCoverageId ASC);

CREATE INDEX XIF1CertImgIds ON CertificateImage (certificateId ASC,sourceId ASC);

CREATE INDEX XIF1CertTxbltyDvr ON CertTxbltyDriver (certificateId ASC,sourceId ASC);

CREATE INDEX XIF2CertTxbltyDvr ON CertTxbltyDriver (txbltyDvrId ASC,sourceId ASC);

CREATE INDEX XIF3CertTxDvrCtId ON CertTxbltyDriver (certificateId ASC);

CREATE INDEX XIF1CrtTrnsTypCtId ON CertificateTransType (certificateId ASC);

CREATE INDEX XIF1TaxReg ON TaxRegistration (partyId ASC,sourceId ASC);

CREATE INDEX XIF1TaxRegJur ON TaxRegistrationJurisdiction (taxRegistrationId ASC);

CREATE INDEX XIF1TaxRegImp ON TaxRegistrationImposition (taxRegistrationId ASC);

CREATE INDEX XIF1VATGroupDtl ON VATGroupDetail (vatGroupId ASC, vatGroupSourceId ASC, vatGroupDtlId ASC, deletedInd ASC);

CREATE INDEX XIF1VATGrpMembers ON VATGroupMembers (vatGroupDtlId ASC,vatGroupSourceId ASC);

