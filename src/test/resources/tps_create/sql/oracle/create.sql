create table CoverageActionType
(
    coverageActionTypeId NUMBER(18) NOT NULL ,
    coverageActionTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CoverageActionType
    ADD ( CONSTRAINT XPKJICvrgActTyp
PRIMARY KEY (coverageActionTypeId));


create table CertificateStatus
(
    certificateStatusId NUMBER(18) NOT NULL ,
    certificateStatusName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CertificateStatus
    ADD ( CONSTRAINT XPKJICertStat
PRIMARY KEY (certificateStatusId));


create table CertificateApprovalStatus
(
    approvalStatusId NUMBER(18) NOT NULL ,
    certApprovalStatusName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CertificateApprovalStatus
    ADD ( CONSTRAINT XPKCertApprStatus
PRIMARY KEY (approvalStatusId));


create table ContactRoleType
(
    contactRoleTypeId NUMBER(18) NOT NULL ,
    contactRoleTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE ContactRoleType
    ADD ( CONSTRAINT XPKContactRoleType
PRIMARY KEY (contactRoleTypeId));


create table FormFieldType
(
    formFieldTypeId NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NULL 
);

ALTER TABLE FormFieldType
    ADD ( CONSTRAINT XPKFormField
PRIMARY KEY (formFieldTypeId));


create table TaxRegistrationType
(
    taxRegistrationTypeId NUMBER(18) NOT NULL ,
    taxRegistrationTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxRegistrationType
    ADD ( CONSTRAINT XPKTaxRegType
PRIMARY KEY (taxRegistrationTypeId));


create table ApportionmentType
(
    apportionTypeId NUMBER(18) NOT NULL ,
    apportionTypeName NVARCHAR2(30) NOT NULL 
);

ALTER TABLE ApportionmentType
    ADD ( CONSTRAINT XPKApportionType
PRIMARY KEY (apportionTypeId));


create table SitusConditionType
(
    situsCondTypeId NUMBER(18) NOT NULL ,
    situsCondTypeName NVARCHAR2(30) NULL 
);

ALTER TABLE SitusConditionType
    ADD ( CONSTRAINT XPKSitusCondType
PRIMARY KEY (situsCondTypeId));


create table ChainTransType
(
    chainTransId NUMBER(18) NOT NULL ,
    chainTransName NVARCHAR2(60) NULL 
);

ALTER TABLE ChainTransType
    ADD ( CONSTRAINT XPKChainTransType
PRIMARY KEY (chainTransId));


create table TitleTransferType
(
    titleTransferId NUMBER(18) NOT NULL ,
    titleTransferName NVARCHAR2(60) NULL 
);

ALTER TABLE TitleTransferType
    ADD ( CONSTRAINT XPKTitleTransType
PRIMARY KEY (titleTransferId));


create table AssistedState
(
    assistedStateId NUMBER(18) NOT NULL ,
    assistedStateName NVARCHAR2(60) NULL 
);

ALTER TABLE AssistedState
    ADD ( CONSTRAINT XPKAssistedState
PRIMARY KEY (assistedStateId));


create table RateClassification
(
    rateClassId NUMBER(18) NOT NULL ,
    rateClassName NVARCHAR2(60) NULL 
);

ALTER TABLE RateClassification
    ADD ( CONSTRAINT XPKRateClass
PRIMARY KEY (rateClassId));


create table DMFilter
(
    filterId NUMBER(18) NOT NULL ,
    filterName NVARCHAR2(60) NOT NULL ,
    sourceId NUMBER(18) NULL ,
    activityTypeId NUMBER(18) NOT NULL ,
    filterDescription NVARCHAR2(255) NULL ,
    followupInd NUMBER(1) NULL 
);

ALTER TABLE DMFilter
    ADD ( CONSTRAINT XPKDMFilter
PRIMARY KEY (filterId));


create table BusinessTransType
(
    busTransTypeId NUMBER(18) NOT NULL ,
    busTransTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE BusinessTransType
    ADD ( CONSTRAINT XPKBusTransType
PRIMARY KEY (busTransTypeId));


create table TransactionType
(
    transactionTypeId NUMBER(18) NOT NULL ,
    transactionTypName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TransactionType
    ADD ( CONSTRAINT XPKTransactionType
PRIMARY KEY (transactionTypeId));


create table DataType
(
    dataTypeId NUMBER(18) NOT NULL ,
    dataTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE DataType
    ADD ( CONSTRAINT XPKDataType
PRIMARY KEY (dataTypeId));


create table InputParameterType
(
    inputParamTypeId NUMBER(18) NOT NULL ,
    inputParamTypeName NVARCHAR2(60) NULL ,
    lookupStrategyId NUMBER(18) NULL ,
    commodityCodeInd NUMBER(1) NULL ,
    commodityCodeLength NUMBER(18) NULL ,
    isTelecommLineType NUMBER(1) NULL 
);

ALTER TABLE InputParameterType
    ADD ( CONSTRAINT XPKInputParamType
PRIMARY KEY (inputParamTypeId));


create table JurTypeSet
(
    jurTypeSetId NUMBER(18) NOT NULL ,
    jurTypeSetName NVARCHAR2(60) NULL 
);

ALTER TABLE JurTypeSet
    ADD ( CONSTRAINT XPKJurTypeSet
PRIMARY KEY (jurTypeSetId));


create table JurTypeSetMember
(
    jurTypeSetId NUMBER(18) NOT NULL ,
    jurisdictionTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE JurTypeSetMember
    ADD ( CONSTRAINT XPKJurTypeSetMem
PRIMARY KEY (jurTypeSetId, jurisdictionTypeId));


ALTER TABLE JurTypeSetMember
    ADD ( CONSTRAINT f1JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

create table TaxResultType
(
    taxResultTypeId NUMBER(18) NOT NULL ,
    taxResultTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxResultType
    ADD ( CONSTRAINT XPKTaxResultType
PRIMARY KEY (taxResultTypeId));


create table RecoverableResultType
(
    recoverableResultTypeId NUMBER(18) NOT NULL ,
    recoverableResultTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE RecoverableResultType
    ADD ( CONSTRAINT XPKRcvResultType
PRIMARY KEY (recoverableResultTypeId));


create table PartyType
(
    partyTypeId NUMBER(18) NOT NULL ,
    partyTypeName NVARCHAR2(20) NOT NULL 
);

ALTER TABLE PartyType
    ADD ( CONSTRAINT XPKPartyType
PRIMARY KEY (partyTypeId));


create table LocationRoleType
(
    locationRoleTypeId NUMBER(18) NOT NULL ,
    locationRoleTypNam NVARCHAR2(30) NOT NULL 
);

ALTER TABLE LocationRoleType
    ADD ( CONSTRAINT XPKLocationRoleTyp
PRIMARY KEY (locationRoleTypeId));


create table ShippingTerms
(
    shippingTermsId NUMBER(18) NOT NULL ,
    shippingTermsName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE ShippingTerms
    ADD ( CONSTRAINT XPKShippingTerms
PRIMARY KEY (shippingTermsId));


create table BasisType
(
    basisTypeId NUMBER(18) NOT NULL ,
    basisTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE BasisType
    ADD ( CONSTRAINT XPKBasisType
PRIMARY KEY (basisTypeId));


create table DMFilterDate
(
    dateTypeId NUMBER(18) NOT NULL ,
    criteriaDate NUMBER(8) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    filterId NUMBER(18) NOT NULL 
);

ALTER TABLE DMFilterDate
    ADD ( CONSTRAINT XPKDMFilterDate
PRIMARY KEY (filterId, dateTypeId, criteriaOrderNum));


ALTER TABLE DMFilterDate
    ADD ( CONSTRAINT f1DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId))
;

create table OutputNotice
(
    outputNoticeId NUMBER(18) NOT NULL ,
    outputNoticeName NVARCHAR2(30) NOT NULL 
);

ALTER TABLE OutputNotice
    ADD ( CONSTRAINT XPKOutputNotice
PRIMARY KEY (outputNoticeId));


create table CustomsStatusType
(
    customsStatusId NUMBER(18) NOT NULL ,
    customsStatusName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CustomsStatusType
    ADD ( CONSTRAINT XPKCustomsStatType
PRIMARY KEY (customsStatusId));


create table CreationSource
(
    creationSourceId NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CreationSource
    ADD ( CONSTRAINT XPKCreationSource
PRIMARY KEY (creationSourceId));


create table MovementMethodType
(
    movementMethodId NUMBER(18) NOT NULL ,
    movementMethodName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE MovementMethodType
    ADD ( CONSTRAINT XPKMvmntMethodType
PRIMARY KEY (movementMethodId));


create table SitusCondition
(
    situsConditionId NUMBER(18) NOT NULL ,
    locRoleTypeId NUMBER(18) NULL ,
    locRoleType2Id NUMBER(18) NULL ,
    situsCondTypeId NUMBER(18) NULL ,
    transactionTypeId NUMBER(18) NULL ,
    transPrspctvTypId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    jurisdictionTypeId NUMBER(18) NULL ,
    endDate NUMBER(8) NOT NULL ,
    jurTypeSetId NUMBER(18) NULL ,
    partyRoleTypeId NUMBER(18) NULL ,
    currencyUnitId NUMBER(18) NULL ,
    locRoleTypeId1Name NVARCHAR2(60) NULL ,
    locRoleTypeId2Name NVARCHAR2(60) NULL ,
    stsSubRtnNodeId NUMBER(18) NULL ,
    boolFactName NVARCHAR2(60) NULL ,
    boolFactValue NUMBER(1) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL ,
    customsStatusId NUMBER(18) NULL ,
    movementMethodId NUMBER(18) NULL ,
    titleTransferId NUMBER(18) NULL 
);

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT XPKSitusCondition
PRIMARY KEY (situsConditionId));


ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f2LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f1SCdTyp FOREIGN KEY (situsCondTypeId)
    REFERENCES SitusConditionType (situsCondTypeId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f3TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f1LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f3JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f1CstStT FOREIGN KEY (customsStatusId)
    REFERENCES CustomsStatusType (customsStatusId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f1MvMthT FOREIGN KEY (movementMethodId)
    REFERENCES MovementMethodType (movementMethodId))
;

ALTER TABLE SitusCondition
    ADD ( CONSTRAINT f1TtTfrT FOREIGN KEY (titleTransferId)
    REFERENCES TitleTransferType (titleTransferId))
;

create table BracketTaxCalcType
(
    brcktTaxCalcTypeId NUMBER(18) NOT NULL ,
    brcktTaxCalcTypNam NVARCHAR2(60) NULL 
);

ALTER TABLE BracketTaxCalcType
    ADD ( CONSTRAINT XPKBrcktTaxCalcTyp
PRIMARY KEY (brcktTaxCalcTypeId));


create table SitusConcType
(
    situsConcTypeId NUMBER(18) NOT NULL ,
    situsConcTypeName NVARCHAR2(60) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE SitusConcType
    ADD ( CONSTRAINT XPKSitusConcType
PRIMARY KEY (situsConcTypeId));


create table ReasonCategory
(
    reasonCategoryId NUMBER(18) NOT NULL ,
    reasonCategoryName NVARCHAR2(60) NULL ,
    isUserDefined NUMBER(1) NULL 
);

ALTER TABLE ReasonCategory
    ADD ( CONSTRAINT XPKReasonCategory
PRIMARY KEY (reasonCategoryId));


create table ReasonCategoryJur
(
    reasonCategoryId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NULL 
);

ALTER TABLE ReasonCategoryJur
    ADD ( CONSTRAINT XPKReasonCatJur
PRIMARY KEY (reasonCategoryId, jurisdictionId, effDate));


ALTER TABLE ReasonCategoryJur
    ADD ( CONSTRAINT f7RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

create table SitusCondJur
(
    situsConditionId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL 
);

ALTER TABLE SitusCondJur
    ADD ( CONSTRAINT XPKSitusCondJur
PRIMARY KEY (situsConditionId, jurisdictionId));


ALTER TABLE SitusCondJur
    ADD ( CONSTRAINT f2StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId))
;

create table SitusCondShippingTerms
(
    situsConditionId NUMBER(18) NOT NULL ,
    shippingTermsId NUMBER(18) NOT NULL 
);

ALTER TABLE SitusCondShippingTerms
    ADD ( CONSTRAINT XPKSitusCondShipTm
PRIMARY KEY (situsConditionId, shippingTermsId));


ALTER TABLE SitusCondShippingTerms
    ADD ( CONSTRAINT f4StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId))
;

create table TaxType
(
    taxTypeId NUMBER(18) NOT NULL ,
    taxTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxType
    ADD ( CONSTRAINT XPKTaxType
PRIMARY KEY (taxTypeId));


create table FilingCategory
(
    filingCategoryId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    filingCategoryCode NUMBER(5) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    filingCategoryName NVARCHAR2(60) NOT NULL ,
    primaryCategoryInd NUMBER(1) NOT NULL 
);

ALTER TABLE FilingCategory
    ADD ( CONSTRAINT XPKFilingCat
PRIMARY KEY (filingCategoryId));


create table FilingCategoryOvrd
(
    filingCatOvrdId NUMBER(18) NOT NULL ,
    filingCategoryId NUMBER(18) NOT NULL ,
    ovrdFilingCatId NUMBER(18) NOT NULL ,
    taxTypeId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE FilingCategoryOvrd
    ADD ( CONSTRAINT XPKFCOvd
PRIMARY KEY (filingCatOvrdId));


ALTER TABLE FilingCategoryOvrd
    ADD ( CONSTRAINT f2FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId))
;

ALTER TABLE FilingCategoryOvrd
    ADD ( CONSTRAINT f3FilCat FOREIGN KEY (ovrdFilingCatId)
    REFERENCES FilingCategory (filingCategoryId))
;

ALTER TABLE FilingCategoryOvrd
    ADD ( CONSTRAINT f5TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table SitusConclusion
(
    situsConclusionId NUMBER(18) NOT NULL ,
    situsConcTypeId NUMBER(18) NULL ,
    taxTypeId NUMBER(18) NULL ,
    jurisdictionTypeId NUMBER(18) NULL ,
    locRoleTypeId NUMBER(18) NULL ,
    locRoleType2Id NUMBER(18) NULL ,
    multiSitusRecTypId NUMBER(18) NULL ,
    jurTypeSetId NUMBER(18) NULL ,
    taxType2Id NUMBER(18) NULL ,
    locRoleTypeId1Name NVARCHAR2(60) NULL ,
    locRoleTypeId2Name NVARCHAR2(60) NULL ,
    taxTypeIdName NVARCHAR2(60) NULL ,
    boolFactName NVARCHAR2(60) NULL ,
    boolFactValue NUMBER(1) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL ,
    outputNoticeId NUMBER(18) NULL ,
    impsnTypeId NUMBER(18) NULL ,
    impsnTypeSourceId NUMBER(18) NULL ,
    txbltyCatId NUMBER(18) NULL 
);

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT XPKSitusConc
PRIMARY KEY (situsConclusionId));


ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f1OutNot FOREIGN KEY (outputNoticeId)
    REFERENCES OutputNotice (outputNoticeId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f3LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f4JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f1SCcTyp FOREIGN KEY (situsConcTypeId)
    REFERENCES SitusConcType (situsConcTypeId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f3TaxTyp FOREIGN KEY (taxType2Id)
    REFERENCES TaxType (taxTypeId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f4LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusConclusion
    ADD ( CONSTRAINT f4TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table TaxScope
(
    taxScopeId NUMBER(18) NOT NULL ,
    taxScopeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxScope
    ADD ( CONSTRAINT XPKTaxScope
PRIMARY KEY (taxScopeId));


create table RoundingRule
(
    roundingRuleId NUMBER(18) NOT NULL ,
    initialPrecision NUMBER(15) NULL ,
    taxScopeId NUMBER(18) NOT NULL ,
    finalPrecision NUMBER(15) NOT NULL ,
    threshold NUMBER(15) NOT NULL ,
    decimalPosition NUMBER(15) NOT NULL ,
    roundingTypeId NUMBER(18) NULL 
);

ALTER TABLE RoundingRule
    ADD ( CONSTRAINT XPKRoundingRule
PRIMARY KEY (roundingRuleId));


ALTER TABLE RoundingRule
    ADD ( CONSTRAINT f2TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId))
;

create table TransSubType
(
    transSubTypeId NUMBER(18) NOT NULL ,
    transSubTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE TransSubType
    ADD ( CONSTRAINT XPKTransSubType
PRIMARY KEY (transSubTypeId));


create table VertexProductType
(
    vertexProductId NUMBER(18) NOT NULL ,
    vertexProductName NVARCHAR2(60) NULL 
);

ALTER TABLE VertexProductType
    ADD ( CONSTRAINT XPKVertexProdType
PRIMARY KEY (vertexProductId));


create table PartyRoleType
(
    partyRoleTypeId NUMBER(18) NOT NULL ,
    partyRoleTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE PartyRoleType
    ADD ( CONSTRAINT XPKPartyRoleType
PRIMARY KEY (partyRoleTypeId));


create table TelecomUnitConversion
(
    telecomUnitConversionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NOT NULL ,
    sourceUnitOfMeasureISOCode NVARCHAR2(20) NOT NULL ,
    targetUnitOfMeasureISOCode NVARCHAR2(20) NOT NULL ,
    firstConvertCount NUMBER(8) NOT NULL ,
    additionalConvertCount NUMBER(8) NULL ,
    isDefault NUMBER(1) NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversion
    ADD ( CONSTRAINT XPKTelComUntConvsn
PRIMARY KEY (telecomUnitConversionId, sourceId));


create table TaxStructureType
(
    taxStructureTypeId NUMBER(18) NOT NULL ,
    taxStrucTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxStructureType
    ADD ( CONSTRAINT XPKTaxStrucType
PRIMARY KEY (taxStructureTypeId));


create table DeductionType
(
    deductionTypeId NUMBER(18) NOT NULL ,
    deductionTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE DeductionType
    ADD ( CONSTRAINT XPKDeductionType
PRIMARY KEY (deductionTypeId));


create table TransOrigType
(
    transOrigTypeId NUMBER(18) NOT NULL ,
    transOrigTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE TransOrigType
    ADD ( CONSTRAINT XPKTransOrigType
PRIMARY KEY (transOrigTypeId));


create table TaxRuleType
(
    taxRuleTypeId NUMBER(18) NOT NULL ,
    taxRuleTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE TaxRuleType
    ADD ( CONSTRAINT XPKTaxRuleType
PRIMARY KEY (taxRuleTypeId));


create table WithholdingType
(
    withholdingTypeId NUMBER(18) NOT NULL ,
    withholdingTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE WithholdingType
    ADD ( CONSTRAINT XPKWithholdingType
PRIMARY KEY (withholdingTypeId));


create table AccumulationByType
(
    id NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NULL 
);

ALTER TABLE AccumulationByType
    ADD ( CONSTRAINT XPKAccByType
PRIMARY KEY (id));


create table AccumulationPeriodType
(
    id NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NULL 
);

ALTER TABLE AccumulationPeriodType
    ADD ( CONSTRAINT XPKAccPeriodType
PRIMARY KEY (id));


create table AccumulationType
(
    id NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NULL 
);

ALTER TABLE AccumulationType
    ADD ( CONSTRAINT XPKAccType
PRIMARY KEY (id));


create table TaxRuleTaxImpositionType
(
    taxRuleTaxImpositionTypeId NUMBER(18) NOT NULL ,
    taxRuleTaxImpositionTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE TaxRuleTaxImpositionType
    ADD ( CONSTRAINT XPKTxRuleImpsnType
PRIMARY KEY (taxRuleTaxImpositionTypeId));


create table ValidationType
(
    validationTypeId NUMBER(18) NOT NULL ,
    validationTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE ValidationType
    ADD ( CONSTRAINT XPKValidationType
PRIMARY KEY (validationTypeId));


create table DMFilterStrng
(
    stringTypeId NUMBER(18) NOT NULL ,
    criteriaString NVARCHAR2(255) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    filterId NUMBER(18) NOT NULL 
);

ALTER TABLE DMFilterStrng
    ADD ( CONSTRAINT XPKDMFilterStrng
PRIMARY KEY (filterId, stringTypeId, criteriaOrderNum));


ALTER TABLE DMFilterStrng
    ADD ( CONSTRAINT f3DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId))
;

create table CertClassType
(
    certClassTypeId NUMBER(18) NOT NULL ,
    certClassTypeName NVARCHAR2(60) NULL 
);

ALTER TABLE CertClassType
    ADD ( CONSTRAINT XPKCertClassType
PRIMARY KEY (certClassTypeId));


create table DiscountCategory
(
    discountCatId NUMBER(18) NOT NULL ,
    discountCatName NVARCHAR2(60) NOT NULL ,
    discountCatDesc NVARCHAR2(100) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE DiscountCategory
    ADD ( CONSTRAINT XPKDiscountCat
PRIMARY KEY (discountCatId));


create table TaxabilityCategory
(
    txbltyCatId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxabilityCategory
    ADD ( CONSTRAINT XPKTaxabilityCat
PRIMARY KEY (txbltyCatId, txbltyCatSrcId));


create table TxbltyCatDetail
(
    txbltyCatDtlId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    txbltyCatCode NVARCHAR2(60) NOT NULL ,
    txbltyCatName NVARCHAR2(60) NOT NULL ,
    txbltyCatDesc NVARCHAR2(1000) NULL ,
    txbltyCatDefaultId NUMBER(18) NOT NULL ,
    prntTxbltyCatId NUMBER(18) NULL ,
    prntTxbltyCatSrcId NUMBER(18) NULL ,
    dataTypeId NUMBER(18) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    allowRelatedInd NUMBER(1) NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TxbltyCatDetail
    ADD ( CONSTRAINT XPKTxbltyCatDetail
PRIMARY KEY (txbltyCatDtlId, txbltyCatSrcId));


ALTER TABLE TxbltyCatDetail
    ADD ( CONSTRAINT f1TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TxbltyCatDetail
    ADD ( CONSTRAINT f2TaxCat FOREIGN KEY (prntTxbltyCatId, prntTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TxbltyCatDetail
    ADD ( CONSTRAINT f1DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId))
;

create table FlexFieldDef
(
    flexFieldDefId NUMBER(18) NOT NULL ,
    flexFieldDefSrcId NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE FlexFieldDef
    ADD ( CONSTRAINT XPKFlexibleField
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId));


create table FlexFieldDefDetail
(
    flexFieldDefDtlId NUMBER(18) NOT NULL ,
    flexFieldDefSrcId NUMBER(18) NOT NULL ,
    flexFieldDefId NUMBER(18) NOT NULL ,
    dataTypeId NUMBER(18) NOT NULL ,
    calcOutputInd NUMBER(1) DEFAULT 0 NOT NULL ,
    flexFieldDesc NVARCHAR2(1000) NULL ,
    flexFieldDefRefNum NUMBER(2) NOT NULL ,
    flexFieldDefSeqNum NUMBER(8) NOT NULL ,
    shortName NVARCHAR2(10) NOT NULL ,
    longName NVARCHAR2(60) NULL ,
    txbltyCatId NUMBER(18) NULL ,
    txbltyCatSrcId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE FlexFieldDefDetail
    ADD ( CONSTRAINT XPKFlexFieldDetail
PRIMARY KEY (flexFieldDefDtlId, flexFieldDefSrcId));


ALTER TABLE FlexFieldDefDetail
    ADD ( CONSTRAINT f2DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId))
;

ALTER TABLE FlexFieldDefDetail
    ADD ( CONSTRAINT f5TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE FlexFieldDefDetail
    ADD ( CONSTRAINT f3FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId))
;

create table TaxJurDetail
(
    taxTypeId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    regGroupAllowedInd NUMBER(1) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    registrationReqInd NUMBER(1) NOT NULL ,
    reqLocsForRprtgInd NUMBER(1) NOT NULL ,
    reqLocsForSitusInd NUMBER(1) NOT NULL 
);

ALTER TABLE TaxJurDetail
    ADD ( CONSTRAINT XPKTaxJurDetail
PRIMARY KEY (jurisdictionId, taxTypeId, effDate, sourceId));


ALTER TABLE TaxJurDetail
    ADD ( CONSTRAINT f16TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table DMActivityLog
(
    activityLogId NUMBER(18) NOT NULL ,
    filterName NVARCHAR2(60) NULL ,
    activityTypeId NUMBER(18) NOT NULL ,
    startTime DATE NOT NULL ,
    endTime DATE NULL ,
    lastPingTime DATE NOT NULL ,
    nextPingTime DATE NOT NULL ,
    userName NVARCHAR2(60) NOT NULL ,
    userId NUMBER(18) NOT NULL ,
    sourceName NVARCHAR2(60) NULL ,
    sourceId NUMBER(18) NULL ,
    activityStatusId NUMBER(18) NOT NULL ,
    filterDescription NVARCHAR2(255) NULL ,
    activityLogMessage NVARCHAR2(1000) NULL ,
    hostName NVARCHAR2(128) NOT NULL ,
    outputFileName NVARCHAR2(255) NULL ,
    masterAdminInd NUMBER(1) NULL ,
    sysAdminInd NUMBER(1) NULL ,
    followupInd NUMBER(1) NULL 
);

ALTER TABLE DMActivityLog
    ADD ( CONSTRAINT XPKDMActivityLog
PRIMARY KEY (activityLogId));


create table DMActivityLogNum
(
    numberTypeId NUMBER(18) NOT NULL ,
    criteriaNum NUMBER(18) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    activityLogId NUMBER(18) NOT NULL 
);

ALTER TABLE DMActivityLogNum
    ADD ( CONSTRAINT XPKDMActLogNum
PRIMARY KEY (activityLogId, numberTypeId, criteriaOrderNum));


ALTER TABLE DMActivityLogNum
    ADD ( CONSTRAINT f2DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId))
;

create table DMActivityLogDate
(
    dateTypeId NUMBER(18) NOT NULL ,
    criteriaDate NUMBER(8) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    activityLogId NUMBER(18) NOT NULL 
);

ALTER TABLE DMActivityLogDate
    ADD ( CONSTRAINT XPKDMActLogDate
PRIMARY KEY (activityLogId, dateTypeId, criteriaOrderNum));


ALTER TABLE DMActivityLogDate
    ADD ( CONSTRAINT f1DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId))
;

create table DMActivityLogInd
(
    indicatorTypeId NUMBER(18) NOT NULL ,
    criteriaIndicator NUMBER(1) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    activityLogId NUMBER(18) NOT NULL 
);

ALTER TABLE DMActivityLogInd
    ADD ( CONSTRAINT XPKDMActLogInd
PRIMARY KEY (activityLogId, indicatorTypeId, criteriaOrderNum));


ALTER TABLE DMActivityLogInd
    ADD ( CONSTRAINT f4DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId))
;

create table TpsSchVersion
(
    schemaVersionId NUMBER(18) NOT NULL ,
    subjectAreaId NUMBER(18) NULL ,
    schemaVersionCode NVARCHAR2(30) NOT NULL ,
    syncVersionId NVARCHAR2(64) NULL ,
    schemaSubVersionId NUMBER(18) NULL 
);

ALTER TABLE TpsSchVersion
    ADD ( CONSTRAINT XPKTpsSchemaVer
PRIMARY KEY (schemaVersionId));


create table TpsPatchDataEvent
(
    dataEventId NUMBER(18) NOT NULL ,
    patchId NUMBER(18) NOT NULL ,
    patchInterimId NUMBER(18) NOT NULL ,
    eventDate NUMBER(8) NOT NULL 
);

ALTER TABLE TpsPatchDataEvent
    ADD ( CONSTRAINT XPKTPSPchDataEvent
PRIMARY KEY (dataEventId));


create table ApportionmentFriendlyStates
(
    friendlyStateId NUMBER(18) NOT NULL ,
    jurId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE ApportionmentFriendlyStates
    ADD ( CONSTRAINT XPKApptionFriendly
PRIMARY KEY (friendlyStateId));


create table SitusConditionNode
(
    situsCondNodeId NUMBER(18) NOT NULL ,
    situsConditionId NUMBER(18) NOT NULL ,
    rootNodeInd NUMBER(1) NOT NULL 
);

ALTER TABLE SitusConditionNode
    ADD ( CONSTRAINT XPKSitusCondNode
PRIMARY KEY (situsCondNodeId));


ALTER TABLE SitusConditionNode
    ADD ( CONSTRAINT f1StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId))
;

create table TrueSitusCond
(
    prntTruSitusCondId NUMBER(18) NOT NULL ,
    chldTruSitusCondId NUMBER(18) NOT NULL 
);

ALTER TABLE TrueSitusCond
    ADD ( CONSTRAINT XPKTrueSitusCond
PRIMARY KEY (prntTruSitusCondId, chldTruSitusCondId));


ALTER TABLE TrueSitusCond
    ADD ( CONSTRAINT f1StCdNd FOREIGN KEY (prntTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

ALTER TABLE TrueSitusCond
    ADD ( CONSTRAINT f2StCdNd FOREIGN KEY (chldTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

create table SitusConcNode
(
    situsConcNodeId NUMBER(18) NOT NULL ,
    situsConclusionId NUMBER(18) NOT NULL ,
    truForSitusCondId NUMBER(18) NULL ,
    flsForSitusCondId NUMBER(18) NULL 
);

ALTER TABLE SitusConcNode
    ADD ( CONSTRAINT XPKSitusConcNode
PRIMARY KEY (situsConcNodeId));


ALTER TABLE SitusConcNode
    ADD ( CONSTRAINT f1StsCnc FOREIGN KEY (situsConclusionId)
    REFERENCES SitusConclusion (situsConclusionId))
;

ALTER TABLE SitusConcNode
    ADD ( CONSTRAINT f5StCdNd FOREIGN KEY (truForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

ALTER TABLE SitusConcNode
    ADD ( CONSTRAINT f6StCdNd FOREIGN KEY (flsForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

create table Party
(
    partyId NUMBER(18) NOT NULL ,
    partySourceId NUMBER(18) NOT NULL ,
    partyCreationDate NUMBER(8) NOT NULL 
);

ALTER TABLE Party
    ADD ( CONSTRAINT XPKParty
PRIMARY KEY (partyId, partySourceId));


create table PartyDetail
(
    partyDtlId NUMBER(18) NOT NULL ,
    partySourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    creationSourceId NUMBER(18) DEFAULT 1 NOT NULL ,
    parentPartyId NUMBER(18) NULL ,
    partyTypeId NUMBER(18) NOT NULL ,
    partyName NVARCHAR2(60) NOT NULL ,
    userPartyIdCode NVARCHAR2(40) NOT NULL ,
    partyRelInd NUMBER(1) NOT NULL ,
    taxpayerType NVARCHAR2(60) NULL ,
    filingEntityInd NUMBER(1) NOT NULL ,
    prntInheritenceInd NUMBER(1) NOT NULL ,
    ersInd NUMBER(1) NOT NULL ,
    taxThresholdAmt NUMBER(18,3) NULL ,
    taxThresholdPct NUMBER(10,6) NULL ,
    taxOvrThresholdAmt NUMBER(18,3) NULL ,
    taxOvrThresholdPct NUMBER(10,6) NULL ,
    partyClassInd NUMBER(1) NOT NULL ,
    shippingTermsId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    discountCatId NUMBER(18) NULL ,
    customField1Value NVARCHAR2(60) NULL ,
    customField2Value NVARCHAR2(60) NULL ,
    customField3Value NVARCHAR2(60) NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL ,
    parentCustomerId NUMBER(18) NULL 
);

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT XPKPartyDtl
PRIMARY KEY (partyDtlId, partySourceId));


ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f1ShpTrm FOREIGN KEY (shippingTermsId)
    REFERENCES ShippingTerms (shippingTermsId))
;

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f1PtyTyp FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId))
;

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f1Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f2Party FOREIGN KEY (parentPartyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f33Party FOREIGN KEY (parentCustomerId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE PartyDetail
    ADD ( CONSTRAINT f5DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId))
;

create table PartyRoleTaxResult
(
    partyId NUMBER(18) NOT NULL ,
    partySourceId NUMBER(18) NOT NULL ,
    partyRoleTypeId NUMBER(18) NOT NULL ,
    taxResultTypeId NUMBER(18) NOT NULL ,
    reasonCategoryId NUMBER(18) NULL 
);

ALTER TABLE PartyRoleTaxResult
    ADD ( CONSTRAINT XPKPrtyRoleTaxRslt
PRIMARY KEY (partyId, partyRoleTypeId, partySourceId));


ALTER TABLE PartyRoleTaxResult
    ADD ( CONSTRAINT f5RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

ALTER TABLE PartyRoleTaxResult
    ADD ( CONSTRAINT f3Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE PartyRoleTaxResult
    ADD ( CONSTRAINT f3TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId))
;

ALTER TABLE PartyRoleTaxResult
    ADD ( CONSTRAINT f1PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

create table PartyVtxProdType
(
    partyId NUMBER(18) NOT NULL ,
    partySourceId NUMBER(18) NOT NULL ,
    vertexProductId NUMBER(18) NOT NULL 
);

ALTER TABLE PartyVtxProdType
    ADD ( CONSTRAINT XPKPrtyVtxProdTyp
PRIMARY KEY (partyId, partySourceId, vertexProductId));


ALTER TABLE PartyVtxProdType
    ADD ( CONSTRAINT f1VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

ALTER TABLE PartyVtxProdType
    ADD ( CONSTRAINT f8Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

create table BusinessLocation
(
    partyId NUMBER(18) NOT NULL ,
    businessLocationId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxAreaId NUMBER(18) NULL ,
    userLocationCode NVARCHAR2(20) NULL ,
    registrationCode NVARCHAR2(20) NULL ,
    streetInfoDesc NVARCHAR2(100) NULL ,
    streetInfo2Desc NVARCHAR2(100) NULL ,
    cityName NVARCHAR2(60) NULL ,
    subDivisionName NVARCHAR2(60) NULL ,
    mainDivisionName NVARCHAR2(60) NULL ,
    postalCode NVARCHAR2(20) NULL ,
    countryName NVARCHAR2(60) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    busLocationName NVARCHAR2(60) NULL ,
    partyRoleTypeId NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE BusinessLocation
    ADD ( CONSTRAINT XPKBusinessLoc
PRIMARY KEY (businessLocationId, partyId, sourceId));


ALTER TABLE BusinessLocation
    ADD ( CONSTRAINT f4Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE BusinessLocation
    ADD ( CONSTRAINT f3PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

create table PartyNote
(
    partySourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    partyNoteText NVARCHAR2(1000) NULL 
);

ALTER TABLE PartyNote
    ADD ( CONSTRAINT XPKPartyNote
PRIMARY KEY (partyId, partySourceId));


ALTER TABLE PartyNote
    ADD ( CONSTRAINT f14Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

create table PartyContact
(
    partyContactId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    contactRoleTypeId NUMBER(18) NOT NULL ,
    contactFirstName NVARCHAR2(60) NULL ,
    contactLastName NVARCHAR2(60) NULL ,
    streetInfoDesc NVARCHAR2(100) NULL ,
    streetInfo2Desc NVARCHAR2(100) NULL ,
    cityName NVARCHAR2(60) NULL ,
    mainDivisionName NVARCHAR2(60) NULL ,
    postalCode NVARCHAR2(20) NULL ,
    countryName NVARCHAR2(60) NULL ,
    phoneNumber NVARCHAR2(20) NULL ,
    phoneExtension NVARCHAR2(20) NULL ,
    faxNumber NVARCHAR2(20) NULL ,
    emailAddress NVARCHAR2(100) NULL ,
    departmentIdCode NVARCHAR2(20) NULL ,
    deletedInd NUMBER(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE PartyContact
    ADD ( CONSTRAINT XPKPartyContact
PRIMARY KEY (partyContactId, sourceId));


ALTER TABLE PartyContact
    ADD ( CONSTRAINT f6Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE PartyContact
    ADD ( CONSTRAINT f1CntcRT FOREIGN KEY (contactRoleTypeId)
    REFERENCES ContactRoleType (contactRoleTypeId))
;

create table TaxabilityDriver
(
    txbltyDvrId NUMBER(18) NOT NULL ,
    txbltyDvrSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxabilityDriver
    ADD ( CONSTRAINT XPKTaxabilityDvr
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId));


create table TxbltyDriverDetail
(
    txbltyDvrDtlId NUMBER(18) NOT NULL ,
    txbltyDvrSrcId NUMBER(18) NOT NULL ,
    txbltyDvrId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    inputParamTypeId NUMBER(18) NOT NULL ,
    txbltyDvrCode NVARCHAR2(40) NOT NULL ,
    txbltyDvrName NVARCHAR2(60) NOT NULL ,
    reasonCategoryId NUMBER(18) NULL ,
    exemptInd NUMBER(1) NOT NULL ,
    taxpayerPartyId NUMBER(18) NULL ,
    taxpayerSrcId NUMBER(18) NULL ,
    discountCatId NUMBER(18) NULL ,
    flexFieldDefId NUMBER(18) NULL ,
    flexFieldDefSrcId NUMBER(18) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT XPKTxbltyDvrDetail
PRIMARY KEY (txbltyDvrDtlId, txbltyDvrSrcId));


ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT f1TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT f8RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT f2InPTyp FOREIGN KEY (inputParamTypeId)
    REFERENCES InputParameterType (inputParamTypeId))
;

ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT f3DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId))
;

ALTER TABLE TxbltyDriverDetail
    ADD ( CONSTRAINT f2FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId))
;

create table TxbltyDriverNote
(
    txbltyDvrId NUMBER(18) NOT NULL ,
    txbltyDvrSrcId NUMBER(18) NOT NULL ,
    txbltyDvrNoteText NVARCHAR2(1000) NULL 
);

ALTER TABLE TxbltyDriverNote
    ADD ( CONSTRAINT XPKTxbltyDvrNote
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId));


ALTER TABLE TxbltyDriverNote
    ADD ( CONSTRAINT f9TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

create table TxbltyDvrVtxPrdTyp
(
    vertexProductId NUMBER(18) NOT NULL ,
    txbltyDvrId NUMBER(18) NOT NULL ,
    txbltyDvrSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD ( CONSTRAINT XPKTxbltyDvrVtxPrd
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId, vertexProductId));


ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD ( CONSTRAINT f2TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD ( CONSTRAINT f8VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table TelecomUnitConversionLineType
(
    telecomUnitConvnLineTypeId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    lineTypeId NUMBER(18) NOT NULL ,
    lineTypeSourceId NUMBER(18) NOT NULL ,
    telecomUnitConversionId NUMBER(18) NOT NULL ,
    telecomUnitConversionSourceId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversionLineType
    ADD ( CONSTRAINT XPKTCUntCvnLnType
PRIMARY KEY (telecomUnitConvnLineTypeId, sourceId));


ALTER TABLE TelecomUnitConversionLineType
    ADD ( CONSTRAINT f1conv FOREIGN KEY (telecomUnitConversionId, sourceId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId))
;

ALTER TABLE TelecomUnitConversionLineType
    ADD ( CONSTRAINT f2LineType FOREIGN KEY (lineTypeId, lineTypeSourceId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

create table TxbltyCatMap
(
    txbltyCatMapId NUMBER(18) NOT NULL ,
    txbltyCatMapSrcId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL ,
    taxpayerPartyId NUMBER(18) NULL ,
    otherPartyId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    txbltyCatMapNote NVARCHAR2(1000) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TxbltyCatMap
    ADD ( CONSTRAINT XPKTxbltyCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId));


ALTER TABLE TxbltyCatMap
    ADD ( CONSTRAINT f7TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TxbltyCatMap
    ADD ( CONSTRAINT f24Party FOREIGN KEY (taxpayerPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE TxbltyCatMap
    ADD ( CONSTRAINT f25Party FOREIGN KEY (otherPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId))
;

create table TxbltyDriverCatMap
(
    txbltyCatMapId NUMBER(18) NOT NULL ,
    txbltyCatMapSrcId NUMBER(18) NOT NULL ,
    txbltyDvrId NUMBER(18) NOT NULL 
);

ALTER TABLE TxbltyDriverCatMap
    ADD ( CONSTRAINT XPKTxbltyDvrCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId));


ALTER TABLE TxbltyDriverCatMap
    ADD ( CONSTRAINT f5TaxDvr FOREIGN KEY (txbltyDvrId, txbltyCatMapSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

ALTER TABLE TxbltyDriverCatMap
    ADD ( CONSTRAINT f1TxCMap FOREIGN KEY (txbltyCatMapId, txbltyCatMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId))
;

create table InvoiceTextType
(
    invoiceTextTypeId NUMBER(18) NOT NULL ,
    invoiceTextTypeSrcId NUMBER(18) NOT NULL ,
    invoiceTextTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE InvoiceTextType
    ADD ( CONSTRAINT XPKInvoiceTextType
PRIMARY KEY (invoiceTextTypeId, invoiceTextTypeSrcId));


create table InvoiceText
(
    invoiceTextId NUMBER(18) NOT NULL ,
    invoiceTextSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE InvoiceText
    ADD ( CONSTRAINT XPKInvText
PRIMARY KEY (invoiceTextId, invoiceTextSrcId));


create table InvoiceTextDetail
(
    invoiceTextDtlId NUMBER(18) NOT NULL ,
    invoiceTextSrcId NUMBER(18) NOT NULL ,
    invoiceTextId NUMBER(18) NOT NULL ,
    invoiceTextTypeId NUMBER(18) NOT NULL ,
    invoiceTextTypeSrcId NUMBER(18) NOT NULL ,
    invoiceTextCode NVARCHAR2(60) NOT NULL ,
    invoiceText NVARCHAR2(200) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE InvoiceTextDetail
    ADD ( CONSTRAINT XPKInvTextDetail
PRIMARY KEY (invoiceTextDtlId, invoiceTextSrcId));


ALTER TABLE InvoiceTextDetail
    ADD ( CONSTRAINT f1InvText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId))
;

ALTER TABLE InvoiceTextDetail
    ADD ( CONSTRAINT f2InvTextTyp FOREIGN KEY (invoiceTextTypeId, invoiceTextTypeSrcId)
    REFERENCES InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId))
;

create table DMFilterInd
(
    indicatorTypeId NUMBER(18) NOT NULL ,
    criteriaIndicator NUMBER(1) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    filterId NUMBER(18) NOT NULL 
);

ALTER TABLE DMFilterInd
    ADD ( CONSTRAINT XPKDMFilterInd
PRIMARY KEY (filterId, indicatorTypeId, criteriaOrderNum));


ALTER TABLE DMFilterInd
    ADD ( CONSTRAINT f4DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId))
;

create table DiscountType
(
    sourceId NUMBER(18) NOT NULL ,
    discountTypeId NUMBER(18) NOT NULL ,
    taxpayerPartyId NUMBER(18) NOT NULL ,
    discountCatId NUMBER(18) NOT NULL ,
    discountCode NVARCHAR2(20) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE DiscountType
    ADD ( CONSTRAINT XPKDiscountType
PRIMARY KEY (discountTypeId, sourceId));


ALTER TABLE DiscountType
    ADD ( CONSTRAINT f1DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId))
;

ALTER TABLE DiscountType
    ADD ( CONSTRAINT f2DscCat FOREIGN KEY (taxpayerPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

create table TaxStructure
(
    taxStructureSrcId NUMBER(18) NOT NULL ,
    taxStructureId NUMBER(18) NOT NULL ,
    taxStructureTypeId NUMBER(18) NOT NULL ,
    brcktMaximumBasis NUMBER(18,3) NULL ,
    reductAmtDedTypeId NUMBER(18) NULL ,
    reductReasonCategoryId NUMBER(18) NULL ,
    allAtTopTierTypInd NUMBER(1) NOT NULL ,
    singleRate NUMBER(12,8) NULL ,
    taxPerUnitAmount NUMBER(18,6) NULL ,
    unitOfMeasureQty NUMBER(18,6) NOT NULL ,
    childTaxStrucSrcId NUMBER(18) NULL ,
    unitBasedInd NUMBER(1) NOT NULL ,
    basisReductFactor NUMBER(12,8) NULL ,
    brcktTaxCalcType NUMBER(18) NULL ,
    unitOfMeasISOCode NVARCHAR2(3) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    childTaxStrucId NUMBER(18) NULL ,
    usesStdRateInd NUMBER(1) NOT NULL ,
    flatTaxAmt NUMBER(18,3) NULL ,
    telecomUnitConversionId NUMBER(18) NULL ,
    telecomUnitConversionSrcId NUMBER(18) NULL 
);

ALTER TABLE TaxStructure
    ADD ( CONSTRAINT XPKTaxStructure
PRIMARY KEY (taxStructureId, taxStructureSrcId));


ALTER TABLE TaxStructure
    ADD ( CONSTRAINT f1TxSTyp FOREIGN KEY (taxStructureTypeId)
    REFERENCES TaxStructureType (taxStructureTypeId))
;

ALTER TABLE TaxStructure
    ADD ( CONSTRAINT f1BTCTyp FOREIGN KEY (brcktTaxCalcType)
    REFERENCES BracketTaxCalcType (brcktTaxCalcTypeId))
;

ALTER TABLE TaxStructure
    ADD ( CONSTRAINT f1DedTyp FOREIGN KEY (reductAmtDedTypeId)
    REFERENCES DeductionType (deductionTypeId))
;

ALTER TABLE TaxStructure
    ADD ( CONSTRAINT f1TelCon FOREIGN KEY (telecomUnitConversionId, telecomUnitConversionSrcId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId))
;

create table ImpositionType
(
    impsnTypeId NUMBER(18) NOT NULL ,
    impsnTypeSrcId NUMBER(18) NOT NULL ,
    impsnTypeName NVARCHAR2(60) NOT NULL ,
    creditInd NUMBER(1) NULL ,
    notInTotalInd NUMBER(1) NULL ,
    withholdingTypeId NUMBER(18) NULL 
);

ALTER TABLE ImpositionType
    ADD ( CONSTRAINT XPKImpositionType
PRIMARY KEY (impsnTypeId, impsnTypeSrcId));


ALTER TABLE ImpositionType
    ADD ( CONSTRAINT f1WitTyp FOREIGN KEY (withholdingTypeId)
    REFERENCES WithholdingType (withholdingTypeId))
;

create table TaxImposition
(
    jurisdictionId NUMBER(18) NOT NULL ,
    taxImpsnId NUMBER(18) NOT NULL ,
    taxImpsnSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxImposition
    ADD ( CONSTRAINT XPKTaxImp
PRIMARY KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId));


create table TaxImpsnDetail
(
    taxImpsnDtlId NUMBER(18) NOT NULL ,
    taxImpsnSrcId NUMBER(18) NOT NULL ,
    taxImpsnId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    impsnTypeId NUMBER(18) NOT NULL ,
    impsnTypeSrcId NUMBER(18) NOT NULL ,
    taxImpsnName NVARCHAR2(60) NOT NULL ,
    taxImpsnAbrv NVARCHAR2(15) NULL ,
    taxImpsnDesc NVARCHAR2(100) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    taxTypeId NUMBER(18) NULL ,
    taxResponsibilityRoleTypeId NUMBER(18) NULL ,
    conditionInd NUMBER(1) NULL ,
    primaryImpositionInd NUMBER(1) NULL 
);

ALTER TABLE TaxImpsnDetail
    ADD ( CONSTRAINT XPKTaxImpsnDetail
PRIMARY KEY (taxImpsnDtlId, taxImpsnSrcId));


ALTER TABLE TaxImpsnDetail
    ADD ( CONSTRAINT f1ImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId))
;

ALTER TABLE TaxImpsnDetail
    ADD ( CONSTRAINT f6TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId))
;

ALTER TABLE TaxImpsnDetail
    ADD ( CONSTRAINT f18TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

ALTER TABLE TaxImpsnDetail
    ADD ( CONSTRAINT f1TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

create table TaxImpQualCond
(
    taxImpQualCondId NUMBER(18) NOT NULL ,
    taxImpsnSrcId NUMBER(18) NOT NULL ,
    taxImpsnDtlId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxImpQualCond
    ADD ( CONSTRAINT XPKTaxImpQualCond
PRIMARY KEY (taxImpQualCondId, taxImpsnSrcId));


ALTER TABLE TaxImpQualCond
    ADD ( CONSTRAINT f9TaxImp FOREIGN KEY (taxImpsnDtlId, taxImpsnSrcId)
    REFERENCES TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId))
;

ALTER TABLE TaxImpQualCond
    ADD ( CONSTRAINT f12TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

create table FilingOverride
(
    filingOverrideId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    jurTypeSetId NUMBER(18) NOT NULL ,
    filingCategoryId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE FilingOverride
    ADD ( CONSTRAINT XPKFilingOverride
PRIMARY KEY (filingOverrideId));


ALTER TABLE FilingOverride
    ADD ( CONSTRAINT f5FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId))
;

ALTER TABLE FilingOverride
    ADD ( CONSTRAINT f5JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

create table Tier
(
    minBasisAmount NUMBER(18,3) NULL ,
    tierNum NUMBER(3) NOT NULL ,
    taxResultTypeId NUMBER(18) NULL ,
    maxBasisAmount NUMBER(18,3) NULL ,
    minQuantity NUMBER(18,6) NULL ,
    maxQuantity NUMBER(18,6) NULL ,
    unitOfMeasISOCode NVARCHAR2(3) NULL ,
    unitOfMeasureQty NUMBER(18,6) NULL ,
    taxPerUnitAmount NUMBER(18,6) NULL ,
    tierTaxRate NUMBER(12,8) NULL ,
    taxStructureSrcId NUMBER(18) NOT NULL ,
    taxStructureId NUMBER(18) NOT NULL ,
    reasonCategoryId NUMBER(18) NULL ,
    usesStdRateInd NUMBER(1) NOT NULL ,
    filingCategoryId NUMBER(18) NULL ,
    rateClassId NUMBER(18) NULL ,
    taxStructureTypeId NUMBER(18) NULL ,
    equivalentQuantity NUMBER(18,6) NULL ,
    additionalQuantity NUMBER(18,6) NULL ,
    equivalentAdditionalQuantity NUMBER(18,6) NULL 
);

ALTER TABLE Tier
    ADD ( CONSTRAINT XPKTier
PRIMARY KEY (tierNum, taxStructureSrcId, taxStructureId));


ALTER TABLE Tier
    ADD ( CONSTRAINT f2TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId))
;

ALTER TABLE Tier
    ADD ( CONSTRAINT f5TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId))
;

ALTER TABLE Tier
    ADD ( CONSTRAINT f4RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

ALTER TABLE Tier
    ADD ( CONSTRAINT f6FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId))
;

ALTER TABLE Tier
    ADD ( CONSTRAINT f3RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId))
;

create table Bracket
(
    taxStructureId NUMBER(18) NOT NULL ,
    taxStructureSrcId NUMBER(18) NOT NULL ,
    minBasisAmount NUMBER(18,3) NULL ,
    bracketNum NUMBER(3) NOT NULL ,
    maxBasisAmount NUMBER(18,3) NULL ,
    bracketTaxAmount NUMBER(18,3) NULL 
);

ALTER TABLE Bracket
    ADD ( CONSTRAINT XPKBracket
PRIMARY KEY (taxStructureId, taxStructureSrcId, bracketNum));


ALTER TABLE Bracket
    ADD ( CONSTRAINT f4TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId))
;

create table TaxRule
(
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    partyRoleTypeId NUMBER(18) NULL ,
    partyId NUMBER(18) NULL ,
    partySourceId NUMBER(18) NULL ,
    taxpayerRoleTypeId NUMBER(18) NULL ,
    taxpayerPartyId NUMBER(18) NULL ,
    taxpayerPartySrcId NUMBER(18) NULL ,
    qualDetailDesc NVARCHAR2(100) NULL ,
    uniqueToLevelInd NUMBER(1) NOT NULL ,
    conditionSeqNum NUMBER(18) NULL ,
    taxRuleTypeId NUMBER(18) NULL ,
    taxResultTypeId NUMBER(18) NULL ,
    recoverableResultTypeId NUMBER(18) NULL ,
    defrdJurTypeId NUMBER(18) NULL ,
    defersToStdRuleInd NUMBER(1) NOT NULL ,
    taxStructureSrcId NUMBER(18) NULL ,
    taxStructureId NUMBER(18) NULL ,
    automaticRuleInd NUMBER(1) NOT NULL ,
    standardRuleInd NUMBER(1) NOT NULL ,
    reasonCategoryId NUMBER(18) NULL ,
    filingCategoryId NUMBER(18) NULL ,
    appToSingleImpInd NUMBER(1) NULL ,
    appToSingleJurInd NUMBER(1) NULL ,
    discountTypeId NUMBER(18) NULL ,
    discountTypeSrcId NUMBER(18) NULL ,
    maxTaxRuleType NUMBER(18) NULL ,
    taxScopeId NUMBER(18) NULL ,
    maxTaxAmount NUMBER(18,3) NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    taxImpsnId NUMBER(18) NOT NULL ,
    taxImpsnSrcId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    discountCatId NUMBER(18) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    apportionTypeId NUMBER(18) NULL ,
    appnTxbltyCatId NUMBER(18) NULL ,
    appnTxbltyCatSrcId NUMBER(18) NULL ,
    appnFlexFieldDefId NUMBER(18) NULL ,
    appnFlexFieldDefSrcId NUMBER(18) NULL ,
    rateClassId NUMBER(18) NULL ,
    locationRoleTypeId NUMBER(18) NULL ,
    applyFilingCatInd NUMBER(1) DEFAULT 0 NULL ,
    recoverablePct NUMBER(10,6) NULL ,
    taxResponsibilityRoleTypeId NUMBER(18) NULL ,
    currencyUnitId NUMBER(18) NULL ,
    salestaxHolidayInd NUMBER(1) NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL ,
    includesAllTaxCats NUMBER(1) NULL ,
    invoiceTextId NUMBER(18) NULL ,
    invoiceTextSrcId NUMBER(18) NULL ,
    computationTypeId NUMBER(18) NULL ,
    defrdImpsnTypeId NUMBER(18) NULL ,
    defrdImpsnTypeSrcId NUMBER(18) NULL ,
    notAllowZeroRateInd NUMBER(1) NULL ,
    accumulationAsJurisdictionId NUMBER(18) NULL ,
    accumulationAsTaxImpsnId NUMBER(18) NULL ,
    accumulationAsTaxImpsnSrcId NUMBER(18) NULL ,
    accumulationTypeId NUMBER(18) NULL ,
    periodTypeId NUMBER(18) NULL ,
    startMonth NUMBER(2) NULL ,
    maxLines NUMBER(18) NULL ,
    unitOfMeasISOCode NVARCHAR2(3) NULL ,
    telecomUnitConversionId NUMBER(18) NULL ,
    telecomUnitConversionSrcId NUMBER(18) NULL ,
    lineTypeCatId NUMBER(18) NULL ,
    lineTypeCatSourceId NUMBER(18) NULL 
);

ALTER TABLE TaxRule
    ADD ( CONSTRAINT XPKTaxRule
PRIMARY KEY (taxRuleId, taxRuleSourceId));


ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1TRlTyp FOREIGN KEY (taxRuleTypeId)
    REFERENCES TaxRuleType (taxRuleTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f2PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f3RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1DscTyp FOREIGN KEY (discountTypeId, discountTypeSrcId)
    REFERENCES DiscountType (discountTypeId, sourceId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f6TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f4DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f28Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f30Party FOREIGN KEY (taxpayerPartyId, taxpayerPartySrcId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1ApprtTyp FOREIGN KEY (apportionTypeId)
    REFERENCES ApportionmentType (apportionTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f7LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f2TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1InvoiceText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f2ImpTyp FOREIGN KEY (defrdImpsnTypeId, defrdImpsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId))
;

ALTER TABLE TaxRule
    ADD ( CONSTRAINT f1RcvRTyp FOREIGN KEY (recoverableResultTypeId)
    REFERENCES RecoverableResultType (recoverableResultTypeId))
;

create table TaxRuleNote
(
    taxRuleNoteId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleNoteSrcId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleNoteText NVARCHAR2(1000) NULL 
);

ALTER TABLE TaxRuleNote
    ADD ( CONSTRAINT XPKTaxRuleNote
PRIMARY KEY (taxRuleNoteId, taxRuleNoteSrcId));


ALTER TABLE TaxRuleNote
    ADD ( CONSTRAINT f4TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table ExprConditionType
(
    exprCondTypeId NUMBER(18) NOT NULL ,
    exprCondTypeName NVARCHAR2(60) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE ExprConditionType
    ADD ( CONSTRAINT XPKExprCondType
PRIMARY KEY (exprCondTypeId));


create table TaxRuleQualCond
(
    taxRuleQualCondId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    txbltyDvrId NUMBER(18) NULL ,
    txbltyDvrSrcId NUMBER(18) NULL ,
    txbltyCatId NUMBER(18) NULL ,
    txbltyCatSrcId NUMBER(18) NULL ,
    flexFieldDefId NUMBER(18) NULL ,
    flexFieldDefSrcId NUMBER(18) NULL ,
    minDate NUMBER(8) NULL ,
    maxDate NUMBER(8) NULL ,
    compareValue NUMBER(18,3) NULL ,
    exprCondTypeId NUMBER(18) NULL 
);

ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT XPKTaxRuleQualCond
PRIMARY KEY (taxRuleQualCondId, taxRuleSourceId));


ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT f8TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT f4TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT f7TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT f1FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId))
;

ALTER TABLE TaxRuleQualCond
    ADD ( CONSTRAINT f2ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId))
;

create table TaxRuleTaxType
(
    taxRuleTaxTypeId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxTypeId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE TaxRuleTaxType
    ADD ( CONSTRAINT XPKTaxRuleTaxType
PRIMARY KEY (taxRuleTaxTypeId, taxRuleSourceId));


ALTER TABLE TaxRuleTaxType
    ADD ( CONSTRAINT f14TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

ALTER TABLE TaxRuleTaxType
    ADD ( CONSTRAINT f9TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table TaxRuleTaxImposition
(
    taxRuleTaxImpositionId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleTaxImpositionTypeId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NULL ,
    taxImpsnId NUMBER(18) NULL ,
    taxImpsnSrcId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    txbltyCatId NUMBER(18) NULL ,
    txbltyCatSrcId NUMBER(18) NULL ,
    overTxbltyCatId NUMBER(18) NULL ,
    overTxbltyCatSrcId NUMBER(18) NULL ,
    underTxbltyCatId NUMBER(18) NULL ,
    underTxbltyCatSrcId NUMBER(18) NULL ,
    invoiceThresholdAmt NUMBER(18,5) NULL ,
    thresholdCurrencyUnitId NUMBER(18) NULL ,
    includeTaxableAmountInd NUMBER(1) NULL ,
    includeTaxAmountInd NUMBER(1) NULL ,
    rate NUMBER(12,8) NULL ,
    locationRoleTypeId NUMBER(18) NULL ,
    impositionTypeId NUMBER(18) NULL ,
    impositionTypeSrcId NUMBER(18) NULL ,
    jurTypeId NUMBER(18) NULL ,
    taxTypeId NUMBER(18) NULL ,
    isLeftInd NUMBER(1) NULL ,
    groupId NUMBER(3) NULL 
);

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT XPKTaxRuleTaxImpsn
PRIMARY KEY (taxRuleTaxImpositionId, taxRuleSourceId));


ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f10TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f7TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f9TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f10TaxCat FOREIGN KEY (overTxbltyCatId, overTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f11TaxCat FOREIGN KEY (underTxbltyCatId, underTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f12LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE TaxRuleTaxImposition
    ADD ( CONSTRAINT f13TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table MaxTaxRuleAdditionalCond
(
    maxTaxRuleCondId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL 
);

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD ( CONSTRAINT XPKMaxTaxRuleCond
PRIMARY KEY (maxTaxRuleCondId));


ALTER TABLE MaxTaxRuleAdditionalCond
    ADD ( CONSTRAINT f34TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD ( CONSTRAINT f34TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

create table ComputationType
(
    computationTypeId NUMBER(18) NOT NULL ,
    compTypeName NVARCHAR2(60) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE ComputationType
    ADD ( CONSTRAINT XPKCompType
PRIMARY KEY (computationTypeId));


create table TaxFactorType
(
    taxFactorTypeId NUMBER(18) NOT NULL ,
    taxFactorTypeName NVARCHAR2(60) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE TaxFactorType
    ADD ( CONSTRAINT XPKTaxFactorType
PRIMARY KEY (taxFactorTypeId));


create table TaxFactor
(
    taxFactorId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxFactorTypeId NUMBER(18) NOT NULL ,
    constantValue NUMBER(18,6) NULL ,
    basisTypeId NUMBER(18) NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL ,
    txbltyCatId NUMBER(18) NULL ,
    txbltyCatSrcId NUMBER(18) NULL ,
    flexFieldDefId NUMBER(18) NULL ,
    flexFieldDefSrcId NUMBER(18) NULL ,
    impositionTypeId NUMBER(18) NULL ,
    impositionTypeSrcId NUMBER(18) NULL ,
    locationRoleTypeId NUMBER(18) NULL ,
    jurTypeId NUMBER(18) NULL ,
    taxTypeId NUMBER(18) NULL 
);

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT XPKTaxFactor
PRIMARY KEY (taxFactorId, sourceId));


ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f8TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId))
;

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f4FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId))
;

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f1TxFcTp FOREIGN KEY (taxFactorTypeId)
    REFERENCES TaxFactorType (taxFactorTypeId))
;

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f2BssTyp FOREIGN KEY (basisTypeId)
    REFERENCES BasisType (basisTypeId))
;

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f17LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE TaxFactor
    ADD ( CONSTRAINT f19TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table ComputationFactor
(
    taxFactorId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    computationTypeId NUMBER(18) NOT NULL ,
    leftTaxFactorId NUMBER(18) NOT NULL ,
    rightTaxFactorId NUMBER(18) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE ComputationFactor
    ADD ( CONSTRAINT XPKCompFactor
PRIMARY KEY (taxFactorId, sourceId));


ALTER TABLE ComputationFactor
    ADD ( CONSTRAINT f3TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE ComputationFactor
    ADD ( CONSTRAINT f4TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE ComputationFactor
    ADD ( CONSTRAINT f5TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE ComputationFactor
    ADD ( CONSTRAINT f1CmpTyp FOREIGN KEY (computationTypeId)
    REFERENCES ComputationType (computationTypeId))
;

create table ConditionalTaxExpr
(
    condTaxExprId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    exprCondTypeId NUMBER(18) NOT NULL ,
    leftTaxFactorId NUMBER(18) NOT NULL ,
    rightTaxFactorId NUMBER(18) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE ConditionalTaxExpr
    ADD ( CONSTRAINT XPKCondTaxExpr
PRIMARY KEY (condTaxExprId, sourceId));


ALTER TABLE ConditionalTaxExpr
    ADD ( CONSTRAINT f1TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE ConditionalTaxExpr
    ADD ( CONSTRAINT f2TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE ConditionalTaxExpr
    ADD ( CONSTRAINT f1ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId))
;

ALTER TABLE ConditionalTaxExpr
    ADD ( CONSTRAINT f6TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table TaxBasisConclusion
(
    taxBasisConcId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxFactorId NUMBER(18) NOT NULL ,
    taxTypeId NUMBER(18) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE TaxBasisConclusion
    ADD ( CONSTRAINT XPKTaxBasisConc
PRIMARY KEY (taxBasisConcId, sourceId));


ALTER TABLE TaxBasisConclusion
    ADD ( CONSTRAINT f7TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

ALTER TABLE TaxBasisConclusion
    ADD ( CONSTRAINT f6TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId))
;

ALTER TABLE TaxBasisConclusion
    ADD ( CONSTRAINT f6TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table TaxabilityMapping
(
    taxabilityMapId NUMBER(18) NOT NULL ,
    taxabilityMapSrcId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    txbltyCatMapId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TaxabilityMapping
    ADD ( CONSTRAINT XPKTaxabilityMap
PRIMARY KEY (taxabilityMapId, taxabilityMapSrcId));


ALTER TABLE TaxabilityMapping
    ADD ( CONSTRAINT f3TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

ALTER TABLE TaxabilityMapping
    ADD ( CONSTRAINT f2TxCMap FOREIGN KEY (txbltyCatMapId, taxabilityMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId))
;

create table LineItemTaxDtlType
(
    lineItemTxDtlTypId NUMBER(18) NOT NULL ,
    lineItemTxDtlTypNm NVARCHAR2(60) NULL 
);

ALTER TABLE LineItemTaxDtlType
    ADD ( CONSTRAINT XPKLinItmTaxDtlTyp
PRIMARY KEY (lineItemTxDtlTypId));


create table FalseSitusCond
(
    prntFlsSitusCondId NUMBER(18) NOT NULL ,
    chldFlsSitusCondId NUMBER(18) NOT NULL 
);

ALTER TABLE FalseSitusCond
    ADD ( CONSTRAINT XPKFalseSitusCond
PRIMARY KEY (prntFlsSitusCondId, chldFlsSitusCondId));


ALTER TABLE FalseSitusCond
    ADD ( CONSTRAINT f4StCdNd FOREIGN KEY (chldFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

ALTER TABLE FalseSitusCond
    ADD ( CONSTRAINT f3StCdNd FOREIGN KEY (prntFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

create table TaxRuleTransType
(
    taxRuleTransTypeId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    transactionTypeId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE TaxRuleTransType
    ADD ( CONSTRAINT XPKTaxRuleTranTyp
PRIMARY KEY (taxRuleTransTypeId, taxRuleSourceId));


ALTER TABLE TaxRuleTransType
    ADD ( CONSTRAINT f1TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId))
;

ALTER TABLE TaxRuleTransType
    ADD ( CONSTRAINT f1TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table DMActivityLogStrng
(
    stringTypeId NUMBER(18) NOT NULL ,
    criteriaString NVARCHAR2(255) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    activityLogId NUMBER(18) NOT NULL 
);

ALTER TABLE DMActivityLogStrng
    ADD ( CONSTRAINT XPKDMActLogStrng
PRIMARY KEY (activityLogId, stringTypeId, criteriaOrderNum));


ALTER TABLE DMActivityLogStrng
    ADD ( CONSTRAINT f3DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId))
;

create table DMActivityLogFile
(
    activityLogId NUMBER(18) NOT NULL ,
    fileId NUMBER(18) NOT NULL ,
    fileName NVARCHAR2(255) NOT NULL ,
    statusNum NUMBER(1) NOT NULL 
);

ALTER TABLE DMActivityLogFile
    ADD ( CONSTRAINT XPKDMActLogFile
PRIMARY KEY (activityLogId, fileId));


ALTER TABLE DMActivityLogFile
    ADD ( CONSTRAINT f5DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId))
;

create table TaxRuleDescription
(
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    taxRuleDescText NVARCHAR2(1000) NULL 
);

ALTER TABLE TaxRuleDescription
    ADD ( CONSTRAINT XPKTaxRuleDesc
PRIMARY KEY (taxRuleId, taxRuleSourceId));


ALTER TABLE TaxRuleDescription
    ADD ( CONSTRAINT f5TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table SitusCondTxbltyCat
(
    txbltyCatSrcId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    situsConditionId NUMBER(18) NOT NULL 
);

ALTER TABLE SitusCondTxbltyCat
    ADD ( CONSTRAINT XPKSitusCondTaxCat
PRIMARY KEY (situsConditionId, txbltyCatSrcId, txbltyCatId));


ALTER TABLE SitusCondTxbltyCat
    ADD ( CONSTRAINT f3StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId))
;

create table TaxRuleCondJur
(
    taxRuleCondJurId NUMBER(18) NOT NULL ,
    taxRuleSourceId NUMBER(18) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE TaxRuleCondJur
    ADD ( CONSTRAINT XPKTaxRuleCondJur
PRIMARY KEY (taxRuleCondJurId, taxRuleSourceId));


ALTER TABLE TaxRuleCondJur
    ADD ( CONSTRAINT f10TxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId))
;

create table DMFilterNum
(
    numberTypeId NUMBER(18) NOT NULL ,
    criteriaNum NUMBER(18) NULL ,
    criteriaOrderNum NUMBER(18) NOT NULL ,
    filterId NUMBER(18) NOT NULL 
);

ALTER TABLE DMFilterNum
    ADD ( CONSTRAINT XPKDMFilterNum
PRIMARY KEY (filterId, numberTypeId, criteriaOrderNum));


ALTER TABLE DMFilterNum
    ADD ( CONSTRAINT f2DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId))
;

create table SitusTreatment
(
    situsTreatmentId NUMBER(18) NOT NULL ,
    situsCondNodeId NUMBER(18) NULL ,
    situsTreatmentName NVARCHAR2(60) NOT NULL ,
    situsTreatmentDesc NVARCHAR2(1000) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE SitusTreatment
    ADD ( CONSTRAINT XPKSitusTreatment
PRIMARY KEY (situsTreatmentId));


ALTER TABLE SitusTreatment
    ADD ( CONSTRAINT f7StCdNd FOREIGN KEY (situsCondNodeId)
    REFERENCES SitusConditionNode (situsCondNodeId))
;

create table StsTrtmntVtxPrdTyp
(
    situsTreatmentId NUMBER(18) NOT NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD ( CONSTRAINT XPKStsTmtVtxPrdTyp
PRIMARY KEY (situsTreatmentId, vertexProductId));


ALTER TABLE StsTrtmntVtxPrdTyp
    ADD ( CONSTRAINT f1Trmt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId))
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD ( CONSTRAINT f2VxtPrd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table TransTypePrspctv
(
    transTypePrspctvId NUMBER(18) NOT NULL ,
    transactionTypeId NUMBER(18) NULL ,
    partyRoleTypeId NUMBER(18) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL ,
    name NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TransTypePrspctv
    ADD ( CONSTRAINT XPKTransTypPrspctv
PRIMARY KEY (transTypePrspctvId));


ALTER TABLE TransTypePrspctv
    ADD ( CONSTRAINT f5PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId))
;

ALTER TABLE TransTypePrspctv
    ADD ( CONSTRAINT f4TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId))
;

create table SitusTreatmentRule
(
    situsTrtmntRuleId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    situsTreatmentId NUMBER(18) NOT NULL ,
    txbltyCatSrcId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL ,
    jurisdiction1Id NUMBER(18) NULL ,
    jurisdiction2Id NUMBER(18) NULL ,
    locationRoleTyp1Id NUMBER(18) NULL ,
    locationRoleTyp2Id NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE SitusTreatmentRule
    ADD ( CONSTRAINT XPKSitusTrtmntRule
PRIMARY KEY (situsTrtmntRuleId, sourceId));


ALTER TABLE SitusTreatmentRule
    ADD ( CONSTRAINT f6LRlTyp FOREIGN KEY (locationRoleTyp2Id)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusTreatmentRule
    ADD ( CONSTRAINT f5LRlTyp FOREIGN KEY (locationRoleTyp1Id)
    REFERENCES LocationRoleType (locationRoleTypeId))
;

ALTER TABLE SitusTreatmentRule
    ADD ( CONSTRAINT f1SitTrt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId))
;

create table SitusTrtmntRulNote
(
    situsTrtmntRuleId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    noteText NVARCHAR2(1000) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE SitusTrtmntRulNote
    ADD ( CONSTRAINT XPKSitusTrtmntRlNt
PRIMARY KEY (situsTrtmntRuleId, sourceId));


ALTER TABLE SitusTrtmntRulNote
    ADD ( CONSTRAINT f1SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId))
;

create table SitusTrtmntPrspctv
(
    situsTrtmntRuleId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    transTypePrspctvId NUMBER(18) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE SitusTrtmntPrspctv
    ADD ( CONSTRAINT XPKSitusTrtmntPrsp
PRIMARY KEY (situsTrtmntRuleId, sourceId, transTypePrspctvId));


ALTER TABLE SitusTrtmntPrspctv
    ADD ( CONSTRAINT f2SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId))
;

ALTER TABLE SitusTrtmntPrspctv
    ADD ( CONSTRAINT f1TrTypP FOREIGN KEY (transTypePrspctvId)
    REFERENCES TransTypePrspctv (transTypePrspctvId))
;

create table CurrencyRndRule
(
    currencyRndRuleId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    currencyUnitId NUMBER(18) NOT NULL ,
    roundingRuleId NUMBER(18) NOT NULL ,
    taxTypeId NUMBER(18) NULL ,
    jurisdictionId NUMBER(18) NULL ,
    partyId NUMBER(18) NULL ,
    configurableInd NUMBER(1) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(8) NULL ,
    lastUpdateDate NUMBER(8) NULL 
);

ALTER TABLE CurrencyRndRule
    ADD ( CONSTRAINT XPKCurrRndRule
PRIMARY KEY (currencyRndRuleId, sourceId));


ALTER TABLE CurrencyRndRule
    ADD ( CONSTRAINT f22Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE CurrencyRndRule
    ADD ( CONSTRAINT f1RndRul FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId))
;

create table AllowCurrRndRule
(
    allowCurrRndRuleId NUMBER(18) NOT NULL ,
    currencyUnitId NUMBER(18) NOT NULL ,
    roundingRuleId NUMBER(18) NOT NULL ,
    taxTypeId NUMBER(18) NULL ,
    jurisdictionId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE AllowCurrRndRule
    ADD ( CONSTRAINT XPKAllowCurRndRule
PRIMARY KEY (allowCurrRndRuleId));


ALTER TABLE AllowCurrRndRule
    ADD ( CONSTRAINT f2RndRule FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId))
;

ALTER TABLE AllowCurrRndRule
    ADD ( CONSTRAINT f10TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId))
;

create table TaxRecoverablePct
(
    taxRecovPctId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    partySourceId NUMBER(18) NOT NULL ,
    costCenter NVARCHAR2(40) NULL ,
    recovPct NUMBER(10,6) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    accrualReliefInd NUMBER(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRecoverablePct
    ADD ( CONSTRAINT XPKTaxRecovPct
PRIMARY KEY (taxRecovPctId));


ALTER TABLE TaxRecoverablePct
    ADD ( CONSTRAINT f27Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId))
;

create table TaxAssistRule
(
    sourceId NUMBER(18) NOT NULL ,
    ruleId NUMBER(18) NOT NULL ,
    ruleCode NVARCHAR2(60) NOT NULL ,
    ruleDesc NVARCHAR2(1000) NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    precedence NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    phaseId NUMBER(1) DEFAULT 0 NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TaxAssistRule
    ADD ( CONSTRAINT XPKTaxAssistRule
PRIMARY KEY (ruleId, sourceId));


ALTER TABLE TaxAssistRule
    ADD ( CONSTRAINT f2VtxProduct FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table TaxAssistCondition
(
    sourceId NUMBER(18) NOT NULL ,
    ruleId NUMBER(18) NOT NULL ,
    conditionId NUMBER(18) NOT NULL ,
    conditionText NVARCHAR2(2000) NULL ,
    conditionText2 NVARCHAR2(2000) NULL 
);

ALTER TABLE TaxAssistCondition
    ADD ( CONSTRAINT XPKTxAstCondition
PRIMARY KEY (ruleId, sourceId, conditionId));


ALTER TABLE TaxAssistCondition
    ADD ( CONSTRAINT f1TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId))
;

create table TaxAssistConclude
(
    sourceId NUMBER(18) NOT NULL ,
    ruleId NUMBER(18) NOT NULL ,
    conclusionId NUMBER(18) NOT NULL ,
    conclusionText NVARCHAR2(2000) NULL ,
    conclusionText2 NVARCHAR2(2000) NULL 
);

ALTER TABLE TaxAssistConclude
    ADD ( CONSTRAINT XPKTxAstConclude
PRIMARY KEY (ruleId, sourceId, conclusionId));


ALTER TABLE TaxAssistConclude
    ADD ( CONSTRAINT f2TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId))
;

create table TaxAssistLookup
(
    sourceId NUMBER(18) NOT NULL ,
    tableId NUMBER(18) NOT NULL ,
    tableName NVARCHAR2(60) NOT NULL ,
    dataType NUMBER(18) NOT NULL ,
    description NVARCHAR2(1000) NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    resultName NVARCHAR2(60) NOT NULL ,
    param1Name NVARCHAR2(60) NOT NULL ,
    param2Name NVARCHAR2(60) NULL ,
    param3Name NVARCHAR2(60) NULL ,
    param4Name NVARCHAR2(60) NULL ,
    param5Name NVARCHAR2(60) NULL ,
    param6Name NVARCHAR2(60) NULL ,
    param7Name NVARCHAR2(60) NULL ,
    param8Name NVARCHAR2(60) NULL ,
    param9Name NVARCHAR2(60) NULL ,
    param10Name NVARCHAR2(60) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TaxAssistLookup
    ADD ( CONSTRAINT XPKTxAstLookup
PRIMARY KEY (tableId, sourceId));


ALTER TABLE TaxAssistLookup
    ADD ( CONSTRAINT f1TxAstLookup FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table TaxAssistLookupRcd
(
    sourceId NUMBER(18) NOT NULL ,
    recordId NUMBER(18) NOT NULL ,
    tableId NUMBER(18) NOT NULL ,
    result NVARCHAR2(200) NULL ,
    param1 NVARCHAR2(200) NULL ,
    param2 NVARCHAR2(200) NULL ,
    param3 NVARCHAR2(200) NULL ,
    param4 NVARCHAR2(200) NULL ,
    param5 NVARCHAR2(200) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE TaxAssistLookupRcd
    ADD ( CONSTRAINT XPKTxAstLookupRcd
PRIMARY KEY (recordId, sourceId));


ALTER TABLE TaxAssistLookupRcd
    ADD ( CONSTRAINT f1TxAsLk FOREIGN KEY (tableId, sourceId)
    REFERENCES TaxAssistLookup (tableId, sourceId))
;

create table NumericType
(
    numericTypeId NUMBER(18) NOT NULL ,
    numericTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE NumericType
    ADD ( CONSTRAINT XPKNumericType
PRIMARY KEY (numericTypeId));


create table TaxAssistAllocationTable
(
    sourceId NUMBER(18) NOT NULL ,
    allocationTableId NUMBER(18) NOT NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    allocationTableName NVARCHAR2(60) NOT NULL ,
    allocationTableDesc NVARCHAR2(1000) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE TaxAssistAllocationTable
    ADD ( CONSTRAINT XPKTxAstAlocTbl
PRIMARY KEY (allocationTableId, sourceId));


ALTER TABLE TaxAssistAllocationTable
    ADD ( CONSTRAINT f1TxAstAlloc FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table TaxAssistAllocationColumn
(
    sourceId NUMBER(18) NOT NULL ,
    allocationTableId NUMBER(18) NOT NULL ,
    columnId NUMBER(18) NOT NULL ,
    columnName NVARCHAR2(60) NOT NULL ,
    dataTypeId NUMBER(18) NOT NULL ,
    columnSeqNum NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumn
    ADD ( CONSTRAINT XPKTxAstAllocCol
PRIMARY KEY (allocationTableId, sourceId, columnId));


ALTER TABLE TaxAssistAllocationColumn
    ADD ( CONSTRAINT f1TxAsAC FOREIGN KEY (allocationTableId, sourceId)
    REFERENCES TaxAssistAllocationTable (allocationTableId, sourceId))
;

ALTER TABLE TaxAssistAllocationColumn
    ADD ( CONSTRAINT f1TxAsACDT FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId))
;

create table TaxAssistAllocationColumnValue
(
    sourceId NUMBER(18) NOT NULL ,
    allocationTableId NUMBER(18) NOT NULL ,
    columnId NUMBER(18) NOT NULL ,
    recordId NUMBER(18) NOT NULL ,
    recordCode NVARCHAR2(20) NOT NULL ,
    numericTypeId NUMBER(18) NULL ,
    columnValue NVARCHAR2(60) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumnValue
    ADD ( CONSTRAINT XPKTxAstAllocCoVal
PRIMARY KEY (allocationTableId, sourceId, columnId, recordId, recordCode));


ALTER TABLE TaxAssistAllocationColumnValue
    ADD ( CONSTRAINT f1TxAsACV FOREIGN KEY (allocationTableId, sourceId, columnId)
    REFERENCES TaxAssistAllocationColumn (allocationTableId, sourceId, columnId))
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD ( CONSTRAINT f1TxAsACVNT FOREIGN KEY (numericTypeId)
    REFERENCES NumericType (numericTypeId))
;

create table TpsJurisdiction
(
    jurId NUMBER(18) NOT NULL ,
    jurVersionId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    jurLayerId NUMBER(18) NOT NULL ,
    jurTypeId NUMBER(18) NOT NULL ,
    jurTypeName NVARCHAR2(60) NOT NULL ,
    name NVARCHAR2(60) NOT NULL ,
    standardName NVARCHAR2(60) NULL ,
    description NVARCHAR2(80) NULL ,
    updateId NUMBER(18) NOT NULL 
);

ALTER TABLE TpsJurisdiction
    ADD ( CONSTRAINT XPKTpsJurisdiction
PRIMARY KEY (jurId, jurVersionId));


create table TaxAreaJurNames
(
    taxAreaId NUMBER(18) NOT NULL ,
    countryName NVARCHAR2(60) NOT NULL ,
    countryISOCode2 NVARCHAR2(2) NULL ,
    countryISOCode3 NVARCHAR2(3) NOT NULL ,
    mainDivJurTypeName NVARCHAR2(60) NULL ,
    mainDivName NVARCHAR2(60) NULL ,
    subDivJurTypeName NVARCHAR2(60) NULL ,
    subDivName NVARCHAR2(60) NULL ,
    cityJurTypeName NVARCHAR2(60) NULL ,
    cityName NVARCHAR2(60) NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE TaxAreaJurNames
    ADD ( CONSTRAINT XPKTaxAreaJurNames
PRIMARY KEY (taxAreaId));


create table JurHierarchy
(
    depthFromParent NUMBER(18) NOT NULL ,
    topmostInd NUMBER(1) NOT NULL ,
    chldJurisdictionId NUMBER(18) NOT NULL ,
    prntJurisdictionId NUMBER(18) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE JurHierarchy
    ADD ( CONSTRAINT XPKJurHierarchy
PRIMARY KEY (prntJurisdictionId, chldJurisdictionId));


create table SimplificationType
(
    simpTypeId NUMBER(18) NOT NULL ,
    simpTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE SimplificationType
    ADD ( CONSTRAINT XPKSimpType
PRIMARY KEY (simpTypeId));


create table DMActivityType
(
    activityTypeId NUMBER(18) NOT NULL ,
    activityTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE DMActivityType
    ADD ( CONSTRAINT XPKDMActivityType
PRIMARY KEY (activityTypeId));


create table DMStatusType
(
    statusTypeId NUMBER(18) NOT NULL ,
    statusTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE DMStatusType
    ADD ( CONSTRAINT XPKDMStatusType
PRIMARY KEY (statusTypeId));


create table InputOutputType
(
    inputOutputTypeId NUMBER(18) NOT NULL ,
    inputOutputTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE InputOutputType
    ADD ( CONSTRAINT XPKInpOutType
PRIMARY KEY (inputOutputTypeId));


create table CertificateReasonType
(
    certificateReasonTypeId NUMBER(18) NOT NULL ,
    certificateReasonTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CertificateReasonType
    ADD ( CONSTRAINT XPKCertRsnType
PRIMARY KEY (certificateReasonTypeId));


create table CertReasonTypeJurisdiction
(
    certReasonTypeJurId NUMBER(18) NOT NULL ,
    certificateReasonTypeId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    reasonCategoryId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertReasonTypeJurisdiction
    ADD ( CONSTRAINT XPKCertRsnTypeJur
PRIMARY KEY (certReasonTypeJurId));


ALTER TABLE CertReasonTypeJurisdiction
    ADD ( CONSTRAINT f1CrtRsnTypRsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId))
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD ( CONSTRAINT f1CrtRsnTypRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId))
;

create table CertificateExemptionType
(
    certificateExemptionTypeId NUMBER(18) NOT NULL ,
    certificateExemptionTypeName NVARCHAR2(60) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateExemptionType
    ADD ( CONSTRAINT XPKCertExemType
PRIMARY KEY (certificateExemptionTypeId));


create table CertExemptionTypeJurImp
(
    certExemptionTypeJurImpId NUMBER(18) NOT NULL ,
    certificateExemptionTypeId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    impsnTypeId NUMBER(18) NOT NULL ,
    impsnTypeSrcId NUMBER(18) NOT NULL ,
    allStatesInd NUMBER(1) NULL ,
    allCitiesInd NUMBER(1) NULL ,
    allCountiesInd NUMBER(1) NULL ,
    allOthersInd NUMBER(1) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertExemptionTypeJurImp
    ADD ( CONSTRAINT XPKCertExmTypeJur
PRIMARY KEY (certExemptionTypeJurImpId));


ALTER TABLE CertExemptionTypeJurImp
    ADD ( CONSTRAINT f1ImpCrtExmTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId))
;

ALTER TABLE CertExemptionTypeJurImp
    ADD ( CONSTRAINT f1CrtExmTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId))
;

create table Form
(
    formId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL 
);

ALTER TABLE Form
    ADD ( CONSTRAINT XPKForm
PRIMARY KEY (formId, sourceId));


create table FormDetail
(
    formDetailId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    formId NUMBER(18) NOT NULL ,
    formIdCode NVARCHAR2(30) NOT NULL ,
    formName NVARCHAR2(60) NULL ,
    formDesc NVARCHAR2(255) NULL ,
    formImageFileName NVARCHAR2(255) NULL ,
    replacedByFormId NUMBER(18) NULL ,
    replacementDate NUMBER(8) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL 
);

ALTER TABLE FormDetail
    ADD ( CONSTRAINT XPKFormDtl
PRIMARY KEY (formDetailId, sourceId));


ALTER TABLE FormDetail
    ADD ( CONSTRAINT f1FormDtlForm FOREIGN KEY (replacedByFormId, sourceId)
    REFERENCES Form (formId, sourceId))
;

ALTER TABLE FormDetail
    ADD ( CONSTRAINT f2FormDtlForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId))
;

create table FormJurisdiction
(
    formJurisdictionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    formId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE FormJurisdiction
    ADD ( CONSTRAINT XPKFormJur
PRIMARY KEY (formJurisdictionId, sourceId));


ALTER TABLE FormJurisdiction
    ADD ( CONSTRAINT f1FormJurForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId))
;

create table FormFieldDef
(
    fieldDefId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NOT NULL ,
    formId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NULL ,
    formFieldTypeId NUMBER(18) NOT NULL ,
    requiredInd NUMBER(1) DEFAULT 0 NULL ,
    hiddenInd NUMBER(1) DEFAULT 0 NULL ,
    attributeId NUMBER(18) NULL ,
    fieldDefPageNum NUMBER(18) NOT NULL ,
    groupName NVARCHAR2(60) NULL ,
    parentFieldDefId NUMBER(18) NULL ,
    parentFieldDefSelectInd NUMBER(1) DEFAULT 1 NULL ,
    parentValue NVARCHAR2(20) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE FormFieldDef
    ADD ( CONSTRAINT XPKFormFieldDef
PRIMARY KEY (fieldDefId, sourceId));


ALTER TABLE FormFieldDef
    ADD ( CONSTRAINT f1FormFieldDef FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId))
;

create table FormFieldTypeValue
(
    fieldDefId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    validValue NVARCHAR2(20) NULL ,
    trueValue NVARCHAR2(20) NULL ,
    falseValue NVARCHAR2(20) NULL ,
    isDefault NUMBER(1) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE FormFieldTypeValue
    ADD ( CONSTRAINT XPKFormFldTypeVle
PRIMARY KEY (fieldDefId, sourceId));


create table FormFieldAttribute
(
    attributeId NUMBER(18) NOT NULL ,
    name NVARCHAR2(60) NOT NULL 
);

ALTER TABLE FormFieldAttribute
    ADD ( CONSTRAINT XPKFormFieldAttr
PRIMARY KEY (attributeId));


create table Certificate
(
    certificateId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateCreationDate NUMBER(8) NOT NULL 
);

ALTER TABLE Certificate
    ADD ( CONSTRAINT XPKCertificate
PRIMARY KEY (certificateId, sourceId));


create table CertificateDetail
(
    certificateDetailId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    creationSourceId NUMBER(18) DEFAULT 1 NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    certificateHolderPartyId NUMBER(18) NOT NULL ,
    certClassTypeId NUMBER(18) NOT NULL ,
    formId NUMBER(18) NULL ,
    formSourceId NUMBER(18) NULL ,
    taxResultTypeId NUMBER(18) NULL ,
    certificateStatusId NUMBER(18) NOT NULL ,
    partyContactId NUMBER(18) NULL ,
    txbltyCatId NUMBER(18) NULL ,
    txbltyCatSrcId NUMBER(18) NULL ,
    certCopyRcvdInd NUMBER(1) NULL ,
    industryTypeName NVARCHAR2(60) NULL ,
    hardCopyLocationName NVARCHAR2(255) NULL ,
    certBlanketInd NUMBER(1) NULL ,
    singleUseInd NUMBER(1) NULL ,
    invoiceNumber NVARCHAR2(50) NULL ,
    replacedByCertificateId NUMBER(18) NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    completedInd NUMBER(1) DEFAULT 1 NOT NULL ,
    approvalStatusId NUMBER(18) DEFAULT 3 NOT NULL ,
    uuid NVARCHAR2(36) NULL ,
    ecwCertificateId NUMBER(18) NULL ,
    ecwSyncId NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    createDate NUMBER(18) NOT NULL ,
    lastUpdateDate NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT XPKCertDtl
PRIMARY KEY (certificateDetailId, sourceId));


ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1Certificate FOREIGN KEY (replacedByCertificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtParty FOREIGN KEY (certificateHolderPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtCtClsTyp FOREIGN KEY (certClassTypeId)
    REFERENCES CertClassType (certClassTypeId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtTxRsltTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtPtyCntct FOREIGN KEY (partyContactId, sourceId)
    REFERENCES PartyContact (partyContactId, sourceId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f5VtxProd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

ALTER TABLE CertificateDetail
    ADD ( CONSTRAINT f1CtDtForm FOREIGN KEY (formId, formSourceId)
    REFERENCES Form (formId, sourceId))
;

create table CertificateCoverage
(
    certificateCoverageId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    certificateIdCode NVARCHAR2(30) NULL ,
    validationTypeId NUMBER(18) NULL ,
    validationDate NUMBER(8) NULL ,
    certificateReasonTypeId NUMBER(18) NULL ,
    certificateExemptionTypeId NUMBER(18) NULL ,
    certIssueDate NUMBER(8) NULL ,
    certExpDate NUMBER(8) NULL ,
    certReviewDate NUMBER(8) NULL ,
    allStatesInd NUMBER(1) NOT NULL ,
    allCitiesInd NUMBER(1) NOT NULL ,
    allCountiesInd NUMBER(1) NOT NULL ,
    allOthersInd NUMBER(1) NOT NULL ,
    allImpositionsInd NUMBER(1) DEFAULT 1 NULL ,
    jurActiveInd NUMBER(1) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateCoverage
    ADD ( CONSTRAINT XPKCertCvrg
PRIMARY KEY (certificateCoverageId, sourceId));


ALTER TABLE CertificateCoverage
    ADD ( CONSTRAINT f1CrtCvrgCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE CertificateCoverage
    ADD ( CONSTRAINT f1CrtCvrgRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId))
;

ALTER TABLE CertificateCoverage
    ADD ( CONSTRAINT f1CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId))
;

ALTER TABLE CertificateCoverage
    ADD ( CONSTRAINT f1CrtCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId))
;

create table CertificateJurisdiction
(
    certificateJurisdictionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateCoverageId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    certificateExemptionTypeId NUMBER(18) NULL ,
    coverageActionTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE CertificateJurisdiction
    ADD ( CONSTRAINT XPKCertJur
PRIMARY KEY (certificateJurisdictionId, sourceId));


ALTER TABLE CertificateJurisdiction
    ADD ( CONSTRAINT f1CrtJurCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId))
;

ALTER TABLE CertificateJurisdiction
    ADD ( CONSTRAINT f2CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId))
;

ALTER TABLE CertificateJurisdiction
    ADD ( CONSTRAINT f1CrtJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId))
;

create table CertificateImposition
(
    certificateImpositionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateCoverageId NUMBER(18) NOT NULL ,
    jurTypeSetId NUMBER(18) NOT NULL ,
    impsnTypeId NUMBER(18) NOT NULL ,
    impsnTypeSrcId NUMBER(18) NOT NULL ,
    certificateExemptionTypeId NUMBER(18) NULL ,
    impActiveInd NUMBER(1) NOT NULL 
);

ALTER TABLE CertificateImposition
    ADD ( CONSTRAINT XPKCertImp
PRIMARY KEY (certificateImpositionId, sourceId));


ALTER TABLE CertificateImposition
    ADD ( CONSTRAINT f1CrtImpCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId))
;

ALTER TABLE CertificateImposition
    ADD ( CONSTRAINT f1CrtImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

ALTER TABLE CertificateImposition
    ADD ( CONSTRAINT f1CrtImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId))
;

ALTER TABLE CertificateImposition
    ADD ( CONSTRAINT f3CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId))
;

create table CertImpositionJurisdiction
(
    certImpJurisdictionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateImpositionId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    coverageActionTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE CertImpositionJurisdiction
    ADD ( CONSTRAINT XPKCertImpJur
PRIMARY KEY (certImpJurisdictionId, sourceId));


ALTER TABLE CertImpositionJurisdiction
    ADD ( CONSTRAINT f1CrtImpJurImp FOREIGN KEY (certificateImpositionId, sourceId)
    REFERENCES CertificateImposition (certificateImpositionId, sourceId))
;

ALTER TABLE CertImpositionJurisdiction
    ADD ( CONSTRAINT f1CrtImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId))
;

create table CertificateNote
(
    certNoteText NVARCHAR2(1000) NULL ,
    certificateId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL 
);

ALTER TABLE CertificateNote
    ADD ( CONSTRAINT XPKCertNote
PRIMARY KEY (certificateId, sourceId));


ALTER TABLE CertificateNote
    ADD ( CONSTRAINT f2Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

create table CertificateImage
(
    certificateImageId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    creationSourceId NUMBER(18) DEFAULT 1 NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    imageLocationName NVARCHAR2(255) NOT NULL 
);

ALTER TABLE CertificateImage
    ADD ( CONSTRAINT XPKCertImage
PRIMARY KEY (certificateImageId, sourceId));


ALTER TABLE CertificateImage
    ADD ( CONSTRAINT f5Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

create table CertTxbltyDriver
(
    certTxbltyDriverId NUMBER(18) NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    txbltyDvrId NUMBER(18) NOT NULL ,
    txbltyDvrSrcId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertTxbltyDriver
    ADD ( CONSTRAINT XPKCertTxbltyDvr
PRIMARY KEY (certTxbltyDriverId));


ALTER TABLE CertTxbltyDriver
    ADD ( CONSTRAINT f4Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE CertTxbltyDriver
    ADD ( CONSTRAINT f4TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId))
;

create table CertificateTransType
(
    certTransTypeId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    transactionTypeId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateTransType
    ADD ( CONSTRAINT XPKCertTransType
PRIMARY KEY (certTransTypeId, sourceId));


ALTER TABLE CertificateTransType
    ADD ( CONSTRAINT f1CertTransTypCrt FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE CertificateTransType
    ADD ( CONSTRAINT f2CertTransTypTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId))
;

create table CertificateFormField
(
    certificateFormFieldId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateId NUMBER(18) NOT NULL ,
    formId NUMBER(18) NOT NULL ,
    formSourceId NUMBER(18) NOT NULL ,
    fieldDefId NUMBER(18) NOT NULL ,
    value NVARCHAR2(500) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateFormField
    ADD ( CONSTRAINT XPKCertFormField
PRIMARY KEY (certificateFormFieldId, sourceId));


create table ExpirationRuleType
(
    expirationRuleTypeId NUMBER(18) NOT NULL ,
    expirationRuleTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE ExpirationRuleType
    ADD ( CONSTRAINT XPKExpRuleType
PRIMARY KEY (expirationRuleTypeId));


create table CertificateNumReqType
(
    certificateNumReqTypeId NUMBER(18) NOT NULL ,
    certificateNumReqTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE CertificateNumReqType
    ADD ( CONSTRAINT XPKCertNumReqTyp
PRIMARY KEY (certificateNumReqTypeId));


create table CertificateNumberFormat
(
    certificateNumberFormatId NUMBER(18) NOT NULL ,
    certificateNumberMask NVARCHAR2(30) NOT NULL ,
    certificateNumberExample NVARCHAR2(30) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateNumberFormat
    ADD ( CONSTRAINT XPKCertNumFmt
PRIMARY KEY (certificateNumberFormatId));


create table CertificateValidationRule
(
    certificateValidationRuleId NUMBER(18) NOT NULL ,
    certificateNumValidationDesc NVARCHAR2(100) NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    certificateReasonTypeId NUMBER(18) NOT NULL ,
    certificateNumReqTypeId NUMBER(18) NOT NULL ,
    expirationRuleTypeId NUMBER(18) NOT NULL ,
    numberOfYears NUMBER(18) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE CertificateValidationRule
    ADD ( CONSTRAINT XPKCertValRule
PRIMARY KEY (certificateValidationRuleId));


ALTER TABLE CertificateValidationRule
    ADD ( CONSTRAINT f1CrtVRuleRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId))
;

ALTER TABLE CertificateValidationRule
    ADD ( CONSTRAINT f1CrtVRuleExpTyp FOREIGN KEY (expirationRuleTypeId)
    REFERENCES ExpirationRuleType (expirationRuleTypeId))
;

ALTER TABLE CertificateValidationRule
    ADD ( CONSTRAINT f1CrtVRuleReqTyp FOREIGN KEY (certificateNumReqTypeId)
    REFERENCES CertificateNumReqType (certificateNumReqTypeId))
;

create table CertificateNumFormatValidation
(
    certNumFormatValidationId NUMBER(18) NOT NULL ,
    certificateValidationRuleId NUMBER(18) NOT NULL ,
    certificateNumberFormatId NUMBER(18) NOT NULL 
);

ALTER TABLE CertificateNumFormatValidation
    ADD ( CONSTRAINT XPKCertNumFmtVal
PRIMARY KEY (certNumFormatValidationId));


ALTER TABLE CertificateNumFormatValidation
    ADD ( CONSTRAINT f1CrtNFmtValRule FOREIGN KEY (certificateValidationRuleId)
    REFERENCES CertificateValidationRule (certificateValidationRuleId))
;

ALTER TABLE CertificateNumFormatValidation
    ADD ( CONSTRAINT f1CrtNFmtValNFmt FOREIGN KEY (certificateNumberFormatId)
    REFERENCES CertificateNumberFormat (certificateNumberFormatId))
;

create table TaxAuthority
(
    taxAuthorityId NUMBER(18) NOT NULL ,
    taxAuthorityName NVARCHAR2(60) NOT NULL ,
    phoneNumber NVARCHAR2(20) NULL ,
    webSiteAddress NVARCHAR2(100) NULL 
);

ALTER TABLE TaxAuthority
    ADD ( CONSTRAINT XPKTaxAuth
PRIMARY KEY (taxAuthorityId));


create table TaxAuthorityJurisdiction
(
    taxAuthorityJurisdictionId NUMBER(18) NOT NULL ,
    taxAuthorityId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxAuthorityJurisdiction
    ADD ( CONSTRAINT XPKTaxAuthJur
PRIMARY KEY (taxAuthorityJurisdictionId));


ALTER TABLE TaxAuthorityJurisdiction
    ADD ( CONSTRAINT f1TaxAuthJur FOREIGN KEY (taxAuthorityId)
    REFERENCES TaxAuthority (taxAuthorityId))
;

create table TaxRegistration
(
    taxRegistrationId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR2(40) NULL ,
    taxRegistrationTypeId NUMBER(18) NULL ,
    validationTypeId NUMBER(18) NULL ,
    validationDate NUMBER(8) NULL ,
    formatValidationTypeId NUMBER(18) NULL ,
    formatValidationDate NUMBER(8) NULL ,
    physicalPresInd NUMBER(1) NOT NULL ,
    allStatesInd NUMBER(1) NOT NULL ,
    allCitiesInd NUMBER(1) NOT NULL ,
    allCountiesInd NUMBER(1) NOT NULL ,
    allOthersInd NUMBER(1) NOT NULL ,
    allImpositionsInd NUMBER(1) DEFAULT 1 NULL ,
    jurActiveInd NUMBER(1) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    filingCurrencyUnitId NUMBER(18) NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE TaxRegistration
    ADD ( CONSTRAINT XPKTaxReg
PRIMARY KEY (taxRegistrationId, sourceId));


ALTER TABLE TaxRegistration
    ADD ( CONSTRAINT f1TxRgCvrgTxpyr FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE TaxRegistration
    ADD ( CONSTRAINT f1TxRgTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId))
;

ALTER TABLE TaxRegistration
    ADD ( CONSTRAINT f1TxRgCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId))
;

create table TaxRegistrationJurisdiction
(
    taxRegJurisdictionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxRegistrationId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    coverageActionTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE TaxRegistrationJurisdiction
    ADD ( CONSTRAINT XPKTxRgJur
PRIMARY KEY (taxRegJurisdictionId, sourceId));


ALTER TABLE TaxRegistrationJurisdiction
    ADD ( CONSTRAINT f1TxRgJurCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId))
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD ( CONSTRAINT f1TxRgJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId))
;

create table TaxRegistrationImposition
(
    taxRegImpositionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxRegistrationId NUMBER(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR2(40) NULL ,
    taxRegistrationTypeId NUMBER(18) NULL ,
    jurTypeSetId NUMBER(18) NOT NULL ,
    impsnTypeId NUMBER(18) NOT NULL ,
    impsnTypeSrcId NUMBER(18) NOT NULL ,
    impActiveInd NUMBER(1) NOT NULL ,
    reverseChargeInd NUMBER(1) DEFAULT 0 NOT NULL ,
    formatValidationTypeId NUMBER(18) NULL ,
    formatValidationDate NUMBER(8) NULL 
);

ALTER TABLE TaxRegistrationImposition
    ADD ( CONSTRAINT XPKTxRgImp
PRIMARY KEY (taxRegImpositionId, sourceId));


ALTER TABLE TaxRegistrationImposition
    ADD ( CONSTRAINT f1TxRgImpCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId))
;

ALTER TABLE TaxRegistrationImposition
    ADD ( CONSTRAINT f1TxRgImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId))
;

ALTER TABLE TaxRegistrationImposition
    ADD ( CONSTRAINT f1TxRgImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId))
;

ALTER TABLE TaxRegistrationImposition
    ADD ( CONSTRAINT f1TxRgImpTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId))
;

create table TaxRegImpJurisdiction
(
    taxRegImpJurisdictionId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    taxRegImpositionId NUMBER(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR2(40) NULL ,
    taxRegistrationTypeId NUMBER(18) NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    coverageActionTypeId NUMBER(18) NOT NULL ,
    reverseChargeInd NUMBER(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRegImpJurisdiction
    ADD ( CONSTRAINT XPKTxRgImpJur
PRIMARY KEY (taxRegImpJurisdictionId, sourceId));


ALTER TABLE TaxRegImpJurisdiction
    ADD ( CONSTRAINT f1TxRgImpJurImp FOREIGN KEY (taxRegImpositionId, sourceId)
    REFERENCES TaxRegistrationImposition (taxRegImpositionId, sourceId))
;

ALTER TABLE TaxRegImpJurisdiction
    ADD ( CONSTRAINT f1TxRgImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId))
;

ALTER TABLE TaxRegImpJurisdiction
    ADD ( CONSTRAINT f1TxRgImpJurTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId))
;

create table LetterTemplateType
(
    letterTemplateTypeId NUMBER(18) NOT NULL ,
    letterTemplateTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE LetterTemplateType
    ADD ( CONSTRAINT XPKLtrTempType
PRIMARY KEY (letterTemplateTypeId));


create table LetterDeliveryMethod
(
    letterDeliveryMethodId NUMBER(18) NOT NULL ,
    letterDeliveryMethodName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE LetterDeliveryMethod
    ADD ( CONSTRAINT XPKLtrDlvryMthd
PRIMARY KEY (letterDeliveryMethodId));


create table LetterTemplate
(
    letterTemplateId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    letterTemplateTypeId NUMBER(18) NOT NULL ,
    letterTemplateName NVARCHAR2(60) NOT NULL ,
    letterTemplateDesc NVARCHAR2(100) NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE LetterTemplate
    ADD ( CONSTRAINT XPKLtrTemp
PRIMARY KEY (letterTemplateId, sourceId));


ALTER TABLE LetterTemplate
    ADD ( CONSTRAINT f1LtrTmpType FOREIGN KEY (letterTemplateTypeId)
    REFERENCES LetterTemplateType (letterTemplateTypeId))
;

create table LetterTemplateText
(
    letterTemplateId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    templateTextSeqNum NUMBER(18) NOT NULL ,
    templateText NVARCHAR2(2000) NULL 
);

ALTER TABLE LetterTemplateText
    ADD ( CONSTRAINT XPKLtrTempText
PRIMARY KEY (letterTemplateId, sourceId, templateTextSeqNum));


ALTER TABLE LetterTemplateText
    ADD ( CONSTRAINT f1LtrTmp FOREIGN KEY (letterTemplateId, sourceId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId))
;

create table LetterBatch
(
    letterBatchId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    letterTemplateId NUMBER(18) NOT NULL ,
    letterTemplateSrcId NUMBER(18) NOT NULL ,
    certificateStatusId NUMBER(18) NULL ,
    daysToExpirationNum NUMBER(18) NULL ,
    daysSinceExpirationNum NUMBER(18) NULL ,
    expirationRangeStartDate NUMBER(8) NULL ,
    expirationRangeEndDate NUMBER(8) NULL ,
    noCertificateInd NUMBER(1) NULL ,
    incompleteInd NUMBER(1) NULL ,
    rejectedInd NUMBER(1) NULL ,
    formReplacedInd NUMBER(1) NULL ,
    emailSubject NVARCHAR2(100) NULL ,
    letterBatchDesc NVARCHAR2(100) NULL ,
    letterBatchDate NUMBER(8) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE LetterBatch
    ADD ( CONSTRAINT XPKLtrBatch
PRIMARY KEY (letterBatchId, sourceId));


ALTER TABLE LetterBatch
    ADD ( CONSTRAINT f1LtrBtchCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId))
;

ALTER TABLE LetterBatch
    ADD ( CONSTRAINT f2LtrBtchTmp FOREIGN KEY (letterTemplateId, letterTemplateSrcId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId))
;

create table LetterBatchParty
(
    letterBatchId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL ,
    partyTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE LetterBatchParty
    ADD ( CONSTRAINT XPKLtrBatchPty
PRIMARY KEY (letterBatchId, sourceId, partyId));


ALTER TABLE LetterBatchParty
    ADD ( CONSTRAINT f1LtrBtchPty FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId))
;

ALTER TABLE LetterBatchParty
    ADD ( CONSTRAINT f2LtrBtchPty FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE LetterBatchParty
    ADD ( CONSTRAINT f3LtrBtchPty FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId))
;

create table LetterBatchJurisdiction
(
    letterBatchId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL 
);

ALTER TABLE LetterBatchJurisdiction
    ADD ( CONSTRAINT XPKLtrBatchJur
PRIMARY KEY (letterBatchId, sourceId, jurisdictionId));


ALTER TABLE LetterBatchJurisdiction
    ADD ( CONSTRAINT f1LtrBtchJur FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId))
;

create table FlexFieldDefVtxPrdTyp
(
    flexFieldDefId NUMBER(18) NOT NULL ,
    flexFieldDefSrcId NUMBER(18) NOT NULL ,
    vertexProductId NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD ( CONSTRAINT XPKFFDefVtxPrdTyp
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId, vertexProductId));


ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD ( CONSTRAINT f1FFDrfVtxPrdTyp FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId))
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD ( CONSTRAINT f2FFDrfVtxPrdTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId))
;

create table Letter
(
    letterId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    certificateId NUMBER(18) NULL ,
    overridePartyId NUMBER(18) NULL ,
    letterBatchId NUMBER(18) NOT NULL ,
    letterDeliveryMethodId NUMBER(18) NOT NULL ,
    formId NUMBER(18) NULL ,
    formSrcId NUMBER(18) NULL ,
    letterSentDate NUMBER(8) NOT NULL 
);

ALTER TABLE Letter
    ADD ( CONSTRAINT XPKLetter
PRIMARY KEY (letterId, sourceId));


ALTER TABLE Letter
    ADD ( CONSTRAINT f1LtrDlvryMethod FOREIGN KEY (letterDeliveryMethodId)
    REFERENCES LetterDeliveryMethod (letterDeliveryMethodId))
;

ALTER TABLE Letter
    ADD ( CONSTRAINT f2LtrLtrBatch FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId))
;

ALTER TABLE Letter
    ADD ( CONSTRAINT f3LtrCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId))
;

ALTER TABLE Letter
    ADD ( CONSTRAINT f4LtrForm FOREIGN KEY (formId, formSrcId)
    REFERENCES Form (formId, sourceId))
;

ALTER TABLE Letter
    ADD ( CONSTRAINT f5LtrParty FOREIGN KEY (overridePartyId, sourceId)
    REFERENCES Party (partyId, partySourceId))
;

create table GeographicRegionType
(
    geoRegionTypeId NUMBER(18) NOT NULL ,
    geoRegionLicenseCode NVARCHAR2(30) NOT NULL ,
    geoRegionDisplayName NVARCHAR2(30) NOT NULL 
);

ALTER TABLE GeographicRegionType
    ADD ( CONSTRAINT XPKGeoRegionCode
PRIMARY KEY (geoRegionTypeId));


create table JurGeographicRegType
(
    jurGeoRegTypeId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    geoRegionTypeId NUMBER(18) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE JurGeographicRegType
    ADD ( CONSTRAINT XPKJurGeoRegType
PRIMARY KEY (jurGeoRegTypeId));


ALTER TABLE JurGeographicRegType
    ADD ( CONSTRAINT f1GeoRegionType FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId))
;

create table RoleParty
(
    roleId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NOT NULL ,
    partyId NUMBER(18) NOT NULL 
);

ALTER TABLE RoleParty
    ADD ( CONSTRAINT XPKRoleParty
PRIMARY KEY (roleId, sourceId, partyId));


create table VATRegistrationIdFormat
(
    vatRegistrationIdFormatId NUMBER(18) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    vatRegistrationIdMask NVARCHAR2(40) NOT NULL ,
    vatRegistrationIdExample NVARCHAR2(40) NULL ,
    vatRegistrationIdDescription NVARCHAR2(255) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL 
);

ALTER TABLE VATRegistrationIdFormat
    ADD ( CONSTRAINT XPKVATRegIdFmt
PRIMARY KEY (vatRegistrationIdFormatId));


create table CertWizardLocationUser
(
    certWizardUserId NUMBER(18) NOT NULL ,
    name NVARCHAR2(50) NOT NULL ,
    userId NUMBER(18) NOT NULL ,
    sourceId NUMBER(18) NULL ,
    partyId NUMBER(18) NULL ,
    displayName NVARCHAR2(100) NULL 
);

ALTER TABLE CertWizardLocationUser
    ADD ( CONSTRAINT CertWizardLocationUser_PK
PRIMARY KEY (certWizardUserId));


create table users
(
    username NVARCHAR2(64) NOT NULL ,
    password NVARCHAR2(255) NOT NULL ,
    enabled NUMBER(1) NULL ,
    sourceId NUMBER(18) NULL 
);

ALTER TABLE users
    ADD ( CONSTRAINT users_PK
PRIMARY KEY (username));


create table authorities
(
    username NVARCHAR2(64) NOT NULL ,
    authority NVARCHAR2(50) NOT NULL ,
    sourceId NUMBER(18) NULL 
);

ALTER TABLE authorities
    ADD ( CONSTRAINT f1Auth FOREIGN KEY (username)
    REFERENCES users (username))
;

create table CertWizardUser
(
    loginId NUMBER(18) NOT NULL ,
    certWizardUserId NUMBER(18) NOT NULL ,
    userName NVARCHAR2(64) NULL ,
    companyEmail NVARCHAR2(100) NOT NULL ,
    personalEmail NVARCHAR2(100) NULL ,
    oseriesCustomerCode NVARCHAR2(80) NULL ,
    firstName NVARCHAR2(60) NOT NULL ,
    lastName NVARCHAR2(60) NOT NULL ,
    countryJurId NUMBER(18) NOT NULL ,
    mainDivisionJurId NUMBER(18) NOT NULL ,
    cityName NVARCHAR2(60) NULL ,
    street1 NVARCHAR2(100) NULL ,
    street2 NVARCHAR2(100) NULL ,
    postalCode NVARCHAR2(20) NULL ,
    phoneNumber NVARCHAR2(20) NULL ,
    securityQuestion1Id NUMBER(18) NOT NULL ,
    securityQuestion2Id NUMBER(18) NOT NULL ,
    securityQuestion3Id NUMBER(18) NOT NULL ,
    securityAnswer1 NVARCHAR2(100) NOT NULL ,
    securityAnswer2 NVARCHAR2(100) NOT NULL ,
    securityAnswer3 NVARCHAR2(100) NOT NULL ,
    creationDate NUMBER(8) NOT NULL ,
    newCustomerInd NUMBER(1) NULL 
);

ALTER TABLE CertWizardUser
    ADD ( CONSTRAINT CertWizardUser_PK
PRIMARY KEY (loginId));


ALTER TABLE CertWizardUser
    ADD ( CONSTRAINT f2Users FOREIGN KEY (userName)
    REFERENCES users (username))
;

create table SecurityQuestions
(
    securityQuestionId NUMBER(18) NOT NULL ,
    name NVARCHAR2(255) NOT NULL ,
    question NVARCHAR2(255) NOT NULL 
);

ALTER TABLE SecurityQuestions
    ADD ( CONSTRAINT SecurityQuestions_PK
PRIMARY KEY (securityQuestionId));


create table DataUpdateImpactTaxRule
(
    dataUpdateNumber NUMBER(8,1) NOT NULL ,
    taxRuleId NUMBER(18) NOT NULL ,
    geoRegionTypeId NUMBER(18) NOT NULL ,
    countryName NVARCHAR2(60) NOT NULL ,
    mainDivisionName NVARCHAR2(60) NULL ,
    jurName NVARCHAR2(60) NOT NULL ,
    jurTypeName NVARCHAR2(60) NOT NULL ,
    impositionName NVARCHAR2(60) NOT NULL ,
    categoryName NVARCHAR2(60) NOT NULL ,
    taxRuleType NVARCHAR2(60) NOT NULL ,
    rate NVARCHAR2(60) NULL ,
    salesTaxHolidayInd NUMBER(1) DEFAULT 0 NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    changeType NVARCHAR2(60) NOT NULL ,
    details NVARCHAR2(1000) NULL ,
    newCategoryInd NUMBER(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE DataUpdateImpactTaxRule
    ADD ( CONSTRAINT XPKDataImpTaxRule
PRIMARY KEY (dataUpdateNumber, taxRuleId, geoRegionTypeId));


ALTER TABLE DataUpdateImpactTaxRule
    ADD ( CONSTRAINT f1GeoRegionType1 FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId))
;

create table DataUpdateImpactTaxArea
(
    dataUpdateNumber NUMBER(8,1) NOT NULL ,
    taxAreaId NUMBER(18) NOT NULL ,
    countryName NVARCHAR2(60) NOT NULL ,
    mainDivisionName NVARCHAR2(60) NULL ,
    subDivisionName NVARCHAR2(60) NULL ,
    cityName NVARCHAR2(60) NULL ,
    districtName1 NVARCHAR2(60) NULL ,
    districtName2 NVARCHAR2(60) NULL ,
    districtName3 NVARCHAR2(60) NULL ,
    districtName4 NVARCHAR2(60) NULL ,
    districtName5 NVARCHAR2(60) NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    changeType NVARCHAR2(60) NOT NULL ,
    affectedTaxAreaId NUMBER(18) NOT NULL ,
    affectedCountryName NVARCHAR2(60) NULL ,
    affectedMainDivisionName NVARCHAR2(60) NULL ,
    affectedSubDivisionName NVARCHAR2(60) NULL ,
    affectedCityName NVARCHAR2(60) NULL ,
    affectedDistrictName1 NVARCHAR2(60) NULL ,
    affectedDistrictName2 NVARCHAR2(60) NULL ,
    affectedDistrictName3 NVARCHAR2(60) NULL ,
    affectedDistrictName4 NVARCHAR2(60) NULL ,
    affectedDistrictName5 NVARCHAR2(60) NULL 
);

ALTER TABLE DataUpdateImpactTaxArea
    ADD ( CONSTRAINT XPKDataImpTaxArea
PRIMARY KEY (dataUpdateNumber, taxAreaId, affectedTaxAreaId));


create table TransactionStatusType
(
    transStatusTypeId NUMBER(18) NOT NULL ,
    transStatusTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TransactionStatusType
    ADD ( CONSTRAINT XPKTransStatusType
PRIMARY KEY (transStatusTypeId));


create table FeatureResource
(
    featureResourceId NUMBER(18) NOT NULL ,
    featureResourceName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE FeatureResource
    ADD ( CONSTRAINT XPKFeatureResource
PRIMARY KEY (featureResourceId));


create table FeatureResourceImpositionType
(
    featureResourceId NUMBER(18) NOT NULL ,
    impsnTypeId NUMBER(18) NOT NULL 
);

ALTER TABLE FeatureResourceImpositionType
    ADD ( CONSTRAINT XPKFeatResImpType
PRIMARY KEY (featureResourceId, impsnTypeId));


create table FeatureResourceCategory
(
    featureResourceId NUMBER(18) NOT NULL ,
    txbltyCatId NUMBER(18) NOT NULL 
);

ALTER TABLE FeatureResourceCategory
    ADD ( CONSTRAINT XPKFeatResCat
PRIMARY KEY (featureResourceId, txbltyCatId));


create table TransactionEventType
(
    transEventTypeId NUMBER(18) NOT NULL ,
    transEventTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TransactionEventType
    ADD ( CONSTRAINT XPKTransEventType
PRIMARY KEY (transEventTypeId));


create table TpsDataReleaseEvent
(
    fullRlsId NUMBER(18) NULL ,
    interimRlsId NUMBER(18) NULL ,
    rlsTypeId NUMBER(1) NULL ,
    appliedDate NUMBER(8) NOT NULL ,
    rlsName NVARCHAR2(255) NULL 
);

create table TaxAssistPhaseType
(
    phaseId NUMBER(18) NOT NULL ,
    phaseName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE TaxAssistPhaseType
    ADD ( CONSTRAINT XPKTaxAstPhaseType
PRIMARY KEY (phaseId));


create table VATRegimeType
(
    vatRegimeTypeId NUMBER(18) NOT NULL ,
    vatRegimeTypeName NVARCHAR2(60) NOT NULL 
);

ALTER TABLE VATRegimeType
    ADD ( CONSTRAINT XPKVATRegimeType
PRIMARY KEY (vatRegimeTypeId));


create table VATGroup
(
    vatGroupId NUMBER(18) NOT NULL ,
    vatGroupSourceId NUMBER(18) NOT NULL ,
    vatGrpCreationDate NUMBER(8) NOT NULL 
);

ALTER TABLE VATGroup
    ADD ( CONSTRAINT XPKVATGroup
PRIMARY KEY (vatGroupId, vatGroupSourceId));


create table VATGroupDetail
(
    vatGroupDtlId NUMBER(18) NOT NULL ,
    vatGroupSourceId NUMBER(18) NOT NULL ,
    vatGroupId NUMBER(18) NOT NULL ,
    creationDate NUMBER(8) NOT NULL ,
    effDate NUMBER(8) NOT NULL ,
    endDate NUMBER(8) NOT NULL ,
    repMemTxprId NUMBER(18) NOT NULL ,
    vatGroupIdentifier NVARCHAR2(15) NOT NULL ,
    vatGroupName NVARCHAR2(30) NOT NULL ,
    jurisdictionId NUMBER(18) NOT NULL ,
    vatRegimeTypeId NUMBER(18) NOT NULL ,
    deletedInd NUMBER(1) NOT NULL 
);

ALTER TABLE VATGroupDetail
    ADD ( CONSTRAINT XPKVATGroupDtl
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId));


ALTER TABLE VATGroupDetail
    ADD ( CONSTRAINT f1VatGroup FOREIGN KEY (vatGroupId, vatGroupSourceId)
    REFERENCES VATGroup (vatGroupId, vatGroupSourceId))
;

ALTER TABLE VATGroupDetail
    ADD ( CONSTRAINT f31Party FOREIGN KEY (repMemTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId))
;

ALTER TABLE VATGroupDetail
    ADD ( CONSTRAINT f1VATRegimeType FOREIGN KEY (vatRegimeTypeId)
    REFERENCES VATRegimeType (vatRegimeTypeId))
;

create table VATGroupMembers
(
    vatGroupDtlId NUMBER(18) NOT NULL ,
    vatGroupSourceId NUMBER(18) NOT NULL ,
    memTxprId NUMBER(18) NOT NULL 
);

ALTER TABLE VATGroupMembers
    ADD ( CONSTRAINT XPKVATGroupMembers
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId, memTxprId));


ALTER TABLE VATGroupMembers
    ADD ( CONSTRAINT f1VATGroupDtl FOREIGN KEY (vatGroupDtlId, vatGroupSourceId)
    REFERENCES VATGroupDetail (vatGroupDtlId, vatGroupSourceId))
;

ALTER TABLE VATGroupMembers
    ADD ( CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId))
;

