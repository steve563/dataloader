create column table CoverageActionType
(
    coverageActionTypeId DECIMAL(18) NOT NULL ,
    coverageActionTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CoverageActionType
    ADD CONSTRAINT XPKJICvrgActTyp
PRIMARY KEY (coverageActionTypeId);


create column table CertificateStatus
(
    certificateStatusId DECIMAL(18) NOT NULL ,
    certificateStatusName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CertificateStatus
    ADD CONSTRAINT XPKJICertStat
PRIMARY KEY (certificateStatusId);


create column table CertificateApprovalStatus
(
    approvalStatusId DECIMAL(18) NOT NULL ,
    certApprovalStatusName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CertificateApprovalStatus
    ADD CONSTRAINT XPKCertApprStatus
PRIMARY KEY (approvalStatusId);


create column table ContactRoleType
(
    contactRoleTypeId DECIMAL(18) NOT NULL ,
    contactRoleTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE ContactRoleType
    ADD CONSTRAINT XPKContactRoleType
PRIMARY KEY (contactRoleTypeId);


create column table FormFieldType
(
    formFieldTypeId DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NULL 
);

ALTER TABLE FormFieldType
    ADD CONSTRAINT XPKFormField
PRIMARY KEY (formFieldTypeId);


create column table TaxRegistrationType
(
    taxRegistrationTypeId DECIMAL(18) NOT NULL ,
    taxRegistrationTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxRegistrationType
    ADD CONSTRAINT XPKTaxRegType
PRIMARY KEY (taxRegistrationTypeId);


create column table ApportionmentType
(
    apportionTypeId DECIMAL(18) NOT NULL ,
    apportionTypeName NVARCHAR(30) NOT NULL 
);

ALTER TABLE ApportionmentType
    ADD CONSTRAINT XPKApportionType
PRIMARY KEY (apportionTypeId);


create column table SitusConditionType
(
    situsCondTypeId DECIMAL(18) NOT NULL ,
    situsCondTypeName NVARCHAR(30) NULL 
);

ALTER TABLE SitusConditionType
    ADD CONSTRAINT XPKSitusCondType
PRIMARY KEY (situsCondTypeId);


create column table ChainTransType
(
    chainTransId DECIMAL(18) NOT NULL ,
    chainTransName NVARCHAR(60) NULL 
);

ALTER TABLE ChainTransType
    ADD CONSTRAINT XPKChainTransType
PRIMARY KEY (chainTransId);


create column table TitleTransferType
(
    titleTransferId DECIMAL(18) NOT NULL ,
    titleTransferName NVARCHAR(60) NULL 
);

ALTER TABLE TitleTransferType
    ADD CONSTRAINT XPKTitleTransType
PRIMARY KEY (titleTransferId);


create column table AssistedState
(
    assistedStateId DECIMAL(18) NOT NULL ,
    assistedStateName NVARCHAR(60) NULL 
);

ALTER TABLE AssistedState
    ADD CONSTRAINT XPKAssistedState
PRIMARY KEY (assistedStateId);


create column table RateClassification
(
    rateClassId DECIMAL(18) NOT NULL ,
    rateClassName NVARCHAR(60) NULL 
);

ALTER TABLE RateClassification
    ADD CONSTRAINT XPKRateClass
PRIMARY KEY (rateClassId);


create column table DMFilter
(
    filterId DECIMAL(18) NOT NULL ,
    filterName NVARCHAR(60) NOT NULL ,
    sourceId DECIMAL(18) NULL ,
    activityTypeId DECIMAL(18) NOT NULL ,
    filterDescription NVARCHAR(255) NULL ,
    followupInd DECIMAL(1) NULL 
);

ALTER TABLE DMFilter
    ADD CONSTRAINT XPKDMFilter
PRIMARY KEY (filterId);


create column table BusinessTransType
(
    busTransTypeId DECIMAL(18) NOT NULL ,
    busTransTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE BusinessTransType
    ADD CONSTRAINT XPKBusTransType
PRIMARY KEY (busTransTypeId);


create column table TransactionType
(
    transactionTypeId DECIMAL(18) NOT NULL ,
    transactionTypName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TransactionType
    ADD CONSTRAINT XPKTransactionType
PRIMARY KEY (transactionTypeId);


create column table DataType
(
    dataTypeId DECIMAL(18) NOT NULL ,
    dataTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE DataType
    ADD CONSTRAINT XPKDataType
PRIMARY KEY (dataTypeId);


create column table InputParameterType
(
    inputParamTypeId DECIMAL(18) NOT NULL ,
    inputParamTypeName NVARCHAR(60) NULL ,
    lookupStrategyId DECIMAL(18) NULL ,
    commodityCodeInd DECIMAL(1) NULL ,
    commodityCodeLength DECIMAL(18) NULL ,
    isTelecommLineType DECIMAL(1) NULL 
);

ALTER TABLE InputParameterType
    ADD CONSTRAINT XPKInputParamType
PRIMARY KEY (inputParamTypeId);


create column table JurTypeSet
(
    jurTypeSetId DECIMAL(18) NOT NULL ,
    jurTypeSetName NVARCHAR(60) NULL 
);

ALTER TABLE JurTypeSet
    ADD CONSTRAINT XPKJurTypeSet
PRIMARY KEY (jurTypeSetId);


create column table JurTypeSetMember
(
    jurTypeSetId DECIMAL(18) NOT NULL ,
    jurisdictionTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT XPKJurTypeSetMem
PRIMARY KEY (jurTypeSetId, jurisdictionTypeId);


ALTER TABLE JurTypeSetMember
    ADD CONSTRAINT f1JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

create column table TaxResultType
(
    taxResultTypeId DECIMAL(18) NOT NULL ,
    taxResultTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxResultType
    ADD CONSTRAINT XPKTaxResultType
PRIMARY KEY (taxResultTypeId);


create column table RecoverableResultType
(
    recoverableResultTypeId DECIMAL(18) NOT NULL ,
    recoverableResultTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE RecoverableResultType
    ADD CONSTRAINT XPKRcvResultType
PRIMARY KEY (recoverableResultTypeId);


create column table PartyType
(
    partyTypeId DECIMAL(18) NOT NULL ,
    partyTypeName NVARCHAR(20) NOT NULL 
);

ALTER TABLE PartyType
    ADD CONSTRAINT XPKPartyType
PRIMARY KEY (partyTypeId);


create column table LocationRoleType
(
    locationRoleTypeId DECIMAL(18) NOT NULL ,
    locationRoleTypNam NVARCHAR(30) NOT NULL 
);

ALTER TABLE LocationRoleType
    ADD CONSTRAINT XPKLocationRoleTyp
PRIMARY KEY (locationRoleTypeId);


create column table ShippingTerms
(
    shippingTermsId DECIMAL(18) NOT NULL ,
    shippingTermsName NVARCHAR(60) NOT NULL 
);

ALTER TABLE ShippingTerms
    ADD CONSTRAINT XPKShippingTerms
PRIMARY KEY (shippingTermsId);


create column table BasisType
(
    basisTypeId DECIMAL(18) NOT NULL ,
    basisTypeName NVARCHAR(60) NULL 
);

ALTER TABLE BasisType
    ADD CONSTRAINT XPKBasisType
PRIMARY KEY (basisTypeId);


create column table DMFilterDate
(
    dateTypeId DECIMAL(18) NOT NULL ,
    criteriaDate DECIMAL(8) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    filterId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMFilterDate
    ADD CONSTRAINT XPKDMFilterDate
PRIMARY KEY (filterId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMFilterDate
    ADD CONSTRAINT f1DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId) ON UPDATE CASCADE
;

create column table OutputNotice
(
    outputNoticeId DECIMAL(18) NOT NULL ,
    outputNoticeName NVARCHAR(30) NOT NULL 
);

ALTER TABLE OutputNotice
    ADD CONSTRAINT XPKOutputNotice
PRIMARY KEY (outputNoticeId);


create column table CustomsStatusType
(
    customsStatusId DECIMAL(18) NOT NULL ,
    customsStatusName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CustomsStatusType
    ADD CONSTRAINT XPKCustomsStatType
PRIMARY KEY (customsStatusId);


create column table CreationSource
(
    creationSourceId DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NOT NULL 
);

ALTER TABLE CreationSource
    ADD CONSTRAINT XPKCreationSource
PRIMARY KEY (creationSourceId);


create column table MovementMethodType
(
    movementMethodId DECIMAL(18) NOT NULL ,
    movementMethodName NVARCHAR(60) NOT NULL 
);

ALTER TABLE MovementMethodType
    ADD CONSTRAINT XPKMvmntMethodType
PRIMARY KEY (movementMethodId);


create column table SitusCondition
(
    situsConditionId DECIMAL(18) NOT NULL ,
    locRoleTypeId DECIMAL(18) NULL ,
    locRoleType2Id DECIMAL(18) NULL ,
    situsCondTypeId DECIMAL(18) NULL ,
    transactionTypeId DECIMAL(18) NULL ,
    transPrspctvTypId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    jurisdictionTypeId DECIMAL(18) NULL ,
    endDate DECIMAL(8) NOT NULL ,
    jurTypeSetId DECIMAL(18) NULL ,
    partyRoleTypeId DECIMAL(18) NULL ,
    currencyUnitId DECIMAL(18) NULL ,
    locRoleTypeId1Name NVARCHAR(60) NULL ,
    locRoleTypeId2Name NVARCHAR(60) NULL ,
    stsSubRtnNodeId DECIMAL(18) NULL ,
    boolFactName NVARCHAR(60) NULL ,
    boolFactValue DECIMAL(1) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL ,
    customsStatusId DECIMAL(18) NULL ,
    movementMethodId DECIMAL(18) NULL ,
    titleTransferId DECIMAL(18) NULL 
);

ALTER TABLE SitusCondition
    ADD CONSTRAINT XPKSitusCondition
PRIMARY KEY (situsConditionId);


ALTER TABLE SitusCondition
    ADD CONSTRAINT f2LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1SCdTyp FOREIGN KEY (situsCondTypeId)
    REFERENCES SitusConditionType (situsCondTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f3JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1CstStT FOREIGN KEY (customsStatusId)
    REFERENCES CustomsStatusType (customsStatusId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1MvMthT FOREIGN KEY (movementMethodId)
    REFERENCES MovementMethodType (movementMethodId) ON UPDATE CASCADE
;

ALTER TABLE SitusCondition
    ADD CONSTRAINT f1TtTfrT FOREIGN KEY (titleTransferId)
    REFERENCES TitleTransferType (titleTransferId) ON UPDATE CASCADE
;

create column table BracketTaxCalcType
(
    brcktTaxCalcTypeId DECIMAL(18) NOT NULL ,
    brcktTaxCalcTypNam NVARCHAR(60) NULL 
);

ALTER TABLE BracketTaxCalcType
    ADD CONSTRAINT XPKBrcktTaxCalcTyp
PRIMARY KEY (brcktTaxCalcTypeId);


create column table SitusConcType
(
    situsConcTypeId DECIMAL(18) NOT NULL ,
    situsConcTypeName NVARCHAR(60) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE SitusConcType
    ADD CONSTRAINT XPKSitusConcType
PRIMARY KEY (situsConcTypeId);


create column table ReasonCategory
(
    reasonCategoryId DECIMAL(18) NOT NULL ,
    reasonCategoryName NVARCHAR(60) NULL ,
    isUserDefined DECIMAL(1) NULL 
);

ALTER TABLE ReasonCategory
    ADD CONSTRAINT XPKReasonCategory
PRIMARY KEY (reasonCategoryId);


create column table ReasonCategoryJur
(
    reasonCategoryId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NULL 
);

ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT XPKReasonCatJur
PRIMARY KEY (reasonCategoryId, jurisdictionId, effDate);


ALTER TABLE ReasonCategoryJur
    ADD CONSTRAINT f7RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

create column table SitusCondJur
(
    situsConditionId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL 
);

ALTER TABLE SitusCondJur
    ADD CONSTRAINT XPKSitusCondJur
PRIMARY KEY (situsConditionId, jurisdictionId);


ALTER TABLE SitusCondJur
    ADD CONSTRAINT f2StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId) ON UPDATE CASCADE
;

create column table SitusCondShippingTerms
(
    situsConditionId DECIMAL(18) NOT NULL ,
    shippingTermsId DECIMAL(18) NOT NULL 
);

ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT XPKSitusCondShipTm
PRIMARY KEY (situsConditionId, shippingTermsId);


ALTER TABLE SitusCondShippingTerms
    ADD CONSTRAINT f4StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId) ON UPDATE CASCADE
;

create column table TaxType
(
    taxTypeId DECIMAL(18) NOT NULL ,
    taxTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxType
    ADD CONSTRAINT XPKTaxType
PRIMARY KEY (taxTypeId);


create column table FilingCategory
(
    filingCategoryId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    filingCategoryCode DECIMAL(5) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    filingCategoryName NVARCHAR(60) NOT NULL ,
    primaryCategoryInd DECIMAL(1) NOT NULL 
);

ALTER TABLE FilingCategory
    ADD CONSTRAINT XPKFilingCat
PRIMARY KEY (filingCategoryId);


create column table FilingCategoryOvrd
(
    filingCatOvrdId DECIMAL(18) NOT NULL ,
    filingCategoryId DECIMAL(18) NOT NULL ,
    ovrdFilingCatId DECIMAL(18) NOT NULL ,
    taxTypeId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT XPKFCOvd
PRIMARY KEY (filingCatOvrdId);


ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f2FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId) ON UPDATE CASCADE
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f3FilCat FOREIGN KEY (ovrdFilingCatId)
    REFERENCES FilingCategory (filingCategoryId) ON UPDATE CASCADE
;

ALTER TABLE FilingCategoryOvrd
    ADD CONSTRAINT f5TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table SitusConclusion
(
    situsConclusionId DECIMAL(18) NOT NULL ,
    situsConcTypeId DECIMAL(18) NULL ,
    taxTypeId DECIMAL(18) NULL ,
    jurisdictionTypeId DECIMAL(18) NULL ,
    locRoleTypeId DECIMAL(18) NULL ,
    locRoleType2Id DECIMAL(18) NULL ,
    multiSitusRecTypId DECIMAL(18) NULL ,
    jurTypeSetId DECIMAL(18) NULL ,
    taxType2Id DECIMAL(18) NULL ,
    locRoleTypeId1Name NVARCHAR(60) NULL ,
    locRoleTypeId2Name NVARCHAR(60) NULL ,
    taxTypeIdName NVARCHAR(60) NULL ,
    boolFactName NVARCHAR(60) NULL ,
    boolFactValue DECIMAL(1) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL ,
    outputNoticeId DECIMAL(18) NULL ,
    impsnTypeId DECIMAL(18) NULL ,
    impsnTypeSourceId DECIMAL(18) NULL ,
    txbltyCatId DECIMAL(18) NULL 
);

ALTER TABLE SitusConclusion
    ADD CONSTRAINT XPKSitusConc
PRIMARY KEY (situsConclusionId);


ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1OutNot FOREIGN KEY (outputNoticeId)
    REFERENCES OutputNotice (outputNoticeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3LRlTyp FOREIGN KEY (locRoleType2Id)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f1SCcTyp FOREIGN KEY (situsConcTypeId)
    REFERENCES SitusConcType (situsConcTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f3TaxTyp FOREIGN KEY (taxType2Id)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4LRlTyp FOREIGN KEY (locRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConclusion
    ADD CONSTRAINT f4TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table TaxScope
(
    taxScopeId DECIMAL(18) NOT NULL ,
    taxScopeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxScope
    ADD CONSTRAINT XPKTaxScope
PRIMARY KEY (taxScopeId);


create column table RoundingRule
(
    roundingRuleId DECIMAL(18) NOT NULL ,
    initialPrecision DECIMAL(15) NULL ,
    taxScopeId DECIMAL(18) NOT NULL ,
    finalPrecision DECIMAL(15) NOT NULL ,
    threshold DECIMAL(15) NOT NULL ,
    decimalPosition DECIMAL(15) NOT NULL ,
    roundingTypeId DECIMAL(18) NULL 
);

ALTER TABLE RoundingRule
    ADD CONSTRAINT XPKRoundingRule
PRIMARY KEY (roundingRuleId);


ALTER TABLE RoundingRule
    ADD CONSTRAINT f2TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId) ON UPDATE CASCADE
;

create column table TransSubType
(
    transSubTypeId DECIMAL(18) NOT NULL ,
    transSubTypeName NVARCHAR(60) NULL 
);

ALTER TABLE TransSubType
    ADD CONSTRAINT XPKTransSubType
PRIMARY KEY (transSubTypeId);


create column table VertexProductType
(
    vertexProductId DECIMAL(18) NOT NULL ,
    vertexProductName NVARCHAR(60) NULL 
);

ALTER TABLE VertexProductType
    ADD CONSTRAINT XPKVertexProdType
PRIMARY KEY (vertexProductId);


create column table PartyRoleType
(
    partyRoleTypeId DECIMAL(18) NOT NULL ,
    partyRoleTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE PartyRoleType
    ADD CONSTRAINT XPKPartyRoleType
PRIMARY KEY (partyRoleTypeId);


create column table TelecomUnitConversion
(
    telecomUnitConversionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NOT NULL ,
    sourceUnitOfMeasureISOCode NVARCHAR(20) NOT NULL ,
    targetUnitOfMeasureISOCode NVARCHAR(20) NOT NULL ,
    firstConvertCount DECIMAL(8) NOT NULL ,
    additionalConvertCount DECIMAL(8) NULL ,
    isDefault DECIMAL(1) NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversion
    ADD CONSTRAINT XPKTelComUntConvsn
PRIMARY KEY (telecomUnitConversionId, sourceId);


create column table TaxStructureType
(
    taxStructureTypeId DECIMAL(18) NOT NULL ,
    taxStrucTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxStructureType
    ADD CONSTRAINT XPKTaxStrucType
PRIMARY KEY (taxStructureTypeId);


create column table DeductionType
(
    deductionTypeId DECIMAL(18) NOT NULL ,
    deductionTypeName NVARCHAR(60) NULL 
);

ALTER TABLE DeductionType
    ADD CONSTRAINT XPKDeductionType
PRIMARY KEY (deductionTypeId);


create column table TransOrigType
(
    transOrigTypeId DECIMAL(18) NOT NULL ,
    transOrigTypeName NVARCHAR(60) NULL 
);

ALTER TABLE TransOrigType
    ADD CONSTRAINT XPKTransOrigType
PRIMARY KEY (transOrigTypeId);


create column table TaxRuleType
(
    taxRuleTypeId DECIMAL(18) NOT NULL ,
    taxRuleTypeName NVARCHAR(60) NULL 
);

ALTER TABLE TaxRuleType
    ADD CONSTRAINT XPKTaxRuleType
PRIMARY KEY (taxRuleTypeId);


create column table WithholdingType
(
    withholdingTypeId DECIMAL(18) NOT NULL ,
    withholdingTypeName NVARCHAR(60) NULL 
);

ALTER TABLE WithholdingType
    ADD CONSTRAINT XPKWithholdingType
PRIMARY KEY (withholdingTypeId);


create column table AccumulationByType
(
    id DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NULL 
);

ALTER TABLE AccumulationByType
    ADD CONSTRAINT XPKAccByType
PRIMARY KEY (id);


create column table AccumulationPeriodType
(
    id DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NULL 
);

ALTER TABLE AccumulationPeriodType
    ADD CONSTRAINT XPKAccPeriodType
PRIMARY KEY (id);


create column table AccumulationType
(
    id DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NULL 
);

ALTER TABLE AccumulationType
    ADD CONSTRAINT XPKAccType
PRIMARY KEY (id);


create column table TaxRuleTaxImpositionType
(
    taxRuleTaxImpositionTypeId DECIMAL(18) NOT NULL ,
    taxRuleTaxImpositionTypeName NVARCHAR(60) NULL 
);

ALTER TABLE TaxRuleTaxImpositionType
    ADD CONSTRAINT XPKTxRuleImpsnType
PRIMARY KEY (taxRuleTaxImpositionTypeId);


create column table ValidationType
(
    validationTypeId DECIMAL(18) NOT NULL ,
    validationTypeName NVARCHAR(60) NULL 
);

ALTER TABLE ValidationType
    ADD CONSTRAINT XPKValidationType
PRIMARY KEY (validationTypeId);


create column table DMFilterStrng
(
    stringTypeId DECIMAL(18) NOT NULL ,
    criteriaString NVARCHAR(255) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    filterId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMFilterStrng
    ADD CONSTRAINT XPKDMFilterStrng
PRIMARY KEY (filterId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMFilterStrng
    ADD CONSTRAINT f3DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId) ON UPDATE CASCADE
;

create column table CertClassType
(
    certClassTypeId DECIMAL(18) NOT NULL ,
    certClassTypeName NVARCHAR(60) NULL 
);

ALTER TABLE CertClassType
    ADD CONSTRAINT XPKCertClassType
PRIMARY KEY (certClassTypeId);


create column table DiscountCategory
(
    discountCatId DECIMAL(18) NOT NULL ,
    discountCatName NVARCHAR(60) NOT NULL ,
    discountCatDesc NVARCHAR(100) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE DiscountCategory
    ADD CONSTRAINT XPKDiscountCat
PRIMARY KEY (discountCatId);


create column table TaxabilityCategory
(
    txbltyCatId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxabilityCategory
    ADD CONSTRAINT XPKTaxabilityCat
PRIMARY KEY (txbltyCatId, txbltyCatSrcId);


create column table TxbltyCatDetail
(
    txbltyCatDtlId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    txbltyCatCode NVARCHAR(60) NOT NULL ,
    txbltyCatName NVARCHAR(60) NOT NULL ,
    txbltyCatDesc NVARCHAR(1000) NULL ,
    txbltyCatDefaultId DECIMAL(18) NOT NULL ,
    prntTxbltyCatId DECIMAL(18) NULL ,
    prntTxbltyCatSrcId DECIMAL(18) NULL ,
    dataTypeId DECIMAL(18) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    allowRelatedInd DECIMAL(1) NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT XPKTxbltyCatDetail
PRIMARY KEY (txbltyCatDtlId, txbltyCatSrcId);


ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f2TaxCat FOREIGN KEY (prntTxbltyCatId, prntTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyCatDetail
    ADD CONSTRAINT f1DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId) ON UPDATE CASCADE
;

create column table FlexFieldDef
(
    flexFieldDefId DECIMAL(18) NOT NULL ,
    flexFieldDefSrcId DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE FlexFieldDef
    ADD CONSTRAINT XPKFlexibleField
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId);


create column table FlexFieldDefDetail
(
    flexFieldDefDtlId DECIMAL(18) NOT NULL ,
    flexFieldDefSrcId DECIMAL(18) NOT NULL ,
    flexFieldDefId DECIMAL(18) NOT NULL ,
    dataTypeId DECIMAL(18) NOT NULL ,
    calcOutputInd DECIMAL(1) DEFAULT 0 NOT NULL ,
    flexFieldDesc NVARCHAR(1000) NULL ,
    flexFieldDefRefNum DECIMAL(2) NOT NULL ,
    flexFieldDefSeqNum DECIMAL(8) NOT NULL ,
    shortName NVARCHAR(10) NOT NULL ,
    longName NVARCHAR(60) NULL ,
    txbltyCatId DECIMAL(18) NULL ,
    txbltyCatSrcId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT XPKFlexFieldDetail
PRIMARY KEY (flexFieldDefDtlId, flexFieldDefSrcId);


ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f2DataTp FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId) ON UPDATE CASCADE
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f5TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE FlexFieldDefDetail
    ADD CONSTRAINT f3FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId) ON UPDATE CASCADE
;

create column table TaxJurDetail
(
    taxTypeId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    regGroupAllowedInd DECIMAL(1) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    registrationReqInd DECIMAL(1) NOT NULL ,
    reqLocsForRprtgInd DECIMAL(1) NOT NULL ,
    reqLocsForSitusInd DECIMAL(1) NOT NULL 
);

ALTER TABLE TaxJurDetail
    ADD CONSTRAINT XPKTaxJurDetail
PRIMARY KEY (jurisdictionId, taxTypeId, effDate, sourceId);


ALTER TABLE TaxJurDetail
    ADD CONSTRAINT f16TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table DMActivityLog
(
    activityLogId DECIMAL(18) NOT NULL ,
    filterName NVARCHAR(60) NULL ,
    activityTypeId DECIMAL(18) NOT NULL ,
    startTime TIMESTAMP NOT NULL ,
    endTime TIMESTAMP NULL ,
    lastPingTime TIMESTAMP NOT NULL ,
    nextPingTime TIMESTAMP NOT NULL ,
    userName NVARCHAR(60) NOT NULL ,
    userId DECIMAL(18) NOT NULL ,
    sourceName NVARCHAR(60) NULL ,
    sourceId DECIMAL(18) NULL ,
    activityStatusId DECIMAL(18) NOT NULL ,
    filterDescription NVARCHAR(255) NULL ,
    activityLogMessage NVARCHAR(1000) NULL ,
    hostName NVARCHAR(128) NOT NULL ,
    outputFileName NVARCHAR(255) NULL ,
    masterAdminInd DECIMAL(1) NULL ,
    sysAdminInd DECIMAL(1) NULL ,
    followupInd DECIMAL(1) NULL 
);

ALTER TABLE DMActivityLog
    ADD CONSTRAINT XPKDMActivityLog
PRIMARY KEY (activityLogId);


create column table DMActivityLogNum
(
    numberTypeId DECIMAL(18) NOT NULL ,
    criteriaNum DECIMAL(18) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    activityLogId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT XPKDMActLogNum
PRIMARY KEY (activityLogId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogNum
    ADD CONSTRAINT f2DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId) ON UPDATE CASCADE
;

create column table DMActivityLogDate
(
    dateTypeId DECIMAL(18) NOT NULL ,
    criteriaDate DECIMAL(8) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    activityLogId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT XPKDMActLogDate
PRIMARY KEY (activityLogId, dateTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogDate
    ADD CONSTRAINT f1DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId) ON UPDATE CASCADE
;

create column table DMActivityLogInd
(
    indicatorTypeId DECIMAL(18) NOT NULL ,
    criteriaIndicator DECIMAL(1) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    activityLogId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT XPKDMActLogInd
PRIMARY KEY (activityLogId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogInd
    ADD CONSTRAINT f4DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId) ON UPDATE CASCADE
;

create column table TpsSchVersion
(
    schemaVersionId DECIMAL(18) NOT NULL ,
    subjectAreaId DECIMAL(18) NULL ,
    schemaVersionCode NVARCHAR(30) NOT NULL ,
    syncVersionId NVARCHAR(64) NULL ,
    schemaSubVersionId DECIMAL(18) NULL 
);

ALTER TABLE TpsSchVersion
    ADD CONSTRAINT XPKTpsSchemaVer
PRIMARY KEY (schemaVersionId);


create column table TpsPatchDataEvent
(
    dataEventId DECIMAL(18) NOT NULL ,
    patchId DECIMAL(18) NOT NULL ,
    patchInterimId DECIMAL(18) NOT NULL ,
    eventDate DECIMAL(8) NOT NULL 
);

ALTER TABLE TpsPatchDataEvent
    ADD CONSTRAINT XPKTPSPchDataEvent
PRIMARY KEY (dataEventId);


create column table ApportionmentFriendlyStates
(
    friendlyStateId DECIMAL(18) NOT NULL ,
    jurId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE ApportionmentFriendlyStates
    ADD CONSTRAINT XPKApptionFriendly
PRIMARY KEY (friendlyStateId);


create column table SitusConditionNode
(
    situsCondNodeId DECIMAL(18) NOT NULL ,
    situsConditionId DECIMAL(18) NOT NULL ,
    rootNodeInd DECIMAL(1) NOT NULL 
);

ALTER TABLE SitusConditionNode
    ADD CONSTRAINT XPKSitusCondNode
PRIMARY KEY (situsCondNodeId);


ALTER TABLE SitusConditionNode
    ADD CONSTRAINT f1StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId) ON UPDATE CASCADE
;

create column table TrueSitusCond
(
    prntTruSitusCondId DECIMAL(18) NOT NULL ,
    chldTruSitusCondId DECIMAL(18) NOT NULL 
);

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT XPKTrueSitusCond
PRIMARY KEY (prntTruSitusCondId, chldTruSitusCondId);


ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f1StCdNd FOREIGN KEY (prntTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

ALTER TABLE TrueSitusCond
    ADD CONSTRAINT f2StCdNd FOREIGN KEY (chldTruSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

create column table SitusConcNode
(
    situsConcNodeId DECIMAL(18) NOT NULL ,
    situsConclusionId DECIMAL(18) NOT NULL ,
    truForSitusCondId DECIMAL(18) NULL ,
    flsForSitusCondId DECIMAL(18) NULL 
);

ALTER TABLE SitusConcNode
    ADD CONSTRAINT XPKSitusConcNode
PRIMARY KEY (situsConcNodeId);


ALTER TABLE SitusConcNode
    ADD CONSTRAINT f1StsCnc FOREIGN KEY (situsConclusionId)
    REFERENCES SitusConclusion (situsConclusionId) ON UPDATE CASCADE
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f5StCdNd FOREIGN KEY (truForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

ALTER TABLE SitusConcNode
    ADD CONSTRAINT f6StCdNd FOREIGN KEY (flsForSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

create column table Party
(
    partyId DECIMAL(18) NOT NULL ,
    partySourceId DECIMAL(18) NOT NULL ,
    partyCreationDate DECIMAL(8) NOT NULL 
);

ALTER TABLE Party
    ADD CONSTRAINT XPKParty
PRIMARY KEY (partyId, partySourceId);


create column table PartyDetail
(
    partyDtlId DECIMAL(18) NOT NULL ,
    partySourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    creationSourceId DECIMAL(18) DEFAULT 1 NOT NULL ,
    parentPartyId DECIMAL(18) NULL ,
    partyTypeId DECIMAL(18) NOT NULL ,
    partyName NVARCHAR(60) NOT NULL ,
    userPartyIdCode NVARCHAR(40) NOT NULL ,
    partyRelInd DECIMAL(1) NOT NULL ,
    taxpayerType NVARCHAR(60) NULL ,
    filingEntityInd DECIMAL(1) NOT NULL ,
    prntInheritenceInd DECIMAL(1) NOT NULL ,
    ersInd DECIMAL(1) NOT NULL ,
    taxThresholdAmt DECIMAL(18,3) NULL ,
    taxThresholdPct DECIMAL(10,6) NULL ,
    taxOvrThresholdAmt DECIMAL(18,3) NULL ,
    taxOvrThresholdPct DECIMAL(10,6) NULL ,
    partyClassInd DECIMAL(1) NOT NULL ,
    shippingTermsId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    discountCatId DECIMAL(18) NULL ,
    customField1Value NVARCHAR(60) NULL ,
    customField2Value NVARCHAR(60) NULL ,
    customField3Value NVARCHAR(60) NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL ,
    parentCustomerId DECIMAL(18) NULL 
);

ALTER TABLE PartyDetail
    ADD CONSTRAINT XPKPartyDtl
PRIMARY KEY (partyDtlId, partySourceId);


ALTER TABLE PartyDetail
    ADD CONSTRAINT f1ShpTrm FOREIGN KEY (shippingTermsId)
    REFERENCES ShippingTerms (shippingTermsId) ON UPDATE CASCADE
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1PtyTyp FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId) ON UPDATE CASCADE
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f1Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f2Party FOREIGN KEY (parentPartyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f33Party FOREIGN KEY (parentCustomerId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE PartyDetail
    ADD CONSTRAINT f5DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId) ON UPDATE CASCADE
;

create column table PartyRoleTaxResult
(
    partyId DECIMAL(18) NOT NULL ,
    partySourceId DECIMAL(18) NOT NULL ,
    partyRoleTypeId DECIMAL(18) NOT NULL ,
    taxResultTypeId DECIMAL(18) NOT NULL ,
    reasonCategoryId DECIMAL(18) NULL 
);

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT XPKPrtyRoleTaxRslt
PRIMARY KEY (partyId, partyRoleTypeId, partySourceId);


ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f5RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f3TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId) ON UPDATE CASCADE
;

ALTER TABLE PartyRoleTaxResult
    ADD CONSTRAINT f1PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

create column table PartyVtxProdType
(
    partyId DECIMAL(18) NOT NULL ,
    partySourceId DECIMAL(18) NOT NULL ,
    vertexProductId DECIMAL(18) NOT NULL 
);

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT XPKPrtyVtxProdTyp
PRIMARY KEY (partyId, partySourceId, vertexProductId);


ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f1VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

ALTER TABLE PartyVtxProdType
    ADD CONSTRAINT f8Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table BusinessLocation
(
    partyId DECIMAL(18) NOT NULL ,
    businessLocationId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxAreaId DECIMAL(18) NULL ,
    userLocationCode NVARCHAR(20) NULL ,
    registrationCode NVARCHAR(20) NULL ,
    streetInfoDesc NVARCHAR(100) NULL ,
    streetInfo2Desc NVARCHAR(100) NULL ,
    cityName NVARCHAR(60) NULL ,
    subDivisionName NVARCHAR(60) NULL ,
    mainDivisionName NVARCHAR(60) NULL ,
    postalCode NVARCHAR(20) NULL ,
    countryName NVARCHAR(60) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    busLocationName NVARCHAR(60) NULL ,
    partyRoleTypeId DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE BusinessLocation
    ADD CONSTRAINT XPKBusinessLoc
PRIMARY KEY (businessLocationId, partyId, sourceId);


ALTER TABLE BusinessLocation
    ADD CONSTRAINT f4Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE BusinessLocation
    ADD CONSTRAINT f3PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

create column table PartyNote
(
    partySourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    partyNoteText NVARCHAR(1000) NULL 
);

ALTER TABLE PartyNote
    ADD CONSTRAINT XPKPartyNote
PRIMARY KEY (partyId, partySourceId);


ALTER TABLE PartyNote
    ADD CONSTRAINT f14Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table PartyContact
(
    partyContactId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    contactRoleTypeId DECIMAL(18) NOT NULL ,
    contactFirstName NVARCHAR(60) NULL ,
    contactLastName NVARCHAR(60) NULL ,
    streetInfoDesc NVARCHAR(100) NULL ,
    streetInfo2Desc NVARCHAR(100) NULL ,
    cityName NVARCHAR(60) NULL ,
    mainDivisionName NVARCHAR(60) NULL ,
    postalCode NVARCHAR(20) NULL ,
    countryName NVARCHAR(60) NULL ,
    phoneNumber NVARCHAR(20) NULL ,
    phoneExtension NVARCHAR(20) NULL ,
    faxNumber NVARCHAR(20) NULL ,
    emailAddress NVARCHAR(100) NULL ,
    departmentIdCode NVARCHAR(20) NULL ,
    deletedInd DECIMAL(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE PartyContact
    ADD CONSTRAINT XPKPartyContact
PRIMARY KEY (partyContactId, sourceId);


ALTER TABLE PartyContact
    ADD CONSTRAINT f6Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE PartyContact
    ADD CONSTRAINT f1CntcRT FOREIGN KEY (contactRoleTypeId)
    REFERENCES ContactRoleType (contactRoleTypeId) ON UPDATE CASCADE
;

create column table TaxabilityDriver
(
    txbltyDvrId DECIMAL(18) NOT NULL ,
    txbltyDvrSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxabilityDriver
    ADD CONSTRAINT XPKTaxabilityDvr
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


create column table TxbltyDriverDetail
(
    txbltyDvrDtlId DECIMAL(18) NOT NULL ,
    txbltyDvrSrcId DECIMAL(18) NOT NULL ,
    txbltyDvrId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    inputParamTypeId DECIMAL(18) NOT NULL ,
    txbltyDvrCode NVARCHAR(40) NOT NULL ,
    txbltyDvrName NVARCHAR(60) NOT NULL ,
    reasonCategoryId DECIMAL(18) NULL ,
    exemptInd DECIMAL(1) NOT NULL ,
    taxpayerPartyId DECIMAL(18) NULL ,
    taxpayerSrcId DECIMAL(18) NULL ,
    discountCatId DECIMAL(18) NULL ,
    flexFieldDefId DECIMAL(18) NULL ,
    flexFieldDefSrcId DECIMAL(18) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT XPKTxbltyDvrDetail
PRIMARY KEY (txbltyDvrDtlId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f1TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f8RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2InPTyp FOREIGN KEY (inputParamTypeId)
    REFERENCES InputParameterType (inputParamTypeId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f3DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDriverDetail
    ADD CONSTRAINT f2FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId) ON UPDATE CASCADE
;

create column table TxbltyDriverNote
(
    txbltyDvrId DECIMAL(18) NOT NULL ,
    txbltyDvrSrcId DECIMAL(18) NOT NULL ,
    txbltyDvrNoteText NVARCHAR(1000) NULL 
);

ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT XPKTxbltyDvrNote
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId);


ALTER TABLE TxbltyDriverNote
    ADD CONSTRAINT f9TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

create column table TxbltyDvrVtxPrdTyp
(
    vertexProductId DECIMAL(18) NOT NULL ,
    txbltyDvrId DECIMAL(18) NOT NULL ,
    txbltyDvrSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT XPKTxbltyDvrVtxPrd
PRIMARY KEY (txbltyDvrId, txbltyDvrSrcId, vertexProductId);


ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f2TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDvrVtxPrdTyp
    ADD CONSTRAINT f8VxPTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table TelecomUnitConversionLineType
(
    telecomUnitConvnLineTypeId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    lineTypeId DECIMAL(18) NOT NULL ,
    lineTypeSourceId DECIMAL(18) NOT NULL ,
    telecomUnitConversionId DECIMAL(18) NOT NULL ,
    telecomUnitConversionSourceId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT XPKTCUntCvnLnType
PRIMARY KEY (telecomUnitConvnLineTypeId, sourceId);


ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f1conv FOREIGN KEY (telecomUnitConversionId, sourceId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TelecomUnitConversionLineType
    ADD CONSTRAINT f2LineType FOREIGN KEY (lineTypeId, lineTypeSourceId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

create column table TxbltyCatMap
(
    txbltyCatMapId DECIMAL(18) NOT NULL ,
    txbltyCatMapSrcId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL ,
    taxpayerPartyId DECIMAL(18) NULL ,
    otherPartyId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    txbltyCatMapNote NVARCHAR(1000) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT XPKTxbltyCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId);


ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f7TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f24Party FOREIGN KEY (taxpayerPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyCatMap
    ADD CONSTRAINT f25Party FOREIGN KEY (otherPartyId, txbltyCatMapSrcId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table TxbltyDriverCatMap
(
    txbltyCatMapId DECIMAL(18) NOT NULL ,
    txbltyCatMapSrcId DECIMAL(18) NOT NULL ,
    txbltyDvrId DECIMAL(18) NOT NULL 
);

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT XPKTxbltyDvrCatMap
PRIMARY KEY (txbltyCatMapId, txbltyCatMapSrcId, txbltyDvrId);


ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f5TaxDvr FOREIGN KEY (txbltyDvrId, txbltyCatMapSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

ALTER TABLE TxbltyDriverCatMap
    ADD CONSTRAINT f1TxCMap FOREIGN KEY (txbltyCatMapId, txbltyCatMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId) ON UPDATE CASCADE
;

create column table InvoiceTextType
(
    invoiceTextTypeId DECIMAL(18) NOT NULL ,
    invoiceTextTypeSrcId DECIMAL(18) NOT NULL ,
    invoiceTextTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE InvoiceTextType
    ADD CONSTRAINT XPKInvoiceTextType
PRIMARY KEY (invoiceTextTypeId, invoiceTextTypeSrcId);


create column table InvoiceText
(
    invoiceTextId DECIMAL(18) NOT NULL ,
    invoiceTextSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE InvoiceText
    ADD CONSTRAINT XPKInvText
PRIMARY KEY (invoiceTextId, invoiceTextSrcId);


create column table InvoiceTextDetail
(
    invoiceTextDtlId DECIMAL(18) NOT NULL ,
    invoiceTextSrcId DECIMAL(18) NOT NULL ,
    invoiceTextId DECIMAL(18) NOT NULL ,
    invoiceTextTypeId DECIMAL(18) NOT NULL ,
    invoiceTextTypeSrcId DECIMAL(18) NOT NULL ,
    invoiceTextCode NVARCHAR(60) NOT NULL ,
    invoiceText NVARCHAR(200) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT XPKInvTextDetail
PRIMARY KEY (invoiceTextDtlId, invoiceTextSrcId);


ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f1InvText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId) ON UPDATE CASCADE
;

ALTER TABLE InvoiceTextDetail
    ADD CONSTRAINT f2InvTextTyp FOREIGN KEY (invoiceTextTypeId, invoiceTextTypeSrcId)
    REFERENCES InvoiceTextType (invoiceTextTypeId, invoiceTextTypeSrcId) ON UPDATE CASCADE
;

create column table DMFilterInd
(
    indicatorTypeId DECIMAL(18) NOT NULL ,
    criteriaIndicator DECIMAL(1) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    filterId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMFilterInd
    ADD CONSTRAINT XPKDMFilterInd
PRIMARY KEY (filterId, indicatorTypeId, criteriaOrderNum);


ALTER TABLE DMFilterInd
    ADD CONSTRAINT f4DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId) ON UPDATE CASCADE
;

create column table DiscountType
(
    sourceId DECIMAL(18) NOT NULL ,
    discountTypeId DECIMAL(18) NOT NULL ,
    taxpayerPartyId DECIMAL(18) NOT NULL ,
    discountCatId DECIMAL(18) NOT NULL ,
    discountCode NVARCHAR(20) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE DiscountType
    ADD CONSTRAINT XPKDiscountType
PRIMARY KEY (discountTypeId, sourceId);


ALTER TABLE DiscountType
    ADD CONSTRAINT f1DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId) ON UPDATE CASCADE
;

ALTER TABLE DiscountType
    ADD CONSTRAINT f2DscCat FOREIGN KEY (taxpayerPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table TaxStructure
(
    taxStructureSrcId DECIMAL(18) NOT NULL ,
    taxStructureId DECIMAL(18) NOT NULL ,
    taxStructureTypeId DECIMAL(18) NOT NULL ,
    brcktMaximumBasis DECIMAL(18,3) NULL ,
    reductAmtDedTypeId DECIMAL(18) NULL ,
    reductReasonCategoryId DECIMAL(18) NULL ,
    allAtTopTierTypInd DECIMAL(1) NOT NULL ,
    singleRate DECIMAL(12,8) NULL ,
    taxPerUnitAmount DECIMAL(18,6) NULL ,
    unitOfMeasureQty DECIMAL(18,6) NOT NULL ,
    childTaxStrucSrcId DECIMAL(18) NULL ,
    unitBasedInd DECIMAL(1) NOT NULL ,
    basisReductFactor DECIMAL(12,8) NULL ,
    brcktTaxCalcType DECIMAL(18) NULL ,
    unitOfMeasISOCode NVARCHAR(3) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    childTaxStrucId DECIMAL(18) NULL ,
    usesStdRateInd DECIMAL(1) NOT NULL ,
    flatTaxAmt DECIMAL(18,3) NULL ,
    telecomUnitConversionId DECIMAL(18) NULL ,
    telecomUnitConversionSrcId DECIMAL(18) NULL 
);

ALTER TABLE TaxStructure
    ADD CONSTRAINT XPKTaxStructure
PRIMARY KEY (taxStructureId, taxStructureSrcId);


ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TxSTyp FOREIGN KEY (taxStructureTypeId)
    REFERENCES TaxStructureType (taxStructureTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1BTCTyp FOREIGN KEY (brcktTaxCalcType)
    REFERENCES BracketTaxCalcType (brcktTaxCalcTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1DedTyp FOREIGN KEY (reductAmtDedTypeId)
    REFERENCES DeductionType (deductionTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxStructure
    ADD CONSTRAINT f1TelCon FOREIGN KEY (telecomUnitConversionId, telecomUnitConversionSrcId)
    REFERENCES TelecomUnitConversion (telecomUnitConversionId, sourceId) ON UPDATE CASCADE
;

create column table ImpositionType
(
    impsnTypeId DECIMAL(18) NOT NULL ,
    impsnTypeSrcId DECIMAL(18) NOT NULL ,
    impsnTypeName NVARCHAR(60) NOT NULL ,
    creditInd DECIMAL(1) NULL ,
    notInTotalInd DECIMAL(1) NULL ,
    withholdingTypeId DECIMAL(18) NULL 
);

ALTER TABLE ImpositionType
    ADD CONSTRAINT XPKImpositionType
PRIMARY KEY (impsnTypeId, impsnTypeSrcId);


ALTER TABLE ImpositionType
    ADD CONSTRAINT f1WitTyp FOREIGN KEY (withholdingTypeId)
    REFERENCES WithholdingType (withholdingTypeId) ON UPDATE CASCADE
;

create column table TaxImposition
(
    jurisdictionId DECIMAL(18) NOT NULL ,
    taxImpsnId DECIMAL(18) NOT NULL ,
    taxImpsnSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxImposition
    ADD CONSTRAINT XPKTaxImp
PRIMARY KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId);


create column table TaxImpsnDetail
(
    taxImpsnDtlId DECIMAL(18) NOT NULL ,
    taxImpsnSrcId DECIMAL(18) NOT NULL ,
    taxImpsnId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    impsnTypeId DECIMAL(18) NOT NULL ,
    impsnTypeSrcId DECIMAL(18) NOT NULL ,
    taxImpsnName NVARCHAR(60) NOT NULL ,
    taxImpsnAbrv NVARCHAR(15) NULL ,
    taxImpsnDesc NVARCHAR(100) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    taxTypeId DECIMAL(18) NULL ,
    taxResponsibilityRoleTypeId DECIMAL(18) NULL ,
    conditionInd DECIMAL(1) NULL ,
    primaryImpositionInd DECIMAL(1) NULL 
);

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT XPKTaxImpsnDetail
PRIMARY KEY (taxImpsnDtlId, taxImpsnSrcId);


ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1ImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f6TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f18TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxImpsnDetail
    ADD CONSTRAINT f1TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

create column table TaxImpQualCond
(
    taxImpQualCondId DECIMAL(18) NOT NULL ,
    taxImpsnSrcId DECIMAL(18) NOT NULL ,
    taxImpsnDtlId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT XPKTaxImpQualCond
PRIMARY KEY (taxImpQualCondId, taxImpsnSrcId);


ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f9TaxImp FOREIGN KEY (taxImpsnDtlId, taxImpsnSrcId)
    REFERENCES TaxImpsnDetail (taxImpsnDtlId, taxImpsnSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxImpQualCond
    ADD CONSTRAINT f12TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

create column table FilingOverride
(
    filingOverrideId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    jurTypeSetId DECIMAL(18) NOT NULL ,
    filingCategoryId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE FilingOverride
    ADD CONSTRAINT XPKFilingOverride
PRIMARY KEY (filingOverrideId);


ALTER TABLE FilingOverride
    ADD CONSTRAINT f5FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId) ON UPDATE CASCADE
;

ALTER TABLE FilingOverride
    ADD CONSTRAINT f5JrsTpS FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

create column table Tier
(
    minBasisAmount DECIMAL(18,3) NULL ,
    tierNum DECIMAL(3) NOT NULL ,
    taxResultTypeId DECIMAL(18) NULL ,
    maxBasisAmount DECIMAL(18,3) NULL ,
    minQuantity DECIMAL(18,6) NULL ,
    maxQuantity DECIMAL(18,6) NULL ,
    unitOfMeasISOCode NVARCHAR(3) NULL ,
    unitOfMeasureQty DECIMAL(18,6) NULL ,
    taxPerUnitAmount DECIMAL(18,6) NULL ,
    tierTaxRate DECIMAL(12,8) NULL ,
    taxStructureSrcId DECIMAL(18) NOT NULL ,
    taxStructureId DECIMAL(18) NOT NULL ,
    reasonCategoryId DECIMAL(18) NULL ,
    usesStdRateInd DECIMAL(1) NOT NULL ,
    filingCategoryId DECIMAL(18) NULL ,
    rateClassId DECIMAL(18) NULL ,
    taxStructureTypeId DECIMAL(18) NULL ,
    equivalentQuantity DECIMAL(18,6) NULL ,
    additionalQuantity DECIMAL(18,6) NULL ,
    equivalentAdditionalQuantity DECIMAL(18,6) NULL 
);

ALTER TABLE Tier
    ADD CONSTRAINT XPKTier
PRIMARY KEY (tierNum, taxStructureSrcId, taxStructureId);


ALTER TABLE Tier
    ADD CONSTRAINT f2TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId) ON UPDATE CASCADE
;

ALTER TABLE Tier
    ADD CONSTRAINT f5TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId) ON UPDATE CASCADE
;

ALTER TABLE Tier
    ADD CONSTRAINT f4RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

ALTER TABLE Tier
    ADD CONSTRAINT f6FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId) ON UPDATE CASCADE
;

ALTER TABLE Tier
    ADD CONSTRAINT f3RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId) ON UPDATE CASCADE
;

create column table Bracket
(
    taxStructureId DECIMAL(18) NOT NULL ,
    taxStructureSrcId DECIMAL(18) NOT NULL ,
    minBasisAmount DECIMAL(18,3) NULL ,
    bracketNum DECIMAL(3) NOT NULL ,
    maxBasisAmount DECIMAL(18,3) NULL ,
    bracketTaxAmount DECIMAL(18,3) NULL 
);

ALTER TABLE Bracket
    ADD CONSTRAINT XPKBracket
PRIMARY KEY (taxStructureId, taxStructureSrcId, bracketNum);


ALTER TABLE Bracket
    ADD CONSTRAINT f4TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId) ON UPDATE CASCADE
;

create column table TaxRule
(
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    partyRoleTypeId DECIMAL(18) NULL ,
    partyId DECIMAL(18) NULL ,
    partySourceId DECIMAL(18) NULL ,
    taxpayerRoleTypeId DECIMAL(18) NULL ,
    taxpayerPartyId DECIMAL(18) NULL ,
    taxpayerPartySrcId DECIMAL(18) NULL ,
    qualDetailDesc NVARCHAR(100) NULL ,
    uniqueToLevelInd DECIMAL(1) NOT NULL ,
    conditionSeqNum DECIMAL(18) NULL ,
    taxRuleTypeId DECIMAL(18) NULL ,
    taxResultTypeId DECIMAL(18) NULL ,
    recoverableResultTypeId DECIMAL(18) NULL ,
    defrdJurTypeId DECIMAL(18) NULL ,
    defersToStdRuleInd DECIMAL(1) NOT NULL ,
    taxStructureSrcId DECIMAL(18) NULL ,
    taxStructureId DECIMAL(18) NULL ,
    automaticRuleInd DECIMAL(1) NOT NULL ,
    standardRuleInd DECIMAL(1) NOT NULL ,
    reasonCategoryId DECIMAL(18) NULL ,
    filingCategoryId DECIMAL(18) NULL ,
    appToSingleImpInd DECIMAL(1) NULL ,
    appToSingleJurInd DECIMAL(1) NULL ,
    discountTypeId DECIMAL(18) NULL ,
    discountTypeSrcId DECIMAL(18) NULL ,
    maxTaxRuleType DECIMAL(18) NULL ,
    taxScopeId DECIMAL(18) NULL ,
    maxTaxAmount DECIMAL(18,3) NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    taxImpsnId DECIMAL(18) NOT NULL ,
    taxImpsnSrcId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    discountCatId DECIMAL(18) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    apportionTypeId DECIMAL(18) NULL ,
    appnTxbltyCatId DECIMAL(18) NULL ,
    appnTxbltyCatSrcId DECIMAL(18) NULL ,
    appnFlexFieldDefId DECIMAL(18) NULL ,
    appnFlexFieldDefSrcId DECIMAL(18) NULL ,
    rateClassId DECIMAL(18) NULL ,
    locationRoleTypeId DECIMAL(18) NULL ,
    applyFilingCatInd DECIMAL(1) DEFAULT 0 NULL ,
    recoverablePct DECIMAL(10,6) NULL ,
    taxResponsibilityRoleTypeId DECIMAL(18) NULL ,
    currencyUnitId DECIMAL(18) NULL ,
    salestaxHolidayInd DECIMAL(1) NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL ,
    includesAllTaxCats DECIMAL(1) NULL ,
    invoiceTextId DECIMAL(18) NULL ,
    invoiceTextSrcId DECIMAL(18) NULL ,
    computationTypeId DECIMAL(18) NULL ,
    defrdImpsnTypeId DECIMAL(18) NULL ,
    defrdImpsnTypeSrcId DECIMAL(18) NULL ,
    notAllowZeroRateInd DECIMAL(1) NULL ,
    accumulationAsJurisdictionId DECIMAL(18) NULL ,
    accumulationAsTaxImpsnId DECIMAL(18) NULL ,
    accumulationAsTaxImpsnSrcId DECIMAL(18) NULL ,
    accumulationTypeId DECIMAL(18) NULL ,
    periodTypeId DECIMAL(18) NULL ,
    startMonth DECIMAL(2) NULL ,
    maxLines DECIMAL(18) NULL ,
    unitOfMeasISOCode NVARCHAR(3) NULL ,
    telecomUnitConversionId DECIMAL(18) NULL ,
    telecomUnitConversionSrcId DECIMAL(18) NULL ,
    lineTypeCatId DECIMAL(18) NULL ,
    lineTypeCatSourceId DECIMAL(18) NULL 
);

ALTER TABLE TaxRule
    ADD CONSTRAINT XPKTaxRule
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxScp FOREIGN KEY (taxScopeId)
    REFERENCES TaxScope (taxScopeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TRlTyp FOREIGN KEY (taxRuleTypeId)
    REFERENCES TaxRuleType (taxRuleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1FilCat FOREIGN KEY (filingCategoryId)
    REFERENCES FilingCategory (filingCategoryId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TxRTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f3RsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1DscTyp FOREIGN KEY (discountTypeId, discountTypeSrcId)
    REFERENCES DiscountType (discountTypeId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f6TxStrc FOREIGN KEY (taxStructureId, taxStructureSrcId)
    REFERENCES TaxStructure (taxStructureId, taxStructureSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f4DscCat FOREIGN KEY (discountCatId)
    REFERENCES DiscountCategory (discountCatId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f28Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f30Party FOREIGN KEY (taxpayerPartyId, taxpayerPartySrcId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1ApprtTyp FOREIGN KEY (apportionTypeId)
    REFERENCES ApportionmentType (apportionTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RatCls FOREIGN KEY (rateClassId)
    REFERENCES RateClassification (rateClassId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f7LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2TRPTyp FOREIGN KEY (taxResponsibilityRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1InvoiceText FOREIGN KEY (invoiceTextId, invoiceTextSrcId)
    REFERENCES InvoiceText (invoiceTextId, invoiceTextSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f2ImpTyp FOREIGN KEY (defrdImpsnTypeId, defrdImpsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRule
    ADD CONSTRAINT f1RcvRTyp FOREIGN KEY (recoverableResultTypeId)
    REFERENCES RecoverableResultType (recoverableResultTypeId) ON UPDATE CASCADE
;

create column table TaxRuleNote
(
    taxRuleNoteId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleNoteSrcId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleNoteText NVARCHAR(1000) NULL 
);

ALTER TABLE TaxRuleNote
    ADD CONSTRAINT XPKTaxRuleNote
PRIMARY KEY (taxRuleNoteId, taxRuleNoteSrcId);


ALTER TABLE TaxRuleNote
    ADD CONSTRAINT f4TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table ExprConditionType
(
    exprCondTypeId DECIMAL(18) NOT NULL ,
    exprCondTypeName NVARCHAR(60) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE ExprConditionType
    ADD CONSTRAINT XPKExprCondType
PRIMARY KEY (exprCondTypeId);


create column table TaxRuleQualCond
(
    taxRuleQualCondId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    txbltyDvrId DECIMAL(18) NULL ,
    txbltyDvrSrcId DECIMAL(18) NULL ,
    txbltyCatId DECIMAL(18) NULL ,
    txbltyCatSrcId DECIMAL(18) NULL ,
    flexFieldDefId DECIMAL(18) NULL ,
    flexFieldDefSrcId DECIMAL(18) NULL ,
    minDate DECIMAL(8) NULL ,
    maxDate DECIMAL(8) NULL ,
    compareValue DECIMAL(18,3) NULL ,
    exprCondTypeId DECIMAL(18) NULL 
);

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT XPKTaxRuleQualCond
PRIMARY KEY (taxRuleQualCondId, taxRuleSourceId);


ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f8TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f4TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f7TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f1FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleQualCond
    ADD CONSTRAINT f2ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId) ON UPDATE CASCADE
;

create column table TaxRuleTaxType
(
    taxRuleTaxTypeId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxTypeId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT XPKTaxRuleTaxType
PRIMARY KEY (taxRuleTaxTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f14TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxType
    ADD CONSTRAINT f9TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table TaxRuleTaxImposition
(
    taxRuleTaxImpositionId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleTaxImpositionTypeId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NULL ,
    taxImpsnId DECIMAL(18) NULL ,
    taxImpsnSrcId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    txbltyCatId DECIMAL(18) NULL ,
    txbltyCatSrcId DECIMAL(18) NULL ,
    overTxbltyCatId DECIMAL(18) NULL ,
    overTxbltyCatSrcId DECIMAL(18) NULL ,
    underTxbltyCatId DECIMAL(18) NULL ,
    underTxbltyCatSrcId DECIMAL(18) NULL ,
    invoiceThresholdAmt DECIMAL(18,5) NULL ,
    thresholdCurrencyUnitId DECIMAL(18) NULL ,
    includeTaxableAmountInd DECIMAL(1) NULL ,
    includeTaxAmountInd DECIMAL(1) NULL ,
    rate DECIMAL(12,8) NULL ,
    locationRoleTypeId DECIMAL(18) NULL ,
    impositionTypeId DECIMAL(18) NULL ,
    impositionTypeSrcId DECIMAL(18) NULL ,
    jurTypeId DECIMAL(18) NULL ,
    taxTypeId DECIMAL(18) NULL ,
    isLeftInd DECIMAL(1) NULL ,
    groupId DECIMAL(3) NULL 
);

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT XPKTaxRuleTaxImpsn
PRIMARY KEY (taxRuleTaxImpositionId, taxRuleSourceId);


ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f7TaxImp FOREIGN KEY (jurisdictionId, taxImpsnId, taxImpsnSrcId)
    REFERENCES TaxImposition (jurisdictionId, taxImpsnId, taxImpsnSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f9TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f10TaxCat FOREIGN KEY (overTxbltyCatId, overTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f11TaxCat FOREIGN KEY (underTxbltyCatId, underTxbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f12LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTaxImposition
    ADD CONSTRAINT f13TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table MaxTaxRuleAdditionalCond
(
    maxTaxRuleCondId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL 
);

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT XPKMaxTaxRuleCond
PRIMARY KEY (maxTaxRuleCondId);


ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

ALTER TABLE MaxTaxRuleAdditionalCond
    ADD CONSTRAINT f34TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

create column table ComputationType
(
    computationTypeId DECIMAL(18) NOT NULL ,
    compTypeName NVARCHAR(60) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE ComputationType
    ADD CONSTRAINT XPKCompType
PRIMARY KEY (computationTypeId);


create column table TaxFactorType
(
    taxFactorTypeId DECIMAL(18) NOT NULL ,
    taxFactorTypeName NVARCHAR(60) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE TaxFactorType
    ADD CONSTRAINT XPKTaxFactorType
PRIMARY KEY (taxFactorTypeId);


create column table TaxFactor
(
    taxFactorId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxFactorTypeId DECIMAL(18) NOT NULL ,
    constantValue DECIMAL(18,6) NULL ,
    basisTypeId DECIMAL(18) NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL ,
    txbltyCatId DECIMAL(18) NULL ,
    txbltyCatSrcId DECIMAL(18) NULL ,
    flexFieldDefId DECIMAL(18) NULL ,
    flexFieldDefSrcId DECIMAL(18) NULL ,
    impositionTypeId DECIMAL(18) NULL ,
    impositionTypeSrcId DECIMAL(18) NULL ,
    locationRoleTypeId DECIMAL(18) NULL ,
    jurTypeId DECIMAL(18) NULL ,
    taxTypeId DECIMAL(18) NULL 
);

ALTER TABLE TaxFactor
    ADD CONSTRAINT XPKTaxFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE TaxFactor
    ADD CONSTRAINT f8TaxCat FOREIGN KEY (txbltyCatId, txbltyCatSrcId)
    REFERENCES TaxabilityCategory (txbltyCatId, txbltyCatSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f4FlxFld FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f1TxFcTp FOREIGN KEY (taxFactorTypeId)
    REFERENCES TaxFactorType (taxFactorTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f2BssTyp FOREIGN KEY (basisTypeId)
    REFERENCES BasisType (basisTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f17LRlTyp FOREIGN KEY (locationRoleTypeId)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxFactor
    ADD CONSTRAINT f19TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table ComputationFactor
(
    taxFactorId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    computationTypeId DECIMAL(18) NOT NULL ,
    leftTaxFactorId DECIMAL(18) NOT NULL ,
    rightTaxFactorId DECIMAL(18) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE ComputationFactor
    ADD CONSTRAINT XPKCompFactor
PRIMARY KEY (taxFactorId, sourceId);


ALTER TABLE ComputationFactor
    ADD CONSTRAINT f3TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f4TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f5TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE ComputationFactor
    ADD CONSTRAINT f1CmpTyp FOREIGN KEY (computationTypeId)
    REFERENCES ComputationType (computationTypeId) ON UPDATE CASCADE
;

create column table ConditionalTaxExpr
(
    condTaxExprId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    exprCondTypeId DECIMAL(18) NOT NULL ,
    leftTaxFactorId DECIMAL(18) NOT NULL ,
    rightTaxFactorId DECIMAL(18) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT XPKCondTaxExpr
PRIMARY KEY (condTaxExprId, sourceId);


ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1TxFctr FOREIGN KEY (leftTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f2TxFctr FOREIGN KEY (rightTaxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f1ExCTyp FOREIGN KEY (exprCondTypeId)
    REFERENCES ExprConditionType (exprCondTypeId) ON UPDATE CASCADE
;

ALTER TABLE ConditionalTaxExpr
    ADD CONSTRAINT f6TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table TaxBasisConclusion
(
    taxBasisConcId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxFactorId DECIMAL(18) NOT NULL ,
    taxTypeId DECIMAL(18) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT XPKTaxBasisConc
PRIMARY KEY (taxBasisConcId, sourceId);


ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f7TaxRul FOREIGN KEY (taxRuleId, sourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TxFctr FOREIGN KEY (taxFactorId, sourceId)
    REFERENCES TaxFactor (taxFactorId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxBasisConclusion
    ADD CONSTRAINT f6TaxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table TaxabilityMapping
(
    taxabilityMapId DECIMAL(18) NOT NULL ,
    taxabilityMapSrcId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    txbltyCatMapId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT XPKTaxabilityMap
PRIMARY KEY (taxabilityMapId, taxabilityMapSrcId);


ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f3TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxabilityMapping
    ADD CONSTRAINT f2TxCMap FOREIGN KEY (txbltyCatMapId, taxabilityMapSrcId)
    REFERENCES TxbltyCatMap (txbltyCatMapId, txbltyCatMapSrcId) ON UPDATE CASCADE
;

create column table LineItemTaxDtlType
(
    lineItemTxDtlTypId DECIMAL(18) NOT NULL ,
    lineItemTxDtlTypNm NVARCHAR(60) NULL 
);

ALTER TABLE LineItemTaxDtlType
    ADD CONSTRAINT XPKLinItmTaxDtlTyp
PRIMARY KEY (lineItemTxDtlTypId);


create column table FalseSitusCond
(
    prntFlsSitusCondId DECIMAL(18) NOT NULL ,
    chldFlsSitusCondId DECIMAL(18) NOT NULL 
);

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT XPKFalseSitusCond
PRIMARY KEY (prntFlsSitusCondId, chldFlsSitusCondId);


ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f4StCdNd FOREIGN KEY (chldFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

ALTER TABLE FalseSitusCond
    ADD CONSTRAINT f3StCdNd FOREIGN KEY (prntFlsSitusCondId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

create column table TaxRuleTransType
(
    taxRuleTransTypeId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    transactionTypeId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT XPKTaxRuleTranTyp
PRIMARY KEY (taxRuleTransTypeId, taxRuleSourceId);


ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRuleTransType
    ADD CONSTRAINT f1TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table DMActivityLogStrng
(
    stringTypeId DECIMAL(18) NOT NULL ,
    criteriaString NVARCHAR(255) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    activityLogId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT XPKDMActLogStrng
PRIMARY KEY (activityLogId, stringTypeId, criteriaOrderNum);


ALTER TABLE DMActivityLogStrng
    ADD CONSTRAINT f3DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId) ON UPDATE CASCADE
;

create column table DMActivityLogFile
(
    activityLogId DECIMAL(18) NOT NULL ,
    fileId DECIMAL(18) NOT NULL ,
    fileName NVARCHAR(255) NOT NULL ,
    statusNum DECIMAL(1) NOT NULL 
);

ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT XPKDMActLogFile
PRIMARY KEY (activityLogId, fileId);


ALTER TABLE DMActivityLogFile
    ADD CONSTRAINT f5DMALog FOREIGN KEY (activityLogId)
    REFERENCES DMActivityLog (activityLogId) ON UPDATE CASCADE
;

create column table TaxRuleDescription
(
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    taxRuleDescText NVARCHAR(1000) NULL 
);

ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT XPKTaxRuleDesc
PRIMARY KEY (taxRuleId, taxRuleSourceId);


ALTER TABLE TaxRuleDescription
    ADD CONSTRAINT f5TaxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table SitusCondTxbltyCat
(
    txbltyCatSrcId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    situsConditionId DECIMAL(18) NOT NULL 
);

ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT XPKSitusCondTaxCat
PRIMARY KEY (situsConditionId, txbltyCatSrcId, txbltyCatId);


ALTER TABLE SitusCondTxbltyCat
    ADD CONSTRAINT f3StsCnd FOREIGN KEY (situsConditionId)
    REFERENCES SitusCondition (situsConditionId) ON UPDATE CASCADE
;

create column table TaxRuleCondJur
(
    taxRuleCondJurId DECIMAL(18) NOT NULL ,
    taxRuleSourceId DECIMAL(18) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT XPKTaxRuleCondJur
PRIMARY KEY (taxRuleCondJurId, taxRuleSourceId);


ALTER TABLE TaxRuleCondJur
    ADD CONSTRAINT f10TxRul FOREIGN KEY (taxRuleId, taxRuleSourceId)
    REFERENCES TaxRule (taxRuleId, taxRuleSourceId) ON UPDATE CASCADE
;

create column table DMFilterNum
(
    numberTypeId DECIMAL(18) NOT NULL ,
    criteriaNum DECIMAL(18) NULL ,
    criteriaOrderNum DECIMAL(18) NOT NULL ,
    filterId DECIMAL(18) NOT NULL 
);

ALTER TABLE DMFilterNum
    ADD CONSTRAINT XPKDMFilterNum
PRIMARY KEY (filterId, numberTypeId, criteriaOrderNum);


ALTER TABLE DMFilterNum
    ADD CONSTRAINT f2DMFilt FOREIGN KEY (filterId)
    REFERENCES DMFilter (filterId) ON UPDATE CASCADE
;

create column table SitusTreatment
(
    situsTreatmentId DECIMAL(18) NOT NULL ,
    situsCondNodeId DECIMAL(18) NULL ,
    situsTreatmentName NVARCHAR(60) NOT NULL ,
    situsTreatmentDesc NVARCHAR(1000) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE SitusTreatment
    ADD CONSTRAINT XPKSitusTreatment
PRIMARY KEY (situsTreatmentId);


ALTER TABLE SitusTreatment
    ADD CONSTRAINT f7StCdNd FOREIGN KEY (situsCondNodeId)
    REFERENCES SitusConditionNode (situsCondNodeId) ON UPDATE CASCADE
;

create column table StsTrtmntVtxPrdTyp
(
    situsTreatmentId DECIMAL(18) NOT NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT XPKStsTmtVtxPrdTyp
PRIMARY KEY (situsTreatmentId, vertexProductId);


ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f1Trmt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId) ON UPDATE CASCADE
;

ALTER TABLE StsTrtmntVtxPrdTyp
    ADD CONSTRAINT f2VxtPrd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table TransTypePrspctv
(
    transTypePrspctvId DECIMAL(18) NOT NULL ,
    transactionTypeId DECIMAL(18) NULL ,
    partyRoleTypeId DECIMAL(18) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL ,
    name NVARCHAR(60) NOT NULL 
);

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT XPKTransTypPrspctv
PRIMARY KEY (transTypePrspctvId);


ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f5PRlTyp FOREIGN KEY (partyRoleTypeId)
    REFERENCES PartyRoleType (partyRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE TransTypePrspctv
    ADD CONSTRAINT f4TrnTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId) ON UPDATE CASCADE
;

create column table SitusTreatmentRule
(
    situsTrtmntRuleId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    situsTreatmentId DECIMAL(18) NOT NULL ,
    txbltyCatSrcId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL ,
    jurisdiction1Id DECIMAL(18) NULL ,
    jurisdiction2Id DECIMAL(18) NULL ,
    locationRoleTyp1Id DECIMAL(18) NULL ,
    locationRoleTyp2Id DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT XPKSitusTrtmntRule
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f6LRlTyp FOREIGN KEY (locationRoleTyp2Id)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f5LRlTyp FOREIGN KEY (locationRoleTyp1Id)
    REFERENCES LocationRoleType (locationRoleTypeId) ON UPDATE CASCADE
;

ALTER TABLE SitusTreatmentRule
    ADD CONSTRAINT f1SitTrt FOREIGN KEY (situsTreatmentId)
    REFERENCES SitusTreatment (situsTreatmentId) ON UPDATE CASCADE
;

create column table SitusTrtmntRulNote
(
    situsTrtmntRuleId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    noteText NVARCHAR(1000) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT XPKSitusTrtmntRlNt
PRIMARY KEY (situsTrtmntRuleId, sourceId);


ALTER TABLE SitusTrtmntRulNote
    ADD CONSTRAINT f1SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId) ON UPDATE CASCADE
;

create column table SitusTrtmntPrspctv
(
    situsTrtmntRuleId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    transTypePrspctvId DECIMAL(18) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT XPKSitusTrtmntPrsp
PRIMARY KEY (situsTrtmntRuleId, sourceId, transTypePrspctvId);


ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f2SitTrR FOREIGN KEY (situsTrtmntRuleId, sourceId)
    REFERENCES SitusTreatmentRule (situsTrtmntRuleId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE SitusTrtmntPrspctv
    ADD CONSTRAINT f1TrTypP FOREIGN KEY (transTypePrspctvId)
    REFERENCES TransTypePrspctv (transTypePrspctvId) ON UPDATE CASCADE
;

create column table CurrencyRndRule
(
    currencyRndRuleId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    currencyUnitId DECIMAL(18) NOT NULL ,
    roundingRuleId DECIMAL(18) NOT NULL ,
    taxTypeId DECIMAL(18) NULL ,
    jurisdictionId DECIMAL(18) NULL ,
    partyId DECIMAL(18) NULL ,
    configurableInd DECIMAL(1) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(8) NULL ,
    lastUpdateDate DECIMAL(8) NULL 
);

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT XPKCurrRndRule
PRIMARY KEY (currencyRndRuleId, sourceId);


ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f22Party FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE CurrencyRndRule
    ADD CONSTRAINT f1RndRul FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId) ON UPDATE CASCADE
;

create column table AllowCurrRndRule
(
    allowCurrRndRuleId DECIMAL(18) NOT NULL ,
    currencyUnitId DECIMAL(18) NOT NULL ,
    roundingRuleId DECIMAL(18) NOT NULL ,
    taxTypeId DECIMAL(18) NULL ,
    jurisdictionId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT XPKAllowCurRndRule
PRIMARY KEY (allowCurrRndRuleId);


ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f2RndRule FOREIGN KEY (roundingRuleId)
    REFERENCES RoundingRule (roundingRuleId) ON UPDATE CASCADE
;

ALTER TABLE AllowCurrRndRule
    ADD CONSTRAINT f10TxTyp FOREIGN KEY (taxTypeId)
    REFERENCES TaxType (taxTypeId) ON UPDATE CASCADE
;

create column table TaxRecoverablePct
(
    taxRecovPctId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    partySourceId DECIMAL(18) NOT NULL ,
    costCenter NVARCHAR(40) NULL ,
    recovPct DECIMAL(10,6) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    accrualReliefInd DECIMAL(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT XPKTaxRecovPct
PRIMARY KEY (taxRecovPctId);


ALTER TABLE TaxRecoverablePct
    ADD CONSTRAINT f27Party FOREIGN KEY (partyId, partySourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table TaxAssistRule
(
    sourceId DECIMAL(18) NOT NULL ,
    ruleId DECIMAL(18) NOT NULL ,
    ruleCode NVARCHAR(60) NOT NULL ,
    ruleDesc NVARCHAR(1000) NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    precedence DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    phaseId DECIMAL(1) DEFAULT 0 NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxAssistRule
    ADD CONSTRAINT XPKTaxAssistRule
PRIMARY KEY (ruleId, sourceId);


ALTER TABLE TaxAssistRule
    ADD CONSTRAINT f2VtxProduct FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table TaxAssistCondition
(
    sourceId DECIMAL(18) NOT NULL ,
    ruleId DECIMAL(18) NOT NULL ,
    conditionId DECIMAL(18) NOT NULL ,
    conditionText NVARCHAR(2000) NULL ,
    conditionText2 NVARCHAR(2000) NULL 
);

ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT XPKTxAstCondition
PRIMARY KEY (ruleId, sourceId, conditionId);


ALTER TABLE TaxAssistCondition
    ADD CONSTRAINT f1TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId) ON UPDATE CASCADE
;

create column table TaxAssistConclude
(
    sourceId DECIMAL(18) NOT NULL ,
    ruleId DECIMAL(18) NOT NULL ,
    conclusionId DECIMAL(18) NOT NULL ,
    conclusionText NVARCHAR(2000) NULL ,
    conclusionText2 NVARCHAR(2000) NULL 
);

ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT XPKTxAstConclude
PRIMARY KEY (ruleId, sourceId, conclusionId);


ALTER TABLE TaxAssistConclude
    ADD CONSTRAINT f2TxAsRl FOREIGN KEY (ruleId, sourceId)
    REFERENCES TaxAssistRule (ruleId, sourceId) ON UPDATE CASCADE
;

create column table TaxAssistLookup
(
    sourceId DECIMAL(18) NOT NULL ,
    tableId DECIMAL(18) NOT NULL ,
    tableName NVARCHAR(60) NOT NULL ,
    dataType DECIMAL(18) NOT NULL ,
    description NVARCHAR(1000) NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    resultName NVARCHAR(60) NOT NULL ,
    param1Name NVARCHAR(60) NOT NULL ,
    param2Name NVARCHAR(60) NULL ,
    param3Name NVARCHAR(60) NULL ,
    param4Name NVARCHAR(60) NULL ,
    param5Name NVARCHAR(60) NULL ,
    param6Name NVARCHAR(60) NULL ,
    param7Name NVARCHAR(60) NULL ,
    param8Name NVARCHAR(60) NULL ,
    param9Name NVARCHAR(60) NULL ,
    param10Name NVARCHAR(60) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT XPKTxAstLookup
PRIMARY KEY (tableId, sourceId);


ALTER TABLE TaxAssistLookup
    ADD CONSTRAINT f1TxAstLookup FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table TaxAssistLookupRcd
(
    sourceId DECIMAL(18) NOT NULL ,
    recordId DECIMAL(18) NOT NULL ,
    tableId DECIMAL(18) NOT NULL ,
    result NVARCHAR(200) NULL ,
    param1 NVARCHAR(200) NULL ,
    param2 NVARCHAR(200) NULL ,
    param3 NVARCHAR(200) NULL ,
    param4 NVARCHAR(200) NULL ,
    param5 NVARCHAR(200) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT XPKTxAstLookupRcd
PRIMARY KEY (recordId, sourceId);


ALTER TABLE TaxAssistLookupRcd
    ADD CONSTRAINT f1TxAsLk FOREIGN KEY (tableId, sourceId)
    REFERENCES TaxAssistLookup (tableId, sourceId) ON UPDATE CASCADE
;

create column table NumericType
(
    numericTypeId DECIMAL(18) NOT NULL ,
    numericTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE NumericType
    ADD CONSTRAINT XPKNumericType
PRIMARY KEY (numericTypeId);


create column table TaxAssistAllocationTable
(
    sourceId DECIMAL(18) NOT NULL ,
    allocationTableId DECIMAL(18) NOT NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    allocationTableName NVARCHAR(60) NOT NULL ,
    allocationTableDesc NVARCHAR(1000) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT XPKTxAstAlocTbl
PRIMARY KEY (allocationTableId, sourceId);


ALTER TABLE TaxAssistAllocationTable
    ADD CONSTRAINT f1TxAstAlloc FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table TaxAssistAllocationColumn
(
    sourceId DECIMAL(18) NOT NULL ,
    allocationTableId DECIMAL(18) NOT NULL ,
    columnId DECIMAL(18) NOT NULL ,
    columnName NVARCHAR(60) NOT NULL ,
    dataTypeId DECIMAL(18) NOT NULL ,
    columnSeqNum DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT XPKTxAstAllocCol
PRIMARY KEY (allocationTableId, sourceId, columnId);


ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsAC FOREIGN KEY (allocationTableId, sourceId)
    REFERENCES TaxAssistAllocationTable (allocationTableId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxAssistAllocationColumn
    ADD CONSTRAINT f1TxAsACDT FOREIGN KEY (dataTypeId)
    REFERENCES DataType (dataTypeId) ON UPDATE CASCADE
;

create column table TaxAssistAllocationColumnValue
(
    sourceId DECIMAL(18) NOT NULL ,
    allocationTableId DECIMAL(18) NOT NULL ,
    columnId DECIMAL(18) NOT NULL ,
    recordId DECIMAL(18) NOT NULL ,
    recordCode NVARCHAR(20) NOT NULL ,
    numericTypeId DECIMAL(18) NULL ,
    columnValue NVARCHAR(60) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT XPKTxAstAllocCoVal
PRIMARY KEY (allocationTableId, sourceId, columnId, recordId, recordCode);


ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACV FOREIGN KEY (allocationTableId, sourceId, columnId)
    REFERENCES TaxAssistAllocationColumn (allocationTableId, sourceId, columnId) ON UPDATE CASCADE
;

ALTER TABLE TaxAssistAllocationColumnValue
    ADD CONSTRAINT f1TxAsACVNT FOREIGN KEY (numericTypeId)
    REFERENCES NumericType (numericTypeId) ON UPDATE CASCADE
;

create column table TpsJurisdiction
(
    jurId DECIMAL(18) NOT NULL ,
    jurVersionId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    jurLayerId DECIMAL(18) NOT NULL ,
    jurTypeId DECIMAL(18) NOT NULL ,
    jurTypeName NVARCHAR(60) NOT NULL ,
    name NVARCHAR(60) NOT NULL ,
    standardName NVARCHAR(60) NULL ,
    description NVARCHAR(80) NULL ,
    updateId DECIMAL(18) NOT NULL 
);

ALTER TABLE TpsJurisdiction
    ADD CONSTRAINT XPKTpsJurisdiction
PRIMARY KEY (jurId, jurVersionId);


create column table TaxAreaJurNames
(
    taxAreaId DECIMAL(18) NOT NULL ,
    countryName NVARCHAR(60) NOT NULL ,
    countryISOCode2 NVARCHAR(2) NULL ,
    countryISOCode3 NVARCHAR(3) NOT NULL ,
    mainDivJurTypeName NVARCHAR(60) NULL ,
    mainDivName NVARCHAR(60) NULL ,
    subDivJurTypeName NVARCHAR(60) NULL ,
    subDivName NVARCHAR(60) NULL ,
    cityJurTypeName NVARCHAR(60) NULL ,
    cityName NVARCHAR(60) NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE TaxAreaJurNames
    ADD CONSTRAINT XPKTaxAreaJurNames
PRIMARY KEY (taxAreaId);


create column table JurHierarchy
(
    depthFromParent DECIMAL(18) NOT NULL ,
    topmostInd DECIMAL(1) NOT NULL ,
    chldJurisdictionId DECIMAL(18) NOT NULL ,
    prntJurisdictionId DECIMAL(18) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE JurHierarchy
    ADD CONSTRAINT XPKJurHierarchy
PRIMARY KEY (prntJurisdictionId, chldJurisdictionId);


create column table SimplificationType
(
    simpTypeId DECIMAL(18) NOT NULL ,
    simpTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE SimplificationType
    ADD CONSTRAINT XPKSimpType
PRIMARY KEY (simpTypeId);


create column table DMActivityType
(
    activityTypeId DECIMAL(18) NOT NULL ,
    activityTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE DMActivityType
    ADD CONSTRAINT XPKDMActivityType
PRIMARY KEY (activityTypeId);


create column table DMStatusType
(
    statusTypeId DECIMAL(18) NOT NULL ,
    statusTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE DMStatusType
    ADD CONSTRAINT XPKDMStatusType
PRIMARY KEY (statusTypeId);


create column table InputOutputType
(
    inputOutputTypeId DECIMAL(18) NOT NULL ,
    inputOutputTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE InputOutputType
    ADD CONSTRAINT XPKInpOutType
PRIMARY KEY (inputOutputTypeId);


create column table CertificateReasonType
(
    certificateReasonTypeId DECIMAL(18) NOT NULL ,
    certificateReasonTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CertificateReasonType
    ADD CONSTRAINT XPKCertRsnType
PRIMARY KEY (certificateReasonTypeId);


create column table CertReasonTypeJurisdiction
(
    certReasonTypeJurId DECIMAL(18) NOT NULL ,
    certificateReasonTypeId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    reasonCategoryId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT XPKCertRsnTypeJur
PRIMARY KEY (certReasonTypeJurId);


ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnCat FOREIGN KEY (reasonCategoryId)
    REFERENCES ReasonCategory (reasonCategoryId) ON UPDATE CASCADE
;

ALTER TABLE CertReasonTypeJurisdiction
    ADD CONSTRAINT f1CrtRsnTypRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId) ON UPDATE CASCADE
;

create column table CertificateExemptionType
(
    certificateExemptionTypeId DECIMAL(18) NOT NULL ,
    certificateExemptionTypeName NVARCHAR(60) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateExemptionType
    ADD CONSTRAINT XPKCertExemType
PRIMARY KEY (certificateExemptionTypeId);


create column table CertExemptionTypeJurImp
(
    certExemptionTypeJurImpId DECIMAL(18) NOT NULL ,
    certificateExemptionTypeId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    impsnTypeId DECIMAL(18) NOT NULL ,
    impsnTypeSrcId DECIMAL(18) NOT NULL ,
    allStatesInd DECIMAL(1) NULL ,
    allCitiesInd DECIMAL(1) NULL ,
    allCountiesInd DECIMAL(1) NULL ,
    allOthersInd DECIMAL(1) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT XPKCertExmTypeJur
PRIMARY KEY (certExemptionTypeJurImpId);


ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1ImpCrtExmTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId) ON UPDATE CASCADE
;

ALTER TABLE CertExemptionTypeJurImp
    ADD CONSTRAINT f1CrtExmTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId) ON UPDATE CASCADE
;

create column table Form
(
    formId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL 
);

ALTER TABLE Form
    ADD CONSTRAINT XPKForm
PRIMARY KEY (formId, sourceId);


create column table FormDetail
(
    formDetailId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    formId DECIMAL(18) NOT NULL ,
    formIdCode NVARCHAR(30) NOT NULL ,
    formName NVARCHAR(60) NULL ,
    formDesc NVARCHAR(255) NULL ,
    formImageFileName NVARCHAR(255) NULL ,
    replacedByFormId DECIMAL(18) NULL ,
    replacementDate DECIMAL(8) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL 
);

ALTER TABLE FormDetail
    ADD CONSTRAINT XPKFormDtl
PRIMARY KEY (formDetailId, sourceId);


ALTER TABLE FormDetail
    ADD CONSTRAINT f1FormDtlForm FOREIGN KEY (replacedByFormId, sourceId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE FormDetail
    ADD CONSTRAINT f2FormDtlForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

create column table FormJurisdiction
(
    formJurisdictionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    formId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE FormJurisdiction
    ADD CONSTRAINT XPKFormJur
PRIMARY KEY (formJurisdictionId, sourceId);


ALTER TABLE FormJurisdiction
    ADD CONSTRAINT f1FormJurForm FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

create column table FormFieldDef
(
    fieldDefId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NOT NULL ,
    formId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NULL ,
    formFieldTypeId DECIMAL(18) NOT NULL ,
    requiredInd DECIMAL(1) DEFAULT 0 NULL ,
    hiddenInd DECIMAL(1) DEFAULT 0 NULL ,
    attributeId DECIMAL(18) NULL ,
    fieldDefPageNum DECIMAL(18) NOT NULL ,
    groupName NVARCHAR(60) NULL ,
    parentFieldDefId DECIMAL(18) NULL ,
    parentFieldDefSelectInd DECIMAL(1) DEFAULT 1 NULL ,
    parentValue NVARCHAR(20) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE FormFieldDef
    ADD CONSTRAINT XPKFormFieldDef
PRIMARY KEY (fieldDefId, sourceId);


ALTER TABLE FormFieldDef
    ADD CONSTRAINT f1FormFieldDef FOREIGN KEY (formId, sourceId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

create column table FormFieldTypeValue
(
    fieldDefId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    validValue NVARCHAR(20) NULL ,
    trueValue NVARCHAR(20) NULL ,
    falseValue NVARCHAR(20) NULL ,
    isDefault DECIMAL(1) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE FormFieldTypeValue
    ADD CONSTRAINT XPKFormFldTypeVle
PRIMARY KEY (fieldDefId, sourceId);


create column table FormFieldAttribute
(
    attributeId DECIMAL(18) NOT NULL ,
    name NVARCHAR(60) NOT NULL 
);

ALTER TABLE FormFieldAttribute
    ADD CONSTRAINT XPKFormFieldAttr
PRIMARY KEY (attributeId);


create column table Certificate
(
    certificateId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateCreationDate DECIMAL(8) NOT NULL 
);

ALTER TABLE Certificate
    ADD CONSTRAINT XPKCertificate
PRIMARY KEY (certificateId, sourceId);


create column table CertificateDetail
(
    certificateDetailId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    creationSourceId DECIMAL(18) DEFAULT 1 NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    certificateHolderPartyId DECIMAL(18) NOT NULL ,
    certClassTypeId DECIMAL(18) NOT NULL ,
    formId DECIMAL(18) NULL ,
    formSourceId DECIMAL(18) NULL ,
    taxResultTypeId DECIMAL(18) NULL ,
    certificateStatusId DECIMAL(18) NOT NULL ,
    partyContactId DECIMAL(18) NULL ,
    txbltyCatId DECIMAL(18) NULL ,
    txbltyCatSrcId DECIMAL(18) NULL ,
    certCopyRcvdInd DECIMAL(1) NULL ,
    industryTypeName NVARCHAR(60) NULL ,
    hardCopyLocationName NVARCHAR(255) NULL ,
    certBlanketInd DECIMAL(1) NULL ,
    singleUseInd DECIMAL(1) NULL ,
    invoiceNumber NVARCHAR(50) NULL ,
    replacedByCertificateId DECIMAL(18) NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    completedInd DECIMAL(1) DEFAULT 1 NOT NULL ,
    approvalStatusId DECIMAL(18) DEFAULT 3 NOT NULL ,
    uuid NVARCHAR(36) NULL ,
    ecwCertificateId DECIMAL(18) NULL ,
    ecwSyncId DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    createDate DECIMAL(18) NOT NULL ,
    lastUpdateDate DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE CertificateDetail
    ADD CONSTRAINT XPKCertDtl
PRIMARY KEY (certificateDetailId, sourceId);


ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1Certificate FOREIGN KEY (replacedByCertificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtParty FOREIGN KEY (certificateHolderPartyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtClsTyp FOREIGN KEY (certClassTypeId)
    REFERENCES CertClassType (certClassTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtTxRsltTyp FOREIGN KEY (taxResultTypeId)
    REFERENCES TaxResultType (taxResultTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtPtyCntct FOREIGN KEY (partyContactId, sourceId)
    REFERENCES PartyContact (partyContactId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f5VtxProd FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

ALTER TABLE CertificateDetail
    ADD CONSTRAINT f1CtDtForm FOREIGN KEY (formId, formSourceId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

create column table CertificateCoverage
(
    certificateCoverageId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    certificateIdCode NVARCHAR(30) NULL ,
    validationTypeId DECIMAL(18) NULL ,
    validationDate DECIMAL(8) NULL ,
    certificateReasonTypeId DECIMAL(18) NULL ,
    certificateExemptionTypeId DECIMAL(18) NULL ,
    certIssueDate DECIMAL(8) NULL ,
    certExpDate DECIMAL(8) NULL ,
    certReviewDate DECIMAL(8) NULL ,
    allStatesInd DECIMAL(1) NOT NULL ,
    allCitiesInd DECIMAL(1) NOT NULL ,
    allCountiesInd DECIMAL(1) NOT NULL ,
    allOthersInd DECIMAL(1) NOT NULL ,
    allImpositionsInd DECIMAL(1) DEFAULT 1 NULL ,
    jurActiveInd DECIMAL(1) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT XPKCertCvrg
PRIMARY KEY (certificateCoverageId, sourceId);


ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateCoverage
    ADD CONSTRAINT f1CrtCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId) ON UPDATE CASCADE
;

create column table CertificateJurisdiction
(
    certificateJurisdictionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateCoverageId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    certificateExemptionTypeId DECIMAL(18) NULL ,
    coverageActionTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT XPKCertJur
PRIMARY KEY (certificateJurisdictionId, sourceId);


ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f2CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateJurisdiction
    ADD CONSTRAINT f1CrtJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId) ON UPDATE CASCADE
;

create column table CertificateImposition
(
    certificateImpositionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateCoverageId DECIMAL(18) NOT NULL ,
    jurTypeSetId DECIMAL(18) NOT NULL ,
    impsnTypeId DECIMAL(18) NOT NULL ,
    impsnTypeSrcId DECIMAL(18) NOT NULL ,
    certificateExemptionTypeId DECIMAL(18) NULL ,
    impActiveInd DECIMAL(1) NOT NULL 
);

ALTER TABLE CertificateImposition
    ADD CONSTRAINT XPKCertImp
PRIMARY KEY (certificateImpositionId, sourceId);


ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpCvrg FOREIGN KEY (certificateCoverageId, sourceId)
    REFERENCES CertificateCoverage (certificateCoverageId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f1CrtImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId) ON UPDATE CASCADE
;

ALTER TABLE CertificateImposition
    ADD CONSTRAINT f3CrtExptionTyp FOREIGN KEY (certificateExemptionTypeId)
    REFERENCES CertificateExemptionType (certificateExemptionTypeId) ON UPDATE CASCADE
;

create column table CertImpositionJurisdiction
(
    certImpJurisdictionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateImpositionId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    coverageActionTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT XPKCertImpJur
PRIMARY KEY (certImpJurisdictionId, sourceId);


ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurImp FOREIGN KEY (certificateImpositionId, sourceId)
    REFERENCES CertificateImposition (certificateImpositionId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertImpositionJurisdiction
    ADD CONSTRAINT f1CrtImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId) ON UPDATE CASCADE
;

create column table CertificateNote
(
    certNoteText NVARCHAR(1000) NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL 
);

ALTER TABLE CertificateNote
    ADD CONSTRAINT XPKCertNote
PRIMARY KEY (certificateId, sourceId);


ALTER TABLE CertificateNote
    ADD CONSTRAINT f2Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

create column table CertificateImage
(
    certificateImageId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    creationSourceId DECIMAL(18) DEFAULT 1 NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    imageLocationName NVARCHAR(255) NOT NULL 
);

ALTER TABLE CertificateImage
    ADD CONSTRAINT XPKCertImage
PRIMARY KEY (certificateImageId, sourceId);


ALTER TABLE CertificateImage
    ADD CONSTRAINT f5Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

create column table CertTxbltyDriver
(
    certTxbltyDriverId DECIMAL(18) NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    txbltyDvrId DECIMAL(18) NOT NULL ,
    txbltyDvrSrcId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT XPKCertTxbltyDvr
PRIMARY KEY (certTxbltyDriverId);


ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4Certif FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertTxbltyDriver
    ADD CONSTRAINT f4TaxDvr FOREIGN KEY (txbltyDvrId, txbltyDvrSrcId)
    REFERENCES TaxabilityDriver (txbltyDvrId, txbltyDvrSrcId) ON UPDATE CASCADE
;

create column table CertificateTransType
(
    certTransTypeId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    transactionTypeId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateTransType
    ADD CONSTRAINT XPKCertTransType
PRIMARY KEY (certTransTypeId, sourceId);


ALTER TABLE CertificateTransType
    ADD CONSTRAINT f1CertTransTypCrt FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE CertificateTransType
    ADD CONSTRAINT f2CertTransTypTyp FOREIGN KEY (transactionTypeId)
    REFERENCES TransactionType (transactionTypeId) ON UPDATE CASCADE
;

create column table CertificateFormField
(
    certificateFormFieldId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateId DECIMAL(18) NOT NULL ,
    formId DECIMAL(18) NOT NULL ,
    formSourceId DECIMAL(18) NOT NULL ,
    fieldDefId DECIMAL(18) NOT NULL ,
    value NVARCHAR(500) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateFormField
    ADD CONSTRAINT XPKCertFormField
PRIMARY KEY (certificateFormFieldId, sourceId);


create column table ExpirationRuleType
(
    expirationRuleTypeId DECIMAL(18) NOT NULL ,
    expirationRuleTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE ExpirationRuleType
    ADD CONSTRAINT XPKExpRuleType
PRIMARY KEY (expirationRuleTypeId);


create column table CertificateNumReqType
(
    certificateNumReqTypeId DECIMAL(18) NOT NULL ,
    certificateNumReqTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE CertificateNumReqType
    ADD CONSTRAINT XPKCertNumReqTyp
PRIMARY KEY (certificateNumReqTypeId);


create column table CertificateNumberFormat
(
    certificateNumberFormatId DECIMAL(18) NOT NULL ,
    certificateNumberMask NVARCHAR(30) NOT NULL ,
    certificateNumberExample NVARCHAR(30) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateNumberFormat
    ADD CONSTRAINT XPKCertNumFmt
PRIMARY KEY (certificateNumberFormatId);


create column table CertificateValidationRule
(
    certificateValidationRuleId DECIMAL(18) NOT NULL ,
    certificateNumValidationDesc NVARCHAR(100) NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    certificateReasonTypeId DECIMAL(18) NOT NULL ,
    certificateNumReqTypeId DECIMAL(18) NOT NULL ,
    expirationRuleTypeId DECIMAL(18) NOT NULL ,
    numberOfYears DECIMAL(18) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT XPKCertValRule
PRIMARY KEY (certificateValidationRuleId);


ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleRsnTyp FOREIGN KEY (certificateReasonTypeId)
    REFERENCES CertificateReasonType (certificateReasonTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleExpTyp FOREIGN KEY (expirationRuleTypeId)
    REFERENCES ExpirationRuleType (expirationRuleTypeId) ON UPDATE CASCADE
;

ALTER TABLE CertificateValidationRule
    ADD CONSTRAINT f1CrtVRuleReqTyp FOREIGN KEY (certificateNumReqTypeId)
    REFERENCES CertificateNumReqType (certificateNumReqTypeId) ON UPDATE CASCADE
;

create column table CertificateNumFormatValidation
(
    certNumFormatValidationId DECIMAL(18) NOT NULL ,
    certificateValidationRuleId DECIMAL(18) NOT NULL ,
    certificateNumberFormatId DECIMAL(18) NOT NULL 
);

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT XPKCertNumFmtVal
PRIMARY KEY (certNumFormatValidationId);


ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValRule FOREIGN KEY (certificateValidationRuleId)
    REFERENCES CertificateValidationRule (certificateValidationRuleId) ON UPDATE CASCADE
;

ALTER TABLE CertificateNumFormatValidation
    ADD CONSTRAINT f1CrtNFmtValNFmt FOREIGN KEY (certificateNumberFormatId)
    REFERENCES CertificateNumberFormat (certificateNumberFormatId) ON UPDATE CASCADE
;

create column table TaxAuthority
(
    taxAuthorityId DECIMAL(18) NOT NULL ,
    taxAuthorityName NVARCHAR(60) NOT NULL ,
    phoneNumber NVARCHAR(20) NULL ,
    webSiteAddress NVARCHAR(100) NULL 
);

ALTER TABLE TaxAuthority
    ADD CONSTRAINT XPKTaxAuth
PRIMARY KEY (taxAuthorityId);


create column table TaxAuthorityJurisdiction
(
    taxAuthorityJurisdictionId DECIMAL(18) NOT NULL ,
    taxAuthorityId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT XPKTaxAuthJur
PRIMARY KEY (taxAuthorityJurisdictionId);


ALTER TABLE TaxAuthorityJurisdiction
    ADD CONSTRAINT f1TaxAuthJur FOREIGN KEY (taxAuthorityId)
    REFERENCES TaxAuthority (taxAuthorityId) ON UPDATE CASCADE
;

create column table TaxRegistration
(
    taxRegistrationId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR(40) NULL ,
    taxRegistrationTypeId DECIMAL(18) NULL ,
    validationTypeId DECIMAL(18) NULL ,
    validationDate DECIMAL(8) NULL ,
    formatValidationTypeId DECIMAL(18) NULL ,
    formatValidationDate DECIMAL(8) NULL ,
    physicalPresInd DECIMAL(1) NOT NULL ,
    allStatesInd DECIMAL(1) NOT NULL ,
    allCitiesInd DECIMAL(1) NOT NULL ,
    allCountiesInd DECIMAL(1) NOT NULL ,
    allOthersInd DECIMAL(1) NOT NULL ,
    allImpositionsInd DECIMAL(1) DEFAULT 1 NULL ,
    jurActiveInd DECIMAL(1) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    filingCurrencyUnitId DECIMAL(18) NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE TaxRegistration
    ADD CONSTRAINT XPKTaxReg
PRIMARY KEY (taxRegistrationId, sourceId);


ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgTxpyr FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistration
    ADD CONSTRAINT f1TxRgCvrgVldTyp FOREIGN KEY (validationTypeId)
    REFERENCES ValidationType (validationTypeId) ON UPDATE CASCADE
;

create column table TaxRegistrationJurisdiction
(
    taxRegJurisdictionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxRegistrationId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    coverageActionTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT XPKTxRgJur
PRIMARY KEY (taxRegJurisdictionId, sourceId);


ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistrationJurisdiction
    ADD CONSTRAINT f1TxRgJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId) ON UPDATE CASCADE
;

create column table TaxRegistrationImposition
(
    taxRegImpositionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxRegistrationId DECIMAL(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR(40) NULL ,
    taxRegistrationTypeId DECIMAL(18) NULL ,
    jurTypeSetId DECIMAL(18) NOT NULL ,
    impsnTypeId DECIMAL(18) NOT NULL ,
    impsnTypeSrcId DECIMAL(18) NOT NULL ,
    impActiveInd DECIMAL(1) NOT NULL ,
    reverseChargeInd DECIMAL(1) DEFAULT 0 NOT NULL ,
    formatValidationTypeId DECIMAL(18) NULL ,
    formatValidationDate DECIMAL(8) NULL 
);

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT XPKTxRgImp
PRIMARY KEY (taxRegImpositionId, sourceId);


ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpCvrg FOREIGN KEY (taxRegistrationId, sourceId)
    REFERENCES TaxRegistration (taxRegistrationId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpJurTypSet FOREIGN KEY (jurTypeSetId)
    REFERENCES JurTypeSet (jurTypeSetId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpImpTyp FOREIGN KEY (impsnTypeId, impsnTypeSrcId)
    REFERENCES ImpositionType (impsnTypeId, impsnTypeSrcId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegistrationImposition
    ADD CONSTRAINT f1TxRgImpTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId) ON UPDATE CASCADE
;

create column table TaxRegImpJurisdiction
(
    taxRegImpJurisdictionId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    taxRegImpositionId DECIMAL(18) NOT NULL ,
    taxRegistrationIdCode NVARCHAR(40) NULL ,
    taxRegistrationTypeId DECIMAL(18) NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    coverageActionTypeId DECIMAL(18) NOT NULL ,
    reverseChargeInd DECIMAL(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT XPKTxRgImpJur
PRIMARY KEY (taxRegImpJurisdictionId, sourceId);


ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurImp FOREIGN KEY (taxRegImpositionId, sourceId)
    REFERENCES TaxRegistrationImposition (taxRegImpositionId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurAction FOREIGN KEY (coverageActionTypeId)
    REFERENCES CoverageActionType (coverageActionTypeId) ON UPDATE CASCADE
;

ALTER TABLE TaxRegImpJurisdiction
    ADD CONSTRAINT f1TxRgImpJurTyp FOREIGN KEY (taxRegistrationTypeId)
    REFERENCES TaxRegistrationType (taxRegistrationTypeId) ON UPDATE CASCADE
;

create column table LetterTemplateType
(
    letterTemplateTypeId DECIMAL(18) NOT NULL ,
    letterTemplateTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE LetterTemplateType
    ADD CONSTRAINT XPKLtrTempType
PRIMARY KEY (letterTemplateTypeId);


create column table LetterDeliveryMethod
(
    letterDeliveryMethodId DECIMAL(18) NOT NULL ,
    letterDeliveryMethodName NVARCHAR(60) NOT NULL 
);

ALTER TABLE LetterDeliveryMethod
    ADD CONSTRAINT XPKLtrDlvryMthd
PRIMARY KEY (letterDeliveryMethodId);


create column table LetterTemplate
(
    letterTemplateId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    letterTemplateTypeId DECIMAL(18) NOT NULL ,
    letterTemplateName NVARCHAR(60) NOT NULL ,
    letterTemplateDesc NVARCHAR(100) NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE LetterTemplate
    ADD CONSTRAINT XPKLtrTemp
PRIMARY KEY (letterTemplateId, sourceId);


ALTER TABLE LetterTemplate
    ADD CONSTRAINT f1LtrTmpType FOREIGN KEY (letterTemplateTypeId)
    REFERENCES LetterTemplateType (letterTemplateTypeId) ON UPDATE CASCADE
;

create column table LetterTemplateText
(
    letterTemplateId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    templateTextSeqNum DECIMAL(18) NOT NULL ,
    templateText NVARCHAR(2000) NULL 
);

ALTER TABLE LetterTemplateText
    ADD CONSTRAINT XPKLtrTempText
PRIMARY KEY (letterTemplateId, sourceId, templateTextSeqNum);


ALTER TABLE LetterTemplateText
    ADD CONSTRAINT f1LtrTmp FOREIGN KEY (letterTemplateId, sourceId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId) ON UPDATE CASCADE
;

create column table LetterBatch
(
    letterBatchId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    letterTemplateId DECIMAL(18) NOT NULL ,
    letterTemplateSrcId DECIMAL(18) NOT NULL ,
    certificateStatusId DECIMAL(18) NULL ,
    daysToExpirationNum DECIMAL(18) NULL ,
    daysSinceExpirationNum DECIMAL(18) NULL ,
    expirationRangeStartDate DECIMAL(8) NULL ,
    expirationRangeEndDate DECIMAL(8) NULL ,
    noCertificateInd DECIMAL(1) NULL ,
    incompleteInd DECIMAL(1) NULL ,
    rejectedInd DECIMAL(1) NULL ,
    formReplacedInd DECIMAL(1) NULL ,
    emailSubject NVARCHAR(100) NULL ,
    letterBatchDesc NVARCHAR(100) NULL ,
    letterBatchDate DECIMAL(8) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE LetterBatch
    ADD CONSTRAINT XPKLtrBatch
PRIMARY KEY (letterBatchId, sourceId);


ALTER TABLE LetterBatch
    ADD CONSTRAINT f1LtrBtchCtStat FOREIGN KEY (certificateStatusId)
    REFERENCES CertificateStatus (certificateStatusId) ON UPDATE CASCADE
;

ALTER TABLE LetterBatch
    ADD CONSTRAINT f2LtrBtchTmp FOREIGN KEY (letterTemplateId, letterTemplateSrcId)
    REFERENCES LetterTemplate (letterTemplateId, sourceId) ON UPDATE CASCADE
;

create column table LetterBatchParty
(
    letterBatchId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL ,
    partyTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT XPKLtrBatchPty
PRIMARY KEY (letterBatchId, sourceId, partyId);


ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f1LtrBtchPty FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f2LtrBtchPty FOREIGN KEY (partyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE LetterBatchParty
    ADD CONSTRAINT f3LtrBtchPty FOREIGN KEY (partyTypeId)
    REFERENCES PartyType (partyTypeId) ON UPDATE CASCADE
;

create column table LetterBatchJurisdiction
(
    letterBatchId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL 
);

ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT XPKLtrBatchJur
PRIMARY KEY (letterBatchId, sourceId, jurisdictionId);


ALTER TABLE LetterBatchJurisdiction
    ADD CONSTRAINT f1LtrBtchJur FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId) ON UPDATE CASCADE
;

create column table FlexFieldDefVtxPrdTyp
(
    flexFieldDefId DECIMAL(18) NOT NULL ,
    flexFieldDefSrcId DECIMAL(18) NOT NULL ,
    vertexProductId DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT XPKFFDefVtxPrdTyp
PRIMARY KEY (flexFieldDefId, flexFieldDefSrcId, vertexProductId);


ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f1FFDrfVtxPrdTyp FOREIGN KEY (flexFieldDefId, flexFieldDefSrcId)
    REFERENCES FlexFieldDef (flexFieldDefId, flexFieldDefSrcId) ON UPDATE CASCADE
;

ALTER TABLE FlexFieldDefVtxPrdTyp
    ADD CONSTRAINT f2FFDrfVtxPrdTyp FOREIGN KEY (vertexProductId)
    REFERENCES VertexProductType (vertexProductId) ON UPDATE CASCADE
;

create column table Letter
(
    letterId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    certificateId DECIMAL(18) NULL ,
    overridePartyId DECIMAL(18) NULL ,
    letterBatchId DECIMAL(18) NOT NULL ,
    letterDeliveryMethodId DECIMAL(18) NOT NULL ,
    formId DECIMAL(18) NULL ,
    formSrcId DECIMAL(18) NULL ,
    letterSentDate DECIMAL(8) NOT NULL 
);

ALTER TABLE Letter
    ADD CONSTRAINT XPKLetter
PRIMARY KEY (letterId, sourceId);


ALTER TABLE Letter
    ADD CONSTRAINT f1LtrDlvryMethod FOREIGN KEY (letterDeliveryMethodId)
    REFERENCES LetterDeliveryMethod (letterDeliveryMethodId) ON UPDATE CASCADE
;

ALTER TABLE Letter
    ADD CONSTRAINT f2LtrLtrBatch FOREIGN KEY (letterBatchId, sourceId)
    REFERENCES LetterBatch (letterBatchId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE Letter
    ADD CONSTRAINT f3LtrCert FOREIGN KEY (certificateId, sourceId)
    REFERENCES Certificate (certificateId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE Letter
    ADD CONSTRAINT f4LtrForm FOREIGN KEY (formId, formSrcId)
    REFERENCES Form (formId, sourceId) ON UPDATE CASCADE
;

ALTER TABLE Letter
    ADD CONSTRAINT f5LtrParty FOREIGN KEY (overridePartyId, sourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

create column table GeographicRegionType
(
    geoRegionTypeId DECIMAL(18) NOT NULL ,
    geoRegionLicenseCode NVARCHAR(30) NOT NULL ,
    geoRegionDisplayName NVARCHAR(30) NOT NULL 
);

ALTER TABLE GeographicRegionType
    ADD CONSTRAINT XPKGeoRegionCode
PRIMARY KEY (geoRegionTypeId);


create column table JurGeographicRegType
(
    jurGeoRegTypeId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    geoRegionTypeId DECIMAL(18) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT XPKJurGeoRegType
PRIMARY KEY (jurGeoRegTypeId);


ALTER TABLE JurGeographicRegType
    ADD CONSTRAINT f1GeoRegionType FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId) ON UPDATE CASCADE
;

create column table RoleParty
(
    roleId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NOT NULL ,
    partyId DECIMAL(18) NOT NULL 
);

ALTER TABLE RoleParty
    ADD CONSTRAINT XPKRoleParty
PRIMARY KEY (roleId, sourceId, partyId);


create column table VATRegistrationIdFormat
(
    vatRegistrationIdFormatId DECIMAL(18) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    vatRegistrationIdMask NVARCHAR(40) NOT NULL ,
    vatRegistrationIdExample NVARCHAR(40) NULL ,
    vatRegistrationIdDescription NVARCHAR(255) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL 
);

ALTER TABLE VATRegistrationIdFormat
    ADD CONSTRAINT XPKVATRegIdFmt
PRIMARY KEY (vatRegistrationIdFormatId);


create column table CertWizardLocationUser
(
    certWizardUserId DECIMAL(18) NOT NULL ,
    name NVARCHAR(50) NOT NULL ,
    userId DECIMAL(18) NOT NULL ,
    sourceId DECIMAL(18) NULL ,
    partyId DECIMAL(18) NULL ,
    displayName NVARCHAR(100) NULL 
);

ALTER TABLE CertWizardLocationUser
    ADD CONSTRAINT CertWizardLocationUser_PK
PRIMARY KEY (certWizardUserId);


create column table users
(
    username NVARCHAR(64) NOT NULL ,
    password NVARCHAR(255) NOT NULL ,
    enabled DECIMAL(1) NULL ,
    sourceId DECIMAL(18) NULL 
);

ALTER TABLE users
    ADD CONSTRAINT users_PK
PRIMARY KEY (username);


create column table authorities
(
    username NVARCHAR(64) NOT NULL ,
    authority NVARCHAR(50) NOT NULL ,
    sourceId DECIMAL(18) NULL 
);

ALTER TABLE authorities
    ADD CONSTRAINT f1Auth FOREIGN KEY (username)
    REFERENCES users (username) ON UPDATE CASCADE
;

create column table CertWizardUser
(
    loginId DECIMAL(18) NOT NULL ,
    certWizardUserId DECIMAL(18) NOT NULL ,
    userName NVARCHAR(64) NULL ,
    companyEmail NVARCHAR(100) NOT NULL ,
    personalEmail NVARCHAR(100) NULL ,
    oseriesCustomerCode NVARCHAR(80) NULL ,
    firstName NVARCHAR(60) NOT NULL ,
    lastName NVARCHAR(60) NOT NULL ,
    countryJurId DECIMAL(18) NOT NULL ,
    mainDivisionJurId DECIMAL(18) NOT NULL ,
    cityName NVARCHAR(60) NULL ,
    street1 NVARCHAR(100) NULL ,
    street2 NVARCHAR(100) NULL ,
    postalCode NVARCHAR(20) NULL ,
    phoneNumber NVARCHAR(20) NULL ,
    securityQuestion1Id DECIMAL(18) NOT NULL ,
    securityQuestion2Id DECIMAL(18) NOT NULL ,
    securityQuestion3Id DECIMAL(18) NOT NULL ,
    securityAnswer1 NVARCHAR(100) NOT NULL ,
    securityAnswer2 NVARCHAR(100) NOT NULL ,
    securityAnswer3 NVARCHAR(100) NOT NULL ,
    creationDate DECIMAL(8) NOT NULL ,
    newCustomerInd DECIMAL(1) NULL 
);

ALTER TABLE CertWizardUser
    ADD CONSTRAINT CertWizardUser_PK
PRIMARY KEY (loginId);


ALTER TABLE CertWizardUser
    ADD CONSTRAINT f2Users FOREIGN KEY (userName)
    REFERENCES users (username) ON UPDATE CASCADE
;

create column table SecurityQuestions
(
    securityQuestionId DECIMAL(18) NOT NULL ,
    name NVARCHAR(255) NOT NULL ,
    question NVARCHAR(255) NOT NULL 
);

ALTER TABLE SecurityQuestions
    ADD CONSTRAINT SecurityQuestions_PK
PRIMARY KEY (securityQuestionId);


create column table DataUpdateImpactTaxRule
(
    dataUpdateNumber DECIMAL(8,1) NOT NULL ,
    taxRuleId DECIMAL(18) NOT NULL ,
    geoRegionTypeId DECIMAL(18) NOT NULL ,
    countryName NVARCHAR(60) NOT NULL ,
    mainDivisionName NVARCHAR(60) NULL ,
    jurName NVARCHAR(60) NOT NULL ,
    jurTypeName NVARCHAR(60) NOT NULL ,
    impositionName NVARCHAR(60) NOT NULL ,
    categoryName NVARCHAR(60) NOT NULL ,
    taxRuleType NVARCHAR(60) NOT NULL ,
    rate NVARCHAR(60) NULL ,
    salesTaxHolidayInd DECIMAL(1) DEFAULT 0 NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    changeType NVARCHAR(60) NOT NULL ,
    details NVARCHAR(1000) NULL ,
    newCategoryInd DECIMAL(1) DEFAULT 0 NOT NULL 
);

ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT XPKDataImpTaxRule
PRIMARY KEY (dataUpdateNumber, taxRuleId, geoRegionTypeId);


ALTER TABLE DataUpdateImpactTaxRule
    ADD CONSTRAINT f1GeoRegionType1 FOREIGN KEY (geoRegionTypeId)
    REFERENCES GeographicRegionType (geoRegionTypeId) ON UPDATE CASCADE
;

create column table DataUpdateImpactTaxArea
(
    dataUpdateNumber DECIMAL(8,1) NOT NULL ,
    taxAreaId DECIMAL(18) NOT NULL ,
    countryName NVARCHAR(60) NOT NULL ,
    mainDivisionName NVARCHAR(60) NULL ,
    subDivisionName NVARCHAR(60) NULL ,
    cityName NVARCHAR(60) NULL ,
    districtName1 NVARCHAR(60) NULL ,
    districtName2 NVARCHAR(60) NULL ,
    districtName3 NVARCHAR(60) NULL ,
    districtName4 NVARCHAR(60) NULL ,
    districtName5 NVARCHAR(60) NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    changeType NVARCHAR(60) NOT NULL ,
    affectedTaxAreaId DECIMAL(18) NOT NULL ,
    affectedCountryName NVARCHAR(60) NULL ,
    affectedMainDivisionName NVARCHAR(60) NULL ,
    affectedSubDivisionName NVARCHAR(60) NULL ,
    affectedCityName NVARCHAR(60) NULL ,
    affectedDistrictName1 NVARCHAR(60) NULL ,
    affectedDistrictName2 NVARCHAR(60) NULL ,
    affectedDistrictName3 NVARCHAR(60) NULL ,
    affectedDistrictName4 NVARCHAR(60) NULL ,
    affectedDistrictName5 NVARCHAR(60) NULL 
);

ALTER TABLE DataUpdateImpactTaxArea
    ADD CONSTRAINT XPKDataImpTaxArea
PRIMARY KEY (dataUpdateNumber, taxAreaId, affectedTaxAreaId);


create column table TransactionStatusType
(
    transStatusTypeId DECIMAL(18) NOT NULL ,
    transStatusTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TransactionStatusType
    ADD CONSTRAINT XPKTransStatusType
PRIMARY KEY (transStatusTypeId);


create column table FeatureResource
(
    featureResourceId DECIMAL(18) NOT NULL ,
    featureResourceName NVARCHAR(60) NOT NULL 
);

ALTER TABLE FeatureResource
    ADD CONSTRAINT XPKFeatureResource
PRIMARY KEY (featureResourceId);


create column table FeatureResourceImpositionType
(
    featureResourceId DECIMAL(18) NOT NULL ,
    impsnTypeId DECIMAL(18) NOT NULL 
);

ALTER TABLE FeatureResourceImpositionType
    ADD CONSTRAINT XPKFeatResImpType
PRIMARY KEY (featureResourceId, impsnTypeId);


create column table FeatureResourceCategory
(
    featureResourceId DECIMAL(18) NOT NULL ,
    txbltyCatId DECIMAL(18) NOT NULL 
);

ALTER TABLE FeatureResourceCategory
    ADD CONSTRAINT XPKFeatResCat
PRIMARY KEY (featureResourceId, txbltyCatId);


create column table TransactionEventType
(
    transEventTypeId DECIMAL(18) NOT NULL ,
    transEventTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TransactionEventType
    ADD CONSTRAINT XPKTransEventType
PRIMARY KEY (transEventTypeId);


create column table TpsDataReleaseEvent
(
    fullRlsId DECIMAL(18) NULL ,
    interimRlsId DECIMAL(18) NULL ,
    rlsTypeId DECIMAL(1) NULL ,
    appliedDate DECIMAL(8) NOT NULL ,
    rlsName NVARCHAR(255) NULL 
);

create column table TaxAssistPhaseType
(
    phaseId DECIMAL(18) NOT NULL ,
    phaseName NVARCHAR(60) NOT NULL 
);

ALTER TABLE TaxAssistPhaseType
    ADD CONSTRAINT XPKTaxAstPhaseType
PRIMARY KEY (phaseId);


create column table VATRegimeType
(
    vatRegimeTypeId DECIMAL(18) NOT NULL ,
    vatRegimeTypeName NVARCHAR(60) NOT NULL 
);

ALTER TABLE VATRegimeType
    ADD CONSTRAINT XPKVATRegimeType
PRIMARY KEY (vatRegimeTypeId);


create column table VATGroup
(
    vatGroupId DECIMAL(18) NOT NULL ,
    vatGroupSourceId DECIMAL(18) NOT NULL ,
    vatGrpCreationDate DECIMAL(8) NOT NULL 
);

ALTER TABLE VATGroup
    ADD CONSTRAINT XPKVATGroup
PRIMARY KEY (vatGroupId, vatGroupSourceId);


create column table VATGroupDetail
(
    vatGroupDtlId DECIMAL(18) NOT NULL ,
    vatGroupSourceId DECIMAL(18) NOT NULL ,
    vatGroupId DECIMAL(18) NOT NULL ,
    creationDate DECIMAL(8) NOT NULL ,
    effDate DECIMAL(8) NOT NULL ,
    endDate DECIMAL(8) NOT NULL ,
    repMemTxprId DECIMAL(18) NOT NULL ,
    vatGroupIdentifier NVARCHAR(15) NOT NULL ,
    vatGroupName NVARCHAR(30) NOT NULL ,
    jurisdictionId DECIMAL(18) NOT NULL ,
    vatRegimeTypeId DECIMAL(18) NOT NULL ,
    deletedInd DECIMAL(1) NOT NULL 
);

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT XPKVATGroupDtl
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId);


ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VatGroup FOREIGN KEY (vatGroupId, vatGroupSourceId)
    REFERENCES VATGroup (vatGroupId, vatGroupSourceId) ON UPDATE CASCADE
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f31Party FOREIGN KEY (repMemTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

ALTER TABLE VATGroupDetail
    ADD CONSTRAINT f1VATRegimeType FOREIGN KEY (vatRegimeTypeId)
    REFERENCES VATRegimeType (vatRegimeTypeId) ON UPDATE CASCADE
;

create column table VATGroupMembers
(
    vatGroupDtlId DECIMAL(18) NOT NULL ,
    vatGroupSourceId DECIMAL(18) NOT NULL ,
    memTxprId DECIMAL(18) NOT NULL 
);

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT XPKVATGroupMembers
PRIMARY KEY (vatGroupDtlId, vatGroupSourceId, memTxprId);


ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f1VATGroupDtl FOREIGN KEY (vatGroupDtlId, vatGroupSourceId)
    REFERENCES VATGroupDetail (vatGroupDtlId, vatGroupSourceId) ON UPDATE CASCADE
;

ALTER TABLE VATGroupMembers
    ADD CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)
    REFERENCES Party (partyId, partySourceId) ON UPDATE CASCADE
;

