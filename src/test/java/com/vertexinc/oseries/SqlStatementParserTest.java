package com.vertexinc.oseries;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class SqlStatementParserTest {
  
  @Test
  public void testParse() {

    String sql = String.join(System.lineSeparator(), new String[] {
      "drop table CertificateApprovalStatus;", 
      "", 
      "drop table CertificateStatus;", 
      "", 
      "drop table CoverageActionType;", 
      "", 
      "", 
    });
    
    SqlStatementParser iut = new SqlStatementParser();
    String[] statements = iut.parse(sql);
    
    String[] expected = new String[] {
      "drop table CertificateApprovalStatus", 
      "drop table CertificateStatus", 
      "drop table CoverageActionType", 
    };
    
    assertArrayEquals(expected, statements);
    
    
    
  }
  
  

}
