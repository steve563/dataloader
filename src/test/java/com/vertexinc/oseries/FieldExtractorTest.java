package com.vertexinc.oseries;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
import org.w3c.dom.Document;

public class FieldExtractorTest {

	@Test
	public void shouldGetThreeFieldsFromSchema() throws Exception {
    String xml = String.join(System.lineSeparator(), new String[] {
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
      "<DataSetSchema>",
      " <DbaseSchemaFormat/>",
      " <DelimitedSchemaFormat/>",
      " <FieldSchema name=\"foo\" type=\"LONG\" primaryKey=\"true\" size=\"18\" nullable=\"false\"/>",
      " <FieldSchema name=\"bar\" type=\"STRING\" primaryKey=\"false\" size=\"60\" nullable=\"false\"/>",
      " <FieldSchema name=\"baz\" type=\"STRING\" primaryKey=\"false\" size=\"60\" nullable=\"false\"/>",
      "</DataSetSchema>", });
	  
    Document document = SubjectAreaLoader.buildDocumentFromString(xml);
	  
    FieldSchemaExtractor iut = new FieldSchemaExtractor(new DomScanner<FieldSchema>(document));
    FieldSchema[] actual = iut.getFields();
    FieldSchema[] expected = new FieldSchema[] { new FieldSchema("foo", FieldSchemaType.LONG),
                          new FieldSchema("bar", FieldSchemaType.STRING),
                          new FieldSchema("baz", FieldSchemaType.STRING),
                        };
    assertArrayEquals(expected, actual);
	}
	
  @Test
	public void shouldGetTwoFieldsFromSchema() throws Exception {

		String xml = String.join(System.lineSeparator(), new String[] { "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
				"", "<!--", " ******************************************************",
				" * $Workfile: dmactivitytype.sch $ ",
				" * Copyright $Modtime: 11-22-04 1:42p $ Vertex Inc.  All rights reserved.",
				" ******************************************************", "-->", "", "",
				"<DataSetSchema xmlns=\"etl.vertexinc.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" releaseId=\"1\" name=\"dmactivitytype\">",
				"	<DbaseSchemaFormat table=\"DMActivityType\" logicalName=\"TPS_DB\"/>",
				"	<DelimitedSchemaFormat type=\"DELIMITED\" rowDelimiter=\"\\n\" fieldDelimiter=\"~\"/>",
				"	<FieldSchema name=\"activityTypeId\" type=\"LONG\" primaryKey=\"true\" size=\"18\" nullable=\"false\"/>",
				"	<FieldSchema name=\"activityTypeName\" type=\"STRING\" primaryKey=\"false\" size=\"60\" nullable=\"false\"/>",
				"</DataSetSchema>", });

		Document document = SubjectAreaLoader.buildDocumentFromString(xml);

		FieldSchemaExtractor iut = new FieldSchemaExtractor(new DomScanner<FieldSchema>(document));
		FieldSchema[] actual = iut.getFields();
		FieldSchema[] expected = new FieldSchema[] { new FieldSchema("activityTypeId", FieldSchemaType.LONG),
			                    new FieldSchema("activityTypeName", FieldSchemaType.STRING), };
		assertArrayEquals(expected, actual);
	}

}
