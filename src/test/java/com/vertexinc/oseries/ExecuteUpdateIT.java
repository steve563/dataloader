package com.vertexinc.oseries;

import org.junit.Test;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

public class ExecuteUpdateIT {


  @Test
  public void shouldBeAbleToCreateDatabase() throws Exception {
    
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url");
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");

    try (Connection conn = ConnProvider.getConnection(url, user, pw)) {
      try (Statement stmt = conn.createStatement()) {
        stmt.executeUpdate("DROP DATABASE IF EXISTS STUDENTS");
        stmt.executeUpdate("CREATE DATABASE STUDENTS");
      }
    }
  }
  
}
