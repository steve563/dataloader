package com.vertexinc.oseries;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of BindTarget that simply records 'bind' calls
 * so they can be examined for tests. 
 */
class RecordingBindTarget implements BindTarget {
  
  private final List<String> bindCalls = new ArrayList<>();
  
  public List<String> getBindCalls() {
    return bindCalls;
  }

  @Override
  public void bind(FieldSchemaType type, String stringValue, Long longValue,
    Double doubleValue) {
    bindCalls.add(
      String.format("%s %s %d %f", type.name(), stringValue, longValue, doubleValue));
  }

  @Override
  public void close() throws Exception {
    bindCalls.add("Closed");
  }

  @Override
  public void execute() {
    bindCalls.add("Execute");
  }
  
}