package com.vertexinc.oseries;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class DataRowParserTest {
  
  @Test
  public void shouldParseSingleStringField() {
    
    
    String line = "foo";
 
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("name", FieldSchemaType.STRING),
    };
    
    DataRowParser iut = new DataRowParser(fields);
    
    DataBucket[] dataBuckets = new DataBucket[fields.length];
    iut.fillDatabucketsWithRowData(line, dataBuckets);
    
    DataBucket[] expected = createFilledArray(dataBuckets.length);
    expected[0].stringVal = "foo";
    
    assertArrayEquals(expected, dataBuckets);
    
  }
  
  @Test
  public void shouldParseSingleLongField() {
    
    String line = "123";
 
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("id", FieldSchemaType.LONG),
    };
    
    DataRowParser iut = new DataRowParser(fields);
    
    DataBucket[] dataBuckets = new DataBucket[fields.length];
    iut.fillDatabucketsWithRowData(line, dataBuckets);

    DataBucket[] expected = createFilledArray(dataBuckets.length);
    expected[0].longVal = 123L;
    
    assertArrayEquals(expected, dataBuckets);
    
  }
  
  @Test
  public void shouldParseSingleDoubleField() {
    
    String line = ".01";
 
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("rate", FieldSchemaType.DOUBLE),
    };
    
    DataRowParser iut = new DataRowParser(fields);
    
    DataBucket[] dataBuckets = new DataBucket[fields.length];
    iut.fillDatabucketsWithRowData(line, dataBuckets);

    DataBucket[] expected = createFilledArray(dataBuckets.length);
    expected[0].doubleVal = 0.01;
    
    assertArrayEquals(expected, dataBuckets);
    
  }
  

  @Test
  public void shouldParseDataWithTrailingNullLong() {
    
    
    String line = "foo~";
 
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("name", FieldSchemaType.STRING),
      new FieldSchema("lastUpdateDate", FieldSchemaType.LONG),
    };
    
    DataRowParser iut = new DataRowParser(fields);
    
    DataBucket[] dataBuckets = new DataBucket[fields.length];
    iut.fillDatabucketsWithRowData(line, dataBuckets);
    
    DataBucket[] expected = createFilledArray(dataBuckets.length);
    expected[0].stringVal = "foo";
    expected[0].longVal = null;
    
    assertArrayEquals(expected, dataBuckets);
    
  }
  
  private DataBucket[] createFilledArray(int size) {
    DataBucket[] dataBuckets = new DataBucket[size];
    for (int i = 0; i < dataBuckets.length; i++) {
      dataBuckets[i] = new DataBucket();
    }
    return dataBuckets;
  }




}

