package com.vertexinc.oseries;

import java.util.ArrayList;
import java.util.List;

public class RecordingBindTargetSource implements BindTargetSource {
  
  List<RecordingBindTarget> bindTargets = new ArrayList<>();

  @Override
  public BindTarget getBindTarget(String tableName, String[] fieldNames) {
    RecordingBindTarget newbie = new RecordingBindTarget();
    bindTargets.add(newbie);
    return newbie;
  }

  public List<RecordingBindTarget> getBindTargets() {
    return bindTargets;
  }

}
