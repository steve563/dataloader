package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class PreparedStatementBindTargetTest {
  
  private static final String drop = "drop table if exists Widget";
  
  private static final String create = String.join(System.lineSeparator(), new String[] {
    "create table Widget", 
    "(", 
    "    name nvarchar(60) NOT NULL ,", 
    "    description nvarchar(80) NULL ,", 
    "    code numeric(18) NOT NULL ,", 
    "    altcode numeric(18) NULL ,", 
    "    rate numeric(12,8) NOT NULL, ", 
    "    altrate numeric(12,8) NULL ", 
    ");", 
  });
  
  
  @BeforeClass
  public static void setUpDatabase() throws Exception {
    
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()){
        stmt.executeUpdate(drop);
        stmt.executeUpdate(create);
      }
    }

  }

  @Before
  public void deleteRows() throws Exception {
    
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()){
        stmt.executeUpdate("delete from Widget");
      }
    }

  }

  @AfterClass
  public static void tearDownDatabase() throws Exception {
    
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()){
        stmt.executeUpdate(drop);
      }
    }

  }
  
  private static Connection getConnection() throws Exception {
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url") + "/dataloader";
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");
    return DriverManager.getConnection(url, user, pw);
  }
  
  @Test
  public void shouldBeFast() throws Exception {
    try (Connection conn = getConnection()) {
      String sql = "insert into Widget (name, description, code, altCode, rate, altrate) values (?,?,?,?,?,?) ";
      try  (PreparedStatement ps = conn.prepareStatement(sql)) {
        long start = System.currentTimeMillis();
        try (PreparedStatementBindTarget iut = new PreparedStatementBindTarget(conn, ps)){
          // int numWidgets = 1000001;  // Takes about 17 sec
          int numWidgets = 10001;  // Takes 445 ms
          for (long i = 0; i < numWidgets; i++) {
            // Make a random widget
            Widget randomWidget = new Widget("name", null, i, null, 0.5, null);
            bindWidget(iut, randomWidget);
            iut.execute();
          }
        }
        long finish = System.currentTimeMillis();
        System.out.printf("Load took %d ms.\n", finish-start);
      }
    }
  }
  
  @Test
  public void shouldBeAbleToSave() throws Exception {
    
    Widget w = new Widget("bobo", "a bobo", 123L, 234L, 0.10D, .50D);
    
    try (Connection conn = getConnection()) {
      String sql = "insert into Widget (name, description, code, altCode, rate, altrate) values (?,?,?,?,?,?) ";
      try  (PreparedStatement ps = conn.prepareStatement(sql)) {
        try (PreparedStatementBindTarget iut = new PreparedStatementBindTarget(conn, ps)){
          bindWidget(iut, w);
          iut.execute();
        }
      }
    }
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()) {
        ResultSet rs = stmt.executeQuery("select name, description, code, altCode, rate, altrate from Widget");
        while (rs.next()) {
          checkString(w.name, rs.getString(1), rs.wasNull());
          checkString(w.description, rs.getString(2), rs.wasNull());
          checkLong(w.code, rs.getLong(3), rs.wasNull());
          checkLong(w.altcode, rs.getLong(4), rs.wasNull());
          checkDouble(w.rate, rs.getDouble(5), rs.wasNull());
          checkDouble(w.altrate, rs.getDouble(6), rs.wasNull());
        }
      }
    }
  }
  
  @Test
  public void shouldBeAbleToSaveUsingTargetSource() throws Exception {
    
    Widget w = new Widget("bobo", "a bobo", 123L, 234L, 0.10D, .50D);
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url") + "/dataloader";
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");
    PreparedStatementGenerator psg = new PreparedStatementGenerator();
    
    PreparedStatementBindTargetSource bindTargetSource =  new PreparedStatementBindTargetSource(psg, url, user, pw);
    String[] fieldNames = new String[] {
      "name ", 
      "description", 
      "code", 
      "altcode", 
      "rate", 
      "altrate", 
    };
    try (BindTarget bindTarget = bindTargetSource.getBindTarget("Widget", fieldNames)){
      bindWidget(bindTarget, w);
      bindTarget.execute();
    }
    try (Connection conn = getConnection()) {
      try (Statement stmt = conn.createStatement()) {
        ResultSet rs = stmt.executeQuery("select name, description, code, altCode, rate, altrate from Widget");
        while (rs.next()) {
          checkString(w.name, rs.getString(1), rs.wasNull());
          checkString(w.description, rs.getString(2), rs.wasNull());
          checkLong(w.code, rs.getLong(3), rs.wasNull());
          checkLong(w.altcode, rs.getLong(4), rs.wasNull());
          checkDouble(w.rate, rs.getDouble(5), rs.wasNull());
          checkDouble(w.altrate, rs.getDouble(6), rs.wasNull());
        }
      }
    }
  }
  
  private void bindWidget(BindTarget bindTarget, Widget w) throws Exception {
    bindTarget.bind(FieldSchemaType.STRING, w.name, null, null);
    bindTarget.bind(FieldSchemaType.STRING, w.description, null, null);    
    bindTarget.bind(FieldSchemaType.LONG, null, w.code, null);    
    bindTarget.bind(FieldSchemaType.LONG, null, w.altcode, null);    
    bindTarget.bind(FieldSchemaType.DOUBLE, null, null, w.rate);    
    bindTarget.bind(FieldSchemaType.DOUBLE, null, null, w.altrate);    
  }
  
  private void checkString(String expected, String actual, boolean wasNull) {
    if (wasNull) {
      assertEquals(expected, null);
    }
    else {
      assertEquals(expected, actual);
    }
  }
  
  private void checkLong(Long expected, long actual, boolean wasNull) {
    if (wasNull) {
      assertEquals(expected, null);
    }
    else {
      assertEquals((long)expected, actual);
    }
  }
  
  private void checkDouble(Double expected, double actual, boolean wasNull) {
    if (wasNull) {
      assertEquals(expected, null);
    }
    else {
      assertEquals(expected, actual, 0.001);
    }
  }
  
  private static class Widget {
    final String name; 
    final String description;
    final Long code;
    final Long altcode;
    final Double rate;
    final Double altrate;
    public Widget(String name, String description, Long code, Long altcode,
      Double rate, Double altrate) {
      super();
      this.name = name;
      this.description = description;
      this.code = code;
      this.altcode = altcode;
      this.rate = rate;
      this.altrate = altrate;
    }
  }

  

}

