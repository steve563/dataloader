package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ZipSqlParserTest {
  
  @Test
  public void readTpsCreate() throws Exception {
    // This is a crapy test that just makes sure that it can read
    // the whole zip file and separate into the correct number of sql statements. 
    // My verification is to check the first and last statements. The expectededs
    // are just from looking at the sql file.
    // Not robust
    String zipFilePath = "target/test-classes/tps_create.zip";
    String filePath = "/sql/microsoft sql server/create.sql";

    ZipSqlParser iut = new ZipSqlParser();
    String[] parts = iut.readFile(zipFilePath, filePath);

    {
      String expected = String.join(System.lineSeparator(), new String[] {
        "create table CoverageActionType",
        "(",
        "    coverageActionTypeId numeric(18) NOT NULL ,",
        "    coverageActionTypeName nvarchar(60) NOT NULL ",
        ")",
      });
      assertEquals(parts[0], expected);
    }
    
    {
      String expected = String.join(System.lineSeparator(), new String[] {
        "ALTER TABLE VATGroupMembers", 
        "    ADD CONSTRAINT f32Party FOREIGN KEY (memTxprId, vatGroupSourceId)",
        "    REFERENCES Party (partyId, partySourceId)",
      });
      assertEquals(parts[parts.length-1], expected);
    }
    
  }

}
