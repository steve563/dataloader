package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class DatFileLoaderTest {
  
  @Test
  public void testSomething() throws Exception {
    
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("ruleId", FieldSchemaType.LONG),
      new FieldSchema("ruleName", FieldSchemaType.STRING),
      new FieldSchema("rate", FieldSchemaType.DOUBLE),
    };
    
    String[] arr = { 
      "123~Clothing~.10",
      "234~Bread~.12",
      "345~Beer~.15",
    }; 

    Stream<String> stream = Arrays.stream(arr);
    RecordingBindTarget bindTarget = new RecordingBindTarget();
    DatFileLoader iut = new DatFileLoader(stream, fields, bindTarget);
    iut.loadDatFile();
    
    List<String> expected = Arrays.asList(new String[] {
      "LONG null 123 null", 
      "STRING Clothing null null", 
      "DOUBLE null null 0.100000", 
      "Execute", 

      "LONG null 234 null", 
      "STRING Bread null null",
      "DOUBLE null null 0.120000",
      "Execute", 

      "LONG null 345 null", 
      "STRING Beer null null", 
      "DOUBLE null null 0.150000",
      "Execute", 
    });
    
    assertEquals(expected, bindTarget.getBindCalls());

  }

  @Test
  public void emptyLinesShouldBeSkipped() throws Exception {
    
    FieldSchema[] fields = new FieldSchema[] {
      new FieldSchema("ruleId", FieldSchemaType.LONG),
      new FieldSchema("ruleName", FieldSchemaType.STRING),
      new FieldSchema("rate", FieldSchemaType.DOUBLE),
    };
    
    String[] arr = { 
      "123~Clothing~.10",
      "",
      "234~Bread~.12",
      null,
      "345~Beer~.15",
      "",
    }; 

    Stream<String> stream = Arrays.stream(arr);
    RecordingBindTarget bindTarget = new RecordingBindTarget();
    DatFileLoader iut = new DatFileLoader(stream, fields, bindTarget);
    iut.loadDatFile();
    
    List<String> expected = Arrays.asList(new String[] {
      "LONG null 123 null", 
      "STRING Clothing null null", 
      "DOUBLE null null 0.100000", 
      "Execute", 

      "LONG null 234 null", 
      "STRING Bread null null",
      "DOUBLE null null 0.120000",
      "Execute", 

      "LONG null 345 null", 
      "STRING Beer null null", 
      "DOUBLE null null 0.150000",
      "Execute", 
    });
    
    assertEquals(expected, bindTarget.getBindCalls());

  }

}
