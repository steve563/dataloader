package com.vertexinc.oseries;

import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

public class ZipSqlRunnerTest {

  @BeforeClass
  public static void setUpDatabase() throws Exception {
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url");
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");
    Connection c = DriverManager.getConnection(url, user, pw);
    Statement s = c.createStatement();
    s.executeUpdate("drop database if exists zipsqlrunnertest");
    s.executeUpdate("create database zipsqlrunnertest");
  }
  
  //@AfterClass
  //@Ignore
  public static void tearDownDatabase() throws Exception {
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url");
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");
    Connection c = DriverManager.getConnection(url, user, pw);
    Statement s = c.createStatement();
    s.executeUpdate("drop database if exists zipsqlrunnertest");
  }
  
  
  @Test
  public void testSomething() throws Exception {
    Properties prop = Props.getProperties();
    String url = prop.getProperty("db.url") + "/zipsqlrunnertest";
    String user = prop.getProperty("db.user");
    String pw = prop.getProperty("db.password");
    ZipSqlParser parser = new ZipSqlParser();
    
    
    String zipFileName = "target/test-classes/tps_create.zip";
    String sqlName = "/sql/microsoft sql server/create.sql";
    
    ZipSqlRunner iut = new ZipSqlRunner(parser, url, user, pw);
    iut.readAndRunSql(zipFileName, sqlName);
    
    // TODO - Not quite sure how to veriy. For now, verifying manually
    // by looking in the database. 
  }

}
