package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PreparedStatementGeneratorTest {
  
  @Test
  public void testSomething() {
    
    PreparedStatementGenerator iut = new PreparedStatementGenerator();
    String[] fields = new String[] {
      "name",
      "description",
      "code",
      "altCode",
      "rate",
      "altrate",
    };
    
    String tableName = "Widget";
    
    String actual = iut.generate(tableName, fields);
    String expected = "insert into Widget (name, description, code, altCode, rate, altrate) values (?,?,?,?,?,?)";
    assertEquals(expected, actual);
    
  }

}
