package com.vertexinc.oseries;

/**
 * An implementation of BindTarget that does nothing in response to bind calls. 
 */
class NullBindTarget implements BindTarget {
  
  @Override
  public void bind(FieldSchemaType type, String stringValue, Long longValue,
    Double doubleValue) {
    
  }

  @Override
  public void close() throws Exception {
  }

  @Override
  public void execute() {
  }
  
}