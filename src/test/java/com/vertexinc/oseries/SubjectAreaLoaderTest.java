package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class SubjectAreaLoaderTest {
  
  @Test
  public void testJustReadingTheZipFile() throws Exception {
    BindTargetSource bindTargetSource = new BindTargetSource() {
      
      @Override
      public BindTarget getBindTarget(String tableName, String[] fieldNames) {
        return new NullBindTarget();
      }
      
    };
    String filePath = System.getenv(Constants.RUN_DDL_LOCATION) + "/" + Constants.TAXGIS_UPDATE_DATA;
    Path zipFile = Paths.get(filePath);
    ZipFileReader zipFileReader = new ZipFileReader(zipFile);
    String subjectArea = "taxgis";
    SubjectAreaLoader iut = new SubjectAreaLoader(subjectArea, zipFileReader, bindTargetSource);
    long before = System.currentTimeMillis();
    iut.loadSubjectArea();
    long after = System.currentTimeMillis();
    System.out.printf("Took %d ms to read thru taxgis zip file\n", after - before);
  }
  
  @Test
  public void testBasic() throws Exception {
    
    RecordingBindTargetSource bindTargetSource = new RecordingBindTargetSource();
    Path zipFile = Paths.get("target/test-classes/tiny_taxgis_update_data.zip");
    ZipFileReader zipFileReader = new ZipFileReader(zipFile);
    String subjectArea = "taxgis";
    SubjectAreaLoader iut = new SubjectAreaLoader(subjectArea, zipFileReader, bindTargetSource);
    iut.loadSubjectArea();
    List<RecordingBindTarget> targets = bindTargetSource.getBindTargets();
    assertEquals(3, targets.size());

    {
      List<String> expected = Arrays.asList(new String[] {
        "LONG null 79 null", 
        "STRING VERTEX null null", 
        "STRING DATA MANAGER null null", 
        "STRING DMCDIPROD null null", 
        "STRING ETL null null", 
        "STRING DATA MANAGER null null", 
        "LONG null 20100426 null", 
        "LONG null 20100426 null", 
        "STRING EXPORT 79 null null", 
        "Execute", 
        
        "LONG null 99 null", 
        "STRING VERTEX null null", 
        "STRING DATA MANAGER null null",
        "STRING DMCDIPROD null null", 
        "STRING ETL null null", 
        "STRING DATA MANAGER null null", 
        "LONG null 20111221 null", 
        "LONG null 20111221 null", 
        "STRING EXPORT 99 null null", 
        "Execute", 
        
        "LONG null 121 null", 
        "STRING VERTEX null null", 
        "STRING DATA MANAGER null null", 
        "STRING DMCDIPROD null null", 
        "STRING ETL null null", 
        "STRING DATA MANAGER null null", 
        "LONG null 20131031 null", 
        "LONG null 20131031 null", 
        "STRING EXPORT 121 null null", 
        "Execute", 
        
        "Closed",     
      });
      assertEquals(expected, targets.get(0).getBindCalls());
    }
    {
      List<String> expected = Arrays.asList(new String[] {
        "LONG null 0 null",
        "STRING NONE null null",
        "STRING NO FILTER TYPE null null",
        "STRING NONE null null",
        "LONG null 3 null",
        "Execute", 
        
        "LONG null 3 null",
        "STRING FILTER MULTI STATE ZIP CODES null null",
        "STRING THE TAX AREA SELECTED OVERLAPS A ZIP CODE THAT SPANS INTO A NEIGHBORING STATE null null",
        "STRING MULTI_STATE_ZIP_CODES null null",
        "LONG null 40 null",
        "Execute", 
        
        "LONG null 1 null",
        "STRING POSTAL AREA null null",
        "STRING REPRESENTS AN AREA CREATED TO SUPPORT AN OVERLAPPING POSTAL AREA null null",
        "STRING POSTAL_AREA null null",
        "LONG null 3 null",
        "Execute", 
        
        "Closed",
      });
      assertEquals(expected, targets.get(1).getBindCalls());
    }
    {
      System.out.println(targets.get(2).getBindCalls());
      List<String> expected = Arrays.asList(new String[] {
        "LONG null 1 null", 
        "STRING COUNTRY null null", 
        "STRING COUNTRY null null", 
        "LONG null 1 null", 
        "Execute", 
        
        "LONG null 12 null", 
        "STRING BOROUGH null null", 
        "STRING BOROUGH null null", 
        "LONG null 1 null", 
        "Execute", 
        
        "LONG null 3 null", 
        "STRING PROVINCE null null", 
        "STRING PROVINCE null null", 
        "LONG null 1 null", 
        "Execute", 
        
        "LONG null 11 null", 
        "STRING PARISH null null", 
        "STRING PARISH null null", 
        "LONG null 1 null", 
        "Execute", 
        
        "Closed",
      });
      assertEquals(expected, targets.get(2).getBindCalls());
    }
  }
}
