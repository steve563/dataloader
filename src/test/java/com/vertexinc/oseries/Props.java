package com.vertexinc.oseries;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Props {

  public static Properties getProperties() throws Exception {
    try (InputStream input = new FileInputStream("target/test-classes/dataloader.properties")) {
      Properties prop = new Properties();
      prop.load(input);
      return prop;
    }
  }
  
}
