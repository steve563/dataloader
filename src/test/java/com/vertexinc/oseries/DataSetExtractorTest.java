package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.w3c.dom.Document;

import java.util.Arrays;
import java.util.List;

public class DataSetExtractorTest {
  
  @Test
  public void testSomething() throws Exception {
    
    String[] lines = new String[] {
      "<?xml version=\"1.0\" encoding=\"utf-8\"?>",
      "<!-- edited with XML Spy v3.5 NT (http://www.xmlspy.com)-->",
      "<SubjectArea xmlns=\"etl.vertexinc.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" name=\"taxgis\">",
      "<Description>Subject Area Definition for TaxGIS</Description>",
      "<DataRelease fullReleaseNumber=\"181\" interimReleaseNumber=\"0\" name=\"Global GIS\" type=\"FULL\" date=\"20181231000000\">",
      "<Description>Full Release</Description>",
      "<DataSet name=\"taxgisupdate\" rows=\"181\" />",
      "<DataSet name=\"filtertype\" rows=\"5\" />",
      "<DataSet name=\"jurisdictiontype\" rows=\"16\" />",
      "</DataRelease>",
      "<SchemaRelease id=\"309000001\" name=\"CDI R309000001\" date=\"20150801420000\" logicalName=\"TAXGIS_DB\">",
      "<Description>Release 309000001</Description>",
      "</SchemaRelease>",
      "<PostProcessing>",
      "<SqlProcess logicalName=\"TAXGIS_DB\" schemaReleaseId=\"309000001\" file=\"dropstatistics60.sql\" />",
      "</PostProcessing>",
      "</SubjectArea>",
    };
    String xml = String.join(System.lineSeparator(), lines);
    Document document = SubjectAreaLoader.buildDocumentFromString(xml);

    DataSetExtractor iut = new DataSetExtractor(new DomScanner<DataSet>(document));
    List<DataSet> actual = iut.getDataSets();
    
    List<DataSet> expected = Arrays.asList(
      new DataSet[] { new DataSet("taxgisupdate"),
                          new DataSet("filtertype"),
                          new DataSet("jurisdictiontype"),
                          }
    );
    
    assertEquals(expected, actual);
    
    
  }

}
