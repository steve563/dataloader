package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ZipFileReaderTest {

  @Test
  public void shouldBeAbleToReadAWholeFileIntoAString() throws Exception {
    Path zipFile = Paths.get("target/test-classes/small_dir.zip");
    ZipFileReader iut = new ZipFileReader(zipFile);
    String actual = iut.readAllLinesIntoAString("/subdir1/file1.txt");
    assertEquals("Hello I am a file in subdir1."+System.lineSeparator(), actual);
  }

  @Test
  public void shouldBeAbleToGetAStreamOfLines() throws Exception {
    Path zipFile = Paths.get("target/test-classes/small_dir.zip");
    ZipFileReader iut = new ZipFileReader(zipFile);
    List<String> actual = new ArrayList<>();
    try (Stream<String> stream = iut.getStream("/subdir1/file1.txt")) {
      Iterator<String> iter = stream.iterator();
      while (iter.hasNext()) {
        String s = iter.next();
        actual.add(s);
      }
    }
    List<String> expected = Arrays.asList(new String[] {
      "Hello I am a file in subdir1.",      
    });
    assertEquals(expected, actual);
  }

}
