package com.vertexinc.oseries;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FieldSchemaTypeTest {
  
  @Test
  public void testParse() {
    assertEquals(FieldSchemaType.STRING, FieldSchemaType.parseFieldSchemaType("STRING"));
    assertEquals(FieldSchemaType.DOUBLE, FieldSchemaType.parseFieldSchemaType("DOUBLE"));
    assertEquals(FieldSchemaType.LONG, FieldSchemaType.parseFieldSchemaType("LONG"));
    
  }

}
